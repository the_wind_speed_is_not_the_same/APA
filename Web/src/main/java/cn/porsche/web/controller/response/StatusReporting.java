package cn.porsche.web.controller.response;

import cn.porsche.web.controller.response.reporting.DepartmentStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatusReporting {
    @ApiModelProperty(notes = "所有部门统计数据")
    @JsonProperty("department")
    DepartmentStatus department;

    @ApiModelProperty(notes = "子部门统计数据")
    @JsonProperty("sub_department")
    List<DepartmentStatus> subDepartment;
}
