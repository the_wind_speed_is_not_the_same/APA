package cn.porsche.web.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.common.util.JsonUtils;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.domain.Comment;
import cn.porsche.web.service.LoginService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class CommentControllerTest extends ApplicationTest {
    private static final String API = "/comment";

    private String token;

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void addOAComment() {
        final String api = API + "/add_oa_comment";

        Comment comment;
        comment = Comment.builder()
                .functionId(getFunctionId())
                .message("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.")
                .build();

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(JsonUtils.toString(comment, true))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }

        comment = Comment.builder()
                .functionId(getFunctionId())
                .message("Rather than depending on management to set direction, in Teal organizations, teams and individuals are intrinsically driven to fulfill the purpose of the organization. They do not need extrinsic motivators in the form of objectives and budgets to get them to do their work. They set their own objectives and targets, when they feel it is necessary")
                .build();

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(JsonUtils.toString(comment))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void getOAComments() {
        final String api = API + "/get_oa_comments";
        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void addTSComment() {
        final String api = API + "/add_ts_comment";

        Comment comment;
        comment = Comment.builder()
                .functionId(getFunctionId())
                .topicId(getWhatId())
                .message("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.Knowing what you want is half the battle. Most people go through their whole lives not knowing what they want.")
                .build();

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(JsonUtils.toString(comment, true))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }

//        comment = Comment.builder()
//                .functionId(getFunctionId())
//                .topicId(getTargetId())
//                .message("Rather than depending on management to set direction, in Teal organizations, teams and individuals are intrinsically driven to fulfill the purpose of the organization. They do not need extrinsic motivators in the form of objectives and budgets to get them to do their work. They set their own objectives and targets, when they feel it is necessary")
//                .build();
//
//        try {
//            mvcResult = mvc.perform(
//                    post(api)
//                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
//                            .contentType(MediaType.APPLICATION_JSON_VALUE)
//                            .content(JsonUtils.toString(comment))
//                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
//            ).andExpect(
//                    status().is2xxSuccessful()
//            ).andExpect(
//                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
//            ).andDo(
//                    print()
//            ).andReturn();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Test
    void getTSComments() {
        final String api = API + "/get_ts_comments";
        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("what_id", "1")
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void allTSComments() {
        final String api = API + "/all_ts_comments";
        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}