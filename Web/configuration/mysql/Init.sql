CREATE DATABASE  IF NOT EXISTS `APA` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `APA`;
-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: 54.223.133.122    Database: APA
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `function_id` int(10) unsigned NOT NULL COMMENT 'KPI主键',
  `owner_id` int(10) unsigned NOT NULL COMMENT '消息拥有者主键',
  `type` int(10) unsigned NOT NULL COMMENT '消息类型',
  `topic_id` int(10) unsigned NOT NULL COMMENT '对应的主键',
  `employee_id` int(10) unsigned NOT NULL COMMENT '员工主键',
  `employee_name` varchar(255) NOT NULL COMMENT '员工名称',
  `delegator_id` int(10) unsigned NOT NULL COMMENT '委托人主键',
  `delegator_name` varchar(255) NOT NULL COMMENT '委托人名称',
  `message` text COMMENT '消息提示',
  `is_read` tinyint(1) DEFAULT NULL COMMENT '是否已读',
  `send_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `department_employees`
--

DROP TABLE IF EXISTS `department_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `department_employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `employee_id` int(10) unsigned NOT NULL COMMENT '员工主键',
  `department_1` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  `department_2` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  `department_3` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  `department_4` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  `department_5` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=536 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `department_reporting`
--

DROP TABLE IF EXISTS `department_reporting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `department_reporting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `function_id` int(10) unsigned NOT NULL COMMENT 'KPI主键',
  `department_id` int(10) unsigned NOT NULL COMMENT '部门主键',
  `department_name` varchar(255) DEFAULT NULL COMMENT '部门名称',
  `department_manager` varchar(255) DEFAULT NULL COMMENT '部门主管',
  `pre_top_total` smallint(5) unsigned NOT NULL COMMENT '消息拥有者主键',
  `pre_top_rating` float(4,2) unsigned NOT NULL COMMENT '消息拥有者主键',
  `pre_above_total` smallint(5) unsigned NOT NULL COMMENT '消息拥有者主键',
  `pre_above_rating` float(4,2) unsigned NOT NULL COMMENT '消息拥有者主键',
  `pre_average_total` smallint(5) unsigned NOT NULL COMMENT '消息拥有者主键',
  `pre_average_rating` float(4,2) unsigned NOT NULL COMMENT '消息拥有者主键',
  `pre_below_total` smallint(5) unsigned NOT NULL COMMENT '消息拥有者主键',
  `pre_below_rating` float(4,2) unsigned NOT NULL COMMENT '消息拥有者主键',
  `final_top_total` smallint(5) unsigned NOT NULL COMMENT '消息拥有者主键',
  `final_top_rating` float(4,2) unsigned NOT NULL COMMENT '消息拥有者主键',
  `final_above_total` smallint(5) unsigned NOT NULL COMMENT '消息拥有者主键',
  `final_above_rating` float(4,2) unsigned NOT NULL COMMENT '消息拥有者主键',
  `final_average_total` smallint(5) unsigned NOT NULL COMMENT '消息拥有者主键',
  `final_average_rating` float(4,2) unsigned NOT NULL COMMENT '消息拥有者主键',
  `final_below_total` smallint(5) unsigned NOT NULL COMMENT '消息拥有者主键',
  `final_below_rating` float(4,2) unsigned NOT NULL COMMENT '消息拥有者主键',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级部门主键',
  `manager_id` int(10) unsigned DEFAULT NULL COMMENT '管理员主键',
  `company_code` varchar(63) NOT NULL COMMENT '公司代码',
  `parent_code` varchar(63) DEFAULT NULL COMMENT '上级部门代码',
  `code` varchar(63) NOT NULL COMMENT '部门代码',
  `name` varchar(255) NOT NULL COMMENT '部门名称',
  `count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '员工总数',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `function_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阶段主键',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发送者主键',
  `from` text COMMENT '发送者邮箱地址',
  `to` text COMMENT '发送者邮箱地址',
  `subject` text COMMENT '邮件主题',
  `content` text COMMENT '邮件内容',
  `attachment` text COMMENT '邮件附件',
  `response` text COMMENT '邮件反馈',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `functions`
--

DROP TABLE IF EXISTS `functions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `functions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `year` smallint(4) NOT NULL COMMENT '开始年份',
  `is_ts` tinyint(1) unsigned DEFAULT NULL COMMENT 'TS的状态',
  `ts_time` datetime DEFAULT NULL COMMENT 'TS的开始时间',
  `ts_user_id` int(10) unsigned NOT NULL COMMENT '开启TS的用户',
  `last_ts_email_id` int(10) unsigned DEFAULT NULL COMMENT '最近的一封邮件主键',
  `ts_mail_success` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'TS的邮件是否发送成功',
  `is_apa` tinyint(1) unsigned DEFAULT NULL COMMENT 'APA是否开始状态',
  `apa_user_id` int(10) unsigned DEFAULT NULL COMMENT 'APA的开始的用户',
  `last_apa_email_id` int(10) unsigned DEFAULT NULL COMMENT '最近的一封邮件主键',
  `apa_mail_success` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'APA的邮件是否发送成功',
  `is_complete` tinyint(1) unsigned DEFAULT NULL COMMENT '整个阶段是否完成',
  `complete_time` datetime DEFAULT NULL COMMENT '整个阶段完成时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_roles`
--

DROP TABLE IF EXISTS `group_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组名主键',
  `group_idx` tinyint(3) unsigned DEFAULT NULL COMMENT '组名主键',
  `module_idx` tinyint(3) unsigned DEFAULT NULL COMMENT '模块主键',
  `name` varchar(255) NOT NULL COMMENT '权限名称',
  `module` varchar(255) NOT NULL COMMENT '模块名称',
  `anonymous` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否匿名查看，不需要账号',
  `viewer` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '不检查权限，但一定要有账号',
  `add` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否可添加',
  `check` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否可查看',
  `edit` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否可编辑',
  `download` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否可下载',
  `delegation` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否可授权',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_users`
--

DROP TABLE IF EXISTS `group_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组名主键',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '成员主键',
  `by_hands_user_id` int(10) unsigned DEFAULT NULL COMMENT '手动导入的用户主键',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=676 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `manager_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员主键',
  `width` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组权重值。组越重要，权值越高',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `keys` varchar(255) DEFAULT NULL COMMENT '关键字，关键字之间用半角逗号分隔',
  `is_default` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否为默认的值，未分级的用户全归到此组',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kpi_users`
--

DROP TABLE IF EXISTS `kpi_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kpi_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `employee_id` int(10) unsigned NOT NULL COMMENT '员工主键',
  `function_id` int(10) unsigned NOT NULL COMMENT '阶段主键',
  `target_id` int(10) unsigned NOT NULL COMMENT '当前的KPI类主键',
  `manager_id` int(10) unsigned DEFAULT NULL COMMENT '直线经理主键',
  `delegator_id` int(10) unsigned DEFAULT NULL COMMENT '委托人主键',
  `department_id` int(10) unsigned DEFAULT NULL COMMENT '部门主键',
  `is_delegator` tinyint(1) unsigned DEFAULT NULL COMMENT '是否为委托人',
  `en_name` varchar(1024) DEFAULT NULL COMMENT '英文名称',
  `cn_name` varchar(1024) DEFAULT NULL COMMENT '中文名称',
  `department` varchar(1024) DEFAULT NULL COMMENT '部门名称',
  `delegator` varchar(1024) DEFAULT NULL COMMENT '委托人',
  `manager` varchar(1024) DEFAULT NULL COMMENT '委托人',
  `position` varchar(1024) DEFAULT NULL COMMENT '职位信息',
  `hire_date` date DEFAULT NULL COMMENT '用户 APA 结束时间',
  `email` varchar(254) DEFAULT NULL COMMENT '邮件地址',
  `telephone` varchar(254) DEFAULT NULL COMMENT '电话号码',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10267 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `login` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户登录的主键',
  `by` varchar(256) NOT NULL COMMENT '用户登录的方式',
  `access_token` varchar(1024) NOT NULL COMMENT '用户登录的密钥',
  `access_token_expire_time` datetime DEFAULT NULL COMMENT '登陆的过期时间',
  `refresh_token` varchar(1024) NOT NULL COMMENT '用户登录的密钥',
  `refresh_token_expire_time` datetime DEFAULT NULL COMMENT '登陆的过期时间',
  `ticket` text COMMENT '第三方的数据凭证',
  `last_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `last_ua` text COMMENT '最后登录的流程器信息',
  `last_dev` text COMMENT '最后登录的UA',
  `auth_id` varchar(256) DEFAULT NULL COMMENT '登录之后的Cookie标识',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `is_logout` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已登出',
  `logout_time` datetime DEFAULT NULL COMMENT '登出时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=411 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `function_id` int(10) unsigned NOT NULL COMMENT 'KPI主键',
  `owner_id` int(10) unsigned NOT NULL COMMENT '消息拥有者主键',
  `target_id` int(10) unsigned DEFAULT NULL COMMENT '绩效主键',
  `what_id` int(10) unsigned DEFAULT NULL COMMENT '绩效主键',
  `sender_id` int(10) unsigned NOT NULL COMMENT '员工主键',
  `sender` varchar(255) NOT NULL COMMENT '发送者',
  `receiver_id` int(10) unsigned NOT NULL COMMENT '委托人主键',
  `receiver` varchar(255) NOT NULL COMMENT '接受者',
  `message` text COMMENT '消息提示',
  `uri` varchar(63) DEFAULT NULL COMMENT '消息对应的页面地址',
  `is_read` tinyint(1) DEFAULT NULL COMMENT '是否已读',
  `send_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `sender_mail` varchar(255) NOT NULL COMMENT '发送者邮箱',
  `receiver_email` varchar(255) NOT NULL COMMENT '接受者邮箱',
  `subject` text COMMENT '提示标题',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `positions` (
  `id` int(10) unsigned NOT NULL COMMENT '主键',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'HRIS系统中上级主键',
  `company_code` varchar(63) DEFAULT NULL COMMENT 'HRIS Company Code',
  `hris_code` varchar(63) DEFAULT NULL COMMENT 'HRIS系统代码',
  `function` varchar(511) DEFAULT NULL COMMENT '显示抬头',
  `occupied` tinyint(1) unsigned DEFAULT NULL COMMENT '是否已占用完毕',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `target_how`
--

DROP TABLE IF EXISTS `target_how`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `target_how` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `function_id` int(10) unsigned NOT NULL COMMENT 'KPI主键',
  `target_id` int(10) unsigned NOT NULL COMMENT 'TS主键',
  `employee_id` int(10) unsigned NOT NULL COMMENT '用户主键',
  `how1_weight` tinyint(3) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how1` text COMMENT '如何做',
  `how1_e_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how1_do_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how1_dt_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how2_weight` tinyint(3) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how2` text COMMENT '如何做',
  `how2_e_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how2_do_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how2_dt_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how3_weight` tinyint(3) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how3` text COMMENT '如何做',
  `how3_e_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how3_do_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how3_dt_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how4_weight` tinyint(3) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how4` text COMMENT '如何做',
  `how4_e_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how4_do_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `how4_dt_score` float(2,1) unsigned DEFAULT NULL COMMENT '如何做权重',
  `is_employee_confirm` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '员工是否确认',
  `is_delegator_confirm` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '代理人是否确认',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `delegator_tip` varchar(32) DEFAULT NULL COMMENT '给经理使用的红点提示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `target_kpis`
--

DROP TABLE IF EXISTS `target_kpis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `target_kpis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `target_id` int(10) unsigned NOT NULL COMMENT 'TS主键',
  `what_id` int(10) unsigned NOT NULL COMMENT 'Waht主键',
  `index` tinyint(1) unsigned NOT NULL COMMENT '下标【3-5】必填',
  `desc` varchar(1024) DEFAULT NULL COMMENT '完成此目标的描述',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `target_what`
--

DROP TABLE IF EXISTS `target_what`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `target_what` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `function_id` int(10) unsigned NOT NULL COMMENT 'KPI主键',
  `target_id` int(10) unsigned NOT NULL COMMENT 'TS主键',
  `employee_id` int(10) unsigned NOT NULL COMMENT '用户主键',
  `name` varchar(1024) DEFAULT NULL COMMENT '名称',
  `desc` varchar(1024) DEFAULT NULL COMMENT '描述',
  `weight` tinyint(3) unsigned DEFAULT NULL COMMENT '权重',
  `finish_date` date DEFAULT NULL COMMENT '权重',
  `is_ts_employee_confirm` tinyint(1) DEFAULT '0' COMMENT '员工是否已Confirm',
  `is_ts_delegator_confirm` tinyint(1) DEFAULT '0' COMMENT '小组长是否Confirm',
  `employee_score` float(2,1) DEFAULT NULL COMMENT '员工自评',
  `employee_comment` varchar(1024) DEFAULT NULL COMMENT '员工自评说明',
  `first_score` float(2,1) DEFAULT NULL COMMENT '委托人第一次评分',
  `second_score` float(2,1) DEFAULT NULL COMMENT '委托人第一次评分',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `reject_comment_id` int(10) unsigned DEFAULT NULL COMMENT '拒绝时的留言主键',
  `delegator_tip` varchar(32) DEFAULT NULL COMMENT '给经理使用的红点提示',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `targets`
--

DROP TABLE IF EXISTS `targets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `targets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `function_id` int(10) unsigned NOT NULL COMMENT 'KPI主键',
  `is_ts_employee_submit` tinyint(1) DEFAULT '0' COMMENT '员工是否已提交KPI',
  `is_ts_employee_confirm` tinyint(1) DEFAULT '0' COMMENT '员工是否已全部Confirm',
  `is_ts_delegator_confirm` tinyint(1) DEFAULT '0' COMMENT '员工是否提交Target',
  `is_apa_employee_submit` tinyint(1) DEFAULT '0' COMMENT '员工是否已提交KPI',
  `is_apa_delegator_submit` tinyint(1) DEFAULT '0' COMMENT '经理是否打过分（第一次）',
  `employee_how_score` smallint(5) unsigned DEFAULT NULL COMMENT '员工How自评总分数',
  `employee_how_level` char(1) DEFAULT NULL COMMENT '员工自评等级',
  `employee_how_bonus_rate` float(4,2) unsigned DEFAULT NULL COMMENT '员工对How评分后对应的Bounds系数',
  `employee_what_score` smallint(5) unsigned DEFAULT NULL COMMENT '员工What自评总分数',
  `employee_what_bonus_rate` float(4,2) unsigned DEFAULT NULL COMMENT '员工对How评分后对应的Bounds系数',
  `employee_final_bonus_rate` float(4,2) unsigned DEFAULT NULL COMMENT '员工对How评分后对应的Bounds系数',
  `employee_how_result` varchar(31) DEFAULT NULL COMMENT '员工自评等级（TOP, BELOW, AVAGER, ..）',
  `first_how_score` smallint(5) unsigned DEFAULT NULL COMMENT '经理How自评总分数',
  `first_how_level` char(1) DEFAULT NULL COMMENT '经理自评等级',
  `first_how_bonus_rate` float(4,2) unsigned DEFAULT NULL COMMENT '经理对How评分后对应的Bounds系数',
  `first_what_score` smallint(5) unsigned DEFAULT NULL COMMENT '经理What自评总分数',
  `first_what_bonus_rate` float(4,2) unsigned DEFAULT NULL COMMENT '经理对How评分后对应的Bounds系数',
  `first_final_bonus_rate` float(4,2) unsigned DEFAULT NULL COMMENT '经理对How评分后对应的Bounds系数',
  `first_how_result` varchar(31) DEFAULT NULL COMMENT '经理自评等级（TOP, BELOW, AVAGER, ..）',
  `second_how_score` smallint(5) unsigned DEFAULT NULL COMMENT '经理How自评总分数',
  `second_how_level` char(1) DEFAULT NULL COMMENT '经理自评等级',
  `second_how_bonus_rate` float(4,2) unsigned DEFAULT NULL COMMENT '经理对How评分后对应的Bounds系数',
  `second_what_score` smallint(5) unsigned DEFAULT NULL COMMENT '经理What自评总分数',
  `second_what_bonus_rate` float(4,2) unsigned DEFAULT NULL COMMENT '经理对How评分后对应的Bounds系数',
  `second_final_bonus_rate` float(4,2) unsigned DEFAULT NULL COMMENT '经理对How评分后对应的Bounds系数',
  `second_how_result` varchar(31) DEFAULT NULL COMMENT '经理自评等级（TOP, BELOW, AVAGER, ..）',
  `is_release_score` tinyint(1) DEFAULT '0' COMMENT '经理释放第二次分数',
  `release_date` date DEFAULT NULL COMMENT '经理释放时间',
  `is_agree_score` tinyint(1) DEFAULT '0' COMMENT '员工是否读报表',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10267 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL COMMENT '主键',
  `position_id` int(10) unsigned NOT NULL COMMENT '职位描述',
  `department_id` int(10) unsigned DEFAULT NULL COMMENT '部门主键',
  `manager_id` int(10) unsigned DEFAULT NULL COMMENT '上级主管主键',
  `code` char(32) NOT NULL COMMENT '外部系统中的员工代码',
  `avatar` varchar(1023) DEFAULT NULL COMMENT '用户头像',
  `first_name` varchar(255) NOT NULL COMMENT '姓',
  `last_name` varchar(255) NOT NULL COMMENT '名',
  `cn_name` varchar(255) NOT NULL COMMENT '中文名称',
  `en_name` varchar(255) NOT NULL COMMENT '英文名称',
  `title` varchar(255) DEFAULT NULL COMMENT '用户职称',
  `email` varchar(255) DEFAULT NULL COMMENT '用户邮箱',
  `mobile` varchar(255) NOT NULL COMMENT '手机号码',
  `tel` varchar(255) DEFAULT NULL COMMENT '电话号码',
  `hire_date` date DEFAULT NULL COMMENT '用户入职日期',
  `need_kpi` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '用户是否需要进行 KPI 考核。有以下情况不需要纳入 KPI考核：高管、未满一年的新人',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '数据状态',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `users_view`
--

DROP TABLE IF EXISTS `users_view`;
/*!50001 DROP VIEW IF EXISTS `users_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `users_view` AS SELECT 
 1 AS `id`,
 1 AS `group_id`,
 1 AS `department_id`,
 1 AS `group_name`,
 1 AS `department`,
 1 AS `manager`,
 1 AS `en_name`,
 1 AS `cn_name`,
 1 AS `title`,
 1 AS `mobile`,
 1 AS `tel`,
 1 AS `email`,
 1 AS `hire_date`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `users_view`
--

/*!50001 DROP VIEW IF EXISTS `users_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`apa`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `users_view` AS select `u`.`id` AS `id`,`g`.`id` AS `group_id`,`u`.`department_id` AS `department_id`,`g`.`name` AS `group_name`,`d`.`name` AS `department`,`m`.`en_name` AS `manager`,`u`.`en_name` AS `en_name`,`u`.`cn_name` AS `cn_name`,`u`.`title` AS `title`,`u`.`mobile` AS `mobile`,`u`.`tel` AS `tel`,`u`.`email` AS `email`,`u`.`hire_date` AS `hire_date` from ((((`users` `u` join `users` `m`) join `departments` `d`) join `groups` `g`) join `group_users` `gu`) where ((`u`.`department_id` = `d`.`id`) and (`u`.`manager_id` = `m`.`id`) and (`gu`.`user_id` = `u`.`id`) and (`gu`.`group_id` = `g`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
