import { request } from '../../common/request.js';
import {mapState} from 'vuex'

import Confirm from '../../components/Confirm/page.vue';

export default {
  data() {
    return {
      currentIndex: null,
      keyWord:'',
      tableData: [],
      function_id: null,
      tableHeight: 412,
      personType:null,
      isShowConfirm:false,
      confirmTxt:'',
      handleType:null,
      confirmTitle:'Warning',
      isSelectAll:false,
      selectedData:[]
    }
  },
  components: {
    Confirm
  },
  computed:{
    ...mapState({
      delegatorInfo:(state)=>state.allState.delegatorInfo
    })
  },
  methods: {
    // 选中所有人
    selectAll(){
      if(!this.isSelectAll){
        this.isSelectAll = true
        this.$refs.form.toggleAllSelection()
        this.selectedData = this.delegatorInfo.persons
      }
    },
    selectCell(selection, row){
      if(selection.length === this.delegatorInfo.persons.length && selection.length > 0){
        this.isSelectAll = true
      }else{
        this.isSelectAll = false
      }
      this.selectedData = selection
    },
    // 初始默认选中成员
    selectedMembers(){
      if(this.delegatorInfo.persons.length > 0){
        let selectedData = []
        this.delegatorInfo.persons.forEach((item)=>{
          if(item.delegator_id === this.delegatorInfo.delegator.employee_id){
            this.$refs.form.toggleRowSelection(item,true)
            selectedData.push(item)
          }
        })
        this.selectedData = selectedData
      }
    },
    rowClick(row,column,event){
      if(this.selectedData.length > 0){
        let index = null
        this.selectedData.forEach((item,idx)=>{
          if(row.employee_id === item.employee_id){
            index = idx
          }
        })

        if(index === null){
          this.selectedData.push(row)
          this.$refs.form.toggleRowSelection(row,true)
        }else{
          this.selectedData.splice(index,1)
          this.$refs.form.toggleRowSelection(row,false)
        }
      }else{
        this.selectedData.push(row)
        this.$refs.form.toggleRowSelection(row,true)
      }
    },
    // 解散组
    disbandmentGroup(){
      this.confirmTitle = 'Warning'
      this.isShowConfirm = true
      this.handleType = 'disbandment'
      this.confirmTxt = 'Do you want to disbandment the group？'
    },
    isSelected(id){
      if(this.selectedData.length > 0){
        let index = null
        this.selectedData.forEach((item,idx)=>{
          if(item.employee_id === id){
            index = idx
          }
        })

        return index === null?false:true
      }else{
        return false
      }
    },
    // 弹框确认
    confirm() {
      switch(this.handleType){
        case 'cancel':
          this.$router.replace('delegation')
          break;
        case 'disbandment':
          // delegatorUnset
          request({
            url: 'delegatorUnset',
            method: 'get',
            params:{
            	function_id:this.function_id,
              delegator_id:this.delegatorInfo.delegator.employee_id
            },
            isShowLoading: true
          }).then(res => {
            if (!res || res.code !== 0) {
                this.confirmTitle = 'Error';
                this.confirmTxt = res.message;
                this.isShowConfirm = true;
            
                return ;
            }
            this.$router.replace('delegation')
          })
          break;
      }

      this.isShowConfirm = false;
    },
    // 弹框取消
    cancel() {
      this.isShowConfirm = false;
    },
    // 取消修改
    goBack(){
      this.confirmTitle = 'Confirm'
      this.isShowConfirm = true
      this.handleType = 'cancel'
      this.confirmTxt = 'Do you want to abandon the modification？'
    },
    // 确定修改
    modify(){
      // delegatorSet
      let employee_ids = []
      if(this.selectedData.length > 0){
        this.selectedData.forEach((item)=>{
          employee_ids.push(item.employee_id)
        })
      }

      request({
        url: 'delegatorSet',
        method: 'post',
        data:{
          employee_ids,
        	function_id:this.function_id,
          delegator_id:this.delegatorInfo.delegator.employee_id
        }
      }).then(res => {
        if (!res || res.code !== 0) {
            this.confirmTitle = 'Error';
            this.confirmTxt = res.message;
            this.isShowConfirm = true;
        
            return ;
        }
        this.$router.replace('delegation')
      })
    }
  },
  created() {
    this.function_id = localStorage.getItem('function_id');
  },
  mounted() {
    this.$nextTick(() => {
      // this.tableHeight = this.$refs.tableWrapper.offsetHeight;
      this.selectedMembers()
    })
  }
}
