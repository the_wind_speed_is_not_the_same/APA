package cn.porsche.compliance.repository;

import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.Question;
import cn.porsche.compliance.emnu.SeasonEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Integer> {
    @Transactional
    void deleteAllByYear(Integer year);

    List<Question> findAllByYear(Integer year);
    List<Question> findAllByYearAndSeason(Integer year, SeasonEnum season);
    List<Question> findAllByYearAndSeasonAndType(Integer year, SeasonEnum season, EmployeeTypeEnum type);
}