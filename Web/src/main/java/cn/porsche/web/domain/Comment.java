package cn.porsche.web.domain;

import cn.porsche.web.constant.CommentTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "comments", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @ApiModelProperty(notes = "KPI主键")
    @JsonProperty("function_id")
    @Column(name = "function_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'KPI主键'")
    Integer functionId;

    @ApiModelProperty(notes = "消息拥有者主键")
    @JsonProperty("owner_id")
    @Column(name = "owner_id",                  columnDefinition = "INT UNSIGNED NOT NULL COMMENT '消息拥有者主键'")
    Integer ownerId;

    @ApiModelProperty(notes = "消息类型【Overall Comments, Target Setting Comments, APA Comments】")
    @JsonProperty("type")
    @Column(name = "type",                      columnDefinition = "INT UNSIGNED NOT NULL COMMENT '消息类型'")
    CommentTypeEnum type;

    @ApiModelProperty(notes = "对应的主键")
    @JsonProperty("topic_id")
    @Column(name = "topic_id",                  columnDefinition = "INT UNSIGNED NOT NULL COMMENT '对应的主键'")
    Integer topicId;

    @ApiModelProperty(notes = "员工主键")
    @JsonProperty("employee_id")
    @Column(name = "employee_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT '员工主键'")
    Integer employeeId;

    @ApiModelProperty(notes = "员工主键")
    @JsonProperty("employee_name")
    @Column(name = "employee_name",             columnDefinition = "VARCHAR(255) NOT NULL COMMENT '员工名称'")
    String employeeName;

    @ApiModelProperty(notes = "委托人主键")
    @JsonProperty("delegator_id")
    @Column(name = "delegator_id",              columnDefinition = "INT UNSIGNED NOT NULL COMMENT '委托人主键'")
    Integer delegatorId;

    @ApiModelProperty(notes = "委托人主键")
    @JsonProperty("delegator_name")
    @Column(name = "delegator_name",            columnDefinition = "VARCHAR(255) NOT NULL COMMENT '委托人名称'")
    String delegatorName;

    @ApiModelProperty(notes = "消息提示")
    @JsonProperty("message")
    @Column(name = "message",                   columnDefinition = "TEXT NULL DEFAULT NULL COMMENT '消息提示'")
    String message;

    @ApiModelProperty(notes = "是否已读")
    @JsonProperty("is_read")
    @Column(name = "is_read",                   columnDefinition = "TINYINT(1) NULL DEFAULT NULL COMMENT '是否已读'")
    boolean read;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @ApiModelProperty(notes = "发送时间")
    @JsonProperty("send_time")
    @Column(name = "send_time",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date sendTime;
}
