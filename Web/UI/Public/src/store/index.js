import Vue from 'vue';
import Vuex from 'vuex';
import { allState } from './modules/allState';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    allState,
  },
});
