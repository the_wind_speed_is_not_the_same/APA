package cn.porsche.common.auth.impl.TestAuth;

import cn.porsche.common.auth.AbstractAuth;
import cn.porsche.common.auth.AccessToken;
import cn.porsche.common.auth.App;
import cn.porsche.common.util.JsonUtils;
import cn.porsche.common.util.RandomUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DemoAuth extends AbstractAuth {
    @Override
    public String getPound() {
        return "demo";
    }

    @Override
    public String getState() {
        return RandomUtils.randomString(32);
    }

    @Override
    public String getResponseType() {
        return "code";
    }

    @Override
    public String getAccessTokenUri() {
        return "https://api.biz.weibo.com/oauth/token";
    }

    @Override
    public String getRefreshTokenUri() {
        return "https://api.biz.weibo.com/oauth/token";
    }

    @Override
    protected List<NameValuePair> getAccessTokenParams(String code, App app) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("'client_id'", app.getAppId()));
        params.add(new BasicNameValuePair("redirect_uri", getBaseCallback()));
        params.add(new BasicNameValuePair("'code'", code));
        params.add(new BasicNameValuePair("grant_type", "authorization_code"));

        return params;
    }

    @Override
    protected List<NameValuePair> getRefreshTokenParams(String refreshToken, App app) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("'client_id'", app.getAppId()));
        params.add(new BasicNameValuePair("grant_type", "refresh_token"));
        params.add(new BasicNameValuePair("refresh_token", refreshToken));
        params.add(new BasicNameValuePair("redirect_uri", getBaseCallback()));

        return params;
    }


    @Override
    protected AccessToken resetAccessToken(String response) {
        return JsonUtils.fromString(response, AccessToken.class);
    }

    @Override
    protected AccessToken resetRefreshToken(String response) {
        return JsonUtils.fromString(response, AccessToken.class);
    }

//    @Override
//    public AccessToken getAccessToken() {
//        return null;
//    }

    private class TokenJson extends AccessToken {
        public TokenJson() {
            super(null, null, null, null, null, null, null, null);
        }
    }
}
