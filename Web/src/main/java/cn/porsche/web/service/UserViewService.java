package cn.porsche.web.service;


import cn.porsche.web.domain.Department;
import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.view.UserView;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserViewService {
    Page<UserView> findByPage(Integer page, Integer size);
    Page<UserView> searchByNeedHands(Integer page, Integer size);
    Page<UserView> searchByGroup(Group group, Integer page, Integer size);
    Page<UserView> searchByDepartment(Department department, Integer page, Integer size);

    Page<UserView> searchByName(String name, Integer page, Integer size);
    Page<UserView> searchByDepartmentAndName(Department department, String name, Integer page, Integer size);
    Page<UserView> searchByDepartmentAndNeedHands(Department department, boolean needHands, Integer page, Integer size);

    Page<UserView> searchByHireDateIsNull(Integer page, Integer size);
    Page<UserView> searchByDepartmentAndHireDateIsNull(Department department, Integer page, Integer size);

}
