package cn.porsche.web.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.common.util.JsonUtils;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.controller.params.WhatParam;
import cn.porsche.web.domain.Comment;
import cn.porsche.web.domain.TargetHow;
import cn.porsche.web.service.LoginService;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
class APAControllerTest extends ApplicationTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String API = "/apa";

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }



    @Test
    void eScoreHow() {
        final String api = API + "/e_score_how";

        TargetHow params = TargetHow.builder()
                .functionId(getFunctionId())
                .how1EScore(2.0f)
                .how2EScore(3.0f)
                .how3EScore(1.0f)
                .how4EScore(4.0f)
                .build();


        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(params, true))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void eScore() {
        final String api = API + "/e_score_what";

        WhatParam param = new WhatParam();
        param.setWhatId(getWhatId());
        param.setEmployeeScore(3.0f);
        param.setEmployeeComment("test");


        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(param, true))
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void eSubmit() {
        final String api = API + "/submit";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void doScoreWhat() {
        final String api = API + "/do_score_what";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("what_id", String.valueOf(getTargetId()))
                            .param("score", "3.5")
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void dtScore() {
        final String api = API + "/dt_score";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("what_id", String.valueOf(getTargetId()))
                            .param("score", "3.5")
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void releaseFinalScore() {
        final String api = API + "/release_final_score";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}