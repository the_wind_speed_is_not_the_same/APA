package cn.porsche.web.controller.params;

import cn.porsche.web.constant.GroupNameEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeGroupRequest {
    @ApiModelProperty(notes = "阶段主键")
    @JsonProperty("function_id")
    Integer functionId;

    @ApiModelProperty(notes = "用户主键")
    @JsonProperty("employees")
    List<Integer> employees;

    @ApiModelProperty(notes = "What主键")
    @JsonProperty("group")
    GroupNameEnum group;
}
