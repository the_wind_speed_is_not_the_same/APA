import {request} from '../../../../common/request.js';
import {deepClone} from '../../../../common/utils.js';
import Confirm from '../../../../components/Confirm/page.vue';

export default {
    props: ['func', 'dimension', 'isSaveBtnDisabled', 'targetDetail'],
    data() {
        return {
            dimensionsListConst: null,
            dimensionsList: [
                {
                    name: 'Herzblut',
                    subTxt: '1. Live Passionately <br /> 2. Know where you come from',
                    weight: '25%',
                    description: '',
                    tootip: 'Herzblut<br/>' +
                        '<p style="text-indent:1em">1.  Live Passionately</p>' +
                        '<p style="text-indent:2em">I inspire others for our work </p>' +
                        '<p style="text-indent:2em">I have high standards for my work </p>' +
                        '<p style="text-indent:2em">I celebrate successes with others </p>' +
                        '<p style="text-indent:2em">We work with commitment and with conviction</p>' +
                        '<p style="text-indent:1em">2.	Know where you come from </p>' +
                        '<p style="text-indent:2em">I do not take the given things for granted</p>' +
                        '<p style="text-indent:2em">I utilize our resources intelligently </p>' +
                        '<p style="text-indent:2em">We know for whom we work - for our customers </p>' +
                        '<p style="text-indent:2em">We foster social partnerships and promote a fair balance </p>'+
                        '<p style="text-indent:2em">between employee and company interests',
                    placeholder: "Please describe how you are going to perform and realize the objectives by living the value of \"Herzblut\"."
                }, {
                    name: "Pioniergeist",
                    subTxt: '1. Look into the future with courage<br />2. Think outside the box',
                    weight: '25%',
                    tootip: 'Pioniergeist<br/>' +
                        '<p style="text-indent:1em">1.  Look into the future with courage</p>' +
                        '<p style="text-indent:2em">I respond flexibly to changing framework conditions</p>' +
                        '<p style="text-indent:2em">I keep the goal in mind when there is uncertainty </p>' +
                        '<p style="text-indent:2em">I am willing to take risks </p>' +
                        '<p style="text-indent:2em">We consider new technologies to be a chance <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and drive forward future-oriented ideas (regardless of their origin)</p>' +
                        '<p style="text-indent:1em">2.	Think outside the box </p>' +
                        '<p style="text-indent:2em">I ensure, that our objectives and backgrounds are transparent</p>' +
                        '<p style="text-indent:2em">I analyze challenges quickly, reduce complexity and derive specific conclusions </p>' +
                        '<p style="text-indent:2em">I question the status quo and bring in solutions </p>' +
                        '<p style="text-indent:2em">We build networks to achieve common objectives',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize the objectives by living the value of \"Pioniergeist\".'
                }, {
                    name: 'Sportlichkeit',
                    subTxt: '1. Fight fair <br/>2. Stay hungry',
                    weight: '25%',
                    tootip: 'Sportlichkeit<br/>' +
                        '<p style="text-indent:1em">1.	Fight fair</p>' +
                        '<p style="text-indent:2em">I voice my opinion and am prepared to disagree with others</p>' +
                        '<p style="text-indent:2em">I discuss other’s opinions in a constructive way</p>' +
                        '<p style="text-indent:2em">I remain objective and tolerant, even when conflict arises </p>' +
                        '<p style="text-indent:2em">Together, we fight for the best result</p>' +
                        '<p style="text-indent:1em">2.	Stay hungry </p>' +
                        '<p style="text-indent:2em">I set myself ambitious goals</p>' +
                        '<p style="text-indent:2em">I seek and provide feedback and examine my own conduct </p>' +
                        '<p style="text-indent:2em">I update and expand my knowledge, learn quickly from mistakes and ' +
                        '<p style="text-indent:2em">acknowledge them openly</p>' +
                        '<p style="text-indent:2em">We strive to find the optimum solution</p>',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize\r\n the objectives by living the value of \"Sportlichkeit\".'
                }, {
                    name: 'One Family',
                    subTxt: '1. Take responsibility<br />' + '2. Respect each other',
                    weight: '25%',
                    tootip: 'One Family<br/>' +
                        '<p style="text-indent:1em">1.	Take responsibility</p>' +
                        '<p style="text-indent:2em">I keep my word and behave with integrity</p>' +
                        '<p style="text-indent:2em">I take responsibility for common objectives</p>' +
                        '<p style="text-indent:2em">I prioritize topics and create leeway for responsibility to be handed over </p>' +
                        '<p style="text-indent:2em">We ensure that objectives are achieved for our topics </p>' +
                        '<p style="text-indent:1em">2.	Respect each other </p>' +
                        '<p style="text-indent:2em">I develop individuals based on their strengths and weaknesses</p>' +
                        '<p style="text-indent:2em">I develop my team cross functionally and encourage its diversity </p>' +
                        '<p style="text-indent:2em">I am respectful and considerate of others and myself</p>' +
                        '<p style="text-indent:2em">We encourage interaction with each other</p>',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize the objectives by living the value of \"One Family\".'
                }
            ],
            isModify: false,
            titleTxt: '',
            confirmTxt: '',
            isShowConfirm: false,
            showTime: 3000,
            isClickType: null,
            isShowButtonLeft: false
        }
    },
    watch: {
        dimensionsList: {
            handler(n, o) {
                if (JSON.stringify(n) !== JSON.stringify(this.dimensionsListConst)) {
                    // 已经修改 How 的内容
                    this.isModify = true;
                } else {
                    this.isModify = false;
                }
            },
            deep: true
        }
    },
    components: {
        Confirm
    },
    computed: {
        isBtnDisabled() {
            return this.inputDisabled || !this.isModify
        },
        isConfirmDisabled() {
            // 开启 APA 状态 或 未修改过内容 禁用
            if (this.dimension.is_delegator_confirm && !this.dimension.is_employee_confirm) {
                // console.log("经理确认，但员工未确认，不禁用");
                return false;
            }

            // console.log("是否禁用Confirm 按钮？" + this.isModify);
            return true;
        },
        inputDisabled() {
            // apa阶段开启，已确定的how不能再修改
            return this.isSaveBtnDisabled && this.targetDetail.is_ts_employee_confirm && this.targetDetail.is_ts_delegator_confirm
        }
    },
    methods: {
        confirm() {
            if (this.isClickType === 'prev') {
                this.$emit('previousIndex');
            } else if (this.isClickType === 'next') {
                this.$emit('nextIndex');
            }
            this.isShowConfirm = false;
        },
        cancel() {
            this.isShowConfirm = false;
        },
        previous() {
            if (this.isModify) {
                this.titleTxt = 'Warning'
                this.confirmTxt = 'There are some changes had not saved,are you sure you want to leave?'
                this.isShowConfirm = true
                this.showTime = 0
                this.isClickType = 'prev'
                this.isShowButtonLeft = true
                return false
            }
            this.$emit('previousIndex');
        },
        next() {
            if (this.isModify) {
                this.titleTxt = 'Warning'
                this.confirmTxt = 'There are some changes had not saved,are you sure to leave?'
                this.isShowConfirm = true
                this.showTime = 0
                this.isClickType = 'next'
                this.isShowButtonLeft = true
                return false
            }
            this.$emit('nextIndex');
        },
        closeComponent() {
            this.$emit('closeComponent');
        },
        saveHow() {
            if (this.isBtnDisabled) return;
            let data = {
                how1: '1',
                how2: '2',
                how3: '3',
                how4: '4',
                function_id: this.func['id']
            };

            this.dimensionsList.forEach((item, index) => {
                data[`how${index + 1}`] = item.description;
            });

            request({
                url: 'updateHow',
                data,
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = 'Save Failed, Please try again.';
                    this.isShowConfirm = false;

                    return false;
                }

                if (this.isBtnDisabled) return;

                let data = {
                    how1: '1',
                    how2: '2',
                    how3: '3',
                    how4: '4',
                    function_id: this.func['id']
                };

                this.isShowButtonLeft = false;
                this.titleTxt = 'Success';
                this.confirmTxt = 'Save Successfully';
                this.isShowConfirm = true;
                this.isModify = false;
                this.showTime = 0;

                this.$emit('employeeSave');

                this.$emit('refreshList');
            })
        },
        confirmHow() {
            if (this.isBtnDisabled) return;
            if (!this.dimension || !this.dimension.id) {
                return false;
            }

            if (!this.isConfirmDisabled) {
                request({
                    url: 'confirmHowByEmployee',
                    method: 'get',
                    params: {
                        how_id: this.dimension.id
                    },
                    isShowLoading: true
                }).then(res => {
                    this.isShowButtonLeft = false

                    if (res && res.code === 0) {

                        /// 我加的代码没用
                        this.titleTxt = 'Success';
                        this.confirmTxt = 'Confirm Successfully';
                        this.isShowConfirm = true;
                        this.dimensionsListConst = deepClone(this.dimensionsList);
                        this.isModify = false;
                        this.showTime = 0
                        this.$emit('refreshList');

                    } else {
                        this.titleTxt = 'Error';
                        this.confirmTxt = 'Confirm Failed, Please try again.';
                        this.isShowConfirm = true;
                    }
                })
            }
        },
        filterScore(score) {
            score = String(score);

            return score.length === 1 ? score + '.0' : score;
        }
    },
    mounted() {
        this.dimensionsList.forEach((item, index) => {
            if (this.dimension && this.dimension[`how${index + 1}`]) {
                item.description = this.dimension && this.dimension[`how${index + 1}`];
            }
        });

        this.dimensionsListConst = deepClone(this.dimensionsList);
    }
}
