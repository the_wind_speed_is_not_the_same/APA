package cn.porsche.compliance.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.Position;
import cn.porsche.compliance.domain.Risk;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.domain.Email;
import cn.porsche.compliance.email.impl.SpringBootEmail;
import cn.porsche.compliance.service.DepartmentService;
import cn.porsche.compliance.service.PositionService;
import cn.porsche.compliance.service.RiskService;
import cn.porsche.compliance.service.UserViewService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;

@RestController
@RequestMapping("/email")
@ApiModel(value = "/email", description = "新增域名白名单")
public class EmailController extends BaseController {
    private static final String HTML = "你的验证码为 %s，请尽快使用";

    @Autowired
    SpringBootEmail emailService;

    @Autowired
    PositionService positionService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    RiskService riskService;

    @Autowired
    UserViewService userViewService;

    @ApiOperation(
            value = "获取当前员工的个人信息",
            notes = "只显示当前员工的个人信息"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = Department.class)
    })
    @PostMapping(value = "/host")
    public String addHost(
            @RequestParam("host") String host
    ) {
        if (noPromise("Human Resources")) {
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (StringUtils.isEmpty(host)) {
            return ApiResult.isBad("您所输入的域名为空，请验证之后再操作。");
        }

        host = host.trim().toLowerCase();
        Department department = departmentService.findByCode(host);
        if (null != department) {
            return ApiResult.isBad("您所添加的域名已存在，请匆重复添加");
        }

        department = Department.builder()
                .code(host)
                .name(host)
                .count(0)
                .managerId(null)
                .companyCode(host)
                .parentCode(host)
                .parentId(0)
                .build();

        departmentService.update(department);

        return ApiResult.builder().data(department).toJson();
    }

    @ApiOperation(
            value = "设置域名下对应的部门信息",
            notes = "提供给外部员工添加自己的部门信息"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @PostMapping(value = "/send_code")
    public String sendCode(
            @RequestParam("email") Integer hostId,
            @RequestParam("name") String name
    ) {
        if (noPromise("Human Resources")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        Department department = departmentService.findById(hostId);
        if (null == department) {
            return ApiResult.isBad("此域名不存在，请重新再试或联系对应的负责人");
        }

        int leftLimit   = 97; // letter 'a'
        int rightLimit  = 122; // letter 'z'
        int lengthLimit = 6;
        Random random = new Random();

        // 生成随机码
        String code = random.ints(leftLimit, rightLimit + 1).limit(lengthLimit).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

        // 获取邮箱参数
        Properties props = emailService.getProperties();

        // 开始邮件发送流程
        Email email = Email.builder()
                .from(props.getProperty("spring.mail.username"))
                .to(name + department.getCode())
                .content(String.format(HTML, code))
                .code(code)
                .build();


        Assert.notNull(email, "email must not be null");


        Session mailSession = Session.getDefaultInstance(props, null);
        Transport transport;
        try {
            transport = mailSession.getTransport();

            MimeMessage message = new MimeMessage(mailSession);
            message.setSubject(email.getSubject());
            message.setText(email.getContent().replaceAll("\n", "<br />"), "UTF-8", "html");
            message.setSentDate(new Date());
            message.setFrom(String.format("Compliance Team<%s>", props.getProperty("mail.username")));

            String[] list;
            if (!org.springframework.util.StringUtils.isEmpty(email.getTo())) {
                list = email.getTo().split(";");
                for (int i = 0, len = list.length; i < len; i++) {
                    if (i > 5) {
                        continue;
                    }

                    try {
                        message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailService.getActive().equals("prod") ? list[i] : props.getProperty("mail.test")));
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                }
            }

            transport.connect(props.getProperty("mail.smtp.host"), (Integer) props.get("mail.smtp.port"), props.getProperty("mail.username"), props.getProperty("mail.password"));
            transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            transport.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return ApiResult.isOk();
    }


//    @ApiOperation(
//            value = "设置域名下对应的部门信息",
//            notes = "提供给外部员工添加自己的部门信息"
//    )
//    @ApiResponses({
//            @ApiResponse(code = 0,   message = "成功", response = Department.class)
//    })
//    @PostMapping(value = "/add_staff")
//    public String addStaff(
//            @RequestParam("host_id") Integer hostId,
//            @RequestParam("name") String name
//    ) {
//        if (noPromise("Human Resources")) {
//            // 未有权限
//            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
//        }
//
//        if (StringUtils.isEmpty(content)) {
//            return ApiResult.isBad("内容为空，设置失败");
//        }
//
//        String[] codes = content.split("\n");
//        for (int i=0; i<codes.length; ++i) {
//            if (null == ( user = userService.findByCode(StringUtils.trim(codes[i])) )) {
//                continue;
//            }
//
//            if (user.getType().equals(EmployeeTypeEnum.VP)) {
//                continue;
//            }
//
//            user.setType(EmployeeTypeEnum.VP);
//            userService.update(user);
//        }
//
//        return ApiResult.isOk();
//    }

    @ApiOperation(
            value = "下属的风险列表",
            notes = "只看下级的风险信息，自己不可看自己的风险数据"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/risks")
    public String risks() {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        Map<String, Object> json = new HashMap<>();

        List<Risk> risks = new ArrayList<>();
        json.put("risks", risks);
        if (null == user.getPositionId()) {
            // 用户没有职位信息
            return ApiResult.builder().data(json).toJson();
        }

        Position position = positionService.findById(user.getPositionId());
        if (null == position) {
            // 用户没有职位信息
            return ApiResult.builder().data(json).toJson();
        }

        // 当前用户添加到返回结果
        json.put("user", userViewService.findById(user.getId()));
        json.put("risk", riskService.findByUser(user));

        User subUser;
        Risk subRisk;
        List<Position> subordinates = positionService.subordinates(position);
        for (Position item : subordinates) {
            if (null != ( subUser = userService.findByPositionId(item.getId()) )) {
                if (null != ( subRisk = riskService.findByUser(subUser)) ) {
                    risks.add(subRisk);
                }
            }
        }

        return ApiResult.builder().data(json).toJson();
    }
}
