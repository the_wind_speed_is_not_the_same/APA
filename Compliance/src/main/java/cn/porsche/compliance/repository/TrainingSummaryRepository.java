package cn.porsche.compliance.repository;

import cn.porsche.compliance.domain.Statistical;
import cn.porsche.compliance.emnu.TrainingTypeEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingSummaryRepository extends CrudRepository<Statistical, Integer> {
    Statistical findByYearAndUserIdAndType(Integer year, Integer userId, TrainingTypeEnum type);
    Statistical findByUserIdAndType(Integer userId, TrainingTypeEnum type);

    List<Statistical> findAllByYearAndDepartmentIdAndType(Integer year, Integer departmentId, TrainingTypeEnum type);
}
