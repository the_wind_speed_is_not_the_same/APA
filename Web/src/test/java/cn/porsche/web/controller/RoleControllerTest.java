package cn.porsche.web.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.service.LoginService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class RoleControllerTest extends ApplicationTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String API = "/role";

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    @Test
    void update() {
        final String api = API + "/update";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .param("group", GroupNameEnum.DisciplinaryManager.toString())
                            .param("module", ModuleNameEnum.SystemSetting.toString())
                            .param("operator", OperatorText.ADD.toString())
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void list() {
        final String api = API + "/list";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}