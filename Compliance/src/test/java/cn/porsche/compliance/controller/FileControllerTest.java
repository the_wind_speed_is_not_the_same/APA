package cn.porsche.compliance.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.ApplicationTest;
import cn.porsche.compliance.service.LoginService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.servlet.http.Cookie;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
class FileControllerTest extends ApplicationTest {
    private static final String API = "/file";

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void detail() {
        final String api = API + "/detail";

        Cookie cookie = new Cookie("auth_id", getHRAuthorization());

        try {
            mvcResult = mvc.perform(
                    multipart(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .cookie(cookie)
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void view() {
        final String api = API + "/training";

        Cookie cookie = new Cookie("auth_id", getHRAuthorization());
        try {
            mvcResult = mvc.perform(
                    multipart(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .cookie(cookie)
//                            .param("year", String.valueOf(year))
//                            .param("season", String.valueOf(season))
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}