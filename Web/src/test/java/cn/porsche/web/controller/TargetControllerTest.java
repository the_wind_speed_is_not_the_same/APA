package cn.porsche.web.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.common.util.JsonUtils;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.controller.params.WhatParam;
import cn.porsche.web.domain.TargetHow;
import cn.porsche.web.domain.TargetKPI;
import cn.porsche.web.service.LoginService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class TargetControllerTest extends ApplicationTest {
    private static final String API = "/target";

    private String token;

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void statusByFunctoin() {
        final String api = API + "/status";

//        WhatParams params = new WhatParams();
//        params.setFunctionId(1);
//        params.setName("My first traget  Teams set their own objectives");
//        params.setDesc("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. ");
//        params.setWeight(30);
//        params.setFinishDate(new SimpleDateFormat("yyy-MM-dd").format(new Date()));

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void addWhat() {
        final String api = API + "/add_what";

        WhatParam params = new WhatParam();
        params.setFunctionId(1);
        params.setName("My first traget  Teams set their own objectives");
        params.setDesc("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. ");
        params.setWeight(30);
//        params.setFinishDate(new SimpleDateFormat("yyy-MM-dd").format(new Date()));

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(params))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void addWhatAndKPI() {
        final String api = API + "/update_what";
        List<TargetKPI> kpis = new ArrayList<>();
        TargetKPI kpi;

        kpi = new TargetKPI();
        kpi.setIndex(0);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        kpi = new TargetKPI();
        kpi.setIndex(1);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        kpi = new TargetKPI();
        kpi.setIndex(2);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        kpi = new TargetKPI();
        kpi.setIndex(3);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        kpi = new TargetKPI();
        kpi.setIndex(4);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        WhatParam params = new WhatParam();
        params.setWhatId(6);
        params.setFunctionId(getFunctionId());
        params.setName("My first traget  Teams set their own objectives");
        params.setDesc("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.");
        params.setWeight(20);
        params.setFinishDate(new Date());
        params.setKpis(kpis);

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(params))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void updateWhatAndKPI() {
        final String api = API + "/update_what";
        List<TargetKPI> kpis = new ArrayList<>();
        TargetKPI kpi;

        kpi = new TargetKPI();
        kpi.setIndex(0);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        kpi = new TargetKPI();
        kpi.setIndex(1);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        kpi = new TargetKPI();
        kpi.setIndex(2);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        kpi = new TargetKPI();
        kpi.setIndex(3);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        kpi = new TargetKPI();
        kpi.setIndex(4);
        kpi.setDesc("*<Please desribe target achievement scenarios>");
        kpis.add(kpi);

        WhatParam params = new WhatParam();
        params.setFunctionId(getFunctionId());
        params.setWhatId(2);
        params.setName("My first traget  Teams set their own objectives");
        params.setDesc("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.");
        params.setWeight(20);
        params.setFinishDate(new Date());
        params.setKpis(kpis);

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(params))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void detail() {
        final String api = API + "/detail";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void getWhats() {
        final String api = API + "/get_whats";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void getWhat() {
        final String api = API + "/get_what";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("what_id", String.valueOf(getWhatId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void updateHow() {
        final String api = API + "/update_how";

        TargetHow how = TargetHow.builder()
                .functionId(getFunctionId())
                .how1("My first traget  Teams set their own objectives")
                .how2("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.")
                .how3("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. ")
                .how4("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. ")
                .EmployeeConfirm(false)
                .DelegatorConfirm(false)
                .build();


        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(how))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void check() {
        final String api = API + "/check";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void submit() {
        final String api = API + "/submit";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void confirmByEmployee() {
        final String api = API + "/employee_confirm";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void confirmByDelegator() {
        final String api = API + "/delegator_confirm";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("employee_id", String.valueOf(getEmployeeId()))
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void detailByEmployee() {
        final String api = API + "/detail";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("employee_id", "504169")
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void listByDelegator() {
        final String api = API + "/list";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void listByManager() {
        final String api = API + "/list";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void statistic() {
        final String api = API + "/statistic";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}