// 菜单
export const V_Menus = [
    {
        'title': '资金管理',
        'url': '/#/settleAccountsManage',
    },
    {
        'title': '结算管理',
        'url': '',
        'subItems': [
            {
                'title': '新增结算',
                'url': '/#/settleAccountsAdd'
            },
            {
                'title': '结算列表',
                'url': '/#/sttleAccountsList'
            },
        ]
    },
    {
        'title': '账单管理',
        'url': '/#/accountsManage'
    },
    {
        'title': '人员管理',
        'url': '/#/employeeManage'
    },
    {
        'title': '发票管理',
        'url': '',
        'subItems': [
            {
                'title': '发票申请',
                'url': '/#/invoiceApplication'
            },
            {
                'title': '发票查询',
                'url': '/#/invoiceInquiry'
            },
        ]
    },
]

// 导航栏
export const V_Navigator = [
    {
        'title': '任务列表',
        'url': '/#/taskList',
    },
    {
        'title': '新增企业',
        'url': '/#/enterPriseAdd',
    },
    {
        'title': '企业列表',
        'url': '/#/enterPriseList'
    },
    {
        'title': '企业充值',
        'url': '/#/investMoney'
    },
    // {
    //     'title': '代传代发',
    //     'url': '/#/sttleAccountsList'
    // }
]