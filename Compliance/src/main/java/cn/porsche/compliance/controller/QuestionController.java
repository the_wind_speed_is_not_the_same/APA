package cn.porsche.compliance.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.*;
import cn.porsche.compliance.emnu.SeasonEnum;
import cn.porsche.compliance.emnu.TrainingTypeEnum;
import cn.porsche.compliance.service.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping("/question")
@ApiModel(value = "/question", description = "问题的操作类")
public class QuestionController extends BaseController {
    @Autowired
    QuestionService questionService;

    @Autowired
    StatisticalService reportService;

    @Autowired
    TrainingService trainingService;

    @Autowired
    QuestionAnswerService questionAnswerService;

    @ApiOperation(
            value = "根据year和season获取到当前季度答题数据"
    )
    @ApiResponses({
            @ApiResponse(code = 0, message = "成功", response = User.class)
    })
    @GetMapping(value = "/me")
    public String me(
            @RequestParam(value = "year", required = false) Integer year,
            @RequestParam(value = "season", required = false) SeasonEnum season
    ) {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (year == null || season == null) {
            Calendar instance = Calendar.getInstance();
            year = instance.get(Calendar.YEAR);
            season = SeasonEnum.from((int) Math.ceil((instance.get(Calendar.MONTH) + 1.0f) / 3));
        }

        // 答题记录
        List<QuestionAnswer> answers = questionAnswerService.findUserAndYearAndSeason(user, year, season);

        log.info("year:{}", year);
        log.info("season:{}", season);
        log.info("user:{}", user);

        Map<String, Object> json = new HashMap<>();
        json.put("user", user);
        json.put("answer", answers);

        log.info("answer:{}", json.get("answer"));
        if (json.get("answer") == null) {
            json.put("answer", false);
        }


        // 生成对应的题库列表
        // VP员工只返回Q1,Q3，普通员工返回：Q1, Q2, Q3, Q4
        json.put("seasons", Arrays.stream(SeasonEnum.values()).filter(item ->
                !(EmployeeTypeEnum.VP.equals(user.getType()) && (
                        SeasonEnum.Q2.equals(item) || SeasonEnum.Q4.equals(item)
                ))
        ).collect(Collectors.toList()));

        return ApiResult.builder().data(json).toJson();
    }


    @ApiOperation(
            value = "给上传人员提供的获取本年度的题库"
    )
    @ApiResponses({
            @ApiResponse(code = 0, message = "成功", response = User.class)
    })
    @GetMapping(value = "/by_year")
    public String byYear(
            @RequestParam(value = "year", required = false) Integer year
    ) {
        if (noPromise("Human Resources")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (year == null) {
            Calendar instance = Calendar.getInstance();
            year = instance.get(Calendar.YEAR);
        }

        return ApiResult.builder().list(questionService.findAllByYear(year)).toJson();
    }

    @ApiOperation(
            value = "给员工提供的本季度返回的题库"
    )
    @ApiResponses({
            @ApiResponse(code = 0, message = "成功", response = User.class)
    })
    @GetMapping(value = "/by_season")
    public String bySeason(
            @RequestParam(value = "year", required = false) Integer year,
            @RequestParam(value = "season", required = false) SeasonEnum season
    ) {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (null == year) {
            Calendar instance = Calendar.getInstance();
            year = instance.get(Calendar.YEAR);
        }

        if (null == season) {
            Calendar instance = Calendar.getInstance();
            season = SeasonEnum.from((int) Math.ceil((instance.get(Calendar.MONTH) + 1.0f) / 3));
        }

        if (user.getType().equals(EmployeeTypeEnum.VP)) {
            // VP 只有上半年和下半年之分
            if (SeasonEnum.Q2.equals(season)) {
                season = SeasonEnum.Q1;
            } else if (SeasonEnum.Q4.equals(season)) {
                season = SeasonEnum.Q3;
            }
        }

        List<Question> questions = questionService.findListByYearAndSeasonAndEmployeeType(year, season, user.getType());
        if (questions.size() == 0) {
            return ApiResult.isBad("The questions of the current quarter have not been uploaded, Please contact Legal Officer");
        }

        for (Question item : questions) {
            item.setAnswer(null);
            item.setScore(null);

            item.setYear(null);
            item.setSeason(null);
        }

        return ApiResult.builder().list(questions).toJson();
    }


    @ApiOperation(
            value = "员工回答问题的接口",
            notes = "answers为数组[A,B,A,D,C]。如果题库接口返回10题，那么answers中元素就有多少个\n" +
                    "返回：\n" +
                    "answers: 正常答案的数组，只需要参考right值：true回答正确\n" +
                    "score：本次的"
    )
    @ApiResponses({
            @ApiResponse(code = 0, message = "成功", response = User.class)
    })
    @PostMapping(value = "/answer")
    public String answer(
            @RequestParam(value = "year", required = false) Integer year,
            @RequestParam(value = "season", required = false) SeasonEnum season,
            @RequestParam(value = "answers") List<String> answers
    ) {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (null == year) {
            Calendar instance = Calendar.getInstance();
            year = instance.get(Calendar.YEAR);
        }

        if (null == season) {
            Calendar instance = Calendar.getInstance();
            season = SeasonEnum.from((int) Math.ceil((instance.get(Calendar.MONTH) + 1.0f) / 3));
        }

        // VP 只有上半年和下半年之分
        if (user.getType().equals(EmployeeTypeEnum.VP)) {
            // VP 只有上半年和下半年之分
            if (SeasonEnum.Q2.equals(season)) {
                season = SeasonEnum.Q1;
            } else if (SeasonEnum.Q4.equals(season)) {
                season = SeasonEnum.Q3;
            }
        }

        List<Question> questions = questionService.findListByYearAndSeasonAndEmployeeType(year, season, user.getType());

        if (questions.size() != answers.size()) {
            return ApiResult.isBad("The questions has changed, Please answer again");
        }

        List<QuestionAnswer> list = new ArrayList<>();
        List<String> rights   = new ArrayList<>();
        List<String> uAnswers = new ArrayList<>();
        Integer score = 0;
        boolean right;
        Question question;
        QuestionAnswer answer;
        for (int i = 0; i < questions.size(); i++) {
            if (StringUtils.isEmpty(answers.get(i))) {
                continue;
            }

            question = questions.get(i);
            right = question.getAnswer().toUpperCase().equals(answers.get(i).toUpperCase());

            answer = QuestionAnswer.builder()
                    .userId(user.getId())
                    .questionId(question.getId())
                    .year(year)
                    .season(season)
                    .answer(answers.get(i))
                    .score(right ? question.getScore() : 0)
                    .Right(right)
                    .build();

            rights.add(question.getAnswer());
            uAnswers.add(answer.getAnswer());
            list.add(answer);

            if (answer.isRight()) {
                score += answer.getScore();
            }
        }

        // 对比高分
        boolean isContinue = false;
        int total = questionAnswerService.summaryScoreByUserAndYearAndSeason(user, year, season);
        if (score > total) {
            // 删除之前的回答记录，保留此次的回答数据
            questionAnswerService.deleteByUser(user, year, season);

            // 先删除所有旧的答案，将新的答案存储起来
            for (int i = 0; i < list.size(); i++) {
                // 以前分烽没有这次高，保存这些次的
                questionAnswerService.insert(list.get(i));
            }

            isContinue = true;
        }

        if (isContinue) {
            // 更新当季的题库记录
            Training training = trainingService.findByYearAndUserAndTypeAndSubject(year, user, TrainingTypeEnum.Question, season.toString());
            if (null == training) {
                training = Training.builder()
                        .year(year)
                        .type(TrainingTypeEnum.Question)
                        .attend(false)
                        .subject(season.toString())

                        .departmentId(user.getDepartmentId())
                        .userId(user.getId())
                        .build();
            }

            if (true != training.getAttend()) {
                training.setAttend(true);
                trainingService.update(training);
            }

            /// 更新角色的培训统计信息
            reportService.refreshQuestion(user);
        }


        Map<String, Object> result = new HashMap<>();
        result.put("rights", rights);
        result.put("users", uAnswers);
        result.put("score", score);

        return ApiResult.builder().data(result).toJson();
    }
}
