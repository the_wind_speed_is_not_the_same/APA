package cn.porsche.web.service.impl;


import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.GroupUser;
import cn.porsche.web.domain.User;
import cn.porsche.web.repository.GroupUserRepository;
import cn.porsche.web.service.GroupUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;


@Service
public class GroupUserServiceImpl implements GroupUserService {
    @Autowired
    GroupUserRepository repository;

    @Override
    public void delete(GroupUser groupUser) {
        repository.delete(groupUser);
    }

    @Override
    public GroupUser update(GroupUser groupUser) {
        return repository.save(groupUser);
    }


    @Override
    public GroupUser add(Group group, User user) {
        return add(group, user, null);
    }

    @Override
    public GroupUser add(Group group, User user, User hands) {
        Assert.notNull(group, "The [group] argument is null");
        Assert.notNull(user, "The [user] argument is null");

        GroupUser groupUser = findByGroupAndUser(group, user);
        if (null != groupUser) {
            return groupUser;
        }

        groupUser = GroupUser.builder()
                .byHandsUserId(hands == null ? null : hands.getId())
                .userId(user.getId())
                .groupId(group.getId())
                .build();

        return repository.save(groupUser);
    }

    @Override
    public List<GroupUser> findByUser(User user) {
        return repository.findByUserId(user.getId());
    }

    @Override
    public List<User> findAllByGroup(Group group) {
        return repository.listByGroupId(group.getId());
    }

    @Override
    public GroupUser findByGroupAndUser(Group group, User user) {
        Assert.notNull(group, "The [group] argument is null");
        Assert.notNull(user, "The [user] argument is null");

        return repository.findByGroupIdAndUserId(group.getId(), user.getId());
    }

    @Override
    public Boolean isExist(Group group, User user) {
        Assert.notNull(group, "The [group] argument is null");
        Assert.notNull(user, "The [user] argument is null");

        return findByGroupAndUser(group, user) != null;
    }

    @Override
    public void remove(Group group, User user) {
        GroupUser data = findByGroupAndUser(group, user);
        if (null != data) {
            repository.delete(data);
        }
    }
}
