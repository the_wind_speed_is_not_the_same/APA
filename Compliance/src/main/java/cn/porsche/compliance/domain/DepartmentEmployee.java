package cn.porsche.compliance.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "department_employees", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class DepartmentEmployee implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    Integer id;

    @JsonProperty("employee_Id")
    @Column(name = "employee_Id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT '员工主键'")
    Integer employeeId;

    @ApiModelProperty(notes = "顶级主键")
    @JsonProperty("department_1")
    @Column(name = "department_1",              columnDefinition = "INT UNSIGNED NULL COMMENT '公司代码'")
    Integer department1;

    @ApiModelProperty(notes = "顶级主键")
    @JsonProperty("department_2")
    @Column(name = "department_2",              columnDefinition = "INT UNSIGNED NULL COMMENT '公司代码'")
    Integer department2;

    @ApiModelProperty(notes = "顶级主键")
    @JsonProperty("department_3")
    @Column(name = "department_3",              columnDefinition = "INT UNSIGNED NULL COMMENT '公司代码'")
    Integer department3;

    @ApiModelProperty(notes = "顶级主键")
    @JsonProperty("department_4")
    @Column(name = "department_4",              columnDefinition = "INT UNSIGNED NULL COMMENT '公司代码'")
    Integer department4;

    @ApiModelProperty(notes = "顶级主键")
    @JsonProperty("department_5")
    @Column(name = "department_5",              columnDefinition = "INT UNSIGNED NULL COMMENT '公司代码'")
    Integer department5;
}
