package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "department_reporting", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class DepartmentReport implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    Integer id;

    @ApiModelProperty(notes = "KPI主键")
    @JsonProperty("function_id")
    @Column(name = "function_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'KPI主键'")
    Integer functionId;

    @ApiModelProperty(notes = "部门主键")
    @JsonProperty("department_id")
    @Column(name = "department_id",             columnDefinition = "INT UNSIGNED NOT NULL COMMENT '部门主键'")
    Integer departmentId;

    @ApiModelProperty(notes = "部门名称")
    @JsonProperty("department_name")
    @Column(name = "department_name",           columnDefinition = "VARCHAR(255) NULL COMMENT '部门名称'")
    String departmentName;

    @ApiModelProperty(notes = "部门主管")
    @JsonProperty("manager")
    @Column(name = "department_manager",        columnDefinition = "VARCHAR(255) NULL COMMENT '部门主管'")
    String departmentManager;

    @JsonProperty("total")
    @Column(name = "total",                     columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '总人数'")
    Integer total;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// 汇总数据
    ///
    @JsonProperty("not_start_ts")
    @Column(name = "not_start_ts",              columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '总人数'")
    Integer notStartTS;

    @JsonProperty("submitted_ts")
    @Column(name = "submitted_ts",              columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '总人数'")
    Integer submittedTS;

    @JsonProperty("reviewed_ts")
    @Column(name = "reviewed_ts",              columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '总人数'")
    Integer reviewedTS;

    @JsonProperty("confirmed_ts")
    @Column(name = "confirmed_ts",              columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '总人数'")
    Integer confirmedTS;

    @JsonProperty("not_start_apa")
    @Column(name = "not_start_apa",             columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '总人数'")
    Integer notStartAPA;

    @JsonProperty("submitted_apa")
    @Column(name = "submitted_apa",             columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '总人数'")
    Integer submittedAPA;

    @JsonProperty("reviewed_apa")
    @Column(name = "reviewed_apa",              columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '总人数'")
    Integer reviewedAPA;

    @JsonProperty("confirmed_apa")
    @Column(name = "confirmed_apa",             columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '总人数'")
    Integer confirmedAPA;
    ///
    /// 汇总数据
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @JsonProperty("pre_a_total")
    @Column(name = "pre_a_total",               columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer preATotal;

    @JsonProperty("pre_a_rating")
    @Column(name = "pre_a_rating",              columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float preARating;

    @JsonProperty("pre_b_total")
    @Column(name = "pre_b_total",               columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer preBTotal;

    @JsonProperty("pre_b_rating")
    @Column(name = "pre_b_rating",              columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float preBRating;

    @JsonProperty("pre_c_total")
    @Column(name = "pre_c_total",               columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer preCTotal;

    @JsonProperty("pre_c_rating")
    @Column(name = "pre_c_rating",              columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float preCRating;

    @JsonProperty("pre_d_total")
    @Column(name = "pre_d_total",               columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer preDTotal;

    @JsonProperty("pre_d_rating")
    @Column(name = "pre_d_rating",              columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float preDRating;

    @JsonProperty("pre_e_total")
    @Column(name = "pre_e_total",               columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer preETotal;

    @JsonProperty("pre_e_rating")
    @Column(name = "pre_e_rating",              columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float preERating;

    @JsonProperty("final_a_total")
    @Column(name = "final_a_total",             columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer finalATotal;

    @JsonProperty("final_a_rating")
    @Column(name = "final_a_rating",            columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float finalARating;

    @JsonProperty("final_b_total")
    @Column(name = "final_b_total",           columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer finalBTotal;

    @JsonProperty("final_b_rating")
    @Column(name = "final_b_rating",          columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float finalBRating;

    @JsonProperty("final_c_total")
    @Column(name = "final_c_total",         columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer finalCTotal;

    @JsonProperty("final_c_rating")
    @Column(name = "final_c_rating",        columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float finalCRating;

    @JsonProperty("final_d_total")
    @Column(name = "final_d_total",           columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer finalDTotal;

    @JsonProperty("final_d_rating")
    @Column(name = "final_d_rating",          columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float finalDRating;

    @JsonProperty("final_e_total")
    @Column(name = "final_e_total",           columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '统计数据'")
    Integer finalETotal;

    @JsonProperty("final_e_rating")
    @Column(name = "final_e_rating",          columnDefinition = "FLOAT(5,2) UNSIGNED NOT NULL COMMENT '统计数据'")
    Float finalERating;
}
