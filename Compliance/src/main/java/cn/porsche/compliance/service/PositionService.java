package cn.porsche.compliance.service;


import cn.porsche.compliance.domain.Position;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PositionService {
    Integer count();
    Page<Position> getListByPage(Integer page, Integer size);

    Position update(Position job);

    Position findById(Integer id);

    Position findByParentId(Integer parentId);

    List<Position> subordinates(Position position);
}
