package cn.porsche.web.service;


import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.User;

import java.util.List;

public interface GroupService {
    Group update(Group group);
    Group findById(Integer groupId);
    Group findByName(String name);

    void delete(Group group, User user);

    Group getDefaultGroup();

    // 返回所有的组信息
    List<Group> findAll();

    // 返回用户所拥有的组
    List<Group> findByUser(User user);
    List<User> findByGroup(Group group);


    void addToEmployeeGroup(List<User> users, User by);
    void addToEmployeeGroup(User user, User by);
    void removeFromEmployeeGroup(User user, User by);
    void removeFromEmployeeGroup(List<User> users, User by);

    // 移动到HRTeam
    void moveToHRPerformanceGroup(List<User> users, User by);
    void moveToHRPerformanceGroup(User user, User by);
    void removeHRPerformanceGroup(User user, User by);
    void removeHRPerformanceGroup(List<User> users, User by);

    // 移动到长假组
    void moveToLongLevelGroup(List<User> users, User by);
    void moveToLongLevelGroup(User user, User by);
    void removeFromLongLevelGroup(User user, User by);
    void removeFromLongLevelGroup(List<User> users, User by);

    // 移动到VP组
    void moveToTopManagerGroup(List<User> users, User by);
    void moveToTopManagerGroup(User user, User by);
    void removeFromTopManagerGroup(User user, User by);
    void removeFromTopManagerGroup(List<User> users, User by);

    // 移动到经理组
    void moveToManagerGroup(List<User> users, User by);
    void moveToManagerGroup(User user, User by);
    void removeFromManagerGroup(User user, User by);
    void removeFromManagerGroup(List<User> users, User by);

    // 移动新员工组
    void moveToInProbationGroup(List<User> users, User by);
    void moveToInProbationGroup(User user, User by);
    void removeFromProbationGroup(User user, User by);
    void removeFromProbationGroup(List<User> users, User by);

    // 移动委托人组
    void moveToDelegatorGroup(List<User> users, User by);
    void moveToDelegatorGroup(User user, User by);
    void removeFromDelegatorGroup(User user, User by);
    void removeFromDelegatorGroup(List<User> users, User by);

    boolean inHRTeamGroup(User user);
    boolean inEmployeeGroup(User user);
    boolean inManagerGroup(User user);
    boolean inLongLevelGroup(User user);
    boolean inTopManagerGroup(User user);
    boolean inIDelegatorGroup(User user);
    boolean inProbationGroup(User user);
}
