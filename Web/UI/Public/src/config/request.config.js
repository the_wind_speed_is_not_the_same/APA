export const apis = {
    /* login */
    // Login: '/login/by_mobile.do',                 // 运营人员使用手机号码和登录用户密码登录
    // Logout: 'Account/Logout',                     // 退出登录
    getUserInfo: '/user/me',                      // 获取用户信息
    getMenu:'/user/menu',                         // 导航数据

    /* comment */
    // getOaCommentTopic: '/message/get_oa_comment_topic',
    addOAComment: '/message/add_oa_comment',
    addTSComment: '/message/add_ts_comment',      // 经理发送Comment给员工
    rejectTSComment: '/target/reject_what',       // 经理拒绝员工的绩效数据
    getOaComments: '/message/get_oa_comments',
    getTsComments: '/message/get_ts_comments',
    addApaComment: '/message/add_apa_comment',

    /* function release */
    getFRStatus: '/function/status',
    // tsEmail: '/function/ts_email',
    tsRelease: '/function/ts_release',
    available: '/function/available',
    // apaEmail: '/function/apa_email',
    apaRelease: '/function/apa_release',

    /* target */
    currentYearDetail: '/target/by_year',           // 绩效详情
    targetDetail: '/target/detail',                 // 绩效详情
    updateHow: '/target/update_how',
    updateWhat: '/target/update_what',
    // getHow: '/target/get_how',                      // 接口用 /target/detail 代替
    submitTarget: '/target/submit',                 // 员工设置好绩效之后，提交给经理审核
    confirmByEmployee: '/target/confirm_what',      // 员工确认每个What的绩效
    confirmByDelegator: '/target/confirm_what',     // 委托人或经理确认每个What的绩效
    confirmHowByDelegator: '/target/confirm_how',   // 委托人或经理确认How绩效
    confirmHowByEmployee: '/target/confirm_how',    // 员工确认How的绩效
    checkData: '/target/check',
    deleteTarget: '/target/delete_what',
    confirmHow:'/target/confirm_how',
    getTargetDetail: '/target/detail',
    setTsEmployeeConfirm:'/target/employee_confirm',
    setTsDelegatorConfirm:'/target/delegator_confirm',

    /* apa */
    eScoreWhat: '/apa/e_score_what',          // 对What打分
    eScoreHow: '/apa/e_score_how',            // 对How打分
    apaSubmit: '/apa/submit',                 // 用户自评分数之后，点击提交
    agree: '/apa/agree',
    // batch_by_level: '/apa/batch_by_level',
    firstWhatScore:'/apa/do_score_what',      // 经理第一次对What打分 http://apa.frp.porsche.cn:81/swagger-ui/#/apa-controller/doScoreWhatUsingGET
    firstHowScore:'/apa/do_score_how',        // 经理第二次对What打分 http://apa.frp.porsche.cn:81/swagger-ui/#/apa-controller/dtScoreWhatUsingGET
    secondWhatScore:'/apa/dt_score_what',     // 经理第一次对How打分  http://apa.frp.porsche.cn:81/swagger-ui/#/apa-controller/doScoreHowUsingPOST
    secondHowScore:'/apa/dt_score_how',       // 经理第二次对How打分  http://apa.frp.porsche.cn:81/swagger-ui/#/apa-controller/dtScoreHowUsingPOST

    /* my-team */
    teamList: '/target/list',
    targetsList: '/target/list',
    release_first_score:'/apa/release_first_score',
    release_final_score:'/apa/release_final_score',

    /*authorization*/
    roleList: '/role/list',
    roleUpdate: '/role/update',


    /*delegation*/
    employee:'/employee/list_by_delegator',
    delegatorSet:'/delegator/delegator_group',
    delegatorUnset:'/delegator/delegator_dismiss',
    /*home*/

   	/*reporting*/
    getReportingStatistic: '/reporting/ts_status',
  	getReportingResult:'/reporting/apa_status',
  	getEmployeeByDepartment: '/reporting/ts_by_department',
    getTargetByDepartment: '/reporting/apa_by_department',
    downloadTSReporting:'/reporting/ts_status_down',
    downloadAPAReporting:'/reporting/apa_status_down',
    downloadTSByDReporting:'/reporting/ts_by_department_down',
    downloadAPAByDReporting:'/reporting/apa_by_department_down',

    /*user-informaion*/
    getEmployeeList:'/employee/list',
    setEmployeeHireDate:'/employee/hire_date',

    /*super-admin*/
    moveEmployee:'/employee/move',

    /*message*/
    getMessageList:'/message/unread',
    readMessage:'/message/read',
    readComment:'/message/read_comment',

    /* 20210105 department */
    getDepartments: '/employee/departments',
    switchRole: '/employee/switch_role',
    importHireDate: '/employee/import_hire_dates',
    importManager: '/employee/import_manager',

    getCompanies: '/reporting/companies',
    getDepartmentByCompany: '/employee/company',
};



//url
let _hostUrl;

switch(process.env.NODE_ENV){
	case "development":
	  // 开发环境
        _hostUrl = '/v1.0';

    // // 服务器环境
		// _hostUrl = 'http://test.web.porsche.cn:81/v1.0';
		break;
	case "production":
		_hostUrl = '/v1.0';
		break;
	default:
        _hostUrl = '/v1.0';
}

export const hostUrl = _hostUrl;
