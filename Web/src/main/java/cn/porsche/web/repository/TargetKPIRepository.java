package cn.porsche.web.repository;

import cn.porsche.web.domain.TargetKPI;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TargetKPIRepository extends CrudRepository<TargetKPI, Integer> {
    void deleteAllByTargetId(Integer targetId);

    List<TargetKPI> findAllByTargetIdAndWhatIdOrderByIndexDesc(Integer targetId, Integer whatId);
}
