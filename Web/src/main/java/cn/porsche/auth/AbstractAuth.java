package cn.porsche.auth;

import cn.porsche.common.util.http.HttpException;
import cn.porsche.common.util.http.HttpUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.springframework.http.HttpMethod;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractAuth implements IAuth {
    private List<Header> headers;

    private App app;
    private String baseCallback;

    public abstract String getPound();
    public abstract String getState();
    public abstract String getResponseType();
    public abstract String getAccessTokenUri();
    public abstract String getRefreshTokenUri();

    protected abstract List<NameValuePair> getAccessTokenParams(String token, App app);
    protected abstract List<NameValuePair> getRefreshTokenParams(String refreshToken, App app);

    protected abstract AccessToken resetAccessToken(String response);
    protected abstract AccessToken resetRefreshToken(String response);

    public AccessToken getAccessToken(String code) {
        try {
            return resetAccessToken(getAccessTokenFromServer(code));
        } catch (HttpException e) {
            e.printStackTrace();
        }

        return null;
    }

    public AccessToken refreshAccessToken(String code) {
        try {
            return resetRefreshToken(getRefreshTokenFromServer(code));
        } catch (HttpException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void setHeader(String key, String value) {
        if (null == headers) {
            headers = new ArrayList<>();
        }
    }

    protected void setCookie(String header, String value) {
    }

    protected String http(String uri, HttpMethod method, List<NameValuePair> params) throws HttpException {
        if (null == params) {
            params = new ArrayList<>();
        }

        switch (method) {
            case HEAD: return HttpUtils.post(uri, params, headers);
            case POST: return HttpUtils.post(uri, params, headers);
            case GET: return HttpUtils.get(uri, params, headers);
            case PATCH: return HttpUtils.post(uri, params, headers);
            case TRACE: return HttpUtils.post(uri, params, headers);
            case DELETE: return HttpUtils.post(uri, params, headers);
            case OPTIONS: return HttpUtils.post(uri, params, headers);
            default: return HttpUtils.get(uri, params, headers);
        }
    }

    protected String getAccessTokenFromServer(String token) throws HttpException {
        List<NameValuePair> params = getAccessTokenParams(token, getApp());

        return http(getAccessTokenUri(), HttpMethod.POST, params);
    }


    protected String getRefreshTokenFromServer(String refreshToken) throws HttpException {
        List<NameValuePair> params = getRefreshTokenParams(refreshToken, getApp());

        return http(getRefreshTokenUri(), HttpMethod.POST, params);
    }

    public AccessToken auto(AccessToken token) throws HttpException {
        if (token.isAccessTokenValidity()) {
            return token;
        }

        if (token.isRefreshTokenValidity()){
            // 刷新Token
            return this.resetRefreshToken(this.getRefreshTokenFromServer(token.getRefreshToken()));
        }

        // Token 已过期
        return null;
    }

    @Override
    public String redirect() {
        return null;
    }

    public String getCallbackUri() {
        StringBuffer sb = new StringBuffer(getBaseCallback());
        if (sb.length() == 0) {
            return null;
        }

        if (sb.indexOf("?") == -1) {
            sb.append('?');
        } else {
            sb.append('&');
        }

        try {
            return sb.append("uri=").append(URLEncoder.encode(getCallbackUri(), HttpUtils.DEFAULT_CHARSET.toString())).toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "";
    }

    public String getBaseCallback() {
        return baseCallback;
    }

    public void setBaseCallback(String baseCallback) {
        this.baseCallback = baseCallback;
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }
}
