package cn.porsche.compliance.service;


import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.Training;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.emnu.TrainingTypeEnum;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TrainingService {
    void delete(Training training);

    @Transactional
    void deleteByType(Integer year, TrainingTypeEnum type);

    @Transactional
    void deleteByType(Integer year, String subject, TrainingTypeEnum type);

    // 删除某个人的培训结果记录
    @Transactional
    void deleteStaffByType(Integer year, String subject, TrainingTypeEnum trainingType, EmployeeTypeEnum employeeType);

    void resetTrainingByQuestion(Integer year, User user);

    Training update(Training training);
    Iterable update(List<Training> training);

    Training findByUserAndTypeAndSubject(User user, TrainingTypeEnum type, String subject);
    Training findByYearAndUserAndTypeAndSubject(Integer year, User user, TrainingTypeEnum type, String subject);
    Integer totalByUserAndType(Integer year, User user, TrainingTypeEnum type);
    Integer attendedTotalByUserAndType(Integer year, User user, TrainingTypeEnum type);


    List<Training> findAllByUserAndYear(Integer year, User user);
    List<Training> findAllByUserAndType(User user, TrainingTypeEnum type);
    List<Training> findAllByYearAndUserAndType(Integer year, User user, TrainingTypeEnum type);
    List<Training> findAllByDepartment(Integer year, Department department);
}
