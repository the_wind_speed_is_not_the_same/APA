package cn.porsche.web.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.service.LoginService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class ReportingControllerTest extends ApplicationTest {
    private static final String API = "/reporting";

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void statusTest() {
        final String api = API + "/status";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void employeeByDepartment() {
        final String api = API + "/department";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("department_id", "19")
//                            .param("department_id", String.valueOf(getDepartmentId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void statistic() {
        final String api = API + "/status";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testResult() {
        final String api = API + "/result";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andDo(
                    print()
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}