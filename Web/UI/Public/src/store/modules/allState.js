import {Module, ActionTree, MutationTree} from 'vuex';
import {request} from '../../src/common/request.js';
import {setLocalStorage} from '../../src/common/utils.js';

const state = {
    targetInfo: {},
    delegatorInfo: JSON.parse(localStorage.getItem('delegatorInfo') || '{}'),
    yearList: ['2020', '2019', '2018', '2017', '2016'],
    allList: [],
    user: null,
    delegator: null,
    group: null,
    notice: null,
    position: null,
    department: null,
    userInfo: null,
    reportingDepartmentId: null,
    reportingDepartment: null,
    currentYear: null,
    currentTeamYear: null,
    menuList: [],
    isTs: false,
    isApa: false
};

// actions
const actions = {
    setTargetInfo(state, info) {
        state.commit('setTargetInfo', info);
    },
    setDelegatorInfo(state, info) {
        localStorage.setItem('delegatorInfo', JSON.stringify(info))
        state.commit('setDelegatorInfo', info);
    },
    setCurrentYear(state, year) {
        state.commit('setCurrentYear', year)
    },
    setYearList(state, info) {
        return new Promise((resolve, reject) => {
            request({
                url: 'available',
                method: 'get',
                params: {}
            }).then(res => {
                if (res && res.list && res.list.length) {
                    let list = res.list;
                    state.commit('setAllList', list.sort((a, b) => b.year - a.year));
                    state.commit('setYearList', list.map(item => item.year));
                    resolve(list);
                } else {
                    reject();
                }
            }).catch(err => {
                // reject();
            })
        })
    },
    getUserInfo(state, info) {
        return new Promise((resolve, reject) => {
            request({
                url: 'getUserInfo',
                method: 'get',
                isShowLoading: true
            }).then(res => {
                console.log(res)
                if (!res || res.code !== 0) {
                    return reject();
                }

                let data = res.data;

                if (!res.data['roles']) {
                    res.data['roles'] = [];
                }

                if (!res.data['roles']) {
                    res.data['roles'] = [];
                }

                if (!res.data['kpi']) {
                    res.data['kpi'] = {};
                }

                res.data['user']['kpi'] = res.data['kpi'];
                res.data['user']['roles'] = res.data['roles'];

                setLocalStorage('manager', JSON.stringify(data.manager));
                setLocalStorage('group', JSON.stringify(data.group));
                setLocalStorage('notice', JSON.stringify(data.notice));
                setLocalStorage('kpi', JSON.stringify(data.kpi));
                setLocalStorage('user', JSON.stringify(data.user));
                setLocalStorage('position', JSON.stringify(data.position));
                setLocalStorage('department', JSON.stringify(data.department));

                state.commit('setUserInfo', data);
                resolve();
            })
        }).catch(err => {
            // reject();
        })
    },
    setReportingDepartmentId(state, id) {
        state.commit('setReportingDepartmentId', id)
    },
    setReportingDepartment(state, name) {
        state.commit('setReportingDepartment', name)
    },
    setReleaseStatus(state, year) {
        request({
            url: 'getFRStatus',
            method: 'get',
            params: {year}
        }).then(res => {
            if (res && res.data) {
                state.commit('setReleaseStatus', {ts: res.data.is_ts, apa: res.data.is_apa})
            }
        })
    }
};

// mutations
const mutations = {
    setUserInfo(state, info) {
        state.delegator = info.delegator;
        state.group = info.group;
        state.notice = info.notice;
        state.user = info.user;
        state.position = info.position;
        state.department = info.department;
        state.userInfo = info;
    },
    setTargetInfo(state, info) {
        state.targetInfo = info;
    },
    setDelegatorInfo(state, info) {
        state.delegatorInfo = info
    },
    setYearList(state, info) {
        state.yearList = info;
    },
    setCurrentYear(state, year) {
        state.currentTeamYear = year
        state.currentYear = state.allList.filter(item => item.year == year)[0]
    },
    setAllList(state, info) {
        let checkItem = info.filter(item => {
            return item.is_ts || item.is_apa;
        })
        if (checkItem.length) {
            state.currentYear = checkItem[0];
        }
        state.allList = info;
    },
    setReportingDepartmentId(state, id) {
        state.reportingDepartmentId = id
    },
    setReportingDepartment(state, name) {
        state.reportingDepartment = name
    },
    setMenuList(state, data) {
        state.menuList = data
    },
    setReleaseStatus(state, {ts, apa}) {
        console.log(ts, apa)
        state.isTs = ts
        state.isApa = apa
    }
};

export const allState = {
    state,
    actions,
    mutations,
};
