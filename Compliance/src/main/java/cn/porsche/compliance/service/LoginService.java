package cn.porsche.compliance.service;


import cn.porsche.common.auth.AccessToken;
import cn.porsche.compliance.domain.Login;
import cn.porsche.compliance.domain.User;

public interface LoginService {
    Integer EXPIRE_DATES        = 3;
    String AUTH_NAME            = "auth_id";
    char AUTH_TOKEN             = '|';

    User getUserFromHeader();
    User getUserFromCookie();
    Login loginByPassword(User user);
    Login loginBySSO(User user, AccessToken token);

    String encryptPassword(String salt, String password);

    Login update(Login login);

    Login logout(User user);
}
