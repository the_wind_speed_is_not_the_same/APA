import {request} from '../../../../common/request.js';
import {deepClone, formatDate, formatDateTime} from '../../../../common/utils.js';
import ListSelect from '../../../../components/ListSelect/page.vue';
import Mantle from '../../../../components/Mask/page.vue';
import Confirm from '../../../../components/Confirm/page.vue';

export default {
    props: ['functionId', 'targetInfo', 'isBtnDisabled', 'tableData', 'currentIndex'],
    data() {
        return {
            form: {
                finish_date: '',
                desc: '',
                function_id: '',
                kpis: [{
                    desc: '',
                    index: 4,
                }, {
                    desc: '',
                    index: 3,
                }, {
                    desc: '',
                    index: 2,
                }, {
                    desc: '',
                    index: 1,
                }, {
                    desc: '',
                    index: 0,
                }],
                name: '',
                what_id: '',
                weight: '',
            },
            isEdit: false,
            isShowKpiBox: false,
            comment: '',
            isShowButtonLeft: false,
            isShowConfirm: false,
            confirmTxt: '',
            titleTxt: 'Warning',
            showTime: 0,
            confirmType: '',
            isShowList: false,
            list: ['1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0'],
            selectIndex: -1,
            score: null,
            initScore: this.tableData[this.currentIndex]['employee_score'],
            comments: [],
            user: {}
        }
    },
    watch: {
        targetInfo: {
            handler(n, o) {
                this.initData(n);
            },
            deep: true
        },
        currentIndex: {
            handler(n, o) {
                this.selectIndex = -1;
                this.score = null;
                // this.score = this.tableData[this.currentIndex].employee_score;
                this.initScore = this.tableData[this.currentIndex].employee_score;
                this.comment = '';
            }
        }
    },
    components: {
        Mantle,
        Confirm,
        ListSelect
    },
    computed: {},
    methods: {
        showScoreList() {
            this.isShowList = !this.isShowList;
        },
        // 确认用户所选分数
        selectScore(index) {
            if (index == this.selectIndex
                || this.list[index] == this.initScore
            ) {
                this.score = null;
                return this.isShowList = false;
            }

            this.isShowList  = false;

            this.selectIndex = index;
            this.score = this.list[index];
        },
        confirm() {
            if (this.isClickType == 'prev') {
                this.$emit('previousIndex');
            } else if (this.isClickType == 'next') {
                this.$emit('nextIndex');
            }
            this.showTime = 0;
            this.isShowConfirm = false;
        },
        closeComponent() {
            this.$emit('closeComponent');
        },
        spreadSelect() {
            this.isShowKpiBox = true;
        },
        closeBox() {
            this.isShowKpiBox = false;
        },
        sendComment() {
            if (!this.comment) return false;

            request({
                url: 'addApaComment',
                data: {
                    message: this.comment,
                    topic_id: this.targetInfo.what_id,
                    function_id: this.functionId
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.isShowConfirm = true;
                    this.isShowButtonLeft = false
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message ? res.message : 'Send Failed, Please try again';

                    return false;
                }

                this.isShowConfirm = true;
                this.isShowButtonLeft = false
                this.showTime = 0
                this.titleTxt = 'Success';
                this.confirmTxt = 'Send Successfully';
                let message = {
                    send_time: formatDateTime(new Date()),
                    message: this.comment,
                    delegator_name: this.user.en_name
                }

                this.targetInfo['apa_comments'].push(message);
                this.comment = '';
            })
        },
        clearComment() {
            this.comment = '';
        },
        reloadDetail() {
            this.$emit('refreshTargetDetail');
        },
        save() {
            if (!this.score || !this.comment) {
                this.isShowConfirm = true;
                this.titleTxt = 'Warning';
                this.confirmTxt = 'You need set a score and comment this.';

                return;
            }

            request({
                url: 'eScoreWhat',
                data: {
                    what_id: this.targetInfo.what_id,
                    employee_score: this.score,
                    employee_comment: this.comment
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;

                    return false;
                }


                this.titleTxt = 'Success';
                this.confirmTxt = 'Save Successfully';
                this.isShowButtonLeft = false;
                this.isShowConfirm = true;
                //
                // let comment = {};
                // comment.message = this.comment;
                // comment.send_time = formatDateTime(new Date());
                // comment.employee_name = this.user.en_name;

                // this.comments.push(comment);

                // this.$emit('setListScore', {currentIndex:this.currentIndex, score:this.score, comment});

                this.comment = '';
                this.selectIndex = -1;

                this.reloadDetail();
            });
        },
        cancel() {
            this.isShowConfirm = false
        },
        callBack() {

        },
        previousIndex() {
            if (this.score != this.initScore || this.comment) {
                this.titleTxt = 'Warning';
                this.confirmTxt = 'There are some changes not saved，are you sure to leave？?'
                this.isShowConfirm = true;
                this.showTime = 0
                this.isClickType = 'prev';
                this.isShowButtonLeft = true;
                return false
            }

            this.$emit('previousIndex');
        },
        nextIndex() {
            if (this.score != this.initScore || this.comment) {
                this.titleTxt = 'Warning';
                this.confirmTxt = 'There are some changes not saved，are you sure to leave？?'
                this.isShowConfirm = true
                this.showTime = 0
                this.isClickType = 'next';
                this.isShowButtonLeft = true;
                return false
            }

            this.$emit('nextIndex');
        },
        initData(targetInfo) {
            this.isTargetNameRed = false;
            this.isDescriptionRed = false;
            this.isKPIRed = false;
            this.isWeightRed = false;
            this.isDateRed = false;
            this.score = this.targetInfo.employee_score;
            let form = deepClone(targetInfo);
            if (!form.kpis.length) {
                form.kpis = [{
                    desc: '',
                    index: 4,
                }, {
                    desc: '',
                    index: 3,
                }, {
                    desc: '',
                    index: 2,
                }, {
                    desc: '',
                    index: 1,
                }, {
                    desc: '',
                    index: 0,
                }];
            }

            this.comments = form.apa_comments || [];
            this.form = form;
        }
    },
    created() {
        let user = localStorage.getItem('user')
        if (user) {
            this.user = JSON.parse(user)
        }
    },
    mounted() {
        this.initData(this.targetInfo);
    }
}
