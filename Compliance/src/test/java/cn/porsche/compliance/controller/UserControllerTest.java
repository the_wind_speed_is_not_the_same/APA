package cn.porsche.compliance.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.ApplicationTest;
import cn.porsche.compliance.service.LoginService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class UserControllerTest extends ApplicationTest {
    private static final String API = "/user";

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    @Test
    void vps() {
        final String api = API + "/vps";

        Cookie cookie = new Cookie("auth_id", getHRAuthorization());
        String contents = "PCN467\n" +
                "PCN502\n" +
                "PCN478\n" +
                "PCN382\n" +
                "PCN421\n" +
                "PCN499\n" +
                "PCN618\n" +
                "PCN696\n" +
                "PCN918\n" +
                "PCN850\n" +
                "PCN922\n" +
                "PCN932\n" +
                "PDC001\n" +
                "PCN069";


        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .cookie(cookie)
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("content", contents)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}