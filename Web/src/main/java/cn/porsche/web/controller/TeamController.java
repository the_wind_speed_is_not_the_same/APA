package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.domain.*;
import cn.porsche.web.service.DepartmentService;
import cn.porsche.web.service.FunctionService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/team")
@Api(value = "/team", description = "委托人相关员工的操作类")
public class TeamController extends BaseController {

    @Autowired
    FunctionService functionService;

    @Autowired
    DepartmentService departmentService;

//
//    @ApiOperation(
//            value = "【经理】获取所有【员工】列表接口",
//            notes = "GET参数<br/>" +
//                    "function_id（阶段主键）<br />" +
//                    "order（如何排序）：有：do（预打分），dt（最后打分）和name（默认名称排序）<br />" +
//                    "返回参数：见：KPIParam"
//    )
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "正常返回", response = KPIParam.class, responseContainer = "List"),
//    })
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
//            @ApiImplicitParam(name = "order",       required = true, dataTypeClass = String.class,  value = "排序：员工姓名(name)、第一次打分(do)、第二次打分(dt)排序")
//    })
//    @GetMapping(value = "/employees")
//    public String employees(
//            @RequestParam(value = "function_id") Integer functionId,
//            @RequestParam(value = "order", defaultValue = "name") String order
//    ) {
//        if (noPromise(ModuleNameEnum.TargetsReview, OperatorText.CHECK, GroupNameEnum.DisciplinaryManager)) {
//            // 未有权限
//            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
//        }
//
//        Function function = functionService.findById(functionId);
//        if (null == function) {
//            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
//        }
//
//        List<KPIUser> users;
//       if (order.toLowerCase().equals("do")) {
//            users = kpiUserService.listOrderByFirstLevelASC(function, user);
//        } else if (order.toLowerCase().equals("dt")) {
//            users = kpiUserService.listOrderByFirstLevelASC(function, user);
//        } else {
//            users = kpiUserService.listOrderByNameASC(function, user);
//        }
//
//        List<KPIParam> kpis = new ArrayList<>();
//       for (KPIUser user :users) {
//           Target target = targetService.get(user.getTargetId());
//           kpis.add(targetService.findTargetDetail(target));
//       }
//
//        return ApiResult.builder().list(kpis).toJson();
//    }




    @ApiOperation(
            value = "【经理】在【APA】 界面下获取所有的【部门列表】的接口",
            notes = "返回数据参数：<br />" +
                    "参考：Schemas中的：KPIUser"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = DepartmentTree.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键")
    })
    @GetMapping(value = "/department_by_manager")
    public String departmentByManager() {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Department topDepartment = departmentService.findByCode("1");
        if (null == topDepartment) {
            return ApiResult.isBad("Unable to find the Department data, please contact your HR to fix it");
        }

        DepartmentTree tree = departmentService.buildTree(topDepartment);
        if (null == tree) {
            return ApiResult.isBad("Unable to find the subDepartment data, please contact your HR to fix it");
        }


        return ApiResult.builder().list(tree.getSubTree()).toJson();
    }
}
