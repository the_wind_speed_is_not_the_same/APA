package cn.porsche.compliance.service;


import cn.porsche.compliance.domain.File;

import java.util.List;

public interface FileService {
    void delete(File file);

    File update(File file);
    File findByMd5(String md5);
    File findByName(String name);

    List<File> findAll();
}
