package cn.porsche.compliance.email;

import cn.porsche.compliance.domain.Email;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

public abstract class AbstractEmail implements IEmail {
    @Value("${spring.profiles.active}")
    protected String active;

    public abstract Properties getProperties();

    public void send(Email email) throws MessagingException {
        Assert.notNull(email, "email must not be null");


        Properties props = getProperties();

        Session mailSession = Session.getDefaultInstance(props, null);
        Transport transport = mailSession.getTransport();

        MimeMessage message = new MimeMessage(mailSession);
        message.setSubject(email.getSubject());
        message.setText(email.getContent());
        message.setSentDate(new Date());
        message.setFrom(String.format("HR Team<%s>", props.getProperty("mail.username")));

        String[] list;
        if (!StringUtils.isEmpty(email.getTo())) {
            list = email.getTo().split(";");
            for (int i = 0, len = list.length; i < len; i++) {
                if (i > 5) {
                    continue;
                }

                message.addRecipient(Message.RecipientType.TO, new InternetAddress(active.equals("prod") ? list[i] : props.getProperty("mail.test")));
            }
        }


        transport.connect(props.getProperty("mail.smtp.host"), (Integer) props.get("mail.smtp.port"), props.getProperty("mail.username"), props.getProperty("mail.password"));
        transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
        transport.close();
    }


    @Override
    public void send(String subject, String content, String to) throws MessagingException {
        Assert.notNull(subject, "subject must not be null");
        Assert.notNull(subject, "content must not be null");
        Assert.notNull(subject, "to must not be null");

        Email mail = Email.builder()
                .subject(subject)
                .content(content)
                .to(to)
                .build();

        send(mail);
    }


    @Override
    public void sendHtml(Email email) throws MessagingException {
        Assert.notNull(email, "email must not be null");


        Properties props = getProperties();

        Session mailSession = Session.getDefaultInstance(props, null);
        Transport transport = mailSession.getTransport();

        MimeMessage message = new MimeMessage(mailSession);
        message.setSubject(email.getSubject());
        message.setText(email.getContent().replaceAll("\n", "<br />"), "UTF-8", "html");
        message.setSentDate(new Date());
        message.setFrom(String.format("HR Team<%s>", props.getProperty("mail.username")));

        String[] list;
        if (!StringUtils.isEmpty(email.getTo())) {
            list = email.getTo().split(";");
            for (int i = 0, len = list.length; i < len; i++) {
                if (i > 5) {
                    continue;
                }

                message.addRecipient(Message.RecipientType.TO, new InternetAddress(active.equals("prod") ? list[i] : props.getProperty("mail.test")));
            }
        }

        transport.connect(props.getProperty("mail.smtp.host"), (Integer) props.get("mail.smtp.port"), props.getProperty("mail.username"), props.getProperty("mail.password"));
        transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
        transport.close();
    }

    @Override
    public void sendHtml(String subject, String content, String to) throws MessagingException {
        Assert.notNull(subject, "subject must not be null");
        Assert.notNull(subject, "content must not be null");
        Assert.notNull(subject, "to must not be null");

        Email mail = Email.builder()
                .subject(subject)
                .content(content)
                .to(to)
                .build();

        sendHtml(mail);
    }

    public String getActive() {
        return active;
    }
}
