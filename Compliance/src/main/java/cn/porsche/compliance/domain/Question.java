package cn.porsche.compliance.domain;

import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.emnu.SeasonEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "questions", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Question implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "题目类别")
    @JsonProperty("type")
    @Column(name = "type",                      columnDefinition = "CHAR(10) NOT NULL COMMENT '采用何种试题'")
    EmployeeTypeEnum type;

    @ApiModelProperty(notes = "题目年份")
    @JsonProperty("year")
    @Column(name = "year",                      columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '部门主键'")
    Integer year;

    @Enumerated(EnumType.STRING)
    @JsonProperty("season")
    @Column(name = "season",                    columnDefinition = "CHAR(2) NOT NULL COMMENT '一、二、三、四复季度'")
    SeasonEnum season;

    @ApiModelProperty(notes = "英文问题")
    @JsonProperty("en_question")
    @Column(name = "en_question",               columnDefinition = "TEXT NULL COMMENT '英文版本问题'")
    String enQuestion;

    @ApiModelProperty(notes = "英文版本问题")
    @JsonProperty("cn_question")
    @Column(name = "cn_question",               columnDefinition = "TEXT NULL COMMENT '英文版本问题'")
    String cnQuestion;

    @ApiModelProperty(notes = "中文答案 A")
    @JsonProperty("cn_option_a")
    @Column(name = "cn_option_a",               columnDefinition = "TEXT NULL COMMENT '中文版本问题'")
    String cnOptionA;

    @ApiModelProperty(notes = "中文答案 B")
    @JsonProperty("cn_option_b")
    @Column(name = "cn_option_b",               columnDefinition = "TEXT NULL COMMENT '中文版本问题'")
    String cnOptionB;

    @ApiModelProperty(notes = "中文答案 C")
    @JsonProperty("cn_option_c")
    @Column(name = "cn_option_c",               columnDefinition = "TEXT NULL COMMENT '中文版本问题'")
    String cnOptionC;

    @ApiModelProperty(notes = "中文答案 D")
    @JsonProperty("cn_option_d")
    @Column(name = "cn_option_d",               columnDefinition = "TEXT NULL COMMENT '中文版本问题'")
    String cnOptionD;

    @ApiModelProperty(notes = "英文答案 A")
    @JsonProperty("en_option_a")
    @Column(name = "en_option_a",               columnDefinition = "TEXT NULL COMMENT '英文版本问题'")
    String enOptionA;

    @ApiModelProperty(notes = "英文答案 B")
    @JsonProperty("en_option_b")
    @Column(name = "en_option_b",               columnDefinition = "TEXT NULL COMMENT '英文版本问题'")
    String enOptionB;

    @ApiModelProperty(notes = "英文答案 C")
    @JsonProperty("en_option_c")
    @Column(name = "en_option_c",               columnDefinition = "TEXT NULL COMMENT '英文版本问题'")
    String enOptionC;

    @ApiModelProperty(notes = "英文答案 D")
    @JsonProperty("en_option_d")
    @Column(name = "en_option_d",               columnDefinition = "TEXT NULL COMMENT '英文版本问题'")
    String enOptionD;

    @ApiModelProperty(notes = "帮助后排队的答案")
    @JsonProperty("help_answer")
    @Column(name = "help_answer",               columnDefinition = "VARCHAR(20) NULL COMMENT '帮助后的排队答案'")
    String helpAnswer;

    @ApiModelProperty(notes = "正确答案")
    @JsonProperty("answer")
    @Column(name = "`answer`",                  columnDefinition = "VARCHAR(20) NULL COMMENT '答案'")
    String answer;

    @ApiModelProperty(notes = "本题的分数")
    @JsonProperty("score")
    @Column(name = "score",                  columnDefinition = "TINYINT(2) UNSIGNED NOT NULL DEFAULT '0' COMMENT '分数'")
    Integer score;

    @ApiModelProperty(notes = "英文提示")
    @JsonProperty("en_tip")
    @Column(name = "en_tip",                    columnDefinition = "TEXT NULL COMMENT '英文提示'")
    String enTip;

    @ApiModelProperty(notes = "中文提示")
    @JsonProperty("cn_tip")
    @Column(name = "cn_tip",                    columnDefinition = "TEXT NULL COMMENT '中文提示'")
    String cnTip;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
