import Confirm from '../../../../components/Confirm/page.vue';
import {request} from '../../../../common/request.js';
import {downloadFile} from '../../../../common/utils.js';
import DashBoard from '../DashBoard/page.vue';
import {mapActions} from 'vuex'

export default {
    props: {
        query: {
            type: Object
        },
        function_id: {
            type: Number | String,
            required: true
        },
        department_code: {
            type: String,
            required: true
        }
    },
    data() {
        return {
            tableData: [],
            tableHeight: 412,
            rowKeys: [],
            currentRow: null,
            currentChild: [],
            currentRowIndex: null,
            currentData: {},
            confirmTxt: '',
            titleTxt: 'Error',
            isShowConfirm: false
        }
    },
    components: {
        Confirm,
        DashBoard
    },
    watch: {
        'function_id': function (val) {
            localStorage.removeItem('reportingStatus')
            localStorage.removeItem('currentData')
            this.currentData = {}
            this.currentRow = null
            this.getData();
        },
        'department_code': function (val) {
            localStorage.removeItem('reportingStatus')
            localStorage.removeItem('currentData')
            this.currentData = {}
            this.currentRow = null
            this.getData();
        }
    },
    methods: {
        ...mapActions(['setReportingDepartmentId']),
        showChildren(row, column, event) {
            this.setReportingDepartmentId(row.id);
            this.$refs['table'].setCurrentRow();

            if (row.sub_tree && row.sub_tree.length > 0) {
                let index = this.rowKeys.indexOf(row.id)
                this.$refs.table.toggleRowExpansion(row, !(index > -1))
                this.currentChild = [];
                if (index > -1) {
                    this.rowKeys.splice(index, 1)
                } else {
                    this.rowKeys.push(row.id)
                }
            }

            return false;
        },
        rowClick(row, column, event) {
            this.setReportingDepartmentId(row.id);
            this.$refs['table'].setCurrentRow();

            if (row.sub_tree && row.sub_tree.length > 0) {
                // if (row.sub_tree && row.sub_tree.length > 0) {
                    let index = this.rowKeys.indexOf(row.id)
                    // this.$refs.table.toggleRowExpansion(row, !(index > -1))
                    this.currentChild = []
                    if (index > -1) {
                        this.rowKeys.splice(index, 1)
                    } else {
                        this.rowKeys.push(row.id)
                    }

                // this.$refs['table'].setCurrentRow();
                // } else {
            }

                let currentData = {
                    // apa_finished:total.apa_status.finished ,
                    apa_total: row.apa_total,
                    apa_status: row.apa_status,
                    // ts_finished:total.ts_status.finished ,
                    ts_total: row.ts_total,
                    ts_status: row.ts_status,
                }

                this.currentRow = row;
                this.currentData = currentData;

                setTimeout(() => {
                    this.$emit('changeComponent', {page: 'child', currentRow: row, currentData})
                }, 300)
            // }
        },
        confirm() {
            this.isShowConfirm = false
        },
        childRowClick(row, rowIndex, index) {
            this.$refs['table'].setCurrentRow()
            this.currentRow = row;
            this.currentChild = [rowIndex, index]
            this.setReportingDepartmentId(row.id)
            this.currentData = {
                apa_total: row.apa_total,
                apa_status: row.apa_status,
                ts_total: row.ts_total,
                ts_status: row.ts_status,
            }

            setTimeout(() => {
                this.$emit('changeComponent', {
                    page: 'child',
                    currentRow: row,
                    parentRow: this.tableData[rowIndex],
                    currentChild: [rowIndex, index],
                    currentData: this.currentData
                })
            }, 300);
        },
        setClass({row, rowIndex}) {
            if (this.currentRow && row.id === this.currentRow.id && !row.sub_tree.length) {
                this.currentData = this.currentRow;

                return 'bg-black'
            } else {
                return ''
            }
        },
        flatData(departments, statistic) {
            let result = []
            if (departments && departments.length) {
                departments.forEach((item, idx) => {
                    let stas = statistic[item.department.id]
                    result[idx] = {
                        ...item.department,
                        ts_finished: stas['confirmed_ts'],
                        ts_total: stas['not_start_ts'] + stas['submitted_ts'] + stas['reviewed_ts'] + stas['confirmed_ts'],
                        ts_status: {
                            in_progress: stas['submitted_ts'] + stas['reviewed_ts'],
                            not_started: stas['not_start_ts'],
                            finished: stas['confirmed_ts'],
                        },

                        apa_finished:  stas['confirmed_apa'],
                        apa_total: stas['not_start_apa'] + stas['submitted_apa'] + stas['reviewed_apa'] + stas['confirmed_apa'],
                        apa_status: {
                            in_progress: stas['submitted_apa'] + stas['reviewed_apa'],
                            not_started: stas['not_start_apa'],
                            finished: stas['confirmed_apa'],
                        },
                        sub_tree: item['sub_tree'] && item.sub_tree.length > 0 ? this.flatData(item.sub_tree, statistic) : []
                    }
                })
            }

            return result;
        },
        getData() {
            if (!this.function_id) {
                // 没有选择任何的年份数据
                return;
            }

            let departmentId = false;

            if (this.jquery && this.jquery['currentRow']['id']) {
                departmentId = this.jquery['currentRow']['id'];
            }

            request({
                url: 'getReportingStatistic',
                method: 'get',
                params: {
                    code: this.department_code,
                    function_id: this.function_id
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.confirmTxt = res.message;
                    this.titleTxt = 'Error';
                    this.isShowConfirm = true;

                    return;
                }

                if (!res.data['departments']) {
                    return false;
                }

                res.data['departments'].unshift({'department': res.data['parent'], 'sub_tree': []});

                let departments = [];
                res.data['departments'].every(item => {
                    departments.push(item);

                    if (item['sub_tree'] && item['sub_tree'].length) {
                        item['sub_tree'].every(sub => {
                            departments.push(sub);
                            return true;
                        });
                    }

                    return true;
                });

                let ts_total = 0;
                let apa_total = 0;
                let ts_finish = 0;
                let apa_finish = 0;
                let apa_status = {
                    in_progress: 0,
                    not_started: 0,
                    finished: 0
                };

                let ts_status = {
                    in_progress: 0,
                    not_started: 0,
                    finished: 0
                };

                ts_total = 0;
                apa_total = 0;
                ts_finish = 0;
                apa_finish = 0;

                departments.every(item => {
                    let report = res.data.statistic[item['department']['id']];
                    ts_total  += report['not_start_ts'];
                    ts_total  += report['submitted_ts'];
                    ts_total  += report['reviewed_ts'];
                    ts_total  += report['confirmed_ts'];

                    ts_finish += report['confirmed_ts'];

                    apa_total += report['not_start_apa'];
                    apa_total += report['submitted_apa'];
                    apa_total += report['reviewed_apa'];
                    apa_total += report['confirmed_apa'];

                    apa_finish += report['confirmed_apa'];

                    ts_status['not_started']  += report['not_start_ts'];
                    ts_status['in_progress']  += report['submitted_ts'] + report['reviewed_ts'];
                    ts_status['finished']     += report['confirmed_ts'];

                    apa_status['not_started'] += report['not_start_apa'];
                    apa_status['in_progress'] += report['submitted_apa'] + report['reviewed_apa'];
                    apa_status['finished']    += report['confirmed_apa'];

                    return true;
                });

                this.tableData = this.flatData(res.data.departments, res.data.statistic);
                this.currentData = {
                    apa_total: apa_total,
                    apa_status: apa_status,
                    ts_total: ts_total,
                    ts_status: ts_status,
                }
            })
        },
        exportAllFile() {
            request({
                url: 'downloadTSReporting',
                method: 'download',
                params: {
                    function_id: this.function_id
                }
            }).then((res) => {
                if (!res || res.code !== 0) {
                    this.confirmTxt = res.message;
                    this.titleTxt = 'Error';
                    this.isShowConfirm = true;

                    return;
                }

                if (res && res.data) {
                    downloadFile(res.data)
                }
            })
        }
    },
    created() {
        this.getData();
    },
    mounted() {
        this.$nextTick(() => {
            // if (localStorage.getItem('currentData')) {
            //     this.currentData = JSON.parse(localStorage.getItem('currentData'))
            // }
        })

        if (this.query) {
            if (this.query.parentRow) {
                let {parentRow, currentChild, currentData} = this.query
                this.$refs['table'].toggleRowExpansion(parentRow, true)

                this.$nextTick(() => {
                    this.currentChild = currentChild
                    this.currentData = currentData
                })

            } else {
                let {currentRow, currentData} = this.query

                this.$nextTick(() => {
                    this.currentRow = currentRow;
                    this.currentData = currentData;
                })
            }
        }
    }
}
