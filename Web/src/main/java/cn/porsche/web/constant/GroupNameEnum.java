package cn.porsche.web.constant;

public enum GroupNameEnum {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 顺序千万不能变
    //

    HRPerformanceTeam("HR Performance Team"),
    DisciplinaryManager("Disciplinary Manager"),

    Employee("Employee"),

    TopManagement ("Top Management"),
    InProbation("New Employees In Probation"),
    LongLeaves ("Long Leaves"),

    //
    // 顺序千万不能变
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Delegator ("Delegator"),
    ;

    private String group;

    GroupNameEnum(String group) {
        this.group = group;
    }

    public String getName() {
        return group;
    }

    //将字符串转换为词性对象
    public static GroupNameEnum from(String string) {
        try {
            return GroupNameEnum.valueOf(string);
        } catch (IllegalArgumentException exception) {
            return null;
        }
    }
}
