package cn.porsche.web.service;


import cn.porsche.web.domain.Position;
import org.springframework.data.domain.Page;

public interface PositionService {
    Integer count();
    Page<Position> getListByPage(Integer page, Integer size);

    Position update(Position job);

    Position findById(Integer id);

    Position findByParentId(Integer parentId);
}
