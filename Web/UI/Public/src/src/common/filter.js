import Vue from 'vue'

function install(Vue,options){

  Vue.filter('formatScore',(value)=>{
    if(!value) return ''
    return Number(value).toFixed(1)
  })

  Vue.filter('formatId',(value)=>{
    if(!value) return ''
    let arr = (value + '').split('').reverse()
    let res = []
    arr.forEach((v,i)=>{
      if(i>0 && !(i%3)){
        res.push(' ')
      }
      res.push(v)
    })

    return res.reverse().join('')

  })

  Vue.filter('formatDate',(date)=>{
    if(!date) return '';

    const oData = new Date(date)
    let y = oData.getFullYear()
    let m = oData.getMonth() + 1
    let d = oData.getDate()

    return (y + '-') + (m < 10?'0' + m:m ) + '-' + (d <10?'0' + d:d);
  })

  Vue.filter('formatDateTime',(date)=>{
    let reg = /^[12]\d{3}(-|\\|\/)\d{1,2}\1\d{1,2}/
    if(!date || !reg.test(date)) return ''

    const oData = new Date(date)
    let y = oData.getFullYear()
    let m = oData.getMonth() + 1
    let d = oData.getDate()
    let h = oData.getHours()
    let M = oData.getMinutes()

    return (m < 10?'0' + m:m )+ '/' + (d <10?'0' + d:d) + '/' + (y + '').substr(2) + ' ' + (h < 10?'0'+h:h) + ':' + (M < 10?'0'+M:M)
  })


}

export default install;
