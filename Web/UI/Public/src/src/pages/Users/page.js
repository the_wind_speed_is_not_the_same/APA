import {request} from '../../common/request.js';
import {mapState, mapActions} from 'vuex'
import Confirm from '../../components/Confirm/page.vue';

export default {
    data() {
        return {
            currentIndex: null,
            departmentId: '',
            departmentList: [],
            keyWord: '',
            isShowLoading: false,
            tableHeight: 412,
            personalInfor: {},
            isShowDialog: false,
            isDialogLoading: false,
            users: [],
            pageIndex: 1,
            pageSize: 25,
            totalRecord: 0,
            initHireDate: null,
            isShowNotice: false,
            isShowImportDialog: false
        }
    },
    components: {
        Confirm
    },
    computed: {
        ...mapState({
        }),
        pageNumbers() {
            return this.totalRecord ? Math.ceil(this.totalRecord / this.pageSize) : 0
        }
    },
    filters: {
        deleteQuate(val) {
            if (val) {
                return val.replace(/(^["'])|(["']$)/g, "")
            }
            return ''
        }
    },
    methods: {
        ...mapActions(['setDelegatorInfo', 'setYearList']),
        rowClick(row) {
            this.personalInfor = row
            this.initHireDate = this.personalInfor.hire_date
            this.isShowDialog = true
        },
        beforeCloseDialog(done) {
            done()
            // this.$refs.table.setCurrentRow()
        },
        closeDialog() {
            this.isShowDialog = false
            this.isDialogLoading = false
            // this.$refs.table.setCurrentRow()
        },
        getUsers() {
            let params = {
                page: this.pageIndex,
                size: this.pageSize
            };

            if (this.departmentId != 0) {
                params['department_id'] = this.departmentId;
            }

            if (this.keyWord) {
                params['key'] = this.keyWord;
            }

            request({
                url: 'getEmployeeList',
                method: 'get',
                params: params,
                isShowLoading: true
            }).then(res => {
                if (res && res.list) {
                    this.users = res.list;
                    this.totalRecord = res['pagebar'].total;
                }
            })
        },
        updateDate() {
            this.isDialogLoading = true
            if (this.initHireDate != this.personalInfor.hire_date) {
                request({
                    url: 'setEmployeeHireDate',
                    method: 'post',
                    data: {
                        employee_id: this.personalInfor.id,
                        hire_date: this.personalInfor.hire_date
                    },
                }).then(res => {
                    this.isShowNotice = true
                    if (res && res.code === 0) {
                        this.getUsers()
                    }

                    setTimeout(() => {
                        this.isShowNotice = false
                    }, 1500)
                    this.closeDialog()
                })
            } else {
                this.closeDialog()
            }
        },
        changeDepartment(){
            this.pageIndex = 1
            this.getUsers()
        },
        pageChange(e) {
            this.pageIndex = e
            this.getUsers()
        },
        importData() {
            this.isShowImportDialog = true
        },
        uploadSuccess(response, file, fileList) {
            // console.log(response, file)
        },
    },
    created() {
        this.getUsers();

    },
    mounted() {
        this.$nextTick(() => {
            // this.tableHeight = this.$refs.tableWrapper.offsetHeight - 28;
        })

        // 获取可筛选的部门
        request({
            url: 'getDepartments',
            method: 'get',
            params: {}
        }).then(res => {
            // console.log(res);
            if (res && res.data) {
                var d = [{'id': 0, 'name': 'All'}];
                d.push(res.data.department);
                res.data.sub_tree.forEach((v,i)=>{
                    v.department.name = v.department.name;
                    d.push(v.department);

                    if(v.sub_tree && v.sub_tree.length>0){
                        v.sub_tree.forEach((v2,i2)=>{
                            v2.department.name = v2.department.name;
                            d.push(v2.department);
                        })
                    }
                })
                this.departmentList = d;
            }
        })
    }
}
