package cn.porsche.compliance.service.impl;

import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.Training;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.emnu.SeasonEnum;
import cn.porsche.compliance.emnu.TrainingTypeEnum;
import cn.porsche.compliance.repository.TrainingRepository;
import cn.porsche.compliance.service.TrainingService;
import cn.porsche.compliance.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrainingServiceImpl implements TrainingService {
    @Autowired
    TrainingRepository repository;

    @Autowired
    UserService userService;

    @Override
    public Training update(Training training) {
        return repository.save(training);
    }

    @Override
    public Iterable<Training> update(List<Training> training) {
        return repository.saveAll(training);
    }

    @Override
    public Training findByUserAndTypeAndSubject(User user, TrainingTypeEnum type, String subject) {
        return repository.findByUserIdAndTypeAndSubject(user.getId(), type, subject);
    }

    @Override
    public void delete(Training training) {
        repository.delete(training);
    }

    @Override
    public void deleteByType(Integer year, TrainingTypeEnum type) {
        repository.deleteAllByYearAndType(year, type);
    }

    @Override
    public void deleteByType(Integer year, String subject, TrainingTypeEnum type) {
        repository.deleteAllByYearAndSubjectAndType(year, subject, type);
    }

    @Override
    public void deleteStaffByType(Integer year, String subject, TrainingTypeEnum trainingType, EmployeeTypeEnum employeeType) {
        Float count = Float.valueOf(userService.countByType(employeeType).toString());
        int size    = 100;
        int page    = (int) Math.ceil(count / size);

        Training training;
        Page<User> pageable;

        do {
            pageable = userService.findAllByTypeAndPage(employeeType, page, size);

            for (User item : pageable.getContent()) {
                training = findByYearAndUserAndTypeAndSubject(year, item, trainingType, subject);

                if (null != training) {
                    delete(training);
                }
            }
        } while ( -- page > 0);
    }

    @Override
    public void resetTrainingByQuestion(Integer year, User user) {
        TrainingTypeEnum type = TrainingTypeEnum.Question;
        Training training;
        training = findByYearAndUserAndTypeAndSubject(year, user, type, SeasonEnum.Q1.getName());
        if (null == training) {
            training = Training.builder()
                    .year(year)
                    .userId(user.getId())
                    .departmentId(user.getDepartmentId())
                    .subject(SeasonEnum.Q1.getName())
                    .type(type)
                    .attend(false)
                    .build();

            update(training);
        }

        training = findByYearAndUserAndTypeAndSubject(year, user, type, SeasonEnum.Q2.getName());
        if (user.getType().equals(EmployeeTypeEnum.VP) && null != training) {
            // 用户为 VP，答过第二季度的题，需要删除
            delete(training);
        } else if (!user.getType().equals(EmployeeTypeEnum.VP) && null == training) {
            // 用户为一般员工，未生成第二季度的数据，需要新增
            training = Training.builder()
                    .year(year)
                    .userId(user.getId())
                    .departmentId(user.getDepartmentId())
                    .subject(SeasonEnum.Q2.getName())
                    .type(type)
                    .attend(false)
                    .build();

            update(training);
        }

        training = findByYearAndUserAndTypeAndSubject(year, user, type, SeasonEnum.Q3.getName());
        if (null == training) {
            training = Training.builder()
                    .year(year)
                    .userId(user.getId())
                    .departmentId(user.getDepartmentId())
                    .subject(SeasonEnum.Q3.getName())
                    .type(type)
                    .attend(false)
                    .build();

            update(training);
        }

        training = findByYearAndUserAndTypeAndSubject(year, user, type, SeasonEnum.Q4.getName());
        if (user.getType().equals(EmployeeTypeEnum.VP) && null != training) {
            // 用户为 VP，答过第二季度的题，需要删除
            delete(training);
        } else if (!user.getType().equals(EmployeeTypeEnum.VP) && null == training) {
            // 用户为一般员工，未生成第二季度的数据，需要新增
            training = Training.builder()
                    .year(year)
                    .userId(user.getId())
                    .departmentId(user.getDepartmentId())
                    .subject(SeasonEnum.Q4.getName())
                    .type(type)
                    .attend(false)
                    .build();

            update(training);
        }
    }

    @Override
    public Training findByYearAndUserAndTypeAndSubject(Integer year, User user, TrainingTypeEnum type, String subject) {
        return repository.findByYearAndUserIdAndTypeAndSubject(year, user.getId(), type, subject);
    }

    @Override
    public Integer totalByUserAndType(Integer year, User user, TrainingTypeEnum type) {
        return repository.countByUserIdAndYearAndType(user.getId(), year, type);
    }

    @Override
    public Integer attendedTotalByUserAndType(Integer year, User user, TrainingTypeEnum type) {
        return repository.countByUserIdAndYearAndTypeAndAttend(user.getId(), year, type, true);
    }

    @Override
    public List<Training> findAllByUserAndYear(Integer year, User user) {
        return repository.findAllByYearAndUserId(year, user.getId());
    }

    @Override
    public List<Training> findAllByUserAndType(User user, TrainingTypeEnum type) {
        return repository.findAllByUserIdAndType(user.getId(), type);
    }

    @Override
    public List<Training> findAllByYearAndUserAndType(Integer year, User user, TrainingTypeEnum type) {
        return repository.findAllByYearAndUserIdAndType(year, user.getId(), type);
    }

    @Override
    public List<Training> findAllByDepartment(Integer year, Department department) {
        return repository.findAllByYearAndDepartmentId(year, department.getId());
    }
}
