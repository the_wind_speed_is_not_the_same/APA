package cn.porsche.web.repository;

import cn.porsche.web.domain.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends CrudRepository<Position, Integer> {
    Position findByParentId(Integer parentId);

    Page<Position> findAll(Pageable pageable);
}
