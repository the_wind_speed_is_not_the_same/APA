package cn.porsche.web.service.impl;


import cn.porsche.web.domain.Email;
import cn.porsche.web.repository.EmailRepository;
import cn.porsche.web.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class EmailServiceImpl implements EmailService {
    @Autowired
    EmailRepository repository;

    @Override
    public Email update(Email email) {
        return repository.save(email);
    }
}
