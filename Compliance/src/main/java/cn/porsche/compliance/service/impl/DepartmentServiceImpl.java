package cn.porsche.compliance.service.impl;

import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.DepartmentTree;
import cn.porsche.compliance.repository.DepartmentRepository;
import cn.porsche.compliance.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    DepartmentRepository repository;

    @Override
    public Department findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Department findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Department update(Department department) {
        return repository.save(department);
    }

    @Override
    public Department findByCode(String code) {
        return repository.findByCode(code);
    }

    @Override
    public Department findTopDepartment(String name) {
        List<Department> departments = findTopDepartment();

        for (Department item : departments) {
            if (item.getName().toLowerCase().equals(name.toLowerCase())) {
                return item;
            }
        }

        return null;
    }

    @Override
    public List<Department> findTopDepartment() {
        return repository.findTopDepartment();
    }

    @Override
    public List<Department> findSubDepartments(Department department) {
        return repository.findAllByParentId(department.getId());
    }

    @Override
    public DepartmentTree buildTree(Department department) {
        if (null == department) {
            department = findTopDepartment("CEO Office");
            if (department == null) {
                return null;
            }
        }

        List<Department> departments = findSubDepartments(department);
        List<DepartmentTree> subTree = new ArrayList<>();
        if (departments.size() > 0) {
            for (Department item : departments) {
                subTree.add(buildTree(item));
            }
        }

        DepartmentTree tree = new DepartmentTree();
        tree.setDepartment(department);
        tree.setSubTree(subTree);

        return tree;
    }
}
