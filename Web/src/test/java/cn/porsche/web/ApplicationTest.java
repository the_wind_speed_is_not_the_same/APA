package cn.porsche.web;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTest {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    protected String getEmployeeAuthorization() {
        return "d03d03c1c573c076436318db60f5e0a05bce56a262bcb8cd179b9a08fa1305d6e618d265809bc156v641mMQ490qSpNCE3wD1937P6Y42C6p9J99CQ349i1NgH7e7py26eM2GB203SKH7E98aLLpXP61kW53yo16f2T50B1Yk7176x5149511Y2vwmfuJc5j5adb3d8ce46668dd040c286afd43cef21vHH236uc0x6UG7Z92g3245860W80";
    }

    protected String getEmployee2Authorization() {
        return "9743ff0f9544114dd14ffc38ec183f985b97cb3d0ca9aa98a72683ef85d96848cb9722316c59a8d8WiMV97ke59rfqse9c6W198Ob22Z95Q578s460XGyPsz61271678rxL6KJ7f24bJXJ8K2iW95661W4615Rd15x0F79nP3272h1piJ6aF7H0h6oX6pHQiYbd58083c732d3de25fe30b1fa5cf4318kv7O5eV19g8V4Q889u8b5Tl23580";
    }


    protected String getManagerAuthorization() {
        return "c447da4db4ca4db2ed12c83bdb52f96c40de3765e52352b21958b80fc3acd53332f5a497d4636d3e5CFHdEP17i7T2CMbVp4uY25oFZi4993185RDh8608nflUp10e015736E99Dp2712w052WYxeN2Z7Cdq58I1e241AEm81481oP23271MQ3PB624R4RCL5c005a1c16be7a81acfd5b64b199de3838bNn5419783z31070n2a75d39780";
    }

    protected String getDelegatorAuthorization() {
        return "6103d743eb52e9305c8b821975d89909f00ce7749ec54fe303d2e8b621758388e1d0b62d868dc2eegz6t4K65F9f1g2636c57JCuKundeW18YS89xh1V13h5u39p99V9b24940StKcIX58eK7wjTSUL1R42QazDBY575TH17wHv62i3096D43oey0Z8Kn7w551a9a4ff9791e8b448f9c0f0ea15382906HtKlf4v8u85mIh6vi788o9Ck880";
    }

    protected String getHRAuthorization() {
        return "343fd2aa29d095659719116a68f620d22ecbbd32ee92e54715bdbd695de603450235e349ee45b9e87qu2C4str74My42v334p834551atKP91Hf6S2qNvC9dIO3RZ40MrBO5ttZ747176nkUI1o9Zd6h2AJl4tQ82yukHl6m2d36Oh2YrC39r2iYk7fu1yATf20258286844b62ce72901e371aae983133Z60H9xf6Uxm2ejq62ia98IG680";
//        return "6103d743eb52e9305c8b821975d89909f00ce7749ec54fe303d2e8b621758388e1d0b62d868dc2eegz6t4K65F9f1g2636c57JCuKundeW18YS89xh1V13h5u39p99V9b24940StKcIX58eK7wjTSUL1R42QazDBY575TH17wHv62i3096D43oey0Z8Kn7w551a9a4ff9791e8b448f9c0f0ea15382906HtKlf4v8u85mIh6vi788o9Ck880";
    }

    protected String getVPAuthorization() {
        return "51eebebe828f94d779dc8259ae1660dd5b905fb5ccf748304ced4822d022728f2fdf9ba5647ece13311gv8x85WK3N8PuS874tBX375d3Bwc8VHL18zn71A4V33uS25e93Zf0o8tI6o3051jVq98L5gTFuB2w1y404779TXp3u5J36dy3UTCkJdeu75nU2721d1e82ad4122ccaaaad8919b24c14c0e8w4O499S51BBla4c6hKgqZ6COE180";
    }

    protected String getFunctionYear() {
        return "2021";
    }

    protected int getEmployeeId() {
        return 501110;
    }
    protected int getVPId() {
        return 505909;
    }

    protected int getEmployeeId2() {
        return 501104;
    }

    protected int getDelegatorId() {
        return 505909;
    }

    protected int getManagerId() {
        return 501201;
    }

    protected int getHRId() {
        return 501374;
    }

    protected int getDepartmentId() {
        return 2;
    }

    protected int getFunctionId() {
        return 1;
    }

    protected int getTargetId() {
        return 1;
    }

    protected int getWhatId() {
        return 1;
    }

    protected int getHowId() {
        return 1;
    }
}