package cn.porsche.web.bounds.impl;

import cn.porsche.web.bounds.AbstractBounds;
import cn.porsche.web.bounds.constant.LevelEnum;
import cn.porsche.web.constant.ScoreLevelEnum;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ManagerBoundsOf2021 extends AbstractBounds {
    @Override
    public float getWhatRate(int whatScore) {
        BigDecimal bdScore = new BigDecimal(Double.toString(whatScore));
        BigDecimal bd100   = new BigDecimal("100");
        BigDecimal bd05    = new BigDecimal("0.5");

        return bdScore.multiply(bd05).divide(bd100, 2, RoundingMode.HALF_UP).floatValue();
    }

    @Override
    public ScoreLevelEnum getLevelByScore(int score) {
        if (score >= 450) {
            return ScoreLevelEnum.A;
        } else if (score >= 350) {
            return ScoreLevelEnum.B;
        } else if (score >= 250) {
            return ScoreLevelEnum.C;
        } else if (score >= 150) {
            return ScoreLevelEnum.D;
        } else if (score >= 100) {
            return ScoreLevelEnum.E;
        }

        return null;
    }

    @Override
    public int getScoreByLevel(ScoreLevelEnum levelEnum) {
        switch (levelEnum) {
            case A: return 450 + 1;
            case B: return 350 + 1;
            case C: return 250 + 1;
            case D: return 150 + 1;
            default: return 100 + 1;
        }
    }


    @Override
    public int getFinalScore(int whatScore, int howScore) {
        return howScore;
    }
}
