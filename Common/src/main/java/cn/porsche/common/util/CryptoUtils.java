package cn.porsche.common.util;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.spec.InvalidKeySpecException;

public class CryptoUtils {
    public static final String DES = "DES";

    /**
     * 将位byte数组转换密钥Key
     * @param key
     * @param algorithm
     * @return
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    private static Key toKey(byte[] key, String algorithm) throws Exception {
        SecretKey secretKey;
        if (algorithm.equals(DES)) {
            DESKeySpec dks = new DESKeySpec(key);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
            secretKey = keyFactory.generateSecret(dks);
        } else {
            //当使用其他对称加密算法时，如AES、Blowfish等算法时，运行此分支
            secretKey = new SecretKeySpec(key, algorithm);
        }

        return secretKey;
    }

    /**
     * 根据指定的密钥及算法，将字符串进行解密
     * @param data
     * @param key
     * @param algorithm
     * @return
     */
    public static String decrypt(String data, byte[] key, String algorithm) {
        try {
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.DECRYPT_MODE, toKey(key, algorithm));
            return new String(cipher.doFinal(encryptBASE64(data)), "UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据指定的密钥及算法对指定字符串进行可逆加密
     * @param data
     * @param key
     * @param algorithm
     * @return
     */
    public static String encrypt(String data, byte[] key, String algorithm) {
        try {
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(Cipher.ENCRYPT_MODE, toKey(key, algorithm));

            return decryptBASE64(cipher.doFinal(data.getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 生成密钥
     * @return
     * @throws Exception
     */
    public static String initKey() throws Exception {
        return initKey(null);
    }

    /**
     * 通过SecureRandom类生成密钥
     * @param seed
     * @return
     * @throws Exception
     */
    public static String initKey(String seed) throws Exception {
        SecureRandom secureRandom;
        if (seed != null) {
            secureRandom = new SecureRandom(seed.getBytes());
        } else {
            secureRandom = new SecureRandom();
        }
        KeyGenerator kg = KeyGenerator.getInstance(DES);
        kg.init(secureRandom);
        SecretKey secretKey = kg.generateKey();
        return decryptBASE64(secretKey.getEncoded());
    }

    /**
     * 64位byte数组转换为16进制的字符串
     * @param data
     * @return
     */
    private static final String decryptBASE64(byte[] data) {
        StringBuilder valueHex = new StringBuilder();
        for (int i = 0, tmp; i < data.length; i++) {
            tmp = data[i] & 0xff;
            if (tmp < 16) {
                valueHex.append(0);
            }
            valueHex.append(Integer.toHexString(tmp));
        }
        return valueHex.toString();
    }

    /**
     * 16进制表示的字符串转换为64位byte数组
     * @param hexString
     * @return byte[]
     */
    private static byte[] encryptBASE64(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }

        hexString = hexString.toUpperCase();
        char[] hexChars = hexString.toCharArray();
        int length = hexString.length();
        byte[] d = new byte[length >>> 1];

        for (int n = 0; n < length; n += 2) {
            String item = new String(hexChars, n, 2);
            d[n >>> 1] = (byte) Integer.parseInt(item, 16);
        }

        return d;
    }
}
