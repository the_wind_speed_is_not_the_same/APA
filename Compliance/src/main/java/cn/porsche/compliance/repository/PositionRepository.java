package cn.porsche.compliance.repository;

import cn.porsche.compliance.domain.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PositionRepository extends CrudRepository<Position, Integer> {
    Position findByParentId(Integer parentId);
    List<Position> findAllByParentId(Integer parentId);

    Page<Position> findAll(Pageable pageable);
}
