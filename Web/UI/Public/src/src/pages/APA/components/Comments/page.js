import { request } from '../../../../common/request.js';
import { formatDateTime } from '../../../../common/utils.js';
import Confirm from '../../../../components/Confirm/page.vue';

export default {
    props:['func', 'isBtnDisabled', 'comments','function_id','isAgreeScore'],
    data() {
        return {
            topicId: '',
            to_id: '',
            comment: '',
            user:{},
            confirmTitle:'',
            confirmTxt:'',
            isShowConfirm:false,
            commentsArr:this.comments
        }
    },
    computed: {

    },
    components:{
        Confirm
    },
    methods:{
        previous() {
            this.$emit('previousIndex');
        },
        closeComponent() {
            this.$emit('closeComponent');
        },
        scrollToBottom() {
            this.$nextTick(() => {
                let commentList = this.$refs.commentList;
                commentList.scrollTop = commentList.scrollHeight;
            })
        },
        confirm(){
          this.isShowConfirm = false
        },
        addOAComment() {
            if (this.isAgreeScore || !this.comment) return;
            request({
                url: 'addOAComment',
                isShowLoading: true,
                data: {
                    function_id: this.func['id'],
                    message: this.comment,
                    type: 'APA'
                }
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.confirmTitle = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;
                  return ;
                }

                this.commentsArr = [...this.commentsArr,{
                  send_time:formatDateTime(new Date()),
                  type:'me',
                  name: this.user.en_name,
                  message:this.comment
                }];

                this.comment = '';
                // this.getOaComments();
            })
        },
    },
    created() {
      let user = localStorage.getItem('user')
      if(user){
        this.user = JSON.parse(user)
      }
    },
    mounted() {
        this.scrollToBottom();
    }
}
