package cn.porsche.compliance.repository;

import cn.porsche.compliance.domain.File;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends CrudRepository<File, Integer> {
    List<File> findAll();

    File findByMd5(String md5);
    File findByName(String name);
}
