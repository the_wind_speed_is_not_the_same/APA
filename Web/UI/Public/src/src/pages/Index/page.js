import MenuList from '../../components/MenuList/page.vue';
import Navigator from '../../components/Navigator/page.vue';
import { setAuthorization } from '../../common/utils.js';
import { mapState, mapActions } from 'vuex';

import { fontsizerem } from '../../common/utils.js';

export default {
  data() {
    return {
      navigator: [],
      isShowMenu: false,
    }
  },
  computed: {
    ...mapState({
      userInfo: state => state.allState.userInfo
    })
  },
  watch:{
    '$router':{
      handler:function(n,o){
        // console.log(n,o)
      }
    }
  },
  components: {
    Navigator,
    MenuList
  },
  methods: {
    ...mapActions(['getUserInfo']),
    openMenu() {
      this.isShowMenu = true;
    },
    closeMenu() {
      this.isShowMenu = false;
    },
  },
  created() {
    setAuthorization(this.$route.query.type);

    fontsizerem();
  },
  mounted() {

  }
}
