layui.define(['upload', 'laypage', 'layer', 'table', 'form'], function (exports) {
  var $ = layui.$
    , admin = layui.admin
    , upload = layui.upload
    , table = layui.table
    ;

  var tabIns;
  //日期时间范围fsa 
  $('.layui-tab-title').on('click', function (title) {

    $("#input_filetype").val(layui.$(title.toElement).attr("filetype"));

  });
  var active = {
    initupload: function () {
      //选完文件后不自动上传
      upload.render({
        elem: '#uploadExcel'
        // , url: 'localhost:8049/UploadFiles/UpLoadFile/' + Math.random() //改成您自己的上传接口
        , url: '/v1.0/upload/training' //改成您自己的上传接口
        , auto: false
        , accept: 'file' //普通文件
        //, exts: 'xls|excel|xlsx' //只允许上传Excel
        , exts: 'zip|rar|7z' //只允许上传压缩文件
        , number: 1
        , multiple: false
        , bindAction: '#UploadFile'
        , done: function (res) {
          layer.msg('上传成功');
          console.log(res)
        }
      });
    },


    initTable: function () {
      //查看上传文件数据列表
      var uploadList =

      {
        "msg": "",
        "code": 0,
        "count": 27,
        "data": [
          {
            "filetypeInfoId": "bb6965aa-4414-4d3b-9de3-325e1e4964a5",
            "filename": "ceshi0005",
            "filetype": "1",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "69c6bbc0-3924-4086-9843-cfdc424c167a",
            "filename": "ceshi006",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "8cd86716-fee4-421e-9645-38a0dff86b7a",
            "filename": "ceshi007",
            "filetype": "1",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "19695395-0679-4a59-9b1b-2546c06c86f1",
            "filename": "dada",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "0b0623af-cbb8-480f-b893-b32471e9a3d6",
            "filename": "ceshi1",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "c94df26a-cf3d-478b-a940-4107a245a51b",
            "filename": "321ddd",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "a70858d5-cea2-48a3-8d95-3c2b041962d8",
            "filename": "duijie",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "ff971d7c-eaea-4c32-848c-9c437c3f28aa",
            "filename": "eqw",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "5c4d53f8-27da-467e-a030-820b74756808",
            "filename": "辣椒炒肉",
            "filetype": "1",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "3bc8bd3c-2c61-4ab5-811d-e4b7ce5f2ef9",
            "filename": "哈哈哈",
            "filetype": "1",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "ce4b6e2c-6f26-45a4-8018-e0329de2fad0",
            "filename": "第一个码商",
            "filetype": "1",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "b4cc6f85-0556-4efd-a1e7-92bc70a5a519",
            "filename": "管理员建的第二个码商",
            "filetype": "1",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "a75e1ed2-1ba5-4a25-b172-0bc671415ac1",
            "filename": "OK阿南",
            "filetype": "1",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "e3fd5abf-9fd6-4673-a0d1-ac025a324edb",
            "filename": "OO11",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "9e387065-fec9-40a8-84f4-88b50982beaf",
            "filename": "oo2",
            "filetype": "3",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "b3a4b173-892e-44d4-9d08-a00f9907836f",
            "filename": "pp2",
            "filetype": "3",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "8a0dce56-e50b-47ce-836b-cf0d39b64a3b",
            "filename": "齐文测试商户",
            "filetype": "3",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "852d3b3c-6601-41b2-9f3c-bf61e1587e1a",
            "filename": "qiwen",
            "filetype": "3",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "3e72bf06-9600-40a2-ac88-cea03d4d23f0",
            "filename": "注册测试",
            "filetype": "3",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "7865b729-88db-47e2-b6e2-d7b8c1d9ae17",
            "filename": "商户",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "8885bbcf-3959-4837-8821-8070f439ba50",
            "filename": "阿南码",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          },
          {
            "filetypeInfoId": "f7210598-4b4f-40c8-b3aa-9e073761ce32",
            "filename": "莎莎微信",
            "filetype": "2",
            "filesize": 1024,
            "createtime": '2020-11-24 12:18'
          }
        ]
      };
      //弹出界面
      admin.popup({
        title: '文件列表'
        , maxmin: true
        , area: ['900px', '660px']
        , id: 'LAY-popup-upload-filelist'
        , content: $("#popup_fileList")
        , type: 1
        , shade: 0
        , success: function (layero, index) {
          tabIns = table.render({
            elem: '#LAY-file-manage'
            , id: "filemanager"
            , data: uploadList.data
            // , url: setter.apiUrl + setter.apiData.order.FindWithPagerSearch
            , contentType: "text/plain; charset=utf-8"
            // , headers: { "Authorization": "Bearer " + layui.data(setter.tableName).access_token } //ToKen验证时需要
            , cols: [[
               { field: 'filename', title: '文件名称',  width: 180 }
              , { field: 'filetype', title: '文件类型', width: 180,templet: "#LAY-file-type-templet" }
              , { field: 'filesize', width: 80, title: '文件大小', width: "240" }
              , { field: 'createtime', title: '提交时间', width: 200 }

            ]]
            , text: { none: "暂无数据" }
            , page: true
            , limit: 10
            , height: 'full-380'
            , done: function (response, curr, count) {
              debugger;
              if (count == 0) {

                return {
                  "code": 201, //解析接口状态
                  "msg": '无数据' //解析提示文本
                };
              }
              else {
                if (response.Success == false) {
                  if (response.ErrCode == "40005") {
                    layer.closeAll();
                    admin.exit();//退出系统
                  }
                  else {
                    layer.msg(response.ErrMsg);//弹出错误消息
                  }

                  return false;
                }
              }
            }
          });

        }
      });


    }

  };
  //事件绑定
  $('.hasevent').on('click', function () {
    var type = $(this).data('type');
    active[type] ? active[type].call(this) : '';
  });
  exports('upff', active);
});