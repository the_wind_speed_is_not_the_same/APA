import Vue from 'vue'
import Router from 'vue-router';
import store from '../store/index.js'
import Index from '../src/pages/Index/page.vue';
import { request } from '../src/common/request.js'

Vue.use(Router)

// 错误页面
const Error = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Error/page.vue');

const Home = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Home/page.vue');

const TS = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Targets/page.vue');

const APA = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/APA/page.vue');

const FunctionRelease = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Function/page.vue');

const MyTeam = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Team/page.vue');

const Authorization = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Authorization/page.vue');

const Delegation = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Delegation/page.vue');

const Delegator = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Delegator/page.vue');

const Reporting = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Reporting/page.vue');

const SuperAdmin = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Admin/page.vue');

const HireDate = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/SetHireDate/page.vue');

const SetManager = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/SetManager/page.vue');

const Users = () => import(/* webpackChunkName: "lazychunk_data" */ '@/src/pages/Users/page.vue');

const pages = [{
        path: '/',
        name: 'Index',
        component: Index,
        title: 'Index',
        redirect:'/home',
        children:[
          {
              path: '/home',
              name: 'Home',
              component: Home,
              title: 'Home'
          },
        //   {
        //       path: '/ts',
        //       name: 'Targets',
        //       component: Targets,
        //       title: 'Targets'
        //   },
          // {
          //     path: '/apa',
          //     name: 'APA',
          //     component: AunnualPerformanceA,
          //     title: 'APA'
          // },
        //   {
        //       path: '/release',
        //       name: 'Function',
        //       component: Function,
        //       title: 'Function'
        //   },
          // {
          //     path: '/team',
          //     name: 'Team',
          //     component: MyTeam,
          //     title: 'Team'
          // },
        //   {
        //       path: '/delegation',
        //       name: 'Delegation',
        //       component: Delegation,
        //       title: 'Delegation'
        //   },
        //   {
        //     path: '/delegator',
        //     name: 'Delegator',
        //     component: Delegator,
        //     title: 'Delegator'
        //   },
        //   {
        //       path: '/auth',
        //       name: 'Authorization',
        //       component: Authorization,
        //       title: 'Authorization'
        //   },
          // {
          //     path: '/reporting',
          //     name: 'Reporting',
          //     component: Reporting,
          //     title: 'Reporting'
          // },
          // {
          //   path: '/admin',
          //   name: 'SuperAdmin',
          //   component: SuperAdmin,
          //   title: 'Super Admin'
          // },
        //   {
        //     path: '/users',
        //     name: 'Users',
        //     component: Users,
        //     title: 'Users'
        //   }
        ]
    },
    // {
    // 	path:'*',
    // 	name:'404',
    // 	component:Error
    // }
]

const components = {
  'Home': Home,
  'Target Setting': TS,
  'Annual Performance Appraisal': APA ,
  'My Team': MyTeam,
  'Reporting': Reporting,
  'Function Release': FunctionRelease,
  'Authorization': Authorization ,
  'Delegation': Delegation,
  'Delegator': Delegator,
  'User Information': Users ,
  'Set HireDate': HireDate ,
  'Set Manager': SetManager ,
  'Super Admin': SuperAdmin
}

const menuList = [
  // {
  //     title: 'Home',
  //     info: 0,
  //     path: '/home',
  //     index: 0
  // }, {
  //     title: 'Target Setting',
  //     info: 0,
  //     path: '',
  //     index: 1
  // }, {
  //     title: 'Annual Performance Appraisal',
  //     info: 0,
  //     path: '',
  //     index: 2
  // }, {
  //     title: 'My Team',
  //     info: 0,
  //     path: '',
  //     index: 3
  // }, {
  //     title: 'Reporting',
  //     info: 0,
  //     path: '',
  //     index: 4
  // }, {
  //     title: 'Setting',
  //     info: 0,
  //     path: '',
  //     index: 5,
  //     children:[
	// 	  {
	// 		  title: 'Function Release',
	// 		  info: 0,
	// 		  path: '',
	// 		  index: 7,
	// 		  parent_id:5
	// 		}, {
	// 		  title: 'Authorization',
	// 		  info: 0,
	// 		  path: '',
	// 		  index: 8,
	// 		  parent_id:5
	// 	  },{
	// 		  title: 'Delegation',
	// 		  info: 0,
	// 		  path: '',
	// 		  index: 9,
	// 		  parent_id:5
	// 	  },
	// 	  {
	// 		  title: 'User Information',
	// 		  info: 0,
	// 		  path: '',
	// 		  index: 10,
	// 		  parent_id:5
	// 	  },
	// 	{
	// 		title: 'Super Admin',
	// 		info: 0,
	// 		path: '',
	// 		index: 10,
	// 		parent_id:5
	// 	}]
  // }
]

const router = new Router({
  routes: pages,
  // mode:'hash',
})

// 动态添加路由
request({
    url:'getMenu',
    method:'get'
}).then((res) => {
    if (!res || !res.list) {
      // 极有可能 为用户未登录
        return ;
    };

    let index = 0;
    res.list.forEach((item, parentId) => {
        let module = {
            path:   item.path,
            name:   item.name,
            title:  item.name,
            index:  index ++,
            component: components[item.name] ? components[item.name] : null
        };

        if (module.component) {
            router.options.routes[0].children.push(module);

        }

        let children = [];
        if (item.children && item.children.length > 0) {
            item.children.forEach((sub) => {
                let subModule = {
                    path: sub.path,
                    name: sub.name,
                    title: sub.name,
                    index: index ++,
                    parent_id: parentId,
                    component: components[sub.name] ? components[sub.name] : null
                };

                children.push(subModule);
                if (subModule.component) {
                    router.options.routes[0].children.push(subModule);

                    // 对某个component做特殊处理
                    if (sub.name === 'Delegation') {
                        router.options.routes[0].children.push({
                            path:   'delegator',
                            name:   sub.name,
                            title:  sub.name,
                            component: components['Delegator']
                        });
                    }
                }
            })
        }

        if (children.length != 0) {
            module.children = children;
        }

        menuList.push(module);
    });


    store.commit('setMenuList', menuList);


    router.options.routes[0].children.push({
        path:   '*',
        name:   "404 Not Found",
        title:  "404 Not Found",
        component:Error
    });


  router.addRoutes(router.options.routes);
});

export default router
