package cn.porsche.compliance.service;


import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.Question;
import cn.porsche.compliance.emnu.SeasonEnum;

import java.util.List;

public interface QuestionService {
    Question insert(Question question);

    List<Question> findAllByYear(Integer year);
    List<Question> findListByYearAndSeasonAndEmployeeType(Integer year, SeasonEnum season, EmployeeTypeEnum type);
    void removeByYearAndSeasonAndEmployeeType(Integer year, SeasonEnum season, EmployeeTypeEnum type);
}
