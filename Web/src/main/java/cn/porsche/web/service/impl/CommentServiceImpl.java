package cn.porsche.web.service.impl;


import cn.porsche.web.constant.CommentTypeEnum;
import cn.porsche.web.domain.*;
import cn.porsche.web.repository.CommentRepository;
import cn.porsche.web.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;


@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentRepository repository;

    @Override
    public Comment findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Comment add(Function function, User delegator, User employee, User sender, CommentTypeEnum typeEnum, Integer topicId, String message) {
        Assert.notNull(function, "function must not be null");
        Assert.notNull(delegator, "delegator must not be null");
        Assert.notNull(sender, "owner must not be null");
        Assert.notNull(message, "message must not be null");
        Assert.notNull(typeEnum, "typeEnum must not be null");
        Assert.notNull(employee, "employee must not be null");
        Assert.notNull(delegator, "delegator must not be null");

        Comment comment = Comment.builder()
                .functionId(function.getId())
                .ownerId(sender.getId())
                .employeeId(employee.getId())
                .employeeName(employee.getEnName())
                .delegatorId(delegator.getId())
                .delegatorName(delegator.getEnName())
                .type(typeEnum)
                .topicId(topicId)
                .message(message.replace("\r\n", "<br />"))
                .sendTime(new Date())
                .build();

        return repository.save(comment);
    }

    @Override
    public Comment update(Comment comment) {
        return repository.save(comment);
    }

    @Override
    public void read(User user, CommentTypeEnum type, Integer topicId) {
        List<Comment> comments = findByTopic(type, topicId);
        for (Comment item : comments) {
            if (!item.isRead()) {
                if (user.getId().equals(item.getEmployeeId()) || user.getId().equals(item.getDelegatorId())) {
                    if (item.getOwnerId().equals(user.getId())) {
                        continue;
                    }

                    item.setRead(true);

                    update(item);
                }
            }
        }
    }

    @Override
    public List<Comment> findByTopic(CommentTypeEnum type, Integer topicId) {
        return repository.findAllByTopicIdAndTypeOrderByIdDesc(topicId, type);
    }

    @Override
    public List<Comment> findByTopicByIdAsc(CommentTypeEnum type, Integer topicId) {
        return repository.findAllByTopicIdAndTypeOrderByIdAsc(topicId, type);
    }
}
