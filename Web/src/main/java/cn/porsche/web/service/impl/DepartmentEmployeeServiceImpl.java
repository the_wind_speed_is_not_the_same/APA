package cn.porsche.web.service.impl;

import cn.porsche.web.domain.DepartmentEmployee;
import cn.porsche.web.domain.User;
import cn.porsche.web.repository.DepartmentEmployeeRepository;
import cn.porsche.web.service.DepartmentEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class DepartmentEmployeeServiceImpl implements DepartmentEmployeeService {
    @Autowired
    DepartmentEmployeeRepository repository;

    @Override
    public DepartmentEmployee update(DepartmentEmployee data) {
        return repository.save(data);
    }

    @Override
    public DepartmentEmployee findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public DepartmentEmployee findByEmployee(User employee) {
        return repository.findByEmployeeId(employee.getId());
    }
}
