export default {
    props: {
        isShowConfirm: {
            type: Boolean,
            default: false,
        },
        confirmTxt: String,
        isShowButtonLeft: {
            type: Boolean,
            default: true,
        },
        buttonLeftTxt: {
            type: String,
            default: 'Cancel'
        },
        buttonRightTxt: {
            type: String,
            default: 'OK'
        },
        titleTxt: {
            type: String,
            default: 'Confirm'
        },
        showTime: {
            type: Number,
            default: 0
        }
    },
    watch: {
        isShowConfirm(val) {
            this.isShowConfirmSync = val;
            if (val && this.showTime > 0) {
                this.calcelTimeDelay();
            }
        },
    },
    data() {
        return {
            isShowConfirmSync: false
        }
    },
    computed: {
        
    },
    methods: {
        confirm() {
            this.$emit('confirm');
        },
        cancel() {
            this.$emit('cancel');
        },
        calcelTimeDelay() {
            let timer = setTimeout(() => {
                this.cancel();
                this.$emit('callBack');
                timer = null;
            }, this.showTime)
        }
    },
    mounted() {
        this.isShowConfirmSync = this.isShowConfirm;
    }
}