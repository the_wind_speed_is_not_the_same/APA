package cn.porsche.compliance.service;


import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.User;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {
    void removeManagerTag();

    User update(User user);

    User findById(Integer id);
    User findByCode(String code);
    User findByPositionId(Integer positionId);

    Integer count();
    Integer countByType(EmployeeTypeEnum type);
    List<User> findByManager(User manager);
    List<User> findByDepartment(Department department);
    Page<User> findAllByPage(Integer page, Integer size);
    Page<User> findAllByTypeAndPage(EmployeeTypeEnum type, Integer page, Integer size);

    User getManager(User user);
}
