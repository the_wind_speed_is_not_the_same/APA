import { request } from '../../../../common/request.js';
import { deepClone, formatDateTime } from '../../../../common/utils.js';
import Mantle from '../../../../components/Mask/page.vue';
import Confirm from '../../../../components/Confirm/page.vue';
import ListSelect from '../../../../components/ListSelect/page.vue';

export default {
    props: ['functionId', 'whats', 'dimension', 'currentIndex', 'whatIndex', 'tableData', 'isLastOne'],
    data() {
        return {
            remnant: 10,
            comments: [],
            comment: '',
            showTime: 0,
            confirmType: '',
            confirmTxt: '',
            titleTxt: 'Warning',
            scores: ['1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0'],

            editDirection: null,

            score: null,
            initScore: null,

            what: null,

            isShowConfirm: false,
            isShowKpiBox: false,
            isShowScoreSelect: false,
            isShowButtonLeft: false
        }
    },
    watch: {
        whatIndex: {
            handler(n, o) {
                this.initData(this.whats[n]);
            },
        }
    },
    components: {
        Mantle,
        Confirm,
        ListSelect
    },
    computed: {
        getWhat() {
            if (this.whats && this.whats.length) {
                return this.whats[this.whatIndex];
            }
        },
        getTarget() {
            if (this.tableData && this.tableData.length && this.currentIndex !== null) {
                return this.tableData[this.currentIndex].target;
            }

            return {}
        },
        getEmployee() {
            if (this.tableData && this.tableData.length && this.currentIndex !== null) {
                return this.tableData[this.currentIndex].user;
            }

            return {}
        }
    },
    methods: {
        descInput() {
            var txtVal = this.comment.length;
            this.remnant = 20 - txtVal;
            if(this.remnant <= 0 ){
             this.comment = this.comment.slice(0,20);
             this.remnant = 0;
            }
        },
        showScoreList() {
            this.isShowScoreSelect = !this.isShowScoreSelect;
        },
        selectScore(index) {
            this.isShowScoreSelect = false;
            let score = this.scores[index];

            if (score == this.initScore) {
                this.score = null;
            } else {
                this.score = score;
            }
        },
        sendComment() {
            if (!this.comment) return false;
            request({
                url: 'addApaComment',
                data: {
                    function_id: this.function_id,
                    message: this.comment,
                    topic_id: this.getWhat.what_id
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.isShowConfirm = true;
                    this.isShowButtonLeft = false
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message ? res.message : 'Send Failed, Please try again';

                    return false;
                }

                this.isShowConfirm = true;
                this.isShowButtonLeft = false;
                this.titleTxt = 'Success';
                this.confirmTxt = 'Send Successfully';
                let message = {
                    send_time: formatDateTime(new Date()),
                    message: this.comment,
                    delegator_name: this.user.en_name
                }

                this.comments.push(message);

                this.getWhat.apa_comments.unshift(message);
                this.comment = '';
            })
        },
        delegatorScore(score, times) {
            if (!this.score) {
                return false;
            }

            let times1 = times === 1 ? 'first' : 'second';
            let times2 = times === 1 ? 'o' : 'w';

            request({
                url: times1 + 'WhatScore',
                method: 'get',
                params: {
                    score: score,
                    what_id: this.whats[this.whatIndex]['what_id'],
                    function_id: this.functionId
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;
                    this.isShowButtonLeft = false

                    return false;
                }


                // 将数据置为原始状态
                this.initScore = this.whats[this.whatIndex][times1 + '_score'] = this.score;
                this.score = null;

                // 先检查是否打完所有的What，如果全打完了，提示您可以打How了
                let message;
                let howCompleted = false,
                    whatCompleted = this.whats.every(item => item[times1 + '_score']);

                if (whatCompleted) {
                    howCompleted = this.dimension['how1_d' + times2 + '_score']
                        && this.dimension['how2_d' + times2 + '_score']
                        && this.dimension['how3_d' + times2 + '_score']
                        && this.dimension['how3_d' + times2 + '_score'];
                }

                // 还有How未打分
                if (whatCompleted && howCompleted && this.isLastOne) {
                    // 最后一个What打分，提示可以准备开启线下预审会议
                    message = 'You are now ready for the calibration session.';
                } else if (whatCompleted && !howCompleted) {
                    // 是最后一个What打完分， 但how没有打分，提醒他
                    message = 'Save Successfully.<br >“NOTE: You are not finished yet. Please evaluate ‘How’ “. <br >Save all scores and proceed to “HOW” section for your evaluation.';
                } else {
                    message = "Save Successfully";
                }

                // 显示提示信息
                this.titleTxt = 'Success';
                this.confirmTxt = message;
                this.isShowConfirm = true;
                this.isShowButtonLeft = false


                this.$emit('delegatorScore', this.score, times1 + '_score');
            });
        },
        confirm() {
            this.isShowConfirm = false;

            if (this.editDirection === 'prev') {
                this.$emit('changeIndex', { name: 'apa', target: 'pre', current: 'what' });
            } else if (this.editDirection === 'next') {
                this.$emit('changeIndex', { name: 'apa', target: 'next', current: 'what' });
            }
        },
        cancel() {
            this.isShowConfirm = false;
        },
        close() {
            this.$emit('changeComponent', 'close-manage-apa-commnet');
        },
        hasEdit() {
            return this.comment || this.score;
        },
        prevOrNextWarning(direction) {
            this.titleTxt = 'Warning'
            this.confirmTxt = 'There are some changes not saved，are you sure to leave？?'
            this.isShowConfirm = true
            this.isShowButtonLeft = true
            this.editDirection = direction;

            return false
        },
        previous() {
            if (this.hasEdit()) {
                return this.prevOrNextWarning('prev');
            }

            this.$emit('changeIndex', { name: 'apa', target: 'pre', current: 'what' });
        },
        next() {
            if (this.hasEdit()) {
                return this.prevOrNextWarning('next');
            }

            this.$emit('changeIndex', { name: 'apa', target: 'next', current: 'what' });
        },
        showKPIUI() {
            this.isShowKpiBox = true;
        },
        hideKPIUI() {
            this.isShowKpiBox = false;
        },
        initData(what) {
            let form = deepClone(what);
            if (!form.kpis || !form.kpis.length) {
                form.kpis = [{
                    desc: '',
                    index: 4,
                }, {
                    desc: '',
                    index: 3,
                }, {
                    desc: '',
                    index: 2,
                }, {
                    desc: '',
                    index: 1,
                }, {
                    desc: '',
                    index: 0,
                }];
            } else {
                form.kpis.sort((a, b) => {
                    return b.index - a.index;
                })
            }

            if (form.apa_comments && form.apa_comments.length) {
                form.apa_comments.forEach(item => {
                    item.send_time = formatDateTime(item.send_time);
                });
            }

            this.what = form;
            this.comments = form['apa_comments'];
            this.initScore = what['second_score'] || what.first_score;
        }
    },
    created() {
    },
    mounted() {
        this.initData(this.getWhat);
    },
    destroyed() {
        this.score = null;
    }
}
