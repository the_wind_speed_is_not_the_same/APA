package cn.porsche.web.exceptions;

import cn.porsche.common.exception.BaseException;
import cn.porsche.common.response.ApiResult;

public class FunctionException extends BaseException {
    public static FunctionException ReleaseException() {
        return new FunctionException("Release has error, ");
    }

    public static FunctionException EmailAddresIsEmpty() {
        return new FunctionException("Release has error");
    }

    public static FunctionException AutoTargetSettingError() {
        return new FunctionException("The Email Gateway was error");
    }


    public static FunctionException EmailStorageWasError() {
        return new FunctionException("The Email Storage was error");
    }


    public static FunctionException TargetSettinghasBegun() {
        return new FunctionException("KickOff has begun");
    }

    public static FunctionException TargetSettingNotBegin() {
        return new FunctionException("KickOff not begin");
    }


    public static FunctionException TSEmailHadSent() {
        return new FunctionException("The Target Setting's Email had sent");
    }

    public static FunctionException APAhasBegun() {
        return new FunctionException("APA has begun");
    }

    public static FunctionException APANotBegin() {
        return new FunctionException("APA not begin");
    }


    public static FunctionException APAEmailSent() {
        return new FunctionException("APA Email has sent");
    }

    public FunctionException(String message) {
        super(ApiResult.RootErrorCode, message);
    }
}
