package cn.porsche.web.service;


import cn.porsche.web.domain.*;

import java.util.List;


public interface DepartmentReportService {
    DepartmentReport findByDepartment(Integer functionId, Integer departmentId);
    DepartmentReport initReporting(Function function, Department department);

    DepartmentReport refreshReporting(Function function, Department department);

    DepartmentReport releaseFirstAPA(Function function, Department department);
    DepartmentReport releaseSecondAPA(Function function, Department department);

    List<DepartmentReport> findAllByFunction(Function function);
}