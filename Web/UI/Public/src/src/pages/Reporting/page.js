import Confirm from '../../components/Confirm/page.vue';
import ListSelect from '../../components/ListSelect/page.vue';
import {mapState, mapActions} from 'vuex'

import Index from './components/Index/page.vue'
import Results from './components/Results/page.vue'
import ResultDetail from './components/ResultDetail/page.vue'
import StatusDetail from './components/StatusDetail/page.vue'
import {request} from "../../common/request";

export default {
    data() {
        return {
            start: '',
            finish: '',
            releaseYear: '',
            isShowDialog: false,
            isShowConfirm: false,
            isShowListSelect: false,
            selectIndex: 0,
            dataType: 1,
            pageLevel: 'parent',
            query: null,
            function_id: null,
            companies:[],
            departmentCode:''
        }
    },
    components: {
        Confirm,
        ListSelect,
        Index,
        Results
    },
    computed: {
        ...mapState({
            yearList: state => state.allState.yearList,
            allList: state => state.allState.allList,
            currentYear: state => state.allState.currentYear
        }),
        isComponent() {
            if (this.dataType === 2) {
                if (this.pageLevel === 'parent') {
                    return Results
                } else {
                    return ResultDetail
                }
            } else {
                if (this.pageLevel === 'parent') {
                    return Index
                } else {
                    return StatusDetail
                }
            }
        }
    },
    methods: {
        ...mapActions(['setYearList']),
        showDateList() {
            this.isShowListSelect = true;
        },
        initPage() {
            let list = JSON.parse(JSON.stringify(this.allList))
            if (this.allList.length > 1) {
                list.sort((a, b) => a.year - b.year)
            }

            this.allList.forEach((item, idx) => {
                if (item.is_ts) {
                    this.finish = item.year
                    this.start = this.finish - 1
                    this.selectIndex = idx
                    this.function_id = item.id
                }
            });
        },
        confirmIndex(index) {
            this.selectIndex = index;
            this.isShowListSelect = false;
            this.finish = this.yearList[this.selectIndex];
            this.start = this.finish - 1;
            this.function_id = this.allList[this.selectIndex].id
        },
        lookData(type) {
            this.dataType = type
            this.pageLevel = 'parent'
            this.query = null
        },
        getPageData(json) {
            this.pageLevel = json.page
            this.query = json.query || json;
        },
        changeCompany(item) {
            this.departmentCode = item;
        }
    },
    created() {
        if (!this.currentYear) {
            this.setYearList().then((res) => {
                this.initPage()
            })
        } else {
            this.finish = this.currentYear.year
            this.start = this.finish - 1
            this.selectIndex = this.yearList.indexOf(this.finish)
            this.function_id = this.currentYear.id
        }

        request({
            url: 'getCompanies',
            method: 'get',
            params: {}
        }).then(res => {
            if (!res || res.code !== 0) {
                res = {'list': []};
            }

            for (let i=0, len=res.list.length; i<len; ++i) {
                if (res.list[i]['id'] === 10) {
                    this.currentCompany = res.list[i]['name'];
                }
            }

            this.companies = res.list;
        })
    },
    beforeDestroy() {
        localStorage.removeItem('reportingStatus')
        localStorage.removeItem('totalCount')
        localStorage.removeItem('currentData')
    }
}
