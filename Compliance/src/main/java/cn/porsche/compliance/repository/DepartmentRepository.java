package cn.porsche.compliance.repository;

import cn.porsche.compliance.domain.Department;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Integer> {
    Department findByCode(String code);
    Department findByName(String name);

    @Query("SELECT d from Department d WHERE d.parentCode=d.parentId")
    List<Department> findTopDepartment();

    @Query("SELECT d from Department d WHERE d.parentId=:parentId and d.code!=d.parentCode")
    List<Department> findAllByParentId(Integer parentId);
}
