package cn.porsche.common.mail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.*;

public class SendEmail {
    @Autowired
    private JavaMailSender javaMailSender;
    public JavaMailSenderImpl JavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.163.com");
        mailSender.setUsername("justhumblest@163.com");
        mailSender.setPassword("NJUFGCXCQUPMYGVZ");      //邮箱授权码
        return mailSender;
    }
    //一个简单的邮件,只有text信息
    /**
     * @Description:
     * @Param: [subject:标题, text:内容, email:接收人的邮件地址]
     */
    public void SendSimpleEmail(String subject, String text, String email) {
        JavaMailSenderImpl javaMailSender = JavaMailSender();
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setSubject(subject); //邮件的主题
        mailMessage.setText(text);
        mailMessage.setTo(email); //发送给谁
        mailMessage.setFrom(Objects.requireNonNull(javaMailSender.getUsername())); //谁发送的
        javaMailSender.send(mailMessage);
    }

    /**
     * @Description:
     * @Param: [subject:标题, text:内容, html,Boolean html:是否进行html解析
     * email:接收人的邮件地址, attachmentMap:附件的名称和文件路径]
     */
    public void SendMimeEmail(String subject, String text, Boolean html,
                              String email, Map<String, String> attachmentMap) throws MessagingException, MessagingException {
        JavaMailSenderImpl javaMailSender = JavaMailSender();
        //一个复杂的邮件`
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //进行组装
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, html);
        helper.setSubject(subject);
        helper.setText(text, html);
        helper.setSentDate(new Date());
        //附件
        if (attachmentMap != null) {
            Iterator<Map.Entry<String, String>> iterator = attachmentMap.entrySet().iterator();
            //map.entrySet()得到的是set集合，可以使用迭代器遍历
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                helper.addAttachment(entry.getKey(),
                        //附件名称,要写好文件的后缀,不要少写和写错
                        new File(entry.getValue()));
                //附件的文件地址,可以写绝对路径,若是相对路径,如./1.png,代表的是common下的1.png
            }
        }
        helper.setTo(email); //发送给谁
        helper.setFrom(Objects.requireNonNull(javaMailSender.getUsername())); //谁发送的
        javaMailSender.send(mimeMessage);
    }
    //发送普通格式的eamil
    @Test
    public void Test1() throws MessagingException {
        SendEmail e = new SendEmail();
        e.SendSimpleEmail("标题","测试1111111","782917308@qq.com");} //第一个方法



    //发送html格式的带附件的email
    @Test
    public void Test2() throws MessagingException {
        SendEmail e = new SendEmail();
        HashMap<String, String> map = new HashMap<>(); //定义附件
//        map.put("1.png","./1.png");  //相对路径,相对于resources目录下
//        map.put("2.png","D:\\1.png"); //绝对路径
        map.put("111","./1.txt");
        e.SendMimeEmail("s","s",true,"782917308@qq.com",
                map);

    }

}
