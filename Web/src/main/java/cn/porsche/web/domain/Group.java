package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "`groups`", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Group implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED")
    private Integer id;

    @ApiModelProperty(notes = "管理员主键")
    @JsonProperty("manager_id")
    @Column(name = "manager_id",                columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '管理员主键'")
    Integer managerId;

    @ApiModelProperty(notes = "组权重")
    @JsonProperty("width")
    @Column(name = "width",                     columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '组权重值。组越重要，权值越高'")
    Integer width;

    @ApiModelProperty(notes = "名称")
    @JsonProperty("name")
    @Column(name = "`name`",                    columnDefinition = "VARCHAR(255) NOT NULL COMMENT '名称'")
    String name;

    @ApiModelProperty(notes = "关键字，关键字之间用半角逗号分隔")
    @JsonProperty("keys")
    @Column(name = "`keys`",                    columnDefinition = "VARCHAR(255) NULL COMMENT '关键字，关键字之间用半角逗号分隔'")
    String keys;


    @ApiModelProperty(notes = "是否为默认的值，未分级的用户全归到此组")
    @JsonProperty("is_default")
    @Column(name = "is_default",                columnDefinition = "TINYINT NOT NULL DEFAULT '0' COMMENT '是否为默认的值，未分级的用户全归到此组'")
    boolean isDefault;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}