package cn.porsche.common.auth;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class App {
    private String appId;
    private String appSercet;
    private String pound;

    public App(String appId, String appSercet, String pound) {
        this.pound = pound;

        this.appId = appId;
        this.appSercet = appSercet;
    }
}
