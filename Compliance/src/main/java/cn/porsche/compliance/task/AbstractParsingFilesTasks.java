package cn.porsche.compliance.task;

import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.Position;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.service.DepartmentEmployeeService;
import cn.porsche.compliance.service.DepartmentService;
import cn.porsche.compliance.service.PositionService;
import cn.porsche.compliance.service.UserService;
import cn.porsche.compliance.task.parsing.exception.ParsingException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;

public abstract class AbstractParsingFilesTasks {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected final static String PCNToken          = "P(S)?CN\\d+";


    protected final static char   DelimiterDot  = ',';
    protected final static char   DelimiterTab  = '\t';

    protected final static String EMPTY     = "";
//    protected final static String Charset   = "utf8";

    protected final static String SubStrDepartment  = "department";
    protected final static String SubStrEmployee    = "employee";
    protected final static String SubStrPosition    = "position";
    protected final static String SubStrHeader      = "head";


    @Autowired
    protected UserService userService;

    @Autowired
    protected PositionService positionService;

    @Autowired
    protected DepartmentService departmentService;

    @Autowired
    protected DepartmentEmployeeService departmentEmployeeService;

    // 每天最后一秒执行文件
    @Scheduled(cron = "59 59 22 1/1 * ?")
    public void cron() {
        try {
            running();
        } catch (ParsingException e) {
            e.printStackTrace();

            logger.debug(e.getMessage());
        }
    }

    protected boolean filter(String code) {
        if (StringUtils.isEmpty(code)) {
            return true;
        }

        return !Pattern.compile(PCNToken).matcher(code).matches();
    }

    // 指定对应的解析目录
    protected abstract String getDirectory();

    protected abstract String[] getParts(String line, char ch);

    // 解析外部系统提供过来的文件
    protected abstract void parsing(File file) throws ParsingException;

    @Transactional(rollbackFor = ParsingException.class) // 需要开启事务支持
    protected void running() throws ParsingException {
        String directory = getDirectory();
        if (directory == null) {
            throw ParsingException.TheDirectoryIsNotRead(directory);
        }

        File path = new File(directory);
        if (!path.isDirectory() || !path.canRead()) {
            throw ParsingException.TheDirectoryIsNotRead(directory);
        }

        logger.debug("{} -------------> 准备解析 EFlow 文件", new Date());
        String name;
        File header     = null;
        File employee = null;
        File position = null;
        File department = null;
        Iterator<File> files = FileUtils.listFiles(path, null, true).iterator();

        while (files.hasNext()) {
            File file = files.next();
            if (file.isFile() && file.canRead()) {
                name = file.getName().toLowerCase();
                if (name.indexOf(SubStrDepartment) > 0) {
                    department = file;
                } else if (name.indexOf(SubStrEmployee) > 0) {
                    employee = file;
                } else if (name.indexOf(SubStrPosition) > 0) {
                    position = file;
                } else if (name.indexOf(SubStrHeader) > 0) {
                    header = file;
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ///

        // 部门文件
        if (department != null) {
            // 不依赖任何数据
            logger.debug("-----------------> 准备解析部门文件 {} ", department.getName());
            parsing(department);
            logger.debug("-----------------> 准备解析部门文件 {} 完毕", department.getName());
            department.deleteOnExit();
        }

        // 职位描述文件
        if (position != null) {
            // 不依赖任何数据
            logger.debug("-----------------> 准备解析职位信息 {} ", position.getName());
            parsing(position);
            logger.debug("-----------------> 准备解析职位信息 {}  完毕", position.getName());
            position.deleteOnExit();
        }

        if (employee != null) {
            // 依赖职位文件和部门信息
            logger.debug("-----------------> 准备员工信息职位信息 {} ", employee.getName());
            parsing(employee);
            logger.debug("-----------------> 准备员工信息职位信息 {}  完毕", employee.getName());
            employee.deleteOnExit();
        }

        // 部门VP文件
        if (header != null) {
            // 依赖部门和员工文件
            logger.debug("-----------------> 提取VP信息 {} ", header.getName());
            parsing(header);
            logger.debug("-----------------> 提取VP信息 {}  完毕", header.getName());
            header.deleteOnExit();
        }

        ///
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        logger.debug("{} =============> 解析 EFlow 文件完毕", new Date());



        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 更新数据，因为有依赖关系
        ///

        logger.debug("{} -------------> 开始更新用户的上级信息", new Date());
        // 更新用户的上线信息，依赖
        updateUserManager();
        logger.debug("{} =============> 更新用户的上级信息完毕", new Date());


        ///
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    protected int getClosedIndex(String str, char ch, int idx) {
        int len = str.length();

        while (( ++ idx) < len) {
            if (str.charAt(idx) == ch) {
                return idx;
            }
        }

        return -1;
    }

    // 更新用户信息表的User.ManagerId，同时修改绩效表中经理数据
    protected void updateUserManager() {
        double count = Double.valueOf(userService.count());
        if (count == 0f) {
            return;
        }

        Set<Integer> managerIds = Collections.synchronizedSet(new HashSet<>());

        // 开始设置用户的部门数据
        int page = Runtime.getRuntime().availableProcessors() + 1;
        int size = Double.valueOf(Math.ceil(count / page)).intValue();

        CountDownLatch latch = new CountDownLatch(page);
        for (int i = 1; i <= page; i++) {
            new Thread(new SyncProcessUserManager(latch, managerIds, i, size)).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 根据管理员列表，设置对应部门的主管
        for (Integer id : managerIds) {
            User manager = userService.findById(id);
            if (null == manager || null == manager.getDepartmentId()) {
                continue;
            }

            Department department = departmentService.findById(manager.getDepartmentId());
            if (null == department || id.equals(department.getManagerId())) {
                continue;
            }

            if (!id.equals(department.getManagerId())) {
                department.setManagerId(id);

                departmentService.update(department);
            }
        }
    }

    // 更新用户的汇报线
    private class SyncProcessUserManager implements Runnable {
        int page;
        int size;
        CountDownLatch latch;

        Set<Integer> managers;

        public SyncProcessUserManager(CountDownLatch latch, Set<Integer> managers, final int page, final int size) {
            this.page = page;
            this.size = size;

            this.latch = latch;

            this.managers = managers;
        }

        @Override
        public void run() {
            Iterator<User> iterator = userService.findAllByPage(page, size).getContent().iterator();

            User item;
            User delegator;
            while (iterator.hasNext()) {
                item = iterator.next();
                delegator = null;

                logger.debug("开始查找{} 的上级信息 {}, PositionId: {}", item.getId(), item.getCnName(), item.getPositionId());
                if (null == item.getPositionId()) {
                    logger.debug("用户{} {} 没有职位信息", item.getId(), item.getCnName());

                    continue;
                }


                logger.debug("用户{} {} 开始查找上级", item.getId(), item.getCnName());
                Integer positionId = item.getPositionId();

                // 根据 PositionId 查看对应的上级 User 数据
                Position position;
                while (positionId != null) {
                    position  = positionService.findById(positionId);
                    if (null == position || null == position.getParentId()) {
                        // 没有上级信息
                        delegator = null;

                        logger.debug("用户{} {} 没有职位信息，职位数据中无上级数据", item.getId(), item.getCnName());
                        break;
                    }

                    if (null == ( positionId = position.getParentId() )) {
                        logger.debug("用户{} {} 没有职位信息，职位数据中无上级数据，开始查找上级的上级 Id:{}, ParentId:{}", item.getId(), item.getCnName(), position.getId(), position.getParentId());

                        break;
                    }
                }

                synchronized(managers) {
                    if (delegator != null && managers.add(delegator.getId())) {
                        logger.debug("{} 的直线经理 {} {}", item.getCnName(), delegator.getId(), delegator.getCnName());
                    }
                }

                if (delegator != null && !delegator.getId().equals(item.getManagerId())) {
                    item.setManagerId(delegator.getId());

                    logger.debug(" --------------------------> 更新 {} 的汇报经理为 {} 已完成", item.getCnName(), delegator.getCnName());
                    userService.update(item);
                }
            }

            latch.countDown();
        }
    }
}
