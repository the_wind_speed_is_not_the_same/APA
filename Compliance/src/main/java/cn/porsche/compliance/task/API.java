package cn.porsche.compliance.task;

import java.util.ArrayList;
import java.util.List;

public enum  API {
    Active(),
    Department(),
    Position(),
    Employee(),
    ;

    private String active;
    private String apiKey;

    private int maxPage = 1;
    private int page    = 0;
    private int size    = 0;

    private List<String> apis = new ArrayList<>();

    public API getApis() {
        apis.clear();

        if (!active.equalsIgnoreCase("prod")) {
            // TEST
            apis.add("http://apigwaws-lite.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/EmployeeApi?date=%s");
            apis.add("http://apigwaws-lite.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/PositionApi?date=%s");
            apis.add("http://apigwaws-lite.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/DepartmentApi?date=%s");

        } else {
            // PROD
            apis.add("http://apigwaws.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/EmployeeApi?date=%s");
            apis.add("http://apigwaws.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/PositionApi?date=%s");
            apis.add("http://apigwaws.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/DepartmentApi?date=%s");
        }

        page = 1;
        page = 25;

        return this;
    }

    public API next() {
        int idx = this.ordinal() + 1;
        API[] values = values();
        if (idx >= values.length) {
            return null;
        }

        return values[idx];
    }

    public boolean hasNext() {
        return this.ordinal() + 1 > values().length;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getUri() {
        return apis.get(this.ordinal() -1);
    }

    public String getApiKey() {
        return active.equalsIgnoreCase("prod") ? "LmdkMuu5bavHDQ2" : "yI6X025ERcgvFOeFDpzbrIraP9Qn2yGd";
    }

    public int getSize() {
        return size;
    }

    public void resetPage() {
        page = 0;
        maxPage = 0;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public int nextPage() {
        if ( ++ page > maxPage ) {
            page = 0;
        }

        return page;
    }
}
