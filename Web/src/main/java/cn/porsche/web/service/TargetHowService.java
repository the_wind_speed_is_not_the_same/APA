package cn.porsche.web.service;


import cn.porsche.web.domain.*;

import java.util.Map;


public interface TargetHowService {
    void deleteByTarget(Target target);
    TargetHow findById(Integer id);
    TargetHow update(TargetHow how);

    TargetHow findByTarget(Target target);

    TargetHow confirmByEmployee(Target target);
    TargetHow confirmByDelegator(Target target);

    TargetHow eAppraisal(Function function, Target target, TargetHow how);
    TargetHow delegatorFirstAppraisal(Function function, Target target, TargetHow how);
    TargetHow delegatorSecondAppraisal(Function function, Target target, TargetHow how);

    Map<String, Map<String, String>> check(TargetHow how);
}
