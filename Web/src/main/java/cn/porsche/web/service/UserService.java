package cn.porsche.web.service;


import cn.porsche.web.domain.Function;
import cn.porsche.web.domain.User;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;

public interface UserService {
    User update(User user);

    User findById(Integer id);
    User findByCode(String code);
    User findByPositionId(Integer id);

    Integer count();
    Page<User> findAllByPage(Integer page, Integer size);
    List<User> findAllByManager(User manager);

    // 当前可以参加考评的用户
    Page<User> findAllByEnabled(Integer page, Integer size);

    Integer countByNeedHands();
    Page<User> findAllByHands(Integer page, Integer size);

    List<User> findAllInKPIUsers(Function function);

    User getManager(User user);
    User findDelegator(Function function, User user);

    User updateHireDate(User user, Date hireDate);
}
