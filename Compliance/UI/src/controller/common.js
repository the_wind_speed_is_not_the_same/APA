/**

 @Name：layuiAdmin 公共业务
 @Author：贤心
 @Site：http://www.layui.com/admin/
 @License：LPPL
    
 */
//记录用户选择时间

var strArr = null;
var startDate, endDate, date_startDate, date_endDate, commonDateControl;
layui.define(function (exports) {

  var $ = layui.$
    , layer = layui.layer
    , laytpl = layui.laytpl
    , setter = layui.setter
    , device = layui.device
    , admin = layui.admin
    , table = layui.table
    , laydate = layui.laydate
  //公共业务的逻辑处理可以写在此处，切换任何页面都会执行
  //……
  //Cookies 处理

  var pluses = /\+/g;

  // 对Date的扩展，将 Date 转化为指定格式的String
  // 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
  // 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
  // 例子：
  // (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
  // (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
  Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
      "M+": this.getMonth() + 1,                 //月份
      "d+": this.getDate(),                    //日
      "h+": this.getHours(),                   //小时
      "m+": this.getMinutes(),                 //分
      "s+": this.getSeconds(),                 //秒
      "q+": Math.floor((this.getMonth() + 3) / 3), //季度
      "S": this.getMilliseconds()             //毫秒
    };
    if (/(y+)/.test(fmt))
      fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
      if (new RegExp("(" + k + ")").test(fmt))
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
  }
  function encode(s) {
    return config.raw ? s : encodeURIComponent(s);
  }

  function decode(s) {
    return config.raw ? s : decodeURIComponent(s);
  }

  function stringifyCookieValue(value) {
    return encode(config.json ? JSON.stringify(value) : String(value));
  }

  function parseCookieValue(s) {
    if (s.indexOf('"') === 0) {
      // This is a quoted cookie as according to RFC2068, unescape...
      s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
    }

    try {
      // Replace server-side written pluses with spaces.
      // If we can't decode the cookie, ignore it, it's unusable.
      // If we can't parse the cookie, ignore it, it's unusable.
      s = decodeURIComponent(s.replace(pluses, ' '));
      return config.json ? JSON.parse(s) : s;
    } catch (e) { }
  }

  function read(s, converter) {
    var value = config.raw ? s : parseCookieValue(s);
    return $.isFunction(converter) ? converter(value) : value;
  }

  var config = $.cookie = function (key, value, options) {

    // Write

    if (value !== undefined && !$.isFunction(value)) {
      options = $.extend({}, config.defaults, options);

      if (typeof options.expires === 'number') {
        var days = options.expires, t = options.expires = new Date();
        t.setTime(+t + days * 864e+5);
      }

      return (document.cookie = [
        encode(key), '=', stringifyCookieValue(value),
        options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
        options.path ? '; path=' + options.path : '',
        options.domain ? '; domain=' + options.domain : '',
        options.secure ? '; secure' : ''
      ].join(''));
    }

    // Read

    var result = key ? undefined : {};

    // To prevent the for loop in the first place assign an empty array
    // in case there are no cookies at all. Also prevents odd result when
    // calling $.cookie().
    var cookies = document.cookie ? document.cookie.split('; ') : [];

    for (var i = 0, l = cookies.length; i < l; i++) {
      var parts = cookies[i].split('=');
      var name = decode(parts.shift());
      var cookie = parts.join('=');

      if (key && key === name) {
        // If second argument (value) is a function it's a converter...
        result = read(cookie, value);
        break;
      }

      // Prevent storing a cookie that we couldn't decode.
      if (!key && (cookie = read(cookie)) !== undefined) {
        result[name] = cookie;
      }
    }

    return result;
  };

  config.defaults = {};

  $.removeCookie = function (key, options) {
    if ($.cookie(key) === undefined) {
      return false;
    }

    // Must not alter options, thus extending a fresh object...
    $.cookie(key, '', $.extend({}, options, { expires: -1 }));
    return !$.cookie(key);
  };
  // table.set({
  //   headers: { //通过 request 头传递
  //     Authorization: layui.data('layuiAdmin').access_token
  //   }
  //   ,where: { //通过参数传递
  //     Authorization: layui.data('layuiAdmin').access_token
  //   }
  // });
  //数字前置补零
  laytpl.digit = function (num, length, end) {
    var str = '';
    num = String(num);
    length = length || 2;
    for (var i = num.length; i < length; i++) {
      str += '0';
    }
    return num < Math.pow(10, length) ? str + (num | 0) : num;
  };
  //时间处理--start
  laytpl.toDateString = function (d, format) {

    var date = new Date(d || new Date())
      , ymd = [
        this.digit(date.getFullYear(), 4)
        , this.digit(date.getMonth() + 1)
        , this.digit(date.getDate())
      ]
      , hms = [
        this.digit(date.getHours())
        , this.digit(date.getMinutes())
        , this.digit(date.getSeconds())
      ];

    format = format || 'yyyy-MM-dd HH:mm:ss';

    return format.replace(/yyyy/g, ymd[0])
      .replace(/MM/g, ymd[1])
      .replace(/dd/g, ymd[2])
      .replace(/HH/g, hms[0])
      .replace(/mm/g, hms[1])
      .replace(/ss/g, hms[2]);
  };
  //退出
  admin.events.logout = function () {

    // 执行退出接口
    $.when(admin.events.AjaxGet(-1, setter.apiData.Login.Logout, "", false, false, "")).then(
      function (data) {
        admin.exit();
      }
    )

  };
  admin.events.initAllUserSelect = function (objSelectId) {
    //下拉框赋值
    var responseData = admin.events.AjaxGet(-1, setter.apiData.Account.GetAcoountInfoByAll, "?page=1&Limit=999999", false, false, "");
    var q1 = document.getElementById(objSelectId);
    q1.append(new Option("用户帐号", ""));
    $.each(responseData, function (key, val) {
      q1.append(new Option(val.Account, val.AccountInfoId));
    });

    layui.form.render();
  };

  admin.events.initDateTime = function (objTimeName) {
    /*************************************时间控件初始化*************************************/

    var day1 = new Date();
    startDate = day1.getFullYear() + "-" + (day1.getMonth() + 1) + "-" + day1.getDate();
    date_startDate = new Date(startDate);
    var day3 = new Date();
    day3.setTime(day1.getTime()+24*60*60*1000);
    endDate = day1.getFullYear() + "-" + (day1.getMonth() + 1) + "-" + day3.getDate();
    date_endDate = new Date(endDate);
    commonDateControl = layui.laydate.render({
      elem: "#" + objTimeName
      , range: true
      , show: false
      , trigger: 'click'
      , format: 'yyyy-MM-dd'
      , value: '' + startDate + ' - ' + endDate
      , closeStop: '#' + objTimeName//这里代表的意思是：点击 test1 所在元素阻止关闭事件冒泡。如果不设定，则无法弹出控件
      , done: function (value, beginDate, endDate) {
        if (value == "") {
          startDate = "";
          endDate = "";
        } else {
          //回调函数
          strArr = value.split(' - ')
          startDate = strArr[0];
          endDate = strArr[1];
          date_startDate = new Date(startDate);
          date_endDate = new Date(endDate)
        }
      }

    });
    layui.form.render();
  }
  admin.events.prevDate = function (objTimeName) {
  
    var newStartDate = date_startDate.setTime(date_startDate.getTime()-24*60*60*1000);
    var newEndDate = date_endDate.setTime(new Date(newStartDate).getTime()+24*60*60*1000);
 
    date_startDate = new Date(newStartDate);
    date_endDate = new Date(newEndDate);
    startDate = date_startDate.Format("yyyy-MM-dd");
    endDate = date_endDate.Format("yyyy-MM-dd");
    $("#" + objTimeName).val(startDate + ' - ' + endDate);
    layui.form.render();
  };
  admin.events.nextDate = function (objTimeName) {
    var newStartDate = date_startDate.setTime(date_startDate.getTime()+24*60*60*1000);
    var newEndDate = date_endDate.setTime(new Date(newStartDate).getTime()+24*60*60*1000);
    date_startDate = new Date(newStartDate);
    date_endDate = new Date(newEndDate);
    startDate = date_startDate.Format("yyyy-MM-dd");
    endDate = date_endDate.Format("yyyy-MM-dd");
    $("#" + objTimeName).val(startDate + ' - ' + endDate);
    layui.form.render();
  };
  /**
 * [通过参数名获取url中的参数值]
 * 示例URL:http://htmlJsTest/getrequest.html?uid=admin&rid=1&fid=2&name=小明
 * @param  {[string]} queryName [参数名]
 * @return {[string]}           [参数值]
 */
  admin.events.GetQueryValue = function (queryName) {
    var query = decodeURI(window.location.search.substring(1));
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      if (pair[0] == queryName) { return pair[1]; }
    }
    return null;
  },
    admin.events.getNotifyStatus = function (oNotifyStatus) {
      //  <!-- 通知状态:0,未通知,1通知成功,2通知失败   -->
      var statusCN = "";
      switch (oNotifyStatus) {
        case 0: statusCN = "未通知"; break;
        case 1: statusCN = "通知成功"; break;
        case 2: statusCN = "通知失败"; break;
        default: statusCN = oNotifyStatus; break;
      }
      return statusCN;
    },
    admin.events.quertParams = function (oJson) {
      var params = Object.keys(oJson).map(function (key) {

        return encodeURIComponent(key) + "=" + encodeURIComponent(oJson[key]);
      }).join("&");
      return params;
    },
    admin.events.formDataSerizal = function (oFormId) {
      var formData = {};
      var t = $('#' + oFormId).serializeArray();
      $.each(t, function () {

        formData[this.name] = this.value;
      });
      return formData;
    },
    admin.events.AjaxPostQuery = function (oPopupIndex, oUrl, oData, oasync, oIsmsg, oTableName) {
      var responseData = null;

      $.ajax({
        url: setter.apiUrl + oUrl + "?" + oData,
        type: "Post",
        // async: oasync,
        dataType: "json",
        beforeSend: function (XMLHttpRequest) {
          XMLHttpRequest.setRequestHeader("Authorization", "Bearer " + layui.data(setter.tableName).access_token);
        },
        error: function (res) {
          layer.msg('服务器未正常响应，请重试', { icon: 5 });
        },
        success: function (response) {

          if (response.Success == false) {
            if (response.ErrCode == "40005") {
              layer.closeAll();
              admin.exit();
            }
            else if (response.ErrorCode == "40004") {
              return parent.layer.msg("登录超时,请重新登录");
            }
            else {
              return layer.msg(response.ErrMsg);
            }
          }
          else {
            responseData = response.ResData;
            if (oIsmsg) {
              layer.msg('保存成功', { icon: 1, time: 1000 }, function () {
                layer.close(oPopupIndex);

              });
            }

            if (oPopupIndex >= 0)
              layer.close(oPopupIndex);

            if (oTableName != "")
              layui.table.reload(oTableName);
          }
        }
      });
      return responseData;
    }
  admin.events.AjaxPost = function (oPopupIndex, oUrl, oData, oasync, oIsmsg, oTableName) {
    var responseData = null;

    $.ajax({
      url: setter.apiUrl + oUrl,
      type: "post",
      data: oData,
      // async: oasync,
      dataType: "json",
      contentType: "application/json",
      beforeSend: function (XMLHttpRequest) {
        XMLHttpRequest.setRequestHeader("Authorization", "Bearer " + layui.data(setter.tableName).access_token);
      },
      error: function (res) {
        layer.msg('服务器未正常响应，请重试', { icon: 5 });
      },
      success: function (response) {

        if (response.Success == false) {
          if (response.ErrCode == "40005") {
            layer.closeAll();
            admin.exit();
          }
          else if (response.ErrorCode == "40004") {
            return parent.layer.msg("登录超时,请重新登录");
          }
          else {
            layer.msg(response.ErrMsg);

          }
          responseData = response;
        }
        else {

          responseData = response.ResData;
          //是否需要弹出消息
          if (oIsmsg) {
            layer.msg('保存成功', { icon: 1, time: 1000 }, function () {
              if (oPopupIndex >= 0)
                layer.close(oPopupIndex);
              if (oTableName != "")
                layui.table.reload(oTableName);


            });
          }
        }
      },
      error: function (xhr, textStatus, errorThrown) {

        if (xhr.statusCode().status == 404) {
          admin.exit();
        }
        else {
          layer.msg('请求接口错误，错误信息：' + xhr.statusText, { icon: 1, time: 1000 });
        }

      }
    });
    return responseData;
  };
  admin.events.AjaxPostForm = function (oPopupIndex, oUrl, oData, oasync, oIsmsg, oMsgContent, oTableName) {
    var responseData = null;
    $.ajax({
      url: setter.apiUrl + oUrl,
      type: "post",
      data: oData,
      async: oasync,
      dataType: "json",
      //contentType: "application/json-patch+json",
      contentType: "application/x-www-form-urlencoded",
      // contentType: "text/plain; charset=utf-8",
      //contentType: "application/json",
      beforeSend: function (XMLHttpRequest) {
        XMLHttpRequest.setRequestHeader("Authorization", "Bearer " + layui.data(setter.tableName).access_token);
      },
      error: function (res) {
        layer.msg('服务器未正常响应，请重试', { icon: 5 });
      },
      success: function (response) {
        if (response.Success == false) {
          if (response.ErrCode == "40005") {
            layer.closeAll();
            admin.exit();
          }
          else if (response.ErrorCode == "40004") {
            return parent.layer.msg("登录超时,请重新登录");
          }
          else {
            return layer.msg(response.ErrMsg);
          }

        }
        else {
          responseData = response.ResData;
          //是否需要弹出消息
          if (oIsmsg) {
            layer.msg(oMsgContent, { icon: 1, time: 1000 }, function () {

              if (oPopupIndex >= 0)
                layer.close(oPopupIndex);
              if (oTableName != "")
                layui.table.reload(oTableName);


            });
          }


        }
      },
      error: function (xhr, textStatus, errorThrown) {

        if (xhr.statusCode().status == 404) {
          admin.exit();
        }
        else {
          layer.msg('请求接口错误，错误信息：' + xhr.statusText, { icon: 1, time: 1000 });
        }

      }
    });

  };
  admin.events.AjaxGet = function (oPopupIndex, oUrl, oData, oasync, oIsmsg, oTableName) {


    var responseData = null;
    $.ajax({
      url: setter.apiUrl + oUrl + oData,
      type: "get",
      async: oasync,
      dataType: "json",
      beforeSend: function (XMLHttpRequest) {

        if (layui.data(setter.tableName).access_token == undefined) {
          admin.exit();
        }
        else {
          XMLHttpRequest.setRequestHeader("Authorization", "Bearer " + layui.data(setter.tableName).access_token);
        }

      },
      complete: function () {
        layer.close(oPopupIndex);
      },
      error: function (xhr, textStatus, errorThrown) {

        if (xhr.status == 404) {
          admin.exit();
        }
        else {
          layer.msg('请求接口错误，错误信息：' + xhr.statusText, { icon: 1, time: 1000 });
        }

      },
      success: function (response) {
        if (response.Success == false) {
          if (response.ErrCode == "40005") {
            layer.closeAll();
            admin.exit();
          }
          else if (response.ErrorCode == "40004") {
            return parent.layer.msg("登录超时,请重新登录");
          }
          else {
            return parent.layer.msg(response.ErrMsg);
          }

          return false;
        }
        else {
          responseData = response.ResData == undefined ? response.data : response.ResData;
          //是否需要弹出消息
          if (oIsmsg) {

            layer.msg('保存成功', { icon: 1, time: 1000 }, function () {
              if (oPopupIndex >= 0)
                layer.close(oPopupIndex);
              if (oTableName != "")
                layui.table.reload(oTableName);
            });
          }

        }
      }

    });
    return responseData;
  },
    admin.events.DeleteSoftAsync = function (oUrl, oData, oTableName) {
      //删除
      layer.confirm('确认要删除?'
        , { btn: ["确定", "取消"], shade: false }
        , function (index) {
          admin.events.AjaxPostQuery(-1, oUrl, "id=" + oData + "&bltag=1", false, true, oTableName);

        });
    },
    admin.events.exprotExcel = function (oUrl, oTable) {

      //导出功能
      if (device.ie) {
        layer.tips('导出功能不支持 IE，请用 Chrome 等高级浏览器导出', this, {
          tips: 3
        })
      } else {


        var responseData = admin.events.AjaxGet(-1, oUrl, "?page=1&limit=9999999", false, false, "");
        if (oTable == undefined) {
          layer.msg('导出功能出现异常，找不到Table控件：', { icon: 2, time: 2000 });
        }
        else {
          layui.table.exportFile(oTable.config.id, responseData);
        }


      }



    }
  admin.events.getTableCheckStatusData = function (oCheckStatus, oFiled) {
    var ApplicantInfoArray = new Array();
    for (var i = 0; i < oCheckStatus.data.length; i++) {
      var id = oCheckStatus.data[i][oFiled];
      ApplicantInfoArray.push(id);
    }
    return ApplicantInfoArray;

  },
    /*
    *批量删除
    *oUrl 接口地址
    *oData 参数
    *oTableName tableid 刷新使用
    */
    admin.events.DeleteSoftBatchAsync = function (oUrl, oData, oTableName, oCheckStatus, oFiled) {
      //批量删除
      var arr = admin.events.getTableCheckStatusData(oCheckStatus, oFiled);
      layer.confirm('确认要删除?'
        , { btn: ["确定", "取消"], shade: false }
        , function (index) {
          admin.events.AjaxPostQuery(-1, oUrl + "?ids=" + arr.join(","), "", false, true, oTableName);
        });
    }

  //对外暴露的接口
  exports('common', {});
});