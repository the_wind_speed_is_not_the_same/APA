package cn.porsche.web.service.impl;


import cn.porsche.web.domain.*;
import cn.porsche.web.repository.TargetKPIRepository;
import cn.porsche.web.service.TargetKPIService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TargetKPIServiceImpl implements TargetKPIService {
    @Autowired
    TargetKPIRepository repository;

    @Override
    @Transactional
    public void deleteByTarget(Target target) {
        repository.deleteAllByTargetId(target.getId());
    }

    @Override
    public TargetKPI update(TargetKPI kpi) {
        Assert.notNull(kpi, "The [kpi] argument is null");

        return repository.save(kpi);
    }

    @Override
    public List<TargetKPI> findByTarget(Target target, TargetWhat what) {
        Assert.notNull(target, "The [target] argument is null");
        Assert.notNull(what, "The [what] argument is null");

        return repository.findAllByTargetIdAndWhatIdOrderByIndexDesc(target.getId(), what.getId());
    }

    @Override
    public Map<String, Map<String, String>> check(Target target, TargetWhat what) {
        Assert.notNull(target, "The [target] argument is null");
        Assert.notNull(what, "The [what] argument is null");

        Map<String, Map<String, String>> notices = new HashMap<>();
        List<TargetKPI> kpis = findByTarget(target, what);
        if (null == kpis || kpis.size() == 0) {
            notices.put("kpis:" + what.getId(), null);

            return notices;
        }

        Map<String, String> fields = new HashMap<>();
        for (TargetKPI kpi : kpis) {
            if (kpi.getIndex() == 2) {
                fields.put(kpi.getIndex().toString(), StringUtils.isEmpty(kpi.getDesc()) ? null : "yes");
            } else if (kpi.getIndex() == 3) {
                fields.put(kpi.getIndex().toString(), StringUtils.isEmpty(kpi.getDesc()) ? null : "yes");
            } else if (kpi.getIndex() == 4) {
                fields.put(kpi.getIndex().toString(), StringUtils.isEmpty(kpi.getDesc()) ? null : "yes");
            }
        }

        notices.put("kpis:" + what.getId(), fields);

        return notices;
    }
}
