package cn.porsche.web.bounds;

public enum BoundsYearEnum {
    YEAR_2020(2020),
    YEAR_2021(2021),
    YEAR_2022(2022),
    YEAR_2023(2023),
    YEAR_2024(2024),
    ;

    private int year;

    BoundsYearEnum(int year) {
        this.year = year;
    }

    public int getLevel() {
        return year;
    }
    public BoundsYearEnum setLevel(int year) {
        return from(year);
    }

    public static BoundsYearEnum getDefaultBoundsYear() {
        return BoundsYearEnum.YEAR_2020;
    }

    public static BoundsYearEnum from(int year) {
        for (BoundsYearEnum item : BoundsYearEnum.values()) {
            if (item.getLevel() == year) {
                return item;
            }
        }

        return getDefaultBoundsYear();
    }
}
