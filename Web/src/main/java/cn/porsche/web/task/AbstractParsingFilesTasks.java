package cn.porsche.web.task;

import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.domain.*;
import cn.porsche.web.service.*;
import cn.porsche.web.task.parsing.exception.ParsingException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;

public abstract class AbstractParsingFilesTasks {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected final static String PCNToken          = "P(S)?CN\\d+";


    protected final static char   DelimiterDot  = ',';
    protected final static char   DelimiterTab  = '\t';

    protected final static String EMPTY     = "";
//    protected final static String Charset   = "utf8";

    protected final static String SubStrDepartment  = "department";
    protected final static String SubStrEmployee    = "employee";
    protected final static String SubStrPosition    = "position";
    protected final static String SubStrHeader      = "head";


    @Autowired
    protected UserService userService;

    @Autowired
    protected PositionService positionService;

    @Autowired
    protected GroupService groupService;

    @Autowired
    protected GroupUserService groupUserService;

    @Autowired
    protected DepartmentService departmentService;

    @Autowired
    protected DepartmentEmployeeService departmentEmployeeService;

    @Autowired
    protected KPIUserService kpiUserService;

    @Autowired
    protected TargetService targetService;

    @Autowired
    protected FunctionService functionService;

    // 每天最后一秒执行文件
//    @Scheduled(cron = "59 59 22 1/1 * ?")
    public void cron() {
        try {
            running();
        } catch (ParsingException e) {
            e.printStackTrace();

            logger.debug(e.getMessage());
        }
    }

    protected boolean filter(String code) {
        if (StringUtils.isEmpty(code)) {
            return true;
        }

        return !Pattern.compile(PCNToken).matcher(code).matches();
    }

    // 指定对应的解析目录
    protected abstract String getDirectory();

    protected abstract String[] getParts(String line, char ch);

    // 解析外部系统提供过来的文件
    protected abstract void parsing(File file) throws ParsingException;

    @Transactional(rollbackFor = ParsingException.class) // 需要开启事务支持
    protected void running() throws ParsingException {
        String directory = getDirectory();
        if (directory == null) {
            throw ParsingException.TheDirectoryIsNotRead(directory);
        }

        File path = new File(directory);
        if (!path.isDirectory() || !path.canRead()) {
            throw ParsingException.TheDirectoryIsNotRead(directory);
        }

        logger.debug("{} -------------> 准备解析 EFlow 文件", new Date());
        String name;
        File header     = null;
        File employee = null;
        File position = null;
        File department = null;
        Iterator<File> files = FileUtils.listFiles(path, null, true).iterator();

        while (files.hasNext()) {
            File file = files.next();
            if (file.isFile() && file.canRead()) {
                name = file.getName().toLowerCase();
                if (name.indexOf(SubStrDepartment) > 0) {
                    department = file;
                } else if (name.indexOf(SubStrEmployee) > 0) {
                    employee = file;
                } else if (name.indexOf(SubStrPosition) > 0) {
                    position = file;
                } else if (name.indexOf(SubStrHeader) > 0) {
                    header = file;
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ///

        // 部门文件
        if (department != null) {
            // 不依赖任何数据
            logger.debug("-----------------> 准备解析部门文件 {} ", department.getName());
            parsing(department);
            logger.debug("-----------------> 准备解析部门文件 {} 完毕", department.getName());
            department.deleteOnExit();
        }

        // 职位描述文件
        if (position != null) {
            // 不依赖任何数据
            logger.debug("-----------------> 准备解析职位信息 {} ", position.getName());
            parsing(position);
            logger.debug("-----------------> 准备解析职位信息 {}  完毕", position.getName());
            position.deleteOnExit();
        }

        if (employee != null) {
            // 依赖职位文件和部门信息
            logger.debug("-----------------> 准备员工信息职位信息 {} ", employee.getName());
            parsing(employee);
            logger.debug("-----------------> 准备员工信息职位信息 {}  完毕", employee.getName());
            employee.deleteOnExit();
        }

        // 部门VP文件
        if (header != null) {
            // 依赖部门和员工文件
            logger.debug("-----------------> 提取VP信息 {} ", header.getName());
            parsing(header);
            logger.debug("-----------------> 提取VP信息 {}  完毕", header.getName());
            header.deleteOnExit();
        }

        ///
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        logger.debug("{} =============> 解析 EFlow 文件完毕", new Date());



        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 更新数据，因为有依赖关系
        ///

        logger.debug("{} -------------> 开始更新用户的上级信息", new Date());
        // 更新用户的上线信息，依赖
        updateUserManager();
        logger.debug("{} =============> 更新用户的上级信息完毕", new Date());

        // 更新用户组信息，VP，DM，员工组信息
        logger.debug("{} -------------> 更新用户的组信息", new Date());
        updateUserGroup();
        logger.debug("{} =============> 更新用户的组信息完毕", new Date());

        logger.debug("{} -------------> 开始更新用户部门信息", new Date());
        updateUserDepartment();
        logger.debug("{} =============> 更新用户部门信息完毕", new Date());

        ///
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    protected int getClosedIndex(String str, char ch, int idx) {
        int len = str.length();

        while (( ++ idx) < len) {
            if (str.charAt(idx) == ch) {
                return idx;
            }
        }

        return -1;
    }

    // 更新用户信息表的User.ManagerId，同时修改绩效表中经理数据
    protected void updateUserManager() {
        double count = Double.valueOf(userService.count());
        if (count == 0f) {
            return;
        }

        Set<Integer> managerIds = Collections.synchronizedSet(new HashSet<>());

        // 开始设置用户的部门数据
        int page = Runtime.getRuntime().availableProcessors() + 1;
        int size = Double.valueOf(Math.ceil(count / page)).intValue();

        CountDownLatch latch = new CountDownLatch(page);
        for (int i = 1; i <= page; i++) {
            new Thread(new SyncProcessUserManager(latch, managerIds, i, size)).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 根据管理员列表，设置对应部门的主管
        for (Integer id : managerIds) {
            User manager = userService.findById(id);
            if (null == manager || null == manager.getDepartmentId()) {
                continue;
            }

            Department department = departmentService.findById(manager.getDepartmentId());
            if (null == department || id.equals(department.getManagerId())) {
                continue;
            }

            if (!id.equals(department.getManagerId())) {
                department.setManagerId(id);

                departmentService.update(department);
            }
        }
    }


    // 处理直线信息数据
    //  检查用户是否在直线经理或VP中，做相应的处理
    protected void updateUserGroup() {
        double count = Double.valueOf(userService.count());
        if (count == 0f) {
            return;
        }

        Set<Integer> vpIDs = Collections.synchronizedSet(new HashSet<>());
        Set<Integer> dmIDs = Collections.synchronizedSet(new HashSet<>());

        // 开始设置用户的部门数据
        int page = Runtime.getRuntime().availableProcessors() + 1;
        int size = Double.valueOf(Math.ceil(count / page)).intValue();


        CountDownLatch latch = new CountDownLatch(page);
        for (int i = 1; i <= page; i++) {
            new Thread(new SyncProcessUserGroup(latch, vpIDs, dmIDs, i, size)).start();;
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // 根据VP设置
        for (Integer id : vpIDs) {
            User user = userService.findById(id);

            groupService.moveToTopManagerGroup(user, user);
        }


        Group managerGroup  = groupService.findByName(GroupNameEnum.DisciplinaryManager.getName());
        List<User> managers = groupUserService.findAllByGroup(managerGroup);
        // 将旧的管理员数据先清除
        for (User item : managers) {
            if (!dmIDs.contains(item.getId())) {
                groupUserService.remove(managerGroup, item);
            } else {

                // 已经存在新的经理列表中，删除掉
                dmIDs.remove(item.getId());
            }
        }

        for (Integer id : dmIDs) {
            // 不在管理组的添加到经理级中
            User manager = userService.findById(id);
            if (null == manager) {
                continue;
            }

            // 添加到管理组信息中去
            groupService.moveToManagerGroup(manager, manager);
            logger.debug("新增 {} {} 到管理组中", manager.getId(), manager.getCnName());
        }
    }


    // 更新用户所有的部门信息
    protected void updateUserDepartment() {
        double count = Double.valueOf(userService.count());
        if (count == 0f) {
            return;
        }

        Integer departmentId;
        Department department;

        // 生成部门树
        Map<Integer, List<Department>> tree = new HashMap<>();
        List<Department> departments = departmentService.findAllDepartments();
        for (Department item : departments) {
            List<Department> list = new ArrayList<>();

            list.add(item);

            departmentId = item.getParentId();
            while (departmentId != null || departmentId != 0) {
                // 找到父级
                department = departmentService.findById(departmentId);

                if (null == department) {
                    break;
                }

                if (department != null) {
                    list.add(department);

                    if (department.getParentId() == null || department.getParentId() == 0) {
                        break;
                    }

                    departmentId = department.getParentId();
                }
            }

            Collections.reverse(list);

            while (list.size() < 5) {
                // 补齐总数
                list.add(null);
            }

            tree.put(item.getId(), list);
        }

        // 开始设置用户的部门数据
        int page = Runtime.getRuntime().availableProcessors() + 1;
        int size = Double.valueOf(Math.ceil(count / page)).intValue();

        CountDownLatch latch = new CountDownLatch(page);
        for (int i = 1; i <= page; i++) {
            new Thread(new SyncProcessDepartmentInformation(latch, tree, i, size)).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // 更新用户的部门数据
    private class SyncProcessDepartmentInformation implements Runnable {
        int page;
        int size;

        CountDownLatch latch;
        Map<Integer, List<Department>> tree;

        public SyncProcessDepartmentInformation(CountDownLatch latch, final Map<Integer, List<Department>> tree, final int page, final int size) {
            this.page = page;
            this.size = size;

            this.tree = tree;

            this.latch = latch;
        }

        @Override
        public void run() {
            Iterator<User> iterator = userService.findAllByPage(page, size).getContent().iterator();

            User item;
            Integer departmentId;

            // 用于生成员工的部门层级
            List<Department> list;
            while (iterator.hasNext()) {
                item = iterator.next();
                if (null == (departmentId = item.getDepartmentId())) {
                    continue;
                }

                if (null == (list = tree.get(departmentId))) {
                    continue;
                }

                DepartmentEmployee data = departmentEmployeeService.findByEmployee(item);
                if (data == null) {
                    data = DepartmentEmployee.builder().employeeId(item.getId())
                            .department1(0)
                            .department2(0)
                            .department3(0)
                            .department4(0)
                            .department5(0)
                            .build();
                }

                logger.debug(" =============> 开始更新 {} {} 的部门数据", item.getId(), item.getCnName());


                boolean isUpdate = false;
                if (list.get(0) != null && !list.get(0).getId().equals(data.getDepartment1())) {
                    data.setDepartment1(list.get(0).getId());

                    isUpdate = true;
                }

                if (list.get(1) != null && !list.get(1).getId().equals(data.getDepartment2())) {
                    data.setDepartment2(list.get(1).getId());

                    isUpdate = true;
                }

                if (list.get(2) != null && !list.get(2).getId().equals(data.getDepartment3())) {
                    data.setDepartment3(list.get(2).getId());

                    isUpdate = true;
                }

                if (list.get(3) != null && !list.get(3).getId().equals(data.getDepartment4())) {
                    data.setDepartment4(list.get(3).getId());

                    isUpdate = true;
                }

                if (list.get(4) != null && !list.get(4).getId().equals(data.getDepartment5())) {
                    data.setDepartment5(list.get(4).getId());

                    isUpdate = true;
                }

                if (isUpdate) {
                    logger.debug(" =============> 更新的部门信息：{} {}, {} {} {} {} {}",item.getId(), item.getCnName(),
                            data.getDepartment1(),
                            data.getDepartment2(),
                            data.getDepartment3(),
                            data.getDepartment4(),
                            data.getDepartment5()
                    );

                    departmentEmployeeService.update(data);
                }
            }

            latch.countDown();
        }
    }

    // 更新用户的汇报线
    private class SyncProcessUserManager implements Runnable {
        int page;
        int size;
        CountDownLatch latch;

        Set<Integer> managers;

        public SyncProcessUserManager(CountDownLatch latch, Set<Integer> managers, final int page, final int size) {
            this.page = page;
            this.size = size;

            this.latch = latch;

            this.managers = managers;
        }

        @Override
        public void run() {
            Iterator<User> iterator = userService.findAllByPage(page, size).getContent().iterator();

            User item;
            User delegator;
            while (iterator.hasNext()) {
                item = iterator.next();
                delegator = null;

                logger.debug("开始查找{} 的上级信息 {}, PositionId: {}", item.getId(), item.getCnName(), item.getPositionId());
                if (null == item.getPositionId()) {
                    logger.debug("用户{} {} 没有职位信息", item.getId(), item.getCnName());

                    continue;
                }


                logger.debug("用户{} {} 开始查找上级", item.getId(), item.getCnName());
                Integer positionId = item.getPositionId();

                // 根据 PositionId 查看对应的上级 User 数据
                Position position;
                while (positionId != null) {
                    position  = positionService.findById(positionId);
                    if (null == position || null == position.getParentId()) {
                        // 没有上级信息
                        delegator = null;

                        logger.debug("用户{} {} 没有职位信息，职位数据中无上级数据", item.getId(), item.getCnName());
                        break;
                    }

                    delegator = userService.findByPositionId(position.getParentId());
                    if (delegator != null) {
                        logger.debug("用户{} {} 的直属上级position id:{} Manager:{} {}", item.getId(), item.getCnName(), item.getPositionId(), delegator.getId(), delegator.getEnName());

                        break;
                    }

                    if (null == ( positionId = position.getParentId() )) {
                        logger.debug("用户{} {} 没有职位信息，职位数据中无上级数据，开始查找上级的上级 Id:{}, ParentId:{}", item.getId(), item.getCnName(), position.getId(), position.getParentId());

                        break;
                    }
                }

                synchronized(managers) {
                    if (delegator != null && managers.add(delegator.getId())) {
                        logger.debug("{} 的直线经理 {} {}", item.getCnName(), delegator.getId(), delegator.getCnName());
                    }
                }

                if (delegator != null && !delegator.getId().equals(item.getManagerId())) {
                    item.setManagerId(delegator.getId());

                    // 将KPI中的汇报线也一并改为新的管理员
                    // 绩效考核已经完成的，忽略，未完成的，切换为新的管理员
                    List<KPIUser> kpiUsers = kpiUserService.findAllByUser(item);
                    for (KPIUser kpiuser : kpiUsers) {
                        Target target = targetService.findByKPIUser(kpiuser);
                        if (target == null || target.isAgreeScore()) {
                            // 已经结束的绩效记录不必修改
                            continue;
                        }

                        logger.debug(" -------------> 更新绩效数据中的汇报数据 {} 的汇报经理为 {}", item.getCnName(), delegator.getCnName());

                        kpiuser.setDelegatorId(delegator.getId());
                        kpiuser.setDelegator(delegator.getEnName());

                        kpiuser.setManager(delegator.getEnName());
                        kpiuser.setManagerId(delegator.getManagerId());

                        kpiUserService.update(kpiuser);
                    }

                    logger.debug(" --------------------------> 更新 {} 的汇报经理为 {} 已完成", item.getCnName(), delegator.getCnName());
                    userService.update(item);
                }
            }

            latch.countDown();
        }
    }


    // 更新用户的组信息
    private class SyncProcessUserGroup implements Runnable {
        int page;
        int size;

        CountDownLatch latch;

        Set<Integer> vpIDs;
        Set<Integer> dmIDs;


        public SyncProcessUserGroup(CountDownLatch latch, final Set<Integer> vpIDs, final Set<Integer> dmIDs, final int page, final int size) {
            this.page = page;
            this.size = size;

            this.latch  = latch;

            this.vpIDs  = vpIDs;
            this.dmIDs  = dmIDs;
        }

        @Override
        public void run() {
            Iterator<User> iterator = userService.findAllByPage(page, size).getContent().iterator();

            User item;

            Function function = functionService.getLastFunction();
            Group defaultGroup = groupService.getDefaultGroup();
            while (iterator.hasNext()) {
                item = iterator.next();

                // 查看用户是那类角色
                boolean isVP = false;

                // 检查是否为VP
                if (null != item.getPositionId()) {
                    Position position = positionService.findById(item.getPositionId());
                    if (null != position && null != position.getFunction() && position.getFunction().indexOf("Vice President") == 0) {
                        synchronized(vpIDs) {
                            vpIDs.add(item.getId());
                        }

                        isVP = true;
                    }
                }

                // 检查是否为DM（直线经理）
                if (null != item.getManagerId()) {
                    synchronized (dmIDs) {
                        dmIDs.add(item.getManagerId());
                    }
                }

                if (isVP || groupService.inLongLevelGroup(item) || groupService.inProbationGroup(item)) {
                    // 是VP, 或在长假组，或在实习生组
                    continue;
                }


                if (!groupService.inEmployeeGroup(item)) {
                    // 不在员工组，需要创建Target
                    if (null == function) {
                        continue;
                    }

                    groupUserService.add(defaultGroup, item);

                    if (null != function) {
                        Department department = null;
                        if (null != item.getDepartmentId()) {
                            department = departmentService.findById(item.getDepartmentId());
                        }

                        Position position = null;
                        if (null != item.getPositionId()) {
                            position = positionService.findById(item.getPositionId());
                        }

                        User delegator = null;
                        if (null != item.getManagerId()) {
                            delegator = userService.findById(item.getManagerId());
                        }

                        kpiUserService.builder(function, item, delegator, department, position);
                    }

                    logger.debug("用户{} {} 已经添加到 {} 组", item.getId(), item.getCnName(), defaultGroup.getName());
                }
            }

            latch.countDown();
        }
    }
}
