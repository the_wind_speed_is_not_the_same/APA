package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "target_kpis", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class TargetKPI implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    Integer id;

    @JsonIgnore
    @ApiModelProperty(notes = "TS主键")
    @Column(name = "target_id",                 columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'TS主键'")
    Integer targetId;

    @JsonIgnore
    @ApiModelProperty(notes = "What主键")
    @Column(name = "what_id",                   columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'Waht主键'")
    Integer whatId;

    @ApiModelProperty(notes = "对应的下标【3-5】必填")
    @JsonProperty("index")
    @Column(name = "`index`",                   columnDefinition = "TINYINT(1) UNSIGNED NOT NULL COMMENT '下标【3-5】必填'")
    Integer index;

    @ApiModelProperty(notes = "完成此目标的描述")
    @JsonProperty("desc")
    @Column(name = "`desc`",                    columnDefinition = "VARCHAR(1024) NULL COMMENT '完成此目标的描述'")
    String desc;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
