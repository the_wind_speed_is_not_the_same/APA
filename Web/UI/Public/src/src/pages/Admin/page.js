import {request} from '../../common/request.js';
import {mapState, mapActions} from 'vuex'
import Confirm from '../../components/Confirm/page.vue';

export default {
    data() {
        return {
            currentIndex: null,
            function_id: null,
            keyWord: '',
            isShowLoading: false,
            tableHeight: 412,
            selectType: 1,
            selectionArr: [],
            isShowDialog: false,
            moveTo: null,
            isShowConfirm: false,
            confirmTxt: '',
            confirmTitle: 'Warning',
            tableData: [],
            totalRecord: 0,
            pageSize: 25,
            pageIndex: 1,
            groups: [
                {label: 'HRPerformanceTeam', name: 'HR Team'},
                {label: 'TopManagement', name: 'Top Management'},
                // {label:'DisciplinaryManager',name:'Disciplinary Manager'},
                {label: 'Employee', name: 'Employee'},
                {label: 'LongLeaves', name: 'Long Leaves'},
                {label: 'InProbation', name: 'Under Probation'}
            ],
            isShowNotice: false,
            groupIndex: null,
            groupFilter: null,
            groupFilters: [
                {label: '', name: 'All'},
                {label: 'HRPerformanceTeam', name: 'HR Team'},
                {label: 'TopManagement', name: 'Top Management'},
                {label: 'InProbation', name: 'Under Probation'},
                {label: 'LongLeaves', name: 'Long Leaves'}
            ]
        }
    },
    components: {
        Confirm
    },
    computed: {
        ...mapState({
            currentYear: state => state.allState.currentYear
        }),
        pageNumbers() {
            return this.totalRecord ? Math.ceil(this.totalRecord / this.pageSize) : 0
        }
    },
    methods: {
        ...mapActions(['setDelegatorInfo', 'setYearList']),
        getData() {
            request({
                url: 'getEmployeeList',
                method: 'get',
                params: {
                    page: this.pageIndex,
                    size: this.pageSize,
                    group: this.groupFilter
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.confirmTitle = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;
                    return;
                }
                if (res && res.list) {
                    this.tableData = res.list
                    this.totalRecord = res.pagebar.total
                }
            })
        },
        setHeaderClass({row, column, rowIndex, columnIndex}) {
            if (rowIndex === 0) {
                return columnIndex === 0 ? "cell-hide" : (columnIndex === 1 ? 'special-cell' : null)
            }
        },
        setCellClass({row, column, rowIndex, columnIndex}) {
            if (columnIndex === 1) {
                return 'cell-hide'
            }
        },
        selectCell(selection, row) {
            this.selectionArr = selection
            if (this.tableData.length === selection.length) {
                this.selectType = 2
            } else {
                this.selectType = 1
            }
        },
        selectAllCell() {
            if (this.selectType === 1) {
                this.selectType = 2
                this.selectionArr = this.tableData
            } else {
                this.selectType = 1
                this.selectionArr = []
            }
            this.$refs.table.toggleAllSelection()
        },
        rowClick(row, column, event) {
            if (this.selectionArr.length > 0) {
                let index = null
                this.selectionArr.forEach((item, idx) => {
                    if (item.id === row.id) {
                        index = idx
                    }
                })
                if (index === null) {
                    this.$refs.table.toggleRowSelection(row, true)
                    this.selectionArr.push(row)
                } else {
                    this.$refs.table.toggleRowSelection(row, false)
                    this.selectionArr.splice(index, 1)
                }

            } else {
                this.$refs.table.toggleRowSelection(row, true)
                this.selectionArr.push(row)
            }
        },
        spanMethod({row, column, rowIndex, columnIndex}) {
            if (columnIndex === 9) {
                return [1, 2]
            }
        },
        confirmMove() {
            if (!this.moveTo || this.selectionArr.length === 0) {
                return false
            }

            let employees = [];
            this.selectionArr.forEach((item) => {
                employees.push(item.id)
            });

            request({
                url: 'moveEmployee',
                method: 'post',
                data: {
                    employees,
                    function_id: this.function_id,
                    group: this.moveTo
                },
                isShowLoading: true
            }).then(res => {
                // console.log(res)
                if (!res || res.code !== 0) {
                    debugger;
                    this.confirmTitle = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;

                    return;
                }

                if (res.code === 0) {
                    this.isShowNotice = true
                    this.getData()
                    setTimeout(() => {
                        this.isShowNotice = false
                    }, 1500)
                }
            })
            this.isShowDialog = false
        },
        cancelMove() {
            this.isShowDialog = false
        },
        moveRole() {
            this.isShowDialog = true
        },
        deleteRole() {
            this.isShowConfirm = true
        },
        confirmDelete() {
            this.isShowConfirm = false
        },
        cancelDelete() {
            this.isShowConfirm = false
        },
        pageChange(e) {
            this.pageIndex = e
            this.getData()
        },
        searchUser() {
            if (this.keyWord) {
                request({
                    url: 'getEmployeeList',
                    method: 'get',
                    params: {
                        key: this.keyWord
                    }
                }).then(res => {
                    if (!res || res.code !== 0) {
                        this.confirmTitle = 'Error';
                        this.confirmTxt = res.message;
                        this.isShowConfirm = true;
                        return;
                    }

                    if (res && res.list) {
                        this.tableData = res.list || [];
                        this.pageIndex = 1;
                        this.totalRecord = res.list ? res.list.length : 0;
                    }
                })
            } else {
                this.pageIndex = 1
                this.getData();
            }
        },
        filterGroup(group) {
            this.groupFilter = group;

            this.getData();
        }
    },
    created() {
        this.getData();
    },
    mounted() {
        // this.$nextTick(() => {
        //   this.tableHeight = this.$refs.tableWrapper.offsetHeight - 90;
        // })
    }
}
