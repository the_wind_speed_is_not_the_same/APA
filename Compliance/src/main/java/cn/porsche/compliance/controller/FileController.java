package cn.porsche.compliance.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.domain.*;
import cn.porsche.compliance.emnu.TrainingTypeEnum;
import cn.porsche.compliance.service.FileService;
import cn.porsche.compliance.service.StatisticalService;
import cn.porsche.compliance.service.TrainingService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/file")
@ApiModel(value = "/file", description = "培训结果的操作类")
public class FileController extends BaseController {
    @Autowired
    StatisticalService reportService;

    @Autowired
    FileService fileService;

    @Autowired
    TrainingService trainingService;

    @ApiOperation(
            value = "获取用户的文件阅读数"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/detail")
    public String detail() {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        HashMap<String, Object> json = new HashMap<>();

        json.put("detail", trainingService.findAllByUserAndType(user, TrainingTypeEnum.FileView));
        json.put("files", fileService.findAll());

        return ApiResult.builder().data(json).toJson();
    }


    @ApiOperation(
            value = "获取用户的文件阅读数"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/view")
    public String view(
            @RequestParam("name") String name
    ) {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (StringUtils.isEmpty(name = name.trim())) {
            // 未有权限
            return ApiResult.isBad("请指定需要查阅的文件");
        }

        File file = fileService.findByName(name);
        if (null == file) {
            return ApiResult.isBad("not file");
        }

        Training training = trainingService.findByUserAndTypeAndSubject(user, TrainingTypeEnum.FileView, name);
        if (null == training) {
            training = Training.builder()
                    .year(Calendar.getInstance().get(Calendar.YEAR))
                    .type(TrainingTypeEnum.FileView)
                    .departmentId(user.getDepartmentId())
                    .userId(user.getId())
                    .subject(name)
                    .attend(false)
                    .build();
        }

        if (!training.getAttend()) {
            training.setAttend(true);
            trainingService.update(training);

            reportService.refreshViewer(user);
        }

        file.setName("/files/" + file.getName());

        return ApiResult.builder().data(file).toJson();
    }
}
