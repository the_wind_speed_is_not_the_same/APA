package cn.porsche.web.service.impl;


import cn.porsche.web.bounds.BoundsFactory;
import cn.porsche.web.bounds.BoundsYearEnum;
import cn.porsche.web.bounds.IBounds;
import cn.porsche.web.constant.CommentTypeEnum;
import cn.porsche.web.constant.ScoreLevelEnum;
import cn.porsche.web.controller.params.KPIParam;
import cn.porsche.web.controller.params.WhatParam;
import cn.porsche.web.domain.*;
import cn.porsche.web.exceptions.TargetException;
import cn.porsche.web.repository.TargetRepository;
import cn.porsche.web.service.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

@Log4j2
@Service
public class TargetServiceImpl implements TargetService {
    final static String DelegatorFirstTip = "first_score";
    final static String AutoWhatName      = "Target Contribution of This Year";

    @Autowired
    TargetRepository repository;

    @Autowired
    UserService userService;

    @Autowired
    PositionService positionService;

    @Autowired
    DepartmentService departmentService;


    @Autowired
    TargetKPIService kpiService;

    @Autowired
    TargetWhatService whatService;

    @Autowired
    TargetHowService howService;

    @Autowired
    CommentService commentService;

    @Autowired
    KPIUserService kpiUserService;


    @Override
    public Target get(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Target reset(Target target) {
        delete(target);

        target.setTSEConfirm(false);
        target.setTSEConfirm(false);
        target.setTSDConfirm(false);
        target.setAPAESubmit(false);
        target.setAPADSubmit(false);
        
        target.setEmployeeHowScore(0);
        target.setEmployeeWhatScore(0);
        target.setEmployeeWhatRate(0f);
        target.setEmployeeTotalScore(0);
        target.setEmployeeScoreLevel(null);

        target.setFirstHowScore(0);
        target.setFirstWhatScore(0);
        target.setFirstWhatRate(0f);
        target.setFirstTotalScore(0);
        target.setFirstScoreLevel(null);

        target.setSecondHowScore(0);
        target.setSecondWhatScore(0);
        target.setSecondWhatRate(0f);
        target.setSecondTotalScore(0);
        target.setSecondScoreLevel(null);

        target.setReleaseSecond(false);
        target.setReleaseDate(null);
        target.setAgreeScore(false);
        
        return update(target);
    }

    @Override
    public Target delete(Target target) {
        kpiUserService.deleteByTarget(target);
        howService.deleteByTarget(target);
        whatService.deleteByTarget(target);
        kpiService.deleteByTarget(target);

        return target;
    }

    @Override
    public Target update(Target target) {
        return repository.save(target);
    }


    @Override
    public Target findByUser(Function function, User user) {
        Assert.notNull(function, "The [function] argument is null");
        Assert.notNull(user, "user [function] argument is null");

        return repository.findByFunctionIdAndEmployeeId(function.getId(), user.getId());
    }

    @Override
    public Target findByKPIUser(KPIUser user) {
        return repository.findByKPIUserId(user.getId());
    }

    @Override
    public Target open(Function function, User user) {
        // 默认创建Target到系统中
        Target target = findByUser(function, user);
        if (null != target) {
            return target;
        }

        target = Target.builder()
                .functionId(function.getId())
                .TSESubmit(false)
                .TSEConfirm(false)
                .TSDConfirm(false)
                .APAESubmit(false)
                .APADSubmit(false)
                .employeeHowScore(null)
                .employeeWhatScore(null)
                .employeeTotalScore(null)
                .employeeScoreLevel(null)
                .firstHowScore(null)
                .firstWhatScore(null)
                .firstTotalScore(null)
                .firstScoreLevel(null)
                .secondHowScore(null)
                .secondWhatScore(null)
                .secondTotalScore(null)
                .secondScoreLevel(null)

                .ReleaseSecond(false)
                .releaseDate(null)
                .AgreeScore(false)
                .build();

        return update(target);
    }

    @Override
    public KPIParam findTargetDetail(Target target, boolean isDelegator) {
        List<WhatParam> whats = new ArrayList<>();
        List<TargetWhat> list = findAllWhatByTarget(target);
        for (TargetWhat item : list) {
            WhatParam param = new WhatParam();
            param.setTargetId(target.getId());

            param.setFunctionId(target.getFunctionId());
            param.setEmployeeId(item.getEmployeeId());
            param.setWhatId(item.getId());
            param.setName(item.getName());
            param.setDesc(item.getDesc());
            param.setWeight(item.getWeight());
            param.setFinishDate(item.getFinishDate());
            param.setEmployeeRed(item.isEmployeeRed());
            param.setDelegatorRed(item.isDelegatorRed());
            param.setEmployeeScore(item.getEmployeeScore());
            param.setEmployeeComment(item.getEmployeeComment());

            if (!isDelegator) {
                param.setFirstScore(item.getFirstScore());
                param.setSecondScore(item.getSecondScore());

                param.setEmployeeConfirm(item.isTSEConfirm());
                param.setDelegatorConfirm(item.isTSDConfirm());

                param.setApaComments(commentService.findByTopicByIdAsc(CommentTypeEnum.APA, item.getId()));
            }

            param.setKpis(findAllKPIByTarget(target, item));
            param.setTSComments(commentService.findByTopicByIdAsc(CommentTypeEnum.TS, item.getId()));

            whats.add(param);
        }

        KPIUser user            = findKPIUserByTarget(target);
        TargetHow how           = findHowByTarget(target);
        List<Comment> comments  = findAllOverallByTarget(target);

        KPIParam kpi = KPIParam.builder()
                .target(target)
                .overall(comments)
                .whats(whats)
                .how(how)
                .user(user)
                .build();


        return kpi;
    }

    @Override
    public Target updateHow(Target target, TargetHow param, User user) {
        Assert.notNull(target, "The [target] argument is null");
        Assert.notNull(param, "The [how] argument is null");

        TargetHow data = howService.findByTarget(target);
        if (null == data) {
            data = TargetHow.builder()
                    .targetId(target.getId())
                    .employeeId(user.getId())
                    .functionId(target.getFunctionId())
                    .build();
        }

        data.setHow1(param.getHow1());
        data.setHow1Weight(25);
        data.setHow2(param.getHow2());
        data.setHow2Weight(25);
        data.setHow3(param.getHow3());
        data.setHow3Weight(25);
        data.setHow4(param.getHow4());
        data.setHow4Weight(25);

        data.setDelegatorConfirm(false);
        data.setEmployeeConfirm(false);

        if (null == howService.update(data)) {
            return null;
        }

        target.setTSESubmit(false);
        target.setTSDConfirm(false);
        target.setTSEConfirm(false);

        return update(target);
    }

    @Override
    public Target updateWhat(Target target, WhatParam param, User user) {
        Assert.notNull(target, "The [target] argument is null");
        Assert.notNull(param, "The [what] argument is null");
        Assert.notNull(user, "The [user] argument is null");

        TargetWhat data;
        if (param.getWhatId() != null) {
            if (null == ( data = findWhatById(param.getWhatId()) ) || !user.getId().equals(data.getEmployeeId())) {
                return null;
            }
        } else {
            data = TargetWhat.builder()
                    .functionId(param.getFunctionId())
                    .employeeId(user.getId())
                    .targetId(target.getId())
                    .build();
        }

        data.setName(param.getName());
        data.setDesc(param.getDesc());
        data.setWeight(param.getWeight());
        data.setFinishDate(param.getFinishDate());
        data.setTSEConfirm(false);
        data.setTSDConfirm(false);

        data.setFirstScore(null);
        data.setSecondScore(null);
        data.setEmployeeScore(null);
        data.setEmployeeComment(null);


        if (null == whatService.update(target, data)) {
            return null;
        }


        if (null != param.getKpis() && param.getKpis().size() > 0) {
            List<TargetKPI> list = kpiService.findByTarget(target, data);
            Map<Integer, TargetKPI> map = new HashMap<>();
            if (list.size() != 0) {
                for (TargetKPI kpi : list) {
                    map.put(kpi.getIndex(), kpi);
                }
            }

            // 为每个KPI检查是否是更新，还是新增
            for (TargetKPI kpi : param.getKpis()) {
                TargetKPI exist = map.get(kpi.getIndex());
                if (null != exist) {
                    kpi.setId(exist.getId());
                    kpi.setCreateAt(exist.getCreateAt());
                }

                if (null == kpi.getIndex()) {
                    kpi.setIndex(0);
                }

                kpi.setWhatId(data.getId());
                kpi.setTargetId(target.getId());

                kpiService.update(kpi);
            }
        }

        target.setTSESubmit(false);
        target.setTSDConfirm(false);
        target.setTSEConfirm(false);

        return update(target);
    }

    @Override
    public Target deleteWhat(Target target, TargetWhat what) {
        if (null == whatService.deleteByTarget(what) ) {
            return null;
        }

        target.setTSESubmit(false);
        target.setTSDConfirm(false);
        target.setTSEConfirm(false);

        return update(target);
    }

    @Override
    public Target rejectWhatByDelegator(Target target, TargetWhat what, Comment comment) {
        Assert.notNull(target, "The [target] argument is null");
        Assert.notNull(what, "The [what] argument is null");
        Assert.notNull(comment, "The [comment] argument is null");

        if (!what.isEmployeeRed()) {
            what.setEmployeeRed(true);

            whatService.update(what);
        }

        target.setTSESubmit(false);
        target.setTSDConfirm(false);
        target.setTSEConfirm(false);

        target.setAPAESubmit(false);
        target.setAPADSubmit(false);

        return update(target);
    }

    @Override
    public Target confirmByEmployee(Target target) {
        TargetHow how = howService.confirmByDelegator(target);
        if (null == how) {
            return null;
        }

        if (!how.isEmployeeConfirm()) {
            howService.confirmByEmployee(target);
        }

        List<TargetWhat> whats = whatService.findAllByTarget(target);
        for (TargetWhat item : whats) {
            if (!item.isTSDConfirm()) {
                // 经理未Confirm
                return null;
            }

            if (!item.isTSEConfirm()) {
                whatService.tsEConfirm(target, item);
            }
        }

        target.setTSEConfirm(true);

        return update(target);
    }

    @Override
    public Target confirmByDelegator(Target target) {
        TargetHow how = howService.confirmByDelegator(target);
        if (null == how) {
            return null;
        }

        List<TargetWhat> whats = whatService.findAllByTarget(target);
        if (whats.size() == 0) {
            return null;
        }

        for (TargetWhat item : whats) {
            if (!item.isTSDConfirm()) {
                whatService.tsDConfirm(target, item);
            }
        }


        target.setTSDConfirm(true);
        target.setTSEConfirm(false);

        return update(target);
    }

    @Override
    public Target autoTargetSetting(Function function, User user) throws Exception {
        // 创建kpiUser，并且创建Target
        kpiUserService.builder(function, user,
                null == user.getManagerId() ? null : userService.findById(user.getManagerId()),
                null == user.getDepartmentId() ? null : departmentService.findById(user.getDepartmentId()),
                null == user.getPositionId() ? null : positionService.findById(user.getPositionId()));

        Target target = findByUser(function, user);
        if (null == target) {
            throw new Exception("创建失败");
        }

        List<TargetWhat> whats = whatService.findAllByTarget(target);
        if (whats.size() > 0) {
            // 把kpi和对应的数据都删除
            kpiService.deleteByTarget(target);
            whatService.deleteByTarget(target);
        }

        // 已经存在，不需要创建对应的What列表
        WhatParam what = new WhatParam();
        what.setName(AutoWhatName);
        what.setDesc(AutoWhatName);
        what.setWeight(100);
        what.setFinishDate(function.getLastDay());
        // 生成3个Kpi
        for (int i=3, len=5; i<=len; ++ i) {
            TargetKPI kpi = TargetKPI.builder()
                    .index(i-1)
                    .targetId(target.getId())
                    .desc(AutoWhatName)
                    .build();

            if (what.getKpis() == null) {
                what.setKpis(new ArrayList<>());
            }

            what.getKpis().add(kpi);
        }

        // 新增一条What
        if (null == ( target = updateWhat(target, what, user) )) {
            // 新增What出错
            throw new Exception("新增What失败");
        }


        TargetHow how = howService.findByTarget(target);
        if (how != null) {
            howService.deleteByTarget(target);
        }

        // 新增一条What
        how = TargetHow.builder()
                .how1("")
                .how2("")
                .how3("")
                .how4("")
                .build();

        if (null == ( target = updateHow(target, how, user) )) {
            // 新增What出错
            throw new Exception("新增How失败");
        }

        // 员工提交Target
        // 经理先确认What
        if (null == ( target = tsESubmit(function, user) )) {
            // 经理先确认What
            throw new Exception("提交失败");
        }

        // 经理先确认What
        if (null == ( target = confirmByDelegator(target) )) {
            // 经理先确认What
            throw new Exception("确认失败");
        }

        // 员工再确认What
        if (null == ( target = confirmByEmployee(target) )) {
            // 确认What出错
            throw new Exception("确认失败");
        }

        return target;
    }

    // 系统自动对长假人员打分
    @Override
    public Target autoAppraisal(Function function, User user, ScoreLevelEnum levelEnum) {
        Target target = findByUser(function, user);
        if (null == target) {
            return null;
        }

        IBounds bounds = user.getNeedHands()
                ? BoundsFactory.getByYearAndManager(BoundsYearEnum.from(function.getYear()))
                : BoundsFactory.getByYearAndEmployee(BoundsYearEnum.from(function.getYear()));

        int totalHowScore = bounds.getScoreByLevel(levelEnum);

        List<TargetWhat> whats = findAllWhatByTarget(target);
        WhatParam whatAppraisal = new WhatParam();
        whatAppraisal.setEmployeeComment("BY APA System Robot");
        whatAppraisal.setEmployeeScore(3.0f);

        double[] howScore = new double[4];

        // 模拟工员自评
        for (TargetWhat item : whats) {
            eAppraisalWhat(function, user, target, item, whatAppraisal);
        }

        TargetHow how = findHowByTarget(target);
        TargetHow howAppraisal = null;
        if (null != how) {
            howScore[0] = totalHowScore * ( how.getHow1Weight() / 100.0f );
            howScore[0] /= how.getHow1Weight();
            howScore[1] = totalHowScore * ( how.getHow2Weight() / 100.0f );
            howScore[1] /= how.getHow2Weight();
            howScore[2] = totalHowScore * ( how.getHow3Weight() / 100.0f );
            howScore[2] /= how.getHow3Weight();
            howScore[3] = totalHowScore * ( how.getHow4Weight() / 100.0f );
            howScore[3] /= how.getHow4Weight();


            howScore[0] = Math.ceil(howScore[0]);
            howScore[1] = Math.ceil(howScore[1]);
            howScore[2] = Math.ceil(howScore[2]);
            howScore[3] = Math.ceil(howScore[3]);

            if (howAppraisal == null) {
                howAppraisal = TargetHow.builder()
                        .how1EScore((float) howScore[0])
                        .how2EScore((float) howScore[1])
                        .how3EScore((float) howScore[2])
                        .how4EScore((float) howScore[3])
                        .how1DOScore((float) howScore[0])
                        .how2DOScore((float) howScore[1])
                        .how3DOScore((float) howScore[2])
                        .how4DOScore((float) howScore[3])
                        .how1DTScore((float) howScore[0])
                        .how2DTScore((float) howScore[1])
                        .how3DTScore((float) howScore[2])
                        .how4DTScore((float) howScore[3])
                        .build();
            }

            target = eAppraisalHow(function, user, target, howAppraisal);
        }

        try {
            target = apaESubmit(function, user, true);
        } catch (TargetException e) {
            e.printStackTrace();
        }

        // 模拟经理自抨
        for (TargetWhat item : whats) {
            target = firstWhatAppraisal(function, user, target, item, 5.0f);
        }

        if (null != how) {
            target = firstHowAppraisal(function, user, target, howAppraisal);
        }

        try {
            target = apaDOSubmit(function, user, true);
        } catch (TargetException e) {
            e.printStackTrace();
        }

        // 模拟经理最终评分
        for (TargetWhat item : whats) {
            target = secondWhatAppraisal(function, user, target, item, 5.0f);
        }

        if (null != how) {
            target = secondHowAppraisal(function, user, target, howAppraisal);
        }

        try {
            target = apaDTSubmit(function, user, true);
        } catch (TargetException e) {
            e.printStackTrace();
        }

        target = findByUser(function, user);
        target.setAgreeScore(true);

        return update(target);
    }


    @Override
    public Target eAppraisalWhat(Function function, User user, Target target, TargetWhat what, WhatParam appraisal) {
        if (null == whatService.eAppraisal(target, what, appraisal.getEmployeeScore(), appraisal.getEmployeeComment())) {
            return null;
        }

        // 试着时时计算分数
        // 试着计算分数
        try {
            target = apaESubmit(function, user, false);
        } catch (TargetException e) {
            // 对于此处异常不做任何处理
        }

        target.setAPAESubmit(false);
        target.setAPADSubmit(false);

        return update(target);
    }

    @Override
    public Target eAppraisalHow(Function function, User user, Target target, TargetHow appraisal) {
        if (null == howService.eAppraisal(function, target, appraisal)) {
            return null;
        }

        // 试着计算分数
        try {
            target = apaESubmit(function, user, false);
        } catch (TargetException e) {
            // 对于此处异常不做任何处理
        }

        target.setAPAESubmit(false);
        target.setAPADSubmit(false);

        return update(target);
    }

    @Override
    public Target tsESubmit(Function function, User user) throws TargetException {
        Target target = findByUser(function, user);
        if (null == target) {
            throw TargetException.TargetSettingIsEmpty(target);
        }

        if (!canPass(target)) {
            throw TargetException.TargetCheckFail(target);
        }

        // 检查是否有how数据，如果没有自动添加一个
        TargetHow how = howService.findByTarget(target);
        if (how == null) {
            // 新增一条What
            how = TargetHow.builder()
                    .how1("")
                    .how2("")
                    .how3("")
                    .how4("")
                    .build();

            if (null == ( target = updateHow(target, how, user) )) {
                // 新增What出错
                throw TargetException.TargetCheckFail(target);
            }
        }

        target.setTSESubmit(true);

        return update(target);
    }

    @Override
    public Target apaESubmit(Function function, User user, boolean official) throws TargetException {
        Target target = findByUser(function, user);
        if (null == target) {
            throw TargetException.TargetSettingIsEmpty(target);
        }

        // 计算How的分数
        float total = 0f;
        TargetHow how = findHowByTarget(target);
        if (null == how || null == how.getHow1EScore() || null == how.getHow2EScore() || null == how.getHow3EScore() || null == how.getHow4EScore()) {
            return target;
        }

        total += how.getHow1Weight() * how.getHow1EScore();
        total += how.getHow2Weight() * how.getHow2EScore();
        total += how.getHow3Weight() * how.getHow3EScore();
        total += how.getHow4Weight() * how.getHow4EScore();



        // 得到 How 数据的统计信息
        target.setEmployeeHowScore(Math.round(total));


        // 得到 What 数据统计
        total = 0;
        List<TargetWhat> whats = whatService.findAllByTarget(target);
        for (TargetWhat item : whats) {
            if (item.getWeight() == null || item.getEmployeeScore() == null) {
                return target;
            }

            // 每个 What 分数根据是否是员工还是经理决定是否=减 1.0f
            total += user.getNeedHands()
                    ? (item.getEmployeeScore() - 1.0f) * item.getWeight()
                    : item.getEmployeeScore() * item.getWeight();
        }

//        // 正式处理，才为经理生成红点提示
//        if (official) {
//            how.setDelegatorRed(true);
//            howService.update(how);
//
//            for (TargetWhat item : whats) {
//                item.setDelegatorRed(true);
//
//                whatService.update(item);
//            }
//
//            target.setAPAESubmit(true);
//        }

        // 正式处理，设置提交数据
        if (official) {
            target.setAPAESubmit(true);
        }


        target.setEmployeeWhatScore(Math.round(total));

        IBounds bounds = user.getNeedHands()
                ? BoundsFactory.getByYearAndManager(BoundsYearEnum.from(function.getYear()))
                : BoundsFactory.getByYearAndEmployee(BoundsYearEnum.from(function.getYear()));

        target.setEmployeeTotalScore(bounds.getFinalScore(target.getEmployeeWhatScore(), target.getEmployeeHowScore()));
        target.setEmployeeScoreLevel(bounds.getLevelByScore(target.getEmployeeTotalScore()));
        if (user.getNeedHands()) {
            target.setEmployeeWhatRate(bounds.getWhatRate(target.getEmployeeWhatScore()));
        }


        return update(target);
    }


    @Override
    public Target apaDOSubmit(Function function, User user, boolean official) throws TargetException {
        Target target = findByUser(function, user);
        if (null == target) {
            throw TargetException.TargetSettingIsEmpty(target);
        }

        if (target.isAPADSubmit()) {
            return target;
        }

        // 计算How的分数
        float total = 0f;
        TargetHow how = findHowByTarget(target);
        if (null == how || null == how.getHow1DOScore() || null == how.getHow2DOScore() || null == how.getHow3DOScore() || null == how.getHow4DOScore()) {
            // 没有分数
            return target;
        }

        total += how.getHow1Weight() * how.getHow1DOScore();
        total += how.getHow2Weight() * how.getHow2DOScore();
        total += how.getHow3Weight() * how.getHow3DOScore();
        total += how.getHow4Weight() * how.getHow4DOScore();

        // 得到 How 数据的统计信息
        target.setFirstHowScore(Math.round(total));


        // 得到 What 数据统计（需要 - 100）
        total = 0;
        List<TargetWhat> whats = whatService.findAllByTarget(target);
        for (TargetWhat item : whats) {
            if (item.getWeight() == null || item.getFirstScore() == null) {
                return target;
            }

            // 每个 What 分数根据是否是员工还是经理决定是否减 1.0f
            total += user.getNeedHands()
                    ? (item.getFirstScore() - 1.0f) * item.getWeight()
                    : item.getFirstScore() * item.getWeight();
        }


        // 是否只是预打分计算，不设置状态
        if (official) {
            target.setAPADSubmit(true);
        }

        target.setFirstWhatScore(Math.round(total));

        IBounds bounds = user.getNeedHands()
                ? BoundsFactory.getByYearAndManager(BoundsYearEnum.from(function.getYear()))
                : BoundsFactory.getByYearAndEmployee(BoundsYearEnum.from(function.getYear()));

        target.setFirstTotalScore(bounds.getFinalScore(target.getFirstWhatScore(), target.getFirstHowScore()));
        target.setFirstScoreLevel(bounds.getLevelByScore(target.getFirstTotalScore()));
        if (user.getNeedHands()) {
            target.setFirstWhatRate(bounds.getWhatRate(target.getFirstWhatScore()));
        }



        return update(target);
    }


    @Override
    public Target apaDTSubmit(Function function, User user, boolean official) throws TargetException {
        Target target = findByUser(function, user);
        if (null == target) {
            throw TargetException.TargetSettingIsEmpty(target);
        }

        if (target.isReleaseSecond()) {
            return target;
        }


        // 计算How的分数
        float total = 0f;
        TargetHow how = findHowByTarget(target);
        if (null == how || null == how.getHow1DOScore() || null == how.getHow2DOScore() || null == how.getHow3DOScore() || null == how.getHow4DOScore()) {
            // 第一次没有打分
            return null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // 如果经理只进行了第一次打分，没有进行第二次打分，直接使用第一次的分数做为最终得分
        //

        if (null == how.getHow1DTScore() || null == how.getHow2DTScore() || null == how.getHow3DTScore() || null == how.getHow4DTScore()) {
            if (null == how.getHow1DTScore() && null != how.getHow1DOScore()) {
                how.setHow1DTScore(how.getHow1DOScore());
            }

            if (null == how.getHow2DTScore() && null != how.getHow2DOScore()) {
                how.setHow2DTScore(how.getHow2DOScore());
            }

            if (null == how.getHow3DTScore() && null != how.getHow3DOScore()) {
                how.setHow3DTScore(how.getHow3DOScore());
            }

            if (null == how.getHow4DTScore() && null != how.getHow4DOScore()) {
                how.setHow4DTScore(how.getHow4DOScore());
            }

            secondHowAppraisal(function, user, target, how);
        }

        //
        // 如果经理只进行了第一次打分，没有进行第二次打分，直接使用第一次的分数做为最终得分
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        total += how.getHow1Weight() * how.getHow1DTScore();
        total += how.getHow2Weight() * how.getHow2DTScore();
        total += how.getHow3Weight() * how.getHow3DTScore();
        total += how.getHow4Weight() * how.getHow4DTScore();


        // 得到 How 数据的统计信息
        target.setSecondHowScore(Math.round(total));


        // 得到 What 数据统计（需要 - 100）
        total = 0;
        List<TargetWhat> whats = whatService.findAllByTarget(target);
        for (TargetWhat item : whats) {
            if (item.getWeight() == null || item.getFirstScore() == null) {
                return null;
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // 如果经理只进行了第一次打分，没有进行第二次打分，直接使用第一次的分数做为最终得分
            //

            if (null == item.getSecondScore()) {
                item.setSecondScore(item.getFirstScore());

                secondWhatAppraisal(function, user, target, item, item.getFirstScore());
            }

            //
            // 如果经理只进行了第一次打分，没有进行第二次打分，直接使用第一次的分数做为最终得分
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // 每个 What 分数根据是否是员工还是经理决定是否减 1.0f
            total += user.getNeedHands()
                    ? (item.getSecondScore() - 1.0f) * item.getWeight()
                    : item.getSecondScore() * item.getWeight();
        }

        target.setSecondWhatScore(Math.round(total));

        IBounds bounds = user.getNeedHands()
                ? BoundsFactory.getByYearAndManager(BoundsYearEnum.from(function.getYear()))
                : BoundsFactory.getByYearAndEmployee(BoundsYearEnum.from(function.getYear()));

        target.setSecondTotalScore(bounds.getFinalScore(target.getSecondWhatScore(), target.getSecondHowScore()));
        target.setSecondScoreLevel(bounds.getLevelByScore(target.getSecondTotalScore()));
        if (user.getNeedHands()) {
            target.setSecondWhatRate(bounds.getWhatRate(target.getSecondWhatScore()));
        }

        if (official) {
            target.setReleaseSecond(true);
            target.setReleaseDate(new Date());
        }

        return update(target);
    }

    @Override
    public Target firstWhatAppraisal(Function function, User employee, Target target, TargetWhat what, Float score) {
        if (null == whatService.firstAppraisal(target, what, score)) {
            return null;
        }

        // 试着计算分数
        try {
            apaDOSubmit(function, employee, false);
        } catch (TargetException e) {
            // 对于此处异常不做任何处理
        }

        target.setAPADSubmit(false);

        return update(target);
    }

    @Override
    public Target secondWhatAppraisal(Function function, User employee, Target target, TargetWhat what, Float score) {
        if (null == whatService.secondAppraisal(target, what, score)) {
            return null;
        }

        // 试着计算分数
        try {
            apaDTSubmit(function, employee, false);
        } catch (TargetException e) {
            // 对于此处异常不做任何处理
        }

        return target;
    }

    @Override
    public Target firstHowAppraisal(Function function, User employee, Target target, TargetHow appraisal) {
        TargetHow how = howService.delegatorFirstAppraisal(function, target, appraisal);
        if (null == how) {
            return null;
        }

        // 试着计算分数
        try {
            apaDOSubmit(function, employee, false);
        } catch (TargetException e) {
            // 对于此处异常不做任何处理
        }

        return target;
    }

    @Override
    public Target secondHowAppraisal(Function function, User employee, Target target, TargetHow appraisal) {
        TargetHow how = howService.delegatorSecondAppraisal(function, target, appraisal);
        if (null == how) {
            return null;
        }

        // 试着计算分数
        try {
            apaDTSubmit(function, employee, false);
        } catch (TargetException e) {
            // 对于此处异常不做任何处理
        }

        return target;
    }

    @Override
    public List<Target> findAllByManager(Function function, User delegator) {
        return repository.findAllByFunctionIdAndManagerId(function.getId(), delegator.getId());
    }

    @Override
    public List<Target> findAllByDelegator(Function function, User delegator) {
        return repository.findAllByFunctionIdAndDelegatorId(function.getId(), delegator.getId());
    }

    @Override
    public List<Target> findAllByDepartment(Function function, Department department) {
        return repository.findAllByFunctionIdAndDepartmentId(function.getId(), department.getId());
    }

    @Override
    public Page<Target> findAllByDepartment(Function function, Department department, int page, int size) {
        return repository.findAllByFunctionIdAndDepartmentId(function.getId(), department.getId(), PageRequest.of(page -1, size));
    }

    @Override
    public TargetHow findHowByTarget(Target target) {
        return howService.findByTarget(target);
    }

    @Override
    public KPIUser findKPIUserByTarget(Target target) {
        return kpiUserService.findByTarget(target);
    }

    @Override
    public TargetWhat findWhatById(Integer whatId) {
        return whatService.get(whatId);
    }

    @Override
    public TargetHow findHowById(Integer howId) {
        return howService.findById(howId);
    }

    @Override
    public List<TargetWhat> findAllWhatByTarget(Target target) {
        return whatService.findAllByTarget(target);
    }

    @Override
    public List<TargetKPI> findAllKPIByTarget(Target target, TargetWhat what) {
        return kpiService.findByTarget(target, what);
    }

    @Override
    public List<Comment> findAllOverallByTarget(Target target) {
        return commentService.findByTopicByIdAsc(CommentTypeEnum.OV, target.getId());
    }

    @Override
    public Map<String, Map<String, String>> check(Target target) {
        Assert.notNull(target, "The [target] argument is null");

        Map<String, Map<String, String>> notices = new HashMap<>();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 检查 What列表 是否有错误
        ///
        List<TargetWhat> whats = whatService.findAllByTarget(target);
        Map<String, Map<String, String>> itemNotices = whatService.check(whats);
        for (String key : itemNotices.keySet()) {
            notices.put(key, itemNotices.get(key));
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 检查 KPI列表 是否有错误
        ///
        for (TargetWhat what : whats) {
            itemNotices = kpiService.check(target, what);
            for (String key : itemNotices.keySet()) {
                notices.put(key, itemNotices.get(key));
            }
        }

//        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        /// 检查 How 是否有错误
//        ///
//        TargetHow how = howService.findByTarget(target);
//        itemNotices = howService.check(how);
//        for (String key : itemNotices.keySet()) {
//            notices.put(key, itemNotices.get(key));
//        }

        /// 检查完毕
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        return notices;
    }

    @Override
    public boolean canPass(Target target) {
        Map<String, Map<String, String>> result = check(target);
        for (String key : result.keySet()) {
            Map<String, String> item = result.get(key);
            if (null == item) {
                log.debug("检查未通过：{} 为： {}", key, item);
                return false;
            }

            for (String sk : item.keySet()) {
                if (null == item.get(sk)) {
                    log.debug("检查未通过：{} 为： {}", key, item);
                    return false;
                }
            }
        }

        return true;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// My Team DashBroad 处 的图表数据
    ///

    @Override
    public int countTSByNotStatedByDepartment(Function function, Department department) {
        return repository.countTSByNotStartedByDepartmentId(function.getId(), department.getId());
    }

    @Override
    public int countTSBySubmittedByDepartment(Function function, Department department) {
        return repository.countTSBySubmittedByDepartmentId(function.getId(), department.getId());
    }

    @Override
    public int countTSByReviewedByDepartment(Function function, Department department) {
        return repository.countTSByReviewedByDepartmentId(function.getId(), department.getId());
    }

    @Override
    public int countTSByConfirmedByDepartment(Function function, Department department) {
        return repository.countTSByConfirmedByDepartmentId(function.getId(), department.getId());
    }

    @Override
    public int countAPAByNotStatedByDepartment(Function function, Department department) {
        return 0;
    }

    @Override
    public int countAPABySubmittedByDepartment(Function function, Department department) {
        return 0;
    }

    @Override
    public int countAPAByReviewedByDepartment(Function function, Department department) {
        return 0;
    }

    @Override
    public int countAPAByConfirmedByDepartment(Function function, Department department) {
        return 0;
    }

    ///
    /// My Team DashBroad 处 的图表数据
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Reporting 处 的图表数据
    ///

    @Override
    public int countTSByNotStartByDepartment1(Function function, Department department) {
        return repository.countByNotStartedByDepartment1(function.getId(), department.getId());
    }

    @Override
    public int countTSByInProgressByDepartment1(Function function, Department department) {
        return repository.countByInProgressByDepartment1(function.getId(), department.getId());
    }

    @Override
    public int countTSByFinishedByDepartment1(Function function, Department department) {
        return repository.countByFinishedByDepartment1(function.getId(), department.getId());
    }

    @Override
    public int countAPAByNotStartByDepartment1(Function function, Department department) {
        return 0;
    }

    @Override
    public int countAPAByInProgressByDepartment1(Function function, Department department) {
        return 0;
    }

    @Override
    public int countAPAByFinishedByDepartment1(Function function, Department department) {
        return 0;
    }

    @Override
    public int countTSByNotStartByDepartment2(Function function, Department department) {
        return repository.countTSByNotStartedByDepartment2(function.getId(), department.getId());
    }

    @Override
    public int countTSByInProgressByDepartment2(Function function, Department department) {
        return repository.countTSByInProgressByDepartment2(function.getId(), department.getId());
    }

    @Override
    public int countTSByFinishedByDepartment2(Function function, Department department) {
        return repository.countTSByFinishedByDepartment2(function.getId(), department.getId());
    }

    @Override
    public int countAPAByNotStartByDepartment2(Function function, Department department) {
        return repository.countAPAByNotStartedByDepartment2(function.getId(), department.getId());
    }

    @Override
    public int countAPAByInProgressByDepartment2(Function function, Department department) {
        return repository.countAPAByInProgressByDepartment2(function.getId(), department.getId());
    }

    @Override
    public int countAPAByFinishedByDepartment2(Function function, Department department) {
        return repository.countAPAByFinishedByDepartment2(function.getId(), department.getId());
    }

    @Override
    public int countTSByNotStartByDepartment3(Function function, Department department) {
        return repository.countTSByNotStartedByDepartment3(function.getId(), department.getId());
    }

    @Override
    public int countTSByInProgressByDepartment3(Function function, Department department) {
        return repository.countTSByInProgressByDepartment3(function.getId(), department.getId());
    }

    @Override
    public int countTSByFinishedByDepartment3(Function function, Department department) {
        return repository.countTSByFinishedByDepartment3(function.getId(), department.getId());
    }

    @Override
    public int countAPAByNotStartByDepartment3(Function function, Department department) {
        return repository.countAPAByNotStartedByDepartment3(function.getId(), department.getId());
    }

    @Override
    public int countAPAByInProgressByDepartment3(Function function, Department department) {
        return repository.countAPAByInProgressByDepartment3(function.getId(), department.getId());
    }

    @Override
    public int countAPAByFinishedByDepartment3(Function function, Department department) {
        return repository.countAPAByFinishedByDepartment3(function.getId(), department.getId());
    }


    ///
    /// Reporting 处 的图表数据
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
