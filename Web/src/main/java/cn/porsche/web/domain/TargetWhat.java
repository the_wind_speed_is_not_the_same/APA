package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "target_what", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class TargetWhat implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @ApiModelProperty(notes = "KPI主键")
    @JsonProperty("function_id")
    @Column(name = "function_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'KPI主键'")
    Integer functionId;

    @ApiModelProperty(notes = "TS主键")
    @JsonProperty("target_id")
    @Column(name = "target_id",                 columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'TS主键'")
    Integer targetId;

    @ApiModelProperty(notes = "用户主键")
    @JsonProperty("employee_id")
    @Column(name = "employee_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT '用户主键'")
    Integer employeeId;

    @ApiModelProperty(notes = "名称")
    @JsonProperty("name")
    @Column(name = "name",                      columnDefinition = "VARCHAR(1024) NULL COMMENT '名称'")
    String name;

    @ApiModelProperty(notes = "描述")
    @JsonProperty("desc")
    @Column(name = "`desc`",                    columnDefinition = "VARCHAR(1024) NULL COMMENT '描述'")
    String desc;

    @ApiModelProperty(notes = "权重")
    @JsonProperty("weight")
    @Column(name = "weight",                    columnDefinition = "TINYINT(3) UNSIGNED NULL COMMENT '权重'")
    Integer weight;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(notes = "结束时间")
    @JsonProperty("finish_date")
    @Column(name = "finish_date",               columnDefinition = "DATE NULL COMMENT '权重'")
    Date finishDate;

    @ApiModelProperty(notes = "员工是否已全部Confirm")
    @JsonProperty("is_ts_employee_confirm")
    @Column(name = "is_ts_employee_confirm",    columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '员工是否已Confirm'")
    boolean TSEConfirm;

    @ApiModelProperty(notes = "小组长是否已全部Confirm")
    @JsonProperty("is_ts_delegator_confirm")
    @Column(name = "is_ts_delegator_confirm",   columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '小组长是否Confirm'")
    boolean TSDConfirm;

    @ApiModelProperty(notes = "员工自评分数")
    @JsonProperty("employee_score")
    @Column(name = "employee_score",            columnDefinition = "FLOAT(2,1) NULL COMMENT '员工自评'")
    Float employeeScore;

    @ApiModelProperty(notes = "员工自评说明")
    @JsonProperty("employee_comment")
    @Column(name = "employee_comment",          columnDefinition = "VARCHAR(1024) NULL COMMENT '员工自评说明'")
    String employeeComment;

    @ApiModelProperty(notes = "经理第一次评分")
    @JsonProperty("first_score")
    @Column(name = "first_score",               columnDefinition = "FLOAT(2,1) NULL COMMENT '委托人第一次评分'")
    Float firstScore;

    @ApiModelProperty(notes = "经理第二次评分")
    @JsonProperty("second_score")
    @Column(name = "second_score",              columnDefinition = "FLOAT(2,1) NULL COMMENT '委托人第一次评分'")
    Float secondScore;

    @ApiModelProperty(notes = "向小组长展示的红点")
    @JsonProperty("delegator_red")
    @Column(name = "delegator_red",             columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '向小组长展示的红点'")
    boolean delegatorRed;

    @ApiModelProperty(notes = "向员工展示的红点")
    @JsonProperty("employee_red")
    @Column(name = "employee_red",             columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '向员工展示的红点'")
    boolean employeeRed;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
