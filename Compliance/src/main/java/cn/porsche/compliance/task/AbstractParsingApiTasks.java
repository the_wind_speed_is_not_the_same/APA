package cn.porsche.compliance.task;

import cn.porsche.common.util.JsonUtils;
import cn.porsche.common.util.http.HttpException;
import cn.porsche.common.util.http.HttpUtils;
import cn.porsche.compliance.service.DepartmentService;
import cn.porsche.compliance.service.PositionService;
import cn.porsche.compliance.service.UserService;
import cn.porsche.compliance.task.parsing.exception.ParsingException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;

@Log4j2
public abstract class AbstractParsingApiTasks {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${spring.profiles.active}")
    protected String profile;

    protected final static String PCNToken          = "P(S)?CN\\d+";

    private final static String TestDate            = "20200830";
    private final static String DateFormatString    = "yyyMMdd";

    private final static String ApiHeaderKey        = "apikey";
    private final static String ApiHeaderValue      = "LmdkMuu5bavHDQ2";
//    private final static String ApiHeaderValue      = "yI6X025ERcgvFOeFDpzbrIraP9Qn2yGd";

    private final static String ParamPageIndexKey   = "pageIndex";
    private final static String ParamSizeKey        = "pageSize";
    private final static String ParamPageKey        = "page";

    private final static int ParamPageValue         = 1;
    private final static int ParamSizeValue         = 50;

    private final static int HttpStatusCode         = 200;

    private final static String HttpStatusKey       = "code";
    private final static String HttpItemsKey        = "items";
    private final static String HttpMaxPageKey      = "totalPage";


    protected final static String ProdEmployeeURI   = "http://apigwaws.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/EmployeeApi?date=%s";
    protected final static String ProdPositionURI   = "http://apigwaws.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/PositionApi?date=%s";
    protected final static String ProdDepartmentURI = "http://apigwaws.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/DepartmentApi?date=%s";


    // departmentKeys
    //  DEPARTMENTID DEPARTMENTNAME PARENTDEPARTMENDID Company Code-Company
    protected final static String[] dpKeys = {"DEPARTMENTID", "DEPARTMENTNAME", "PARENTDEPARTMENDID", "Company_Code_Company"};


    // employeeKeys
    //  0UserID 1FIRSTNAMEEN 2LASTNAMEEN 3USERNAMECN 4USERCODE 5JOBCODEID 6DEPARTMENTID 7DEPARTMENTNAME 8COSTCENTERCODE 9EMAIL
    //      10ORGANIZATION 11MOBILENO 12TEL 13Preferred Name
    protected final static String[] emKeys = {"UserID", "FIRSTNAMEEN", "LASTNAMEEN", "USERNAMECN", "USERCODE",
            "JOBCODEID", "DEPARTMENTID", "DEPARTMENTNAME", "COSTCENTERCODE", "EMAIL", "ORGANIZATION", "MOBILENO", "TEL", "Preferred_Name"};

    // employeeKeys
    //  JOBCODEID JOBCODE JOBFUNCTION SUPERIORJOBCODEID	POSITIONOCCUPIED Company Code
    protected final static String[] poKeys = {"JOBCODEID", "JOBCODE", "JOBFUNCTION", "SUPERIORJOBCODEID", "POSITIONOCCUPIED", "Company_Code"};


    protected enum API {
        First(null),
        Department(ProdDepartmentURI),
        Position(ProdPositionURI),
        Employee(ProdEmployeeURI),
        ;

        API(String uri) {
            this.uri = uri;
        }

        protected String uri;

        public String getUri() {
            return this.uri;
        }

        public API first()  {
            if (this.ordinal() < 0) {
                return null;
            }

            return values()[0];
        }

        public API next() {
            int idx = this.ordinal() + 1;
            API[] urls = values();
            if (idx >= urls.length) {
                return null;
            }

            return urls[idx];
        }

        public boolean hasNext() {
            return this.ordinal() + 1 < values().length;
        }
    }

    @Autowired
    protected UserService userService;

    @Autowired
    protected PositionService positionService;

    @Autowired
    protected DepartmentService departmentService;



    // 解析外部系统提供过来的文件
    protected abstract void parsing(List<String[]> list, API api) throws ParsingException;


    // 每天最后一秒执行文件
    @Scheduled(cron = "01 01 22 1/1 * ?")
    public void cron() {
        try {
            running();
        } catch (ParsingException e) {
            e.printStackTrace();

            logger.debug(e.getMessage());
        }
    }

    protected boolean filter(String code) {
        return false;
    }

    @Transactional(rollbackFor = ParsingException.class) // 需要开启事务支持
    protected void running() throws ParsingException {
        logger.debug("{} -------------> 准备解析 API 接口", new Date());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);

        String date = new SimpleDateFormat(DateFormatString).format(calendar.getTime());

        List<Header> headers = new ArrayList<>();
        headers.add(new BasicHeader(ApiHeaderKey, ApiHeaderValue));

        Map<String, Integer> params = new HashMap<>();



        API api = API.First;
        BasicHttpEntity entity = new BasicHttpEntity();


        String[] keys;
        String[] values;
        JSONArray list;
        JSONObject item;
        JSONObject json;


        List<String[]> data;
        Object value;
        String response = null;
        do {
            int page = ParamPageValue;
            int size = ParamSizeValue;
            int max  = ParamPageValue;

            api  = api.next();
            data = new ArrayList<>();

            logger.debug("访问 -------------> {}", api.getUri());
            switch (api) {
                case Employee: keys = emKeys; break;
                case Position: keys = poKeys; break;
                case Department: keys = dpKeys; break;
                default: keys = null;
            }

            while (page <= max) {
                params.clear();

                params.put(ParamPageIndexKey, page);
                params.put(ParamSizeKey, size);


                try {
                    entity.setContent(new ByteArrayInputStream(JsonUtils.toString(params).getBytes("UTF-8")));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return;
                }

                try {
                    response = HttpUtils.post(String.format(api.getUri(), date), entity, headers);
                    logger.debug("返回 -------------> {}", response);

                    json = new JSONObject(response);
                } catch (HttpException e) {
                    e.printStackTrace();

                    throw ParsingException.ParsingApiException(response);
                } catch (Exception e) {
                    e.printStackTrace();

                    throw ParsingException.ParsingApiException(response);
                }

                if (HttpStatusCode != json.getInt(HttpStatusKey)) {
                    log.error("获取数据失败，接口地址：{}，返回状态码：{}，错误内容：{}", api.uri, json.getInt(HttpStatusKey), json.getString("msg"));

                    throw ParsingException.ParsingApiException(response);
                }

                if (keys == null) {
                    break;
                }

                list = json.getJSONArray(HttpItemsKey);
                for (int i = 0; i < list.length(); i++) {
                    if (null == ( item = list.getJSONObject(i) )) {
                        continue;
                    }

                    values = new String[keys.length];
                    for (int j = 0; j < keys.length; j++) {
                        try {
                            value = item.get(keys[j]);
                        } catch (JSONException e) {
                            value = JSONObject.NULL;
                        }

                        values[j] = value.equals(JSONObject.NULL) ? null : String.valueOf(value);
                    }

                    data.add(values);
                }

                page = json.getInt(ParamPageKey);
                size = json.getInt(ParamSizeKey);
                max  = json.getInt(HttpMaxPageKey);

                page ++;
            }

            parsing(data, api);
            logger.debug("完成 -------------> {}", api.getUri());

        } while (api.hasNext());


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ///


        ///
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        logger.debug("{} =============> API EFlow 解析完毕", new Date());
    }
}
