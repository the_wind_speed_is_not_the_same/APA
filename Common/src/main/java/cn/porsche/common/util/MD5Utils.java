package cn.porsche.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by shuzheng on 2017/1/19.
 */
public class MD5Utils {

    public final static String md5(String content) {
        StringBuffer sb;
        try {
            //指定加密算法
            MessageDigest digest = MessageDigest.getInstance("MD5");

            //将需要加密的字符串转换成byte类型的数组，然后进行随机哈希
            byte[] bs = digest.digest(content.getBytes());
            //循环遍历bs，然后让其生成32位字符串，固定写法
            sb = new StringBuffer();
            for (byte b : bs) {
                int i = b & 0xff;
                String hexString = Integer.toHexString(i);
                if (hexString.length() < 2) {
                    hexString = "0" + hexString;
                }

                //拼接字符串
                sb.append(hexString);
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static void main(String[] args) {
    }
}
