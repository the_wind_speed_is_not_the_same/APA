package cn.porsche.compliance.swagger.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.*;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Component
@EnableSwagger2
@EnableOpenApi
public class SwaggerConfiguration implements WebMvcConfigurer {
    @Bean
    public Docket createRestApi() {
        //返回文档摘要信息
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.porsche.compliance"))
//                .apis(RequestHandlerSelectors.basePackage(Application.class.getPackage().toString()))
                .paths(PathSelectors.any())
                .build();
    }

    //生成接口信息，包括标题、联系人等
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("接口文档")
                .description("接口文档")
                .contact(new Contact("段绪勇", "https://gitee.com/the_wind_speed_is_not_the_same/APA", "xuyong3@staff.weibo.com"))
                .version("1.0")
                .build();
    }
}
