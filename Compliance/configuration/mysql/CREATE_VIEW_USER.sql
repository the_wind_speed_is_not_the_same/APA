CREATE OR REPLACE VIEW user_views AS
    SELECT
    `u`.`id`,
    `u`.`code`,
    `u`.`en_name`,
	`u`.`cn_name`,
    `u`.`title`,

    ifnull(`u`.`position_id`, "") as `position_id`,
    ifnull(`u`.`department_id`, "") as `department_id`,
    ifnull(`u`.`manager_id`, "") as `manager_id`,
    ifnull(`u`.`is_manager`, "") as `is_manager`,

	ifnull(`m`.`en_name`, "") as `manager`,
	ifnull(`d`.`name`, "") as `department`,
	ifnull(`p`.`function`, "") as `position`,

	ifnull(`u`.`avatar`, "") as `avatar`,
    ifnull(`u`.`email`, "") as `email`,
    ifnull(`u`.`mobile`, "") as `mobile`,
	ifnull(`u`.`tel`, "") as `tel`

    FROM `users` AS u
    left join `users` AS m on `u`.`manager_id`=`m`.`id`
    left join `departments` AS d on `u`.`department_id` = `d`.`id`
    left join `positions` AS p on `u`.`position_id`=`p`.`id`;