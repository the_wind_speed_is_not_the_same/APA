package cn.porsche.compliance.repository;

import cn.porsche.compliance.domain.Training;
import cn.porsche.compliance.emnu.TrainingTypeEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TrainingRepository extends CrudRepository<Training, Integer> {
    Training findByUserIdAndTypeAndSubject(Integer userId, TrainingTypeEnum type, String Subject);
    Training findByYearAndUserIdAndTypeAndSubject(Integer year, Integer userId, TrainingTypeEnum type, String Subject);

    @Transactional
    int deleteAllByYearAndType(Integer year, TrainingTypeEnum type);
    int deleteAllByYearAndSubjectAndType(Integer year, String subject, TrainingTypeEnum type);
    int countByUserIdAndYearAndType(Integer userId, Integer year, TrainingTypeEnum type);
    int countByUserIdAndYearAndTypeAndAttend(Integer userId, Integer year, TrainingTypeEnum type, boolean attend);

    List<Training> findAllByUserIdAndType(Integer userId, TrainingTypeEnum type);
    List<Training> findAllByYearAndType(Integer year, TrainingTypeEnum type);
    List<Training> findAllByYearAndUserId(Integer year, Integer userId);
    List<Training> findAllByYearAndUserIdAndType(Integer year, Integer userId, TrainingTypeEnum type);
    List<Training> findAllByYearAndDepartmentId(Integer year, Integer departmentId);
}
