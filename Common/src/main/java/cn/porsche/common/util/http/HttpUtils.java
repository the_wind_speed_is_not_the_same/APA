package cn.porsche.common.util.http;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpUtils {

    private static final String[] AGENTS =
            new String[]{
                    "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36",
                    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11",
                    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; Media Center PC 5.0; .NET CLR 3.5.21022; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.5.30729; InfoPath.2; .NET CLR 3.0.30729; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/4.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; OfficeLiveConnector.1.4; OfficeLivePatch.1.3; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; GTB6; .NET CLR 2.0.50727; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; GTB6; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 1.1.4322; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; GTB6.3; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 1.1.4322; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; GTB0.0; InfoPath.1; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 4.0.20506; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 1.1.4322; InfoPath.2; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 1.0.3705; .NET CLR 1.1.4322; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; InfoPath.2; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0; .NET CLR 2.0.50727; InfoPath.1; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 1.1.4322; GreenBrowser)",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; .NET CLR 1.1.4322; GreenBrowser)",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.62 Safari/537.36"

    };

    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    private static final ThreadLocal<Charset> defaultCharset = new ThreadLocal<>();

    private static Charset getDefaultCharset() {
        return defaultCharset.get();
    }

    /**
     * Sets the default charset for reading the response, which is used while the server side does not
     * provided the encoding or charset. If {@code null} is set, the {@link Charset#defaultCharset()}
     * will be used to read the response. Note, this is a {@link ThreadLocal} variable.
     *
     * @param charset the charset.
     * @see #responseToString(HttpResponse)
     */
    public static void setDefaultCharset(Charset charset) {
        defaultCharset.set(charset);
    }

    public static String get(String uri, List<NameValuePair> params, List<Header> headers) throws HttpException {
        if (StringUtils.isEmpty(uri)) {
            throw new HttpException("No uri set to http request");
        }

        HttpGet request = new HttpGet();

        StringBuffer url = new StringBuffer(uri);
        if (params != null && !params.isEmpty()) {
            url.append(uri.contains("?") ? '&' : '?');
            url.append(StringUtils.join(params, "&"));
        }

        if (headers != null) {
            for (Header header : headers) {
                request.addHeader(header);
            }
        }

        request.setURI(URI.create(url.toString()));

        return execute(request);
    }

    public static String get(String uri, Map<String, String> params) {
        return get(uri, params, null);
    }


    public static String get(String uri, Map<String, String> params, Map<String, String> headers) {
        if (params == null) {
            params = new HashMap<>();
        }

        if (headers == null) {
            headers = new HashMap<>();
        }

        List<NameValuePair> paramList = new ArrayList<>();
        for (String key : params.keySet()) {
            paramList.add(new BasicNameValuePair(key, params.get(key)));
        }

        List<Header> headerList = new ArrayList<>();
        for (String key : headers.keySet()) {
            paramList.add(new BasicNameValuePair(key, headers.get(key)));
        }

        try {
            return get(uri, paramList, headerList);
        } catch (HttpException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String get(String uri, List<NameValuePair> params) throws HttpException {
        return get(uri, params, null);
    }

    public static String get(String uri) throws HttpException {
        return get(uri, (List<NameValuePair>) null, null);
    }


    public static String post(String uri, Map<String, String> params) {
        return get(uri, params, null);
    }

    public static String post(String uri, Map<String, String> params, Map<String, String> headers) {
        if (params == null) {
            params = new HashMap<>();
        }

        if (headers == null) {
            headers = new HashMap<>();
        }

        List<NameValuePair> paramList = new ArrayList<>();
        for (String key : params.keySet()) {
            paramList.add(new BasicNameValuePair(key, params.get(key)));
        }

        List<Header> headerList = new ArrayList<>();
        for (String key : headers.keySet()) {
            paramList.add(new BasicNameValuePair(key, headers.get(key)));
        }

        try {
            return post(uri, paramList, headerList);
        } catch (HttpException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String post(String uri, HttpEntity postBody, List<Header> headers) throws HttpException {
        HttpPost request = new HttpPost(uri);
        if (postBody != null) {
            request.setEntity(postBody);
        }

        if (headers != null) {
            for (Header header : headers) {
                request.addHeader(header);
            }
        }

        return execute(request);
    }

    public static String post(String uri, HttpEntity postBody) throws HttpException {
        HttpPost request = new HttpPost(uri);
        if (postBody != null) {
            request.setEntity(postBody);
        }

        return execute(request);
    }

    public static String post(String uri, List<NameValuePair> params, List<Header> headers) throws HttpException {
        return post(uri,
                params != null && !params.isEmpty() ? new UrlEncodedFormEntity(params, getDefaultCharset()) : null,
                headers);
    }

    public static String post(String uri, List<NameValuePair> params) throws HttpException {
        return post(uri,
                params != null && !params.isEmpty() ? new UrlEncodedFormEntity(params, getDefaultCharset()) : null,
                null);
    }

    public static String post(String uri) throws HttpException {
        return post(uri, (HttpEntity) null, null);
    }

    private static String execute(HttpRequestBase request) throws HttpException {
        try {
            return responseToString(newClient().execute(request));
        } catch (IOException e) {
            throw new HttpException(e.toString());
        } finally {
            request.releaseConnection();
        }
    }

    public static boolean isRequestSuccess(HttpResponse response) throws HttpException {
        return response.getStatusLine() != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
    }

    public static String randomAgent() {
        return AGENTS[RandomUtils.nextInt(0, AGENTS.length)];
    }

    public static String responseToString(HttpResponse response) throws HttpException {
        HttpEntity entity = response.getEntity();
        String result;
        if (entity == null) {
            throw new HttpException("No response entity.");
        }

        Charset charset = null;
        Header encoding = entity.getContentEncoding();

        if (encoding == null) {
            ContentType contentType = ContentType.get(entity);
            if (contentType != null) {
                charset = contentType.getCharset();
            }
        } else {
            charset = Charset.forName(encoding.getValue());
        }

        if (charset == null) {
            charset = getDefaultCharset();
        }

        try {
            result = IOUtils.toString(entity.getContent(), charset);
        } catch (IOException e) {
            throw new HttpException(e);
        }

        return result;
    }

    public static HttpClient newClient() throws HttpException {
        SSLContext sslContext;

        try {
            sslContext = SSLContext.getDefault();
        } catch (NoSuchAlgorithmException e) {
            throw new HttpException(e.getMessage());
        }

        return HttpClientBuilder
                .create()
                .setSSLContext(sslContext)
                .setMaxConnPerRoute(50)
                .setMaxConnTotal(200)
                .setUserAgent(randomAgent())
                .setDefaultRequestConfig(
                        RequestConfig.custom().setConnectTimeout(30000).setSocketTimeout(30000)
                                .setConnectionRequestTimeout(30000).build()).build();
    }

}
