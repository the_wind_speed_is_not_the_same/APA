package cn.porsche.web.service.impl;

import cn.porsche.auth.AccessToken;
import cn.porsche.common.util.RandomUtils;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.domain.User;
import cn.porsche.web.service.LoginService;
import cn.porsche.web.service.UserService;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

class LoginServiceImplTest extends ApplicationTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserService userService;

    @Autowired
    LoginService loginService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void encryptAuthentication() {
//        String test = "123456789";
//        logger.debug(test.substring(1, 5));
//        logger.debug(test.substring(0, 5));
////
//        String auth = loginService.loginBySSO(RandomUtils.randomString(32), "12345");
//        loginService.decryptAuthentication(auth, "12345");
//
//        // Authentication:12345|i4T3E9e6ewL55H8yU81w5zza4EyT3wAe, Key:qZ075wqo98321O4321TDd5B406, Encrypt:c94c6efa900fc1f5c0cf35e297281bd4086800a403fe04afdad1f4dafc25bc810ddf0eb1043fce5a, String:12345|i4T3E9e6ewL55H8yU81w5zza4EyT3wAe
//        logger.debug(CryptoUtils.decrypt("c94c6efa900fc1f5c0cf35e297281bd4086800a403fe04afdad1f4dafc25bc810ddf0eb1043fce5a", "qZ075wqo98321O4321TDd5B406".getBytes(), CryptoUtils.DES));
    }

    @Test
    void decryptAuthentication() {
    }

    @Test
    void loginBySSOByEmployeeAndDelegator() {
        User user;
        AccessToken token = new AccessToken();

        token.setAppId("Porche" + RandomUtils.randomString(12));
        token.setAppSercet(RandomUtils.randomString(32));
        token.setPound("proche");
        token.setScope("base_info");
        token.setTokenType("bear");


//        user = userService.findById(getEmployeeId2());
//        token.setAccessToken(RandomUtils.randomString(32));
//        token.setAccessTokenExpiresTime(DateUtils.addDays(new Date(), 10));
//        token.setRefreshToken(RandomUtils.randomString(32));
//        token.setRefreshTokenExpiresTime(DateUtils.addDays(new Date(), 70));
//
//        loginService.loginBySSO(user, token);
//
//        user = userService.findById(getDelegatorId());
//
//        token.setAccessToken(RandomUtils.randomString(32));
//        token.setAccessTokenExpiresTime(DateUtils.addDays(new Date(), 10));
//        token.setRefreshToken(RandomUtils.randomString(32));
//        token.setRefreshTokenExpiresTime(DateUtils.addDays(new Date(), 70));
//        loginService.loginBySSO(user, token);

//        user = userService.findById(getHRId());
//
//        token.setAccessToken(RandomUtils.randomString(32));
//        token.setAccessTokenExpiresTime(DateUtils.addDays(new Date(), 10));
//        token.setRefreshToken(RandomUtils.randomString(32));
//        token.setRefreshTokenExpiresTime(DateUtils.addDays(new Date(), 70));
//        loginService.loginBySSO(user, token);


//        user = userService.findById(getManagerId());
//
//        token.setAccessToken(RandomUtils.randomString(32));
//        token.setAccessTokenExpiresTime(DateUtils.addDays(new Date(), 10));
//        token.setRefreshToken(RandomUtils.randomString(32));
//        token.setRefreshTokenExpiresTime(DateUtils.addDays(new Date(), 70));
//        loginService.loginBySSO(user, token);

//        user = userService.findById(getVPId());

//        token.setAccessToken(RandomUtils.randomString(32));
//        token.setAccessTokenExpiresTime(DateUtils.addDays(new Date(), 10));
//        token.setRefreshToken(RandomUtils.randomString(32));
//        token.setRefreshTokenExpiresTime(DateUtils.addDays(new Date(), 70));
//        loginService.loginBySSO(user, token);
    }
}