package cn.porsche.web.service.impl;


import cn.porsche.web.domain.*;
import cn.porsche.web.repository.TargetHowRepository;
import cn.porsche.web.service.TargetHowService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

@Service
public class TargetHowServiceImpl implements TargetHowService {
    @Autowired
    TargetHowRepository repository;

    @Override
    @Transactional
    public void deleteByTarget(Target target) {
        repository.deleteAllByTargetId(target.getId());
    }

    @Override
    public TargetHow findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public TargetHow update(TargetHow how) {
        Assert.notNull(how, "The [how] argument is null");

        return repository.save(how);
    }

    @Override
    public TargetHow findByTarget(Target target) {
        Assert.notNull(target, "The [target] argument is null");

        return repository.findByTargetId(target.getId());
    }

    @Override
    public TargetHow confirmByEmployee(Target target) {
        TargetHow data = findByTarget(target);

        if (data.isEmployeeConfirm()) {
            return data;
        }

        data.setEmployeeConfirm(true);

        return update(data);
    }

    @Override
    public TargetHow confirmByDelegator(Target target) {
        TargetHow data = findByTarget(target);

        if (data.isDelegatorConfirm()) {
            return data;
        }

        data.setDelegatorConfirm(true);

        return update(data);
    }

    @Override
    public TargetHow eAppraisal(Function function, Target target, TargetHow appraisal) {
        TargetHow data = findByTarget(target);

        data.setHow1EScore(appraisal.getHow1EScore());
        data.setHow2EScore(appraisal.getHow2EScore());
        data.setHow3EScore(appraisal.getHow3EScore());
        data.setHow4EScore(appraisal.getHow4EScore());

        data.setHow1(appraisal.getHow1());
        data.setHow2(appraisal.getHow2());
        data.setHow3(appraisal.getHow3());
        data.setHow4(appraisal.getHow4());

        return update(data);
    }

    @Override
    public TargetHow delegatorFirstAppraisal(Function function, Target target, TargetHow appraisal) {
        TargetHow data = findByTarget(target);

        data.setHow1DOScore(appraisal.getHow1DOScore());
        data.setHow2DOScore(appraisal.getHow2DOScore());
        data.setHow3DOScore(appraisal.getHow3DOScore());
        data.setHow4DOScore(appraisal.getHow4DOScore());

        return update(data);
    }

    @Override
    public TargetHow delegatorSecondAppraisal(Function function, Target target, TargetHow appraisal) {
        TargetHow data = findByTarget(target);

        data.setHow1DTScore(appraisal.getHow1DTScore());
        data.setHow2DTScore(appraisal.getHow2DTScore());
        data.setHow3DTScore(appraisal.getHow3DTScore());
        data.setHow4DTScore(appraisal.getHow4DTScore());

        return update(data);
    }

    @Override
    public Map<String, Map<String, String>> check(TargetHow how) {
        Map<String, Map<String, String>> notices = new HashMap<>();

        if (null == how) {
            notices.put("how", null);

            return notices;
        }

        // 检查每一项，都提示
        Map<String, String> fields = new HashMap<>();
        fields.put("how1", StringUtils.isEmpty(how.getHow1()) ? null : "yes");
        fields.put("how2", StringUtils.isEmpty(how.getHow2()) ? null : "yes");
        fields.put("how3", StringUtils.isEmpty(how.getHow3()) ? null : "yes");
        fields.put("how4", StringUtils.isEmpty(how.getHow4()) ? null : "yes");

        notices.put("how", fields);

        return notices;
    }
}
