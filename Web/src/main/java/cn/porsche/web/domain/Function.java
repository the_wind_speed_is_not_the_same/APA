package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "functions", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Function implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    Integer id;

    @ApiModelProperty(notes = "结束年份")
    @JsonProperty("year")
    @Column(name = "year",               columnDefinition = "SMALLINT(4) NOT NULL COMMENT '开始年份'")
    Integer year;

    @ApiModelProperty(notes = "TS的状态")
    @JsonProperty("is_ts")
    @Column(name = "is_ts",                     columnDefinition = "TINYINT(1) UNSIGNED NULL COMMENT 'TS的状态'")
    Boolean TS;

    @JsonIgnore
    @ApiModelProperty(notes = "TS的开始时间")
//    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Column(name = "ts_time",                   columnDefinition = "DATETIME NULL COMMENT 'TS的开始时间'")
    Date TSTime;

    @JsonIgnore
    @ApiModelProperty(notes = "开启TS的用户")
    @Column(name = "ts_user_id",                columnDefinition = "INT UNSIGNED NOT NULL COMMENT '开启TS的用户'")
    Integer TSUserId;

    @JsonIgnore
    @ApiModelProperty(notes = "最近的一封邮件主键")
    @Column(name = "last_ts_email_id",          columnDefinition = "INT UNSIGNED NULL COMMENT '最近的一封邮件主键'")
    Integer lastTSEmailId;

    @JsonIgnore
    @ApiModelProperty(notes = "TS的邮件是否发送成功")
    @Column(name = "ts_mail_success",           columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'TS的邮件是否发送成功'")
    Boolean TSMailSuccess;

    @ApiModelProperty(notes = "APA是否开始状态")
    @JsonProperty("is_apa")
    @Column(name = "is_apa",                    columnDefinition = "TINYINT(1) UNSIGNED NULL COMMENT 'APA是否开始状态'")
    Boolean APA;

    @JsonIgnore
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Column(name = "apa_time",                  columnDefinition = "DATETIME NULL COMMENT 'APA的开始时间'")
    Date APATime;

    @JsonIgnore
    @ApiModelProperty(notes = "APA的开始的用户")
    @Column(name = "apa_user_id",               columnDefinition = "INT UNSIGNED NULL COMMENT 'APA的开始的用户'")
    Integer APAUserId;

    @JsonIgnore
    @ApiModelProperty(notes = "最近的一封邮件主键")
    @Column(name = "last_apa_email_id",         columnDefinition = "INT UNSIGNED NULL COMMENT '最近的一封邮件主键'")
    Integer lastAPAEmailId;

    @JsonIgnore
    @ApiModelProperty(notes = "APA的邮件是否发送成功")
    @Column(name = "apa_mail_success",          columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'APA的邮件是否发送成功'")
    Boolean APAMailSuccess;

    @ApiModelProperty(notes = "整个阶段是否完成")
    @JsonProperty("is_complete")
    @Column(name = "is_complete",               columnDefinition = "TINYINT(1) UNSIGNED NULL COMMENT '整个阶段是否完成'")
    Boolean complete;

    @ApiModelProperty(notes = "当前年份第一天")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("first_day")
    @Column(name = "first_day",                 columnDefinition = "DATE NULL COMMENT '当前年份第一天'")
    Date firstDay;

    @ApiModelProperty(notes = "当前年份最后一天")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("last_day")
    @Column(name = "last_day",                  columnDefinition = "DATE NULL COMMENT '当前年份最后一天'")
    Date lastDay;

    @JsonIgnore
    @Column(name = "complete_time",             columnDefinition = "DATETIME NULL COMMENT '整个阶段完成时间'")
    Date completeTime;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
