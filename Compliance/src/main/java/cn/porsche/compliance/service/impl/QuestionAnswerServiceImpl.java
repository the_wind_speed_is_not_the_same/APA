package cn.porsche.compliance.service.impl;

import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.domain.QuestionAnswer;
import cn.porsche.compliance.emnu.SeasonEnum;
import cn.porsche.compliance.repository.QuestionAnswerRepository;
import cn.porsche.compliance.service.QuestionAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class QuestionAnswerServiceImpl implements QuestionAnswerService {
    @Autowired
    QuestionAnswerRepository repository;

    @Override
    public List<QuestionAnswer> findAllByYearAndSeason(Integer year, Integer season) {
        return repository.findAllByYearAndSeason(year, season);
    }

    @Override
    public QuestionAnswer insert(QuestionAnswer answer) {
        return repository.save(answer);
    }

    @Override
    @Transactional
    public void deleteByUser(User user, Integer year, SeasonEnum season) {
        repository.deleteAllByUserIdAndYearAndSeason(user.getId(), year, season);
    }

    @Override
    public int getTopScoreByYearAndSeason(Integer year, Integer season) {
        List<QuestionAnswer> answers = findAllByYearAndSeason(year, season);

        int total = 0;

        for (QuestionAnswer item : answers) {
            if (item.isRight()) {
                total += item.getScore();
            }
        }

        return total;
    }

    @Override
    public int summaryScoreByUserAndYearAndSeason(User user, Integer year, SeasonEnum season) {
        Integer total = repository.sumScoreByUserAndYearAndSeason(user.getId(), year, season.getName());

        return total == null ? 0 : total;
    }

    @Override
    public List<QuestionAnswer> findUserAndYearAndSeason(User user, Integer year, SeasonEnum season) {
        return repository.findAllByUserIdAndYearAndSeason(user.getId(), year, season);
    }
}
