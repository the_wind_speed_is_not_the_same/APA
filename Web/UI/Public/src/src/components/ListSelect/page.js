import Mantle from '../Mask/page.vue';
export default {
    props: {
        selectIndex: {
            type: Number,
            default: 0
        },
        list: {
            type: Array,
            default: () => []
        },
        Width: {
            type: Number,
            default: 120
        }
    },
    data() {
        return {
            
        }
    },
    components: {
        Mantle
    },
    computed: {
        
    },
    methods: {
        confirm(index) {
            this.$emit('confirmIndex', index !== undefined ? index : this.selectIndex);
        }
    },
    mounted() {
        
    }
}