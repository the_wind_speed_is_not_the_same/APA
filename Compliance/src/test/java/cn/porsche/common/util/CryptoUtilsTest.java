package cn.porsche.common.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;


class CryptoUtilsTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void encrypt() {
        String key = "heasdsdsadsadsallo";
        String en = CryptoUtils.encrypt("hello this is word", key.getBytes(), "DES");
        logger.debug(en);

        String de = CryptoUtils.decrypt(en, key.getBytes(), "DES");
        logger.debug(de);


    }

    @Test
    void decrypt() {
    }
}