package cn.porsche.web.repository;

import cn.porsche.web.domain.Function;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FunctionRepository extends CrudRepository<Function, Integer> {
    Function findByYear(Integer year);

    List<Function> findAllByOrderByIdDesc();
}
