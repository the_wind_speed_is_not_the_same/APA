package cn.porsche.web.service.impl;


import cn.porsche.web.domain.Position;
import cn.porsche.web.repository.PositionRepository;
import cn.porsche.web.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;



@Service
public class PositionServiceImpl implements PositionService {
    @Autowired
    PositionRepository repository;

    @Override
    public Integer count() {
        return Integer.valueOf((int) repository.count());
    }

    @Override
    public Page<Position> getListByPage(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page -1, size));
    }

    @Override
    public Position update(Position job) {
        return repository.save(job);
    }

    @Override
    public Position findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Position findByParentId(Integer parentId) {
        return repository.findByParentId(parentId);
    }
}