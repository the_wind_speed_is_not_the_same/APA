import ListSelect from '../../../../components/ListSelect/page.vue';
import SuccessFail from '../../../../components/SuccessFail/page.vue';
import {request} from '../../../../common/request.js';

import Confirm from '../../../../components/Confirm/page.vue';
import fa from "element-ui/src/locale/lang/fa";

export default {
    props: {
        currentIndex: {
            type: Number,
            default: null
        },
        whatIndex: {
            type: Number,
            default: null
        },
        position: {
            type: String,
            default: 'right'
        },
        tableData: {
            type: Array
        },
        isShowLoading: {
            type: Boolean,
            default: false
        },
        componentType: {
            type: String,
            default: ''
        },
        user: Object,
        functionId: Number
    },
    data() {
        return {
            selectOrderItem: -1,
            isShowListSelect: false,
            orderList: ['Name', 'Pre-Calibration Score', 'Performance Level'],
            levelIndex: -1,
            isShowLevelSelect: false,
            levelList: ['A', 'B', 'C', 'D', 'E'],
            tableIndex: -1,
            tableHeight: 500,
            statusList: [{
                name: 'Not Start',
                class: 'not-start',
            },
                {
                    name: 'Submitted',
                    class: 'submitted'
                },
                {
                    name: 'Reviewed',
                    class: 'reviewed'
                },
                {
                    name: 'Confirmed',
                    class: 'confirmed'
                }
            ],
            titleTxt:'Warning',
            isManager:true,
            isShowConfirm:false,
            isShowButtonLeft: false,
            confirmTxt:'',
            showTime:0,
            editType:null
        }
    },
    watch: {
        currentIndex(n, o) {
            if (n >= 0) {
                this.initData();
            }
        }
    },
    components: {
        ListSelect,
        SuccessFail,
        Confirm
    },
    computed: {
        tsClass() {
            return this.componentType === 'ts' || this.componentType === 'manage-tr' ? 'c-wrapper' : '';
        },
        apaClass() {
            return this.componentType === 'apa' || this.componentType === 'manage-apa' ? 'c-wrapper' : ''
        },
        levelTop() {
            let top = this.tableIndex * 48 + 58;
            return top;
        },
        isRelease() {
            if (this.tableData.length > 0) {
                return this.tableData.every((item) => {
                    return item.target['is_apa_delegator_submit'];
                })
            } else {
                return false
            }
        },
        isPreAppraisal() {
            if (this.tableData.length > 0) {
                return this.tableData.every((item) => {
                    return item.target['is_apa_employee_submit'] && !item.target['is_apa_delegator_submit'];
                })
            } else {
                return false
            }
        },
        isDelegatorSubmit() {
            // 正式需要去掉,改用isPreAppraisal
            if (this.tableData.length > 0) {
                return this.tableData.some((item) => {
                    return item.target[''] && item.target.is_apa_delegator_submit
                })
            } else {
                return false
            }
        }
    },
    methods: {
        tsStatus(val) {
            if (!val.target.is_ts_employee_submit) {
                return 'not-start'
            } else if (val.target.is_ts_employee_submit && !val.target.is_ts_delegator_confirm) {
                return 'submitted'
            } else if (val.target.is_ts_delegator_confirm && !val.target.is_ts_employee_confirm) {
                return 'reviewed'
            } else if (val.target.is_ts_delegator_confirm && val.target.is_ts_employee_confirm) {
                return 'confirmed'
            }
        },
        apaStatus(val) {
            if (!val.target.is_apa_employee_submit) {
                return 'not-start'
            } else if (val.target.is_apa_employee_submit && !val.target.is_apa_delegator_submit) {
                return 'submitted'
            } else if (val.target.is_apa_employee_submit && val.target.is_apa_delegator_submit && !val.target.is_agree_score) {
                return 'reviewed'
            } else if (val.target.is_agree_score) {
                return 'confirmed'
            }
        },
        checkAPA(val) {
            if (!this.isManager) {
                this.isShowConfirm = true
                this.confirmTxt = 'You do not have permission to operate'
                this.showTime = 0
                this.editType = null

                return ;
            }
            if (val.target.is_apa_employee_submit) {
                this.changeComponent('manage-apa', val);
            } else {
                this.isShowConfirm = true
                this.confirmTxt = 'The employee has not submit performance score'
                this.showTime = 0
                this.editType = null
            }
        },
        checkTs(val) {
            if (val.target.is_ts_employee_submit) {

                // if(this.role === 'manage'){
                this.changeComponent('manage-tr', val);
                // }else{
                //   this.changeComponent('ts', val);
                // }
            } else {
                this.isShowConfirm = true
                this.confirmTxt = ' The employee has not submit targets.'
                this.showTime = 0
                this.editType = null
            }
        },
        changeComponent(type, row) {
            this.$emit('changeComponent', type, row)
        },
        handleCurrentChange(row) {
            // this.$emit('changeComponent', this.componentType, row);
        },
        showOrderUI() {
            this.isShowListSelect = true;
        },
        showLevel() {
            this.isShowLevelSelect = true;
        },
        selectOrderIndex(index) {
            this.selectOrderItem = index;
            this.$emit('sortTableData', index);
            this.isShowListSelect = false;
        },
        confirmLevel(index) {
            if (index < 0) return;
            this.tableData[this.tableIndex].target.second_level = this.levelList[index];
            this.isShowLevelSelect = false;
            this.levelIndex = -1;
        },

        showLevelSelect(row) {
            let index = this.tableData.indexOf(row);
            if (row.target.second_level) {
                this.levelIndex = this.levelList.indexOf(row.target.second_level);
            }
            this.tableIndex = index;
            this.$nextTick(() => {
                this.isShowLevelSelect = true;
            })
        },
        // upDateScore() {
        //     this.$emit('upDateScore');
        // },
        initData() {
            if (this.currentIndex !== null) {
                this.$refs.table1.setCurrentRow(this.tableData[this.currentIndex]);
            }
        },
        release() {
            if (!this.functionId) {
                this.isShowButtonLeft = false
                this.isShowConfirm = true

                this.titleTxt = 'Fail'
                this.confirmTxt = " No Function is opened, unable to operate";

                return false;
            }

            this.isShowButtonLeft = true
            this.isShowConfirm = true

            this.titleTxt = 'Confirm'
            this.confirmTxt = 'Do you want to release?'
            this.showTime = null
            this.editType = 'release';
        },
        preAppraisal() {
            if (!this.functionId) {
                this.isShowButtonLeft = false
                this.isShowConfirm = true

                this.titleTxt = 'Fail'
                this.confirmTxt = " No Function is opened, unable to operate";

                return false;
            }

            this.isShowButtonLeft = true
            this.isShowConfirm = true

            this.titleTxt = 'Confirm';
            this.confirmTxt = 'Do you want to pre-appraisal?'
            this.showTime = null
            this.editType = 'appraisal'
        },
        confirm() {
            if (this.editType === 'appraisal') {
                request({
                    url: 'release_first_score',
                    method: 'get',
                    params: {
                        function_id: this.functionId
                    },
                    isShowLoading: true
                }).then((res) => {
                    this.isShowButtonLeft = false
                    this.isShowConfirm = true
                    this.isShowCancel = false;
                    this.editType = null;

                    if (!res || res.code !== 0) {
                        this.titleTxt = 'Fail'
                        this.confirmTxt = res.message;

                        this.editType = null;

                        return false;
                    }

                    this.$emit('toSetTargetDelegatorSubmit', 'is_apa_delegator_submit')
                    this.titleTxt = 'Success';
                    this.confirmTxt = "You are now ready for the calibration session.";

                    this.$emit('updateTable');
                })
            } else if (this.editType === 'release') {
                request({
                    url: 'release_final_score',
                    method: 'get',
                    params: {
                        function_id: this.functionId
                    },
                    isShowLoading: true
                }).then((res) => {
                    this.isShowButtonLeft = false
                    this.isShowConfirm = true

                    if (!res || res.code !== 0) {
                        this.titleTxt = 'Fail'
                        this.confirmTxt = res.message;

                        this.editType = null;

                        return false;
                    }

                    if (res && res.code === 0) {
                        this.$emit('toSetTargetDelegatorSubmit', 'is_release_score')
                        this.titleTxt = 'Success'
                        this.confirmTxt = 'Release successfully.'

                        this.editType = null
                        this.$emit('updateTable');
                    }
                })
            }

            this.isShowConfirm = false;
        },
        // 弹框取消
        cancel() {
            this.isShowConfirm = false;
        },
    },
    created() {
        this.isManager = this.user['roles'].indexOf('manager') >= 0;
    },
    mounted() {
        this.initData();
        this.$nextTick(() => {
            this.tableHeight = this.$refs.tableWrapper.offsetHeight - 30;
        })
    }
}
