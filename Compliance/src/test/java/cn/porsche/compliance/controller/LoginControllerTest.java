package cn.porsche.compliance.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.ApplicationTest;
import cn.porsche.compliance.service.LoginService;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class LoginControllerTest extends ApplicationTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String API = "/login";

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    void logout() {
        final String api = API + "/out";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}