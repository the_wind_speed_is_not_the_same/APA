package cn.porsche.web.controller.response;

import cn.porsche.web.controller.response.reporting.ResultRating;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RatingReporting {
    @ApiModelProperty(notes = "部门分布数据")
    @JsonProperty("result_rating")
    List<ResultRating> resultRating;
}
