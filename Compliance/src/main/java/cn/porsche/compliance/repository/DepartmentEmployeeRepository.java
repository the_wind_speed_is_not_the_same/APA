package cn.porsche.compliance.repository;

import cn.porsche.compliance.domain.DepartmentEmployee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentEmployeeRepository extends CrudRepository<DepartmentEmployee, Integer> {
    DepartmentEmployee findByEmployeeId(Integer employeeId);
}
