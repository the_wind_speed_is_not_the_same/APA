package cn.porsche.web.constant;

public class DataStatus {
    public final static Integer OK = 0;
    public final static Integer DELETE = 255;
}
