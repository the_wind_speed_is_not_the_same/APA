import Introduction from './components/Introduction/page.vue';
import SettingWhat from './components/What/page.vue';
import Dimensions from './components/Dimensions/page.vue';
import OverallComments from './components/Comments/page.vue';
import ListSelect from '../../components/ListSelect/page.vue';
import Report from './components/Report/page.vue';
import Confirm from '../../components/Confirm/page.vue';
import {mapState, mapActions} from 'vuex';
import {request} from '../../common/request';
import {formatDateTime} from '../../common/utils.js';


export default {
    data() {
        return {
            tableData1: [],
            whats: [],
            howData: [{
                name: 'Dimensions'
            }, {
                name: 'Overall Comments'
            }],
            currentIndex: null,
            currentRow: null,
            tableHeight: 412,
            componentName: 'Introduction',
            tsState: 'add',
            targetInfo: null,
            showYearSelect: false,
            selectYearIndex: 0,
            from: null,
            to: null,
            titleTxt: '',
            confirmTxt: '',
            isShowConfirm: false,
            isShowButtonLeft: false,
            showTime: 0,
            func: null,
            target: null,
            comments: [],
            dimension: null,
            isEmployeeWhatAppraisal: false,    // 员工对What打完分（由于What是多个，所以需要遍历每个What都打过分）
            isEmployeeHowAppraisal: false,     // 员工对How打完分
            isAgreeScore: false,
            overall: null,
            user: null,
            // overall comments 未读消息标识
            hasCommentsUnread: false,

            // apa comments 未读消息标识
            apaCommentsUnread:[],
            overallId:null
        }
    },
    computed: {
        ...mapState({
            allList: state => state.allState.allList,
            yearList: state => state.allState.yearList
        }),
        yearVal() {
            return this.allList && this.allList[this.selectYearIndex] || {};
        },
        tableDisabled() {
            return !this.whats || !this.whats.length;
        },
        btnSubmitEnabled() {
            // // 员工打完What 和 How 的分之后，并且没有点击提交的情况下，才显示
            return !!(this.target && !this.target.is_apa_employee_submit && this.isEmployeeWhatAppraisal && this.isEmployeeHowAppraisal);
        },
        isReportDisable() {
            // 经理释放二次分数才可点击
            return !(this.target && this.target.is_release_score);
        },
        statusTxt() {
            let str = ''
            if (!this.is_apa) {
                str = 'Not Started'
            } else if (this.is_apa && !this.is_complete) {
                str = 'In Progress'
            } else {
                str = 'Complete'
            }
            return ` STATUS: ${str}`
        }
    },
    components: {
        Introduction,
        Dimensions,
        OverallComments,
        SettingWhat,
        ListSelect,
        Report,
        Confirm,
    },
    methods: {
        ...mapActions(['setYearList']),
        confirm() {
            this.isShowConfirm = false;
        },
        submit() {
            if (!this.btnSubmitEnabled) return;
            request({
                url: 'apaSubmit',
                method: 'get',
                params: {
                    function_id: this.yearVal.id
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;

                    return;
                }

                this.titleTxt = 'Success';
                this.confirmTxt = 'Submit Successfully';
                this.isShowConfirm = true;

                this.initData();
            })
        },
        showReport() {
            this.closeComponent();
            this.$nextTick(() => {
                this.componentName = 'Report';
            })
        },
        closeComponent() {
            this.$nextTick(() => {
                this.componentName = 'Introduction';
                this.currentIndex = null;
                this.targetType = '';

                this.$refs.howTable.setCurrentRow(null);
                this.$refs.whatTable.setCurrentRow(null);
            })
        },
        selectYear(idx) {
            if (idx == this.selectYearIndex) {
                return this.showYearSelect = false;
            }

            this.selectYearIndex = idx;
            this.showYearSelect = false;

            this.to = this.yearList[this.selectYearIndex];
            this.func = this.allList[this.selectYearIndex];

            this.componentName = 'Introduction';

            this.$nextTick(() => {
                this.initData();
            })
        },
        showYearsUI() {
            this.showYearSelect = true;
        },
        initYear() {
            let process = this.allList.filter(item => {
                return item.is_ts || item.is_apa;
            })

            if (this.allList.length) {
                this.selectYearIndex = this.allList.indexOf(process[0]);
                this.to = process[0].year;
                this.is_apa = process[0].is_apa;
                this.is_complete = process[0].is_complete;

                this.func = process[0];
            }
        },
        callback() {
        },
        initData() {
            if (!this.yearVal.id) {
                return console.log("no params for this.yearVal.id");
            }


            request({
                url: 'targetDetail',
                method: 'get',
                isShowLoading: true,
                params: {
                    function_id: this.yearVal.id
                }
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;

                    return false;
                }

                this.target     = res.data.target;
                this.whats      = res.data.whats;
                this.overall    = res.data.overall;

                if (!this.func || !this.func['is_apa']) {
                    this.whats = [];
                    this.how   = [];

                    this.titleTxt = 'Error';
                    this.confirmTxt = 'Self evaluation hasn\'t started';
                    this.isShowConfirm = true;
                }

                this.isAgreeScore = res.data.target.is_agree_score;

                if (!this.target['is_apa_employee_submit'] && res.data.whats.length > 0) {
                    this.isEmployeeWhatAppraisal = res.data.whats.every((item) => {
                        return !!item.employee_score;
                    });
                }

                if (!this.target.is_apa_employee_submit && res.data.how) {
                    this.isEmployeeHowAppraisal = !!(res.data.how.how1_e_score && res.data.how.how2_e_score && res.data.how.how3_e_score && res.data.how.how4_e_score);
                }

                if (res.data.whats.length) {
                    res.data.whats.forEach((item, idx) => {
                        let redDot = false;
                        if (item['apa_comments'] && item['apa_comments'].length) {
                            redDot = item['apa_comments'].some((item, idx) => {
                                return item['is_read'] === false && item['owner_id'] != this.user['id'];
                            });
                        }

                        // 将红点添加到对应的元素上去
                        this.apaCommentsUnread[idx] = redDot;
                    })
                }

                this.dimension = res.data.how || [];
                if (res.data.overall && res.data.overall.length) {
                    this.setComments(res.data.overall);
                }

                this.$refs.whatTable.setCurrentRow(null);
                this.$refs.howTable.setCurrentRow(null);
            })
        },
        setComments(list) {
            list.forEach((item, index) => {
                if (item.owner_id == item.employee_id) {
                    item.type = 'me';
                    item.name = item.employee_name;
                } else {
                    item.type = 'other';
                    item.name = item.delegator_name;
                }

                item.send_time = formatDateTime(item.send_time);

                if (!this.hasCommentsUnread) {
                    this.hasCommentsUnread = item['is_read'] === false && item['owner_id'] != this.user['id']
                }
            });

            this.comments = list;
        },
        previousIndex() {
            this.settingHandle('previous');
        },
        nextIndex() {
            this.settingHandle('next');
        },
        settingHandle(type) {
            if (type === 'next') {
                if (this.targetType === 'what') {
                    if (this.currentIndex === this.whats.length - 1) {
                        this.$refs.whatTable.setCurrentRow(null);
                        this.currentIndex = 0;
                        this.targetType = 'how';
                        this.$refs.howTable.setCurrentRow(this.howData[this.currentIndex]);
                    } else {
                        this.$refs.whatTable.setCurrentRow(this.whats[++this.currentIndex]);
                        this.scrollTop();
                    }
                } else if (this.targetType === 'how') {
                    this.$refs.howTable.setCurrentRow(this.howData[++this.currentIndex]);
                }
            } else if (type === 'previous') {
                if (this.targetType === 'how') {
                    if (this.currentIndex === 0) {
                        this.currentIndex = this.whats.length - 1;
                        this.$refs.whatTable.setCurrentRow(this.whats[this.currentIndex]);
                        this.scrollTop();
                    } else {
                        this.$refs.howTable.setCurrentRow(this.howData[--this.currentIndex]);
                    }
                } else if (this.targetType === 'what') {
                    if (this.currentIndex === 0) return;
                    this.$refs.whatTable.setCurrentRow(this.whats[--this.currentIndex]);
                    this.scrollTop();
                }
            }
        },
        scrollTop() {
            let tableWrapper = document.getElementsByClassName('el-table__body-wrapper')[0];
            let index = this.currentIndex;
            tableWrapper.scrollTop = 46 * this.currentIndex;
        },
        whatClick(row) {
            if (!row) {
                return false;
            }

            // 取消 how 上的选择框
            this.$refs.howTable.setCurrentRow(null);

            this.targetType = 'what';
            this.currentIndex = this.whats.indexOf(row);
            this.targetInfo = row;
            this.$nextTick(() => {
                this.componentName = 'SettingWhat';
            });

            if (this.apaCommentsUnread[this.currentIndex]) {
                // 需要清除What上的红点提示（APA结构下）
                request({
                    url: 'readComment',
                    method: 'get',
                    params: {
                        topic_id: row['apa_comments'][0]['topic_id'],
                        type:'APA'
                    }
                }).then(res => {
                    if (!res || res.code !== 0) {
                        this.titleTxt = 'Error';
                        this.confirmTxt = res.message;
                        this.isShowButtonLeft = false;
                        this.isShowConfirm = true;

                        return false;
                    }

                    this.$set(this.apaCommentsUnread, this.currentIndex, false);
                })
            }
        },
        howHandle(type) {
            this.$nextTick(() => {
                this.componentName = this.currentIndex == 0 ? 'Dimensions' : 'OverallComments';
            })
        },
        handleCurrentChange2(val) {
            if (this.hasCommentsUnread && val.name == "Overall Comments") {
                request({
                    url: 'readComment',
                    method: 'get',
                    params: {
                        topic_id: this.comments[0]['topic_id'],
                        type:'OV'
                    }
                }).then(res => {
                    if (!res || res.code !== 0) {
                        this.titleTxt = 'Error';
                        this.confirmTxt = res.message;
                        this.isShowButtonLeft = false;
                        this.isShowConfirm = true;

                        return false;
                    }

                    this.hasCommentsUnread = false;
                    this.comments.forEach(item => {
                        if (item['owner_id'] != this.user['id']) {
                            this.$set(item, 'is_read', true);
                        }
                    });
                })
            }

            if (val && !this.tableDisabled) {
                this.targetType = 'how';
                this.currentIndex = this.howData.indexOf(val);
                this.$refs.whatTable.setCurrentRow(null);
                this.howHandle();
            } else {
                this.$refs.howTable.setCurrentRow(null);
            }
        },
        setListScore(res) {
            let {currentIndex, score, comment} = res;
            this.whats[currentIndex].employee_score = score;
            this.whats[currentIndex].apa_comments.push(comment)
        }
    },
    created() {
        let user = localStorage.getItem('user');
        if (user) {
            this.user = JSON.parse(user)
        }

        const {function_id} = this.$route.query;

        this.setYearList().then(res => {
            if (!function_id) {
                this.initYear()

            } else {
                this.functionId = function_id;
                res.forEach((item, idx) => {
                    if (item.id == function_id) {
                        this.to = item.year
                        this.selectYearIndex = idx;

                        this.func = item;
                    }
                })
            }

            this.initData();
        })
    },
    mounted() {
        this.$nextTick(() => {
            this.tableHeight = this.$refs.tableTop.offsetHeight - 62;
        })
    }
}
