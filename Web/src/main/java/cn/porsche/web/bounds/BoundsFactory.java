package cn.porsche.web.bounds;

import cn.porsche.web.bounds.impl.EmployeeBoundsOf2021;
import cn.porsche.web.bounds.impl.ManagerBoundsOf2021;

public final class BoundsFactory {
    public static IBounds getByYearAndEmployee(BoundsYearEnum year) {
        return new EmployeeBoundsOf2021();
    }

    public static IBounds getByYearAndManager(BoundsYearEnum year) {
        return new ManagerBoundsOf2021();
    }
}
