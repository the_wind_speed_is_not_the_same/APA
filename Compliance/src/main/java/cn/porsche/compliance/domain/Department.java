package cn.porsche.compliance.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "departments", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Department implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    //    DEPARTMENTID	DEPARTMENTNAME	                PARENTDEPARTMENDID	Company Code-Company
    //    PCN	        Porsche China	                PCN	                0091_PCN
    //    PCN-BD	    Strategy & Business Development	PCN	                0091_PCN
    //    PCN-CRM	    Customer Relations	            PCN	                0091_PCN
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @ApiModelProperty(notes = "上级部门代码：PARENTDEPARTMENDID")
    @JsonProperty("parent_id")
    @Column(name = "parent_id",                 columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '上级部门主键'")
    Integer parentId;

    @ApiModelProperty(notes = "管理员主键")
    @JsonProperty("manager_id")
    @Column(name = "manager_id",                columnDefinition = "INT UNSIGNED NULL DEFAULT NULL COMMENT '管理员主键'")
    Integer managerId;

    @ApiModelProperty(notes = "公司代码：Company Code-Company")
    @JsonProperty("company_code")
    @Column(name = "company_code",              columnDefinition = "VARCHAR(63) NULL DEFAULT NULL COMMENT '公司代码'")
    String companyCode;

    @ApiModelProperty(notes = "上级部门代码：PARENTDEPARTMENDID")
    @JsonProperty("parent_code")
    @Column(name = "parent_code",               columnDefinition = "VARCHAR(63) NULL DEFAULT NULL COMMENT '上级部门代码'")
    String parentCode;

    @ApiModelProperty(notes = "部门代码：DEPARTMENTID")
    @JsonProperty("code")
    @Column(name = "code",                      columnDefinition = "VARCHAR(63) NOT NULL COMMENT '部门代码'")
    String code;

    @ApiModelProperty(notes = "部门名称：DEPARTMENTNAME")
    @JsonProperty("name")
    @Column(name = "name",                      columnDefinition = "VARCHAR(255) NOT NULL COMMENT '部门名称'")
    String name;

    @ApiModelProperty(notes = "员工总数")
    @JsonProperty("count")
    @Column(name = "count",                     columnDefinition = "SMALLINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '员工总数'")
    Integer count;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
