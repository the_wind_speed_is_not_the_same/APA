import {request} from '../../../../common/request.js';
import ListSelect from '../../../../components/ListSelect/page.vue';
import Confirm from '../../../../components/Confirm/page.vue';

export default {
    props: ['user', 'dimension', 'tableData', 'currentIndex', 'functionId'],
    data() {
        return {
            selectScoreIndex: -1,
            scores: ['1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0'],
            dimensionTop: 0,
            selectScoreTop: 0,

            isShowConfirm: false,
            titleTxt: '',
            confirmTxt: '',
            showTime: 0,
            isShowButtonLeft: false,
            dimensionFields: [
                {
                    name: 'Herzblut',
                    subTxt: '1. Live Passionately <br /> 2. Know where you come from',
                    weight: '25%',
                    description: '',
                    tootip: 'Herzblut<br/>' +
                        '<p style="text-indent:1em">1.  Live Passionately</p>' +
                        '<p style="text-indent:2em">I inspire others for our work </p>' +
                        '<p style="text-indent:2em">I have high standards for my work </p>' +
                        '<p style="text-indent:2em">I celebrate successes with others </p>' +
                        '<p style="text-indent:2em">We work with commitment and with conviction</p>' +
                        '<p style="text-indent:1em">2.	Know where you come from </p>' +
                        '<p style="text-indent:2em">I do not take the given things for granted</p>' +
                        '<p style="text-indent:2em">I utilize our resources intelligently </p>' +
                        '<p style="text-indent:2em">We know for whom we work - for our customers </p>' +
                        '<p style="text-indent:2em">We foster social partnerships and promote a fair balance </p>'+
                        '<p style="text-indent:2em">between employee and company interests',
                    placeholder: "Please describe how you are going to perform and realize the objectives by living the value of \"Herzblut\"."
                }, {
                    name: "Pioniergeist",
                    subTxt: '1. Look into the future with courage<br />2. Think outside the box',
                    weight: '25%',
                    tootip: 'Pioniergeist<br/>' +
                        '<p style="text-indent:1em">1.  Look into the future with courage</p>' +
                        '<p style="text-indent:2em">I respond flexibly to changing framework conditions</p>' +
                        '<p style="text-indent:2em">I keep the goal in mind when there is uncertainty </p>' +
                        '<p style="text-indent:2em">I am willing to take risks </p>' +
                        '<p style="text-indent:2em">We consider new technologies to be a chance <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; and drive forward future-oriented ideas (regardless of their origin)</p>' +
                        '<p style="text-indent:1em">2.	Think outside the box </p>' +
                        '<p style="text-indent:2em">I ensure, that our objectives and backgrounds are transparent</p>' +
                        '<p style="text-indent:2em">I analyze challenges quickly, reduce complexity and derive specific conclusions </p>' +
                        '<p style="text-indent:2em">I question the status quo and bring in solutions </p>' +
                        '<p style="text-indent:2em">We build networks to achieve common objectives',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize the objectives by living the value of \"Pioniergeist\".'
                }, {
                    name: 'Sportlichkeit',
                    subTxt: '1. Fight fair <br/>2. Stay hungry',
                    weight: '25%',
                    tootip: 'Sportlichkeit<br/>' +
                        '<p style="text-indent:1em">1.	Fight fair</p>' +
                        '<p style="text-indent:2em">I voice my opinion and am prepared to disagree with others</p>' +
                        '<p style="text-indent:2em">I discuss other’s opinions in a constructive way</p>' +
                        '<p style="text-indent:2em">I remain objective and tolerant, even when conflict arises </p>' +
                        '<p style="text-indent:2em">Together, we fight for the best result</p>' +
                        '<p style="text-indent:1em">2.	Stay hungry </p>' +
                        '<p style="text-indent:2em">I set myself ambitious goals</p>' +
                        '<p style="text-indent:2em">I seek and provide feedback and examine my own conduct </p>' +
                        '<p style="text-indent:2em">I update and expand my knowledge, learn quickly from mistakes and ' +
                        '<p style="text-indent:2em">acknowledge them openly</p>' +
                        '<p style="text-indent:2em">We strive to find the optimum solution</p>',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize\r\n the objectives by living the value of \"Sportlichkeit\".'
                }, {
                    name: 'One Family',
                    subTxt: '1. Take responsibility<br />' + '2. Respect each other',
                    weight: '25%',
                    tootip: 'One Family<br/>' +
                        '<p style="text-indent:1em">1.	Take responsibility</p>' +
                        '<p style="text-indent:2em">I keep my word and behave with integrity</p>' +
                        '<p style="text-indent:2em">I take responsibility for common objectives</p>' +
                        '<p style="text-indent:2em">I prioritize topics and create leeway for responsibility to be handed over </p>' +
                        '<p style="text-indent:2em">We ensure that objectives are achieved for our topics </p>' +
                        '<p style="text-indent:1em">2.	Respect each other </p>' +
                        '<p style="text-indent:2em">I develop individuals based on their strengths and weaknesses</p>' +
                        '<p style="text-indent:2em">I develop my team cross functionally and encourage its diversity </p>' +
                        '<p style="text-indent:2em">I am respectful and considerate of others and myself</p>' +
                        '<p style="text-indent:2em">We encourage interaction with each other</p>',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize the objectives by living the value of \"One Family\".'
                }],

            hasEdit: false,
            canScore: false,
            editDirection: null,
            howSelectPosition: null,

            howScores: [],

            howScoreTimes: null,

            isShowScoreList: false
        }
    },
    components: {
        ListSelect,
        Confirm
    },
    computed: {

    },
    methods: {
        confirm() {
            if (this.editDirection === 'prev') {
                this.$emit('changeIndex', {name: 'apa', target: 'pre', current: 'how'});
            } else if (this.editDirection === 'next') {
                this.$emit('changeIndex', {name: 'apa', target: 'next', current: 'how'});
            }

            this.isShowConfirm = false;
        },
        cancel() {
            this.isShowConfirm = false;
        },
        showScoreSelect(idx) {
            this.howSelectPosition = idx;
            if (idx < 3) {
                this.selectScoreTop = document.getElementsByClassName('score-wrapper')[idx].offsetTop - document.getElementsByClassName('score-wrapper')[idx].scrollTop;
            } else {
                this.selectScoreTop = document.getElementsByClassName('score-wrapper')[idx].offsetTop - document.getElementsByClassName('score-wrapper')[idx].scrollTop - 224;
            }

            this.isShowScoreList = !this.isShowScoreList;
        },
        selectScore(idx) {
            this.isShowScoreList = false;
            let score = this.scores[idx];
            if (this.howScores[this.howSelectPosition] === score || this.dimension[`how${this.howSelectPosition+1}_d${this.howScoreTimes}_score`] == score) {
                return false;
            }

            this.$set(this.howScores, this.howSelectPosition, score);

            this.howSelectPosition = null;

            if (!this.hasEdit) {
                this.hasEdit = true;
            }

            // 检查是否可以提交对应的分数
            if (!this.canScore) {
                for (let i=0, len=4; i<len; ++ i) {
                    if (!this.howScores[i] && !this.dimension[`how${i+1}_d${this.howScoreTimes}_score`]) {
                        return this.canScore = false;
                    }
                }

                this.canScore = true;
            }
        },

        prevOrNextWarning(direction) {
            this.titleTxt = 'Warning';
            this.confirmTxt = 'There are some changes not saved，are you sure to leave？?';
            this.isShowConfirm = true;
            this.isShowButtonLeft = true;
            this.editDirection = direction;

            return false;
        },
        previous() {
            if (this.anyChange()) {
                return this.prevOrNextWarning('prev');
            }

            this.$emit('changeIndex', {name: 'apa', target: 'pre', current: 'what'});
        },
        next() {
            if (this.anyChange()) {
                return this.prevOrNextWarning('next');
            }

            this.$emit('changeIndex', {name: 'apa', target: 'next', current: 'what'});
        },
        closeComponent() {
            this.$emit('changeComponent', 'close-manage-target-review');
        },
        anyChange() {
            return this.hasEdit = this.howScores.some(item => item);
        },
        delegatorScore() {
            if (!this.canScore) {
                return false
            }

            let params = {
                'function_id': this.functionId,
                'employee_id': this.dimension['employee_id']
            };

            for (let i=0, len=4; i<len; ++ i) {
                if (this.howScores[i] || this.dimension[`how${i+1}_d${this.howScoreTimes}_score`]) {
                    params[`how${i+1}_d${this.howScoreTimes}_score`] = this.howScores[i] || this.dimension[`how${i+1}_d${this.howScoreTimes}_score`];

                    if (this.howScores[i]) {
                        this.$set(this.howScores, i, this.howScores[i] || this.dimension[`how${i+1}_d${this.howScoreTimes}_score`]);
                    }
                }
            }

            let times1 = this.howScoreTimes === 'o' ? 'firstHowScore' : 'secondHowScore';
            let times2 = this.howScoreTimes === 'o' ? 'first' : 'second';

            request({
                url: times1,
                data: params,
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;

                    return false;
                }

                // TODO: 是否已经打完分
                // this.titleTxt = 'Success';
                // // this.confirmTxt = 'You are now ready for the calibration session.' : 'Save successfully';
                // this.confirmTxt = 'Save successfully';

                this.canScore = false;
                this.isShowConfirm = true;
                this.isShowButtonLeft = false;
                this.titleTxt = 'Success';
                this.confirmTxt = 'Save successfully';

                this.$emit('delegatorScoreHow', params);
            })
        }
    },
    mounted() {
        this.howScoreTimes = this.tableData[this.currentIndex]['target']['is_apa_delegator_submit'] === false ? 'o' : 't';
        for (let i=0, len=4; i<len; ++ i) {
            this.howScores[i] = null;
        }
    }
}
