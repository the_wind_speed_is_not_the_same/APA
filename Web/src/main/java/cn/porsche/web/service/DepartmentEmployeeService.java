package cn.porsche.web.service;


import cn.porsche.web.domain.DepartmentEmployee;
import cn.porsche.web.domain.User;

public interface DepartmentEmployeeService {
    DepartmentEmployee update(DepartmentEmployee data);

    DepartmentEmployee findById(Integer id);
    DepartmentEmployee findByEmployee(User employee);
}