package cn.porsche.compliance.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.ApplicationTest;
import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.emnu.SeasonEnum;
import cn.porsche.compliance.service.LoginService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.servlet.http.Cookie;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class UploadControllerTest extends ApplicationTest {
    private static final String API = "/upload";
    private static final String TrainningExcelFileName  = "Training Quiz 1-15.xlsx";
    private static final String LearningFileName        = "Training Quiz 1-15.xlsx";
    private static final String HighRishFileName        = "HighRisk.xlsx";
    private static final String TrainFileName           = "Basic Presentation Integrity for all employees.pptx";

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    private String token;

    @Test
    void uploadTraining() {
        final String api = API + "/training";

        Cookie cookie = new Cookie("auth_id", getHRAuthorization());
        File file = null;
        try {
            file = new ClassPathResource("test/" + TrainningExcelFileName).getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MockMultipartFile upload  = null;

        try {
            upload = new MockMultipartFile(
                    "file",
                    TrainningExcelFileName,
                    MediaType.APPLICATION_OCTET_STREAM_VALUE,
                    new FileInputStream(file)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            mvcResult = mvc.perform(
                    multipart(api).file(upload)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .cookie(cookie)
//                            .param("year", String.valueOf(year))
//                            .param("season", String.valueOf(season))
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void uploadRisk() {
        final String api = API + "/risk";

        Cookie cookie = new Cookie("auth_id", getHRAuthorization());
        File file = null;
        try {
            file = new ClassPathResource("test/" + HighRishFileName).getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MockMultipartFile upload  = null;

        try {
            upload = new MockMultipartFile(
                    "file",
                    TrainningExcelFileName,
                    MediaType.APPLICATION_OCTET_STREAM_VALUE,
                    new FileInputStream(file)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            mvcResult = mvc.perform(
                    multipart(api).file(upload)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .cookie(cookie)
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void uploadStaffQuestion() {
        final String api = API + "/staff_question";

        Cookie cookie = new Cookie("auth_id", getHRAuthorization());
        File file = null;
        try {
            file = new ClassPathResource("test/" + LearningFileName).getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MockMultipartFile upload  = null;

        try {
            upload = new MockMultipartFile(
                    "file",
                    LearningFileName,
                    MediaType.APPLICATION_OCTET_STREAM_VALUE,
                    new FileInputStream(file)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            mvcResult = mvc.perform(
                    multipart(api).file(upload)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .cookie(cookie)
                            .param("type", EmployeeTypeEnum.STAFF.toString())
                            .param("year", "2021")
                            .param("season", SeasonEnum.Q2.toString())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void uploadVPQuestion() {
        final String api = API + "/vp_question";

        Cookie cookie = new Cookie("auth_id", getHRAuthorization());
        File file = null;
        try {
            file = new ClassPathResource("test/question_up.xlsx").getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MockMultipartFile upload  = null;

        try {
            upload = new MockMultipartFile(
                    "file",
                    LearningFileName,
                    MediaType.APPLICATION_OCTET_STREAM_VALUE,
                    new FileInputStream(file)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            mvcResult = mvc.perform(
                    multipart(api).file(upload)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .cookie(cookie)
                            .param("type", EmployeeTypeEnum.STAFF.toString())
                            .param("year", "2021")
                            .param("season", SeasonEnum.Q2.toString())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void uploadOuterQuestion() {
        final String api = API + "/outer_question";

        Cookie cookie = new Cookie("auth_id", getEmployeeAuthorization());
        File file = null;
        try {
            file = new ClassPathResource("test/" + LearningFileName).getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MockMultipartFile upload  = null;

        try {
            upload = new MockMultipartFile(
                    "file",
                    LearningFileName,
                    MediaType.APPLICATION_OCTET_STREAM_VALUE,
                    new FileInputStream(file)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            mvcResult = mvc.perform(
                    multipart(api).file(upload)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .cookie(cookie)
                            .param("type", EmployeeTypeEnum.STAFF.toString())
                            .param("year", "2021")
                            .param("season", SeasonEnum.Q1.toString())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void uploadTrainFile() {
        final String api = API + "/train_file";

        Cookie cookie = new Cookie("auth_id", getHRAuthorization());
        File file = null;
        try {
            file = new ClassPathResource("test/" + TrainFileName).getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MockMultipartFile upload  = null;

        try {
            upload = new MockMultipartFile(
                    "file",
                    TrainFileName,
                    MediaType.APPLICATION_OCTET_STREAM_VALUE,
                    new FileInputStream(file)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            mvcResult = mvc.perform(
                    multipart(api).file(upload)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .cookie(cookie)
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Test
    void uploadVips() {
        final String api = API + "/vps";

        Cookie cookie = new Cookie("auth_id", getEmployeeAuthorization());
        MockMultipartFile upload  = null;
        String[] vps = new String[] {"PCN186", "PCN617", "PCN665", "PCN922", "PCN467", "PCN478", "PCN850", "PCN421", "PCN696", "PCN499"};

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .cookie(cookie)
                            .param("vps", vps)
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}