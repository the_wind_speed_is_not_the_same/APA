package cn.porsche.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class AccessToken {
    @JsonIgnore
    private String appId;

    @JsonIgnore
    private String appSercet;

    @JsonIgnore
    private String tokenType;

    @JsonIgnore
    private String accessToken;

    // 第三方凭证
    @JsonIgnore
    private String ticket;

    @JsonIgnore
    private Date accessTokenExpiresTime;

    @JsonIgnore
    private String refreshToken;

    @JsonIgnore
    private Date refreshTokenExpiresTime;

    @JsonIgnore
    private String openId;

    @JsonIgnore
    private String scope;

    @JsonIgnore
    private String authId;

    @JsonIgnore
    private String pound;

    public AccessToken(String accessToken, String refreshToken, String authId, String tokenType, String scope,
                         Date accessTokenExpiresTime, Date refreshTokenExpiresTime, String pound) {
        this.refreshToken             = refreshToken;
        this.accessToken              = accessToken;
        this.tokenType                = tokenType;

        this.authId                   = authId;
        this.scope                    = scope;
        this.pound                    = pound;

        this.accessTokenExpiresTime   = accessTokenExpiresTime;
        this.refreshTokenExpiresTime  = refreshTokenExpiresTime;
    }


    public boolean isAccessTokenValidity() {
        return null != accessTokenExpiresTime && accessTokenExpiresTime.before(new Date());
    }

    public boolean isRefreshTokenValidity() {
        return null != refreshTokenExpiresTime && refreshTokenExpiresTime.before(new Date());
    }
}
