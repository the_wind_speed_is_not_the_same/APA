package cn.porsche.compliance.service.impl;

import cn.porsche.compliance.domain.File;
import cn.porsche.compliance.repository.FileRepository;
import cn.porsche.compliance.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {
    @Autowired
    FileRepository repository;

    @Override
    public void delete(File file) {
        repository.delete(file);
    }

    @Override
    public File update(File file) {
        return repository.save(file);
    }

    @Override
    public File findByMd5(String md5) {
        return repository.findByMd5(md5);
    }

    @Override
    public File findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public List<File> findAll() {
        return repository.findAll();
    }
}
