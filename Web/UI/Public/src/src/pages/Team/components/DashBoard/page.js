import {mapState, mapActions} from 'vuex'
import echarts from 'echarts';
import ListSelect from '../../../../components/ListSelect/page.vue';
import {deepClone} from '../../../../common/utils.js';

export default {
    props: ['user', 'pieData', 'pieIndex'],
    data() {
        return {
            selectIndex: this.pieIndex,
            isShowListSelect: false,
            list: ['Target Setting', 'APA'],
            isShowYearList: false,
            selectYearIndex: 0
        }
    },
    components: {
        ListSelect
    },
    computed: {
        ...mapState({
            allList: state => state.allState.allList,
            yearList: state => state.allState.yearList,
            currentYear: state => state.allState.currentYear,
            currentTeamYear: state => state.allState.currentTeamYear
        }),
    },
    watch: {
        pieData: {
            handler(n, o) {
                this.initMyChart();
            },
            deep: true,
        }
    },
    methods: {
        ...mapActions(['setCurrentYear', 'setYearList']),
        showList() {
            this.isShowListSelect = true;
        },
        showYearList() {
            this.isShowYearList = true
        },
        confirmYearIndex(index) {
            if (index == this.selectYearIndex) {
                this.isShowYearList = false;
                return;
            }
            this.selectYearIndex = index
            this.setCurrentYear(this.yearList[this.selectYearIndex])
            this.isShowYearList = false
            this.$emit('changeYear', this.allList[index]);
            this.initMyChart(true)
        },
        confirmIndex(index) {
            this.selectIndex = index;
            this.isShowListSelect = false;
            this.$emit('changeType', index);
            this.initMyChart(true)
        },
        initMyChart(bool) {
            let pieData = deepClone(this.pieData.data);
            let pieDataNotNull = pieData.filter(item => item.value != 0);
            if (pieDataNotNull.length) {
                pieData.forEach(item => {
                    if (item.value == 0) {
                        item.name = '';
                    }
                })
            }
            let option = {
                tooltip: {
                    trigger: 'item',
                    formatter: '{b}: {c} ({d}%)'
                },
                series: [{
                    name: 'target-setting',
                    type: 'pie',
                    radius: ['23%', '46%'],
                    avoidLabelOverlap: false,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)',
                        }
                    },
                    label: {
                        formatter: '{b|{b}}',
                        rich: {
                            b: {
                                color: '#000',
                                fontSize: 18,
                                lineHeight: 26,
                            }
                        }
                    },
                    labelLine: {
                        show: false
                    },
                    data: pieData
                }]
            };
            this.myChart.setOption(option, !!bool);
        }
    },
    created() {
        if (this.user['roles'] && this.user['roles'].indexOf('manager') < 0) {
            this.list.pop();
        }


        const {function_id} = this.$route.query

        this.setYearList().then(res => {
            if (function_id) {
                this.function_id = function_id
                res.forEach((item, idx) => {
                    if (item.id == function_id) {
                        // this.to = item.year
                        this.selectYearIndex = idx
                        this.$emit('changeYear', this.allList[idx]);
                    }
                })
            } else {
                if (!this.currentTeamYear) {
                    this.setCurrentYear(this.yearList[this.selectYearIndex])
                } else {
                    let index = this.yearList.indexOf(Number(this.currentTeamYear))
                    this.selectYearIndex = index
                    this.$emit('changeYear', this.allList[index]);
                }
            }
        })

    },
    mounted() {
        this.myChart = echarts.init(this.$refs.myChart);
    }
}
