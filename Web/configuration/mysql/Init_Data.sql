CREATE DATABASE  IF NOT EXISTS `APA` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `APA`;
-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: 54.223.133.122    Database: APA
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1-log


INSERT INTO `groups` VALUES (1,0,0,'HR Performance Team',NULL,0,'2020-10-11 07:00:19'),(2,0,0,'Disciplinary Manager',NULL,0,'2020-10-11 07:00:19'),(3,0,0,'Employee',NULL,1,'2020-10-11 07:00:19'),(4,0,0,'Top Management','Vice President',0,'2020-10-11 07:00:19'),(5,0,0,'New Employees In Probation',NULL,0,'2020-10-11 07:00:19'),(6,0,0,'Long Leaves',NULL,0,'2020-10-11 07:00:19'),(7,0,0,'Delegator',NULL,0,'2020-10-11 07:00:20');

-- Diana Zhuang
-- 正式环境（设置HR）： INSERT INTO `group_users` (`group_id`, `user_id`) VALUES ('1', '1905');
-- 正式环境（设置HR）： INSERT INTO `group_users` (`group_id`, `user_id`) VALUES ('1', '3314');


-- 测试环境（设置HR）： INSERT INTO `group_users` (`group_id`, `user_id`) VALUES ('1', '501376');