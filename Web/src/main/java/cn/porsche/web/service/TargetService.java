package cn.porsche.web.service;


import cn.porsche.web.constant.ScoreLevelEnum;
import cn.porsche.web.controller.params.KPIParam;
import cn.porsche.web.controller.params.WhatParam;
import cn.porsche.web.domain.*;
import cn.porsche.web.exceptions.TargetException;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface TargetService {
    Target get(Integer id);
    Target reset(Target target);
    Target delete(Target target);
    Target update(Target target);
    Target findByUser(Function function, User user);
    Target findByKPIUser(KPIUser user);
    Target open(Function function, User user);

    KPIParam findTargetDetail(Target target, boolean isDelegator);

    // 新增How
    Target updateHow(Target target, TargetHow how, User user);

    // 新增What
    Target updateWhat(Target target, WhatParam param, User user);

    // 删除What
    Target deleteWhat(Target target, TargetWhat what);

    // Confirm What
    Target rejectWhatByDelegator(Target target, TargetWhat what, Comment comment);

    Target confirmByEmployee(Target target);
    Target confirmByDelegator(Target target);

    Target autoTargetSetting(Function function, User user) throws Exception;

    Target autoAppraisal(Function function, User user, ScoreLevelEnum levelEnum);


    // Appraisal
    // 员工打分
    Target eAppraisalWhat(Function function, User user, Target target, TargetWhat what, WhatParam appraisal);

    Target eAppraisalHow(Function function, User user, Target target, TargetHow appraisal);

    Target tsESubmit(Function function, User user) throws TargetException;

    // 员工确认分数
    Target apaESubmit(Function function, User user, boolean official) throws TargetException;
    // 经理第一、二次确认分数
    Target apaDOSubmit(Function function, User user, boolean official) throws TargetException;
    Target apaDTSubmit(Function function, User user, boolean official) throws TargetException;

    // 经理打分
    Target firstWhatAppraisal(Function function, User employee, Target target, TargetWhat what, Float score);
    // 经理最终打分
    Target secondWhatAppraisal(Function function, User employee, Target target, TargetWhat what, Float score);

    // 经理打分
    Target firstHowAppraisal(Function function, User employee, Target target, TargetHow appraisal);
    // 经理最终打分
    Target secondHowAppraisal(Function function, User employee, Target target, TargetHow appraisal);


    // 经理下所有的Target列表
    List<Target> findAllByManager(Function function, User manager);

    // 委托下所有的Target列表
    List<Target> findAllByDelegator(Function function, User delegator);

    // 按部门查找Target列表
    List<Target> findAllByDepartment(Function function, Department department);
    Page<Target> findAllByDepartment(Function function, Department department, int page, int size);

    // 根据Target查找对应的How
    TargetHow findHowByTarget(Target target);

    // 根据
    KPIUser findKPIUserByTarget(Target target);

    // 根据WhatId主键查找对应的TargetWhat
    TargetWhat findWhatById(Integer whatId);

    // 根据HowId主键查找对应的TargetHow
    TargetHow findHowById(Integer howId);

    // 根据Target查找所有的What
    List<TargetWhat> findAllWhatByTarget(Target target);

    // 根据Target查找所有的What
    List<TargetKPI> findAllKPIByTarget(Target target, TargetWhat what);

    List<Comment> findAllOverallByTarget(Target target);

    // 检查Target及自身所有的数据是否符合要求。
    //      符合要求：所有字段都返回 yes
    //      异常字段: 异常字段返回 empty
    Map<String, Map<String, String>> check(Target target);
    boolean canPass(Target target);


    // 页面的统计信息
    int countTSByNotStatedByDepartment(Function function, Department department);
    int countTSBySubmittedByDepartment(Function function, Department department);
    int countTSByReviewedByDepartment(Function function, Department department);
    int countTSByConfirmedByDepartment(Function function, Department department);

    // 页面的统计信息
    int countAPAByNotStatedByDepartment(Function function, Department department);
    int countAPABySubmittedByDepartment(Function function, Department department);
    int countAPAByReviewedByDepartment(Function function, Department department);
    int countAPAByConfirmedByDepartment(Function function, Department department);


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Reporting 处 的图表数据
    ///

    // 一级部门下TS的统计信息
    int countTSByNotStartByDepartment1(Function function, Department department);
    int countTSByInProgressByDepartment1(Function function, Department department);
    int countTSByFinishedByDepartment1(Function function, Department department);

    // 一级部门APA的统计信息
    int countAPAByNotStartByDepartment1(Function function, Department department);
    int countAPAByInProgressByDepartment1(Function function, Department department);
    int countAPAByFinishedByDepartment1(Function function, Department department);

    // 部门下TS的统计信息
    int countTSByNotStartByDepartment2(Function function, Department department);
    int countTSByInProgressByDepartment2(Function function, Department department);
    int countTSByFinishedByDepartment2(Function function, Department department);

    // 部门下APA的统计信息
    int countAPAByNotStartByDepartment2(Function function, Department department);
    int countAPAByInProgressByDepartment2(Function function, Department department);
    int countAPAByFinishedByDepartment2(Function function, Department department);


    // 部门下TS的统计信息
    int countTSByNotStartByDepartment3(Function function, Department department);
    int countTSByInProgressByDepartment3(Function function, Department department);
    int countTSByFinishedByDepartment3(Function function, Department department);

    // 部门下APA的统计信息
    int countAPAByNotStartByDepartment3(Function function, Department department);
    int countAPAByInProgressByDepartment3(Function function, Department department);
    int countAPAByFinishedByDepartment3(Function function, Department department);

    ///
    /// Reporting 处 的图表数据
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
