import {request} from '../../../../common/request.js';
import Confirm from '../../../../components/Confirm/page.vue';

export default {
    props: ['dimension', 'isBtnDisabled', 'componentType', 'tableData', 'currentIndex'],
    data() {
        return {
            dimensionsList: [
                {
                    name: 'Herzblut',
                    subTxt: '1. Live Passionately <br /> 2. Know where you come from',
                    weight: '25%',
                    description: '',
                    tootip: 'Herzblut<br/>' +
                        '<p style="text-indent:1em">1.  Live Passionately</p>' +
                        '<p style="text-indent:2em">I inspire others for our work </p>' +
                        '<p style="text-indent:2em">I have high standards for my work </p>' +
                        '<p style="text-indent:2em">I celebrate successes with others </p>' +
                        '<p style="text-indent:2em">We work with commitment and with conviction</p>' +
                        '<p style="text-indent:1em">2.	Know where you come from </p>' +
                        '<p style="text-indent:2em">I do not take the given things for granted</p>' +
                        '<p style="text-indent:2em">I utilize our resources intelligently </p>' +
                        '<p style="text-indent:2em">We know for whom we work - for our customers </p>' +
                        '<p style="text-indent:2em">We foster social partnerships and promote a fair balance </p>'+
                        '<p style="text-indent:2em">between employee and company interests',
                    placeholder: "Please describe how you are going to perform and realize the objectives by living the value of \"Herzblut\"."
                }, {
                    name: "Pioniergeist",
                    subTxt: '1. Look into the future with courage<br />2. Think outside the box',
                    weight: '25%',
                    tootip: 'Pioniergeist<br/>' +
                        '<p style="text-indent:1em">1.  Look into the future with courage</p>' +
                        '<p style="text-indent:2em">I respond flexibly to changing framework conditions</p>' +
                        '<p style="text-indent:2em">I keep the goal in mind when there is uncertainty </p>' +
                        '<p style="text-indent:2em">I am willing to take risks </p>' +
                        '<p style="text-indent:2em">We consider new technologies to be a chance <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; and drive forward future-oriented ideas (regardless of their origin)</p>' +
                        '<p style="text-indent:1em">2.	Think outside the box </p>' +
                        '<p style="text-indent:2em">I ensure, that our objectives and backgrounds are transparent</p>' +
                        '<p style="text-indent:2em">I analyze challenges quickly, reduce complexity and derive specific conclusions </p>' +
                        '<p style="text-indent:2em">I question the status quo and bring in solutions </p>' +
                        '<p style="text-indent:2em">We build networks to achieve common objectives',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize the objectives by living the value of \"Pioniergeist\".'
                }, {
                    name: 'Sportlichkeit',
                    subTxt: '1. Fight fair <br/>2. Stay hungry',
                    weight: '25%',
                    tootip: 'Sportlichkeit<br/>' +
                        '<p style="text-indent:1em">1.	Fight fair</p>' +
                        '<p style="text-indent:2em">I voice my opinion and am prepared to disagree with others</p>' +
                        '<p style="text-indent:2em">I discuss other’s opinions in a constructive way</p>' +
                        '<p style="text-indent:2em">I remain objective and tolerant, even when conflict arises </p>' +
                        '<p style="text-indent:2em">Together, we fight for the best result</p>' +
                        '<p style="text-indent:1em">2.	Stay hungry </p>' +
                        '<p style="text-indent:2em">I set myself ambitious goals</p>' +
                        '<p style="text-indent:2em">I seek and provide feedback and examine my own conduct </p>' +
                        '<p style="text-indent:2em">I update and expand my knowledge, learn quickly from mistakes and ' +
                        '<p style="text-indent:2em">acknowledge them openly</p>' +
                        '<p style="text-indent:2em">We strive to find the optimum solution</p>',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize\r\n the objectives by living the value of \"Sportlichkeit\".'
                }, {
                    name: 'One Family',
                    subTxt: '1. Take responsibility<br />' + '2. Respect each other',
                    weight: '25%',
                    tootip: 'One Family<br/>' +
                        '<p style="text-indent:1em">1.	Take responsibility</p>' +
                        '<p style="text-indent:2em">I keep my word and behave with integrity</p>' +
                        '<p style="text-indent:2em">I take responsibility for common objectives</p>' +
                        '<p style="text-indent:2em">I prioritize topics and create leeway for responsibility to be handed over </p>' +
                        '<p style="text-indent:2em">We ensure that objectives are achieved for our topics </p>' +
                        '<p style="text-indent:1em">2.	Respect each other </p>' +
                        '<p style="text-indent:2em">I develop individuals based on their strengths and weaknesses</p>' +
                        '<p style="text-indent:2em">I develop my team cross functionally and encourage its diversity </p>' +
                        '<p style="text-indent:2em">I am respectful and considerate of others and myself</p>' +
                        '<p style="text-indent:2em">We encourage interaction with each other</p>',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize the objectives by living the value of \"One Family\".'
                }
            ],
            isShowConfirm: false,
            titleTxt: '',
            confirmTxt: '',
            showTime: 0
        }
    },
    components: {
        Confirm
    },
    computed: {
        canConfirm() {
            return this.tableData[this.currentIndex].target.is_ts_employee_submit && !this.dimension.is_delegator_confirm
        }
    },
    methods: {
        confirm() {
            this.isShowConfirm = false
        },
        previous() {
            this.$emit('changeIndex', {name: 'ts', target: 'pre', current: 'how'});
        },
        next() {
            this.$emit('changeIndex', {name: 'ts', target: 'next', current: 'how'});
        },
        closeComponent() {
            this.$emit('changeComponent', 'close-manage-target-review');
        },
        saveHow() {
            if (this.isBtnDisabled) return;
            let function_id = localStorage.getItem('function_id');
            let data = {
                how1: '1',
                how2: '2',
                how3: '3',
                how4: '4',
                function_id
            }
            this.dimensionsList.forEach((item, index) => {
                data[`how${index + 1}`] = item.description;
            })
            request({
                url: 'updateHow',
                data,
                isShowLoading: true
            }).then(res => {
                if (res && res.data) {

                }
            })
        },
        confirmHow() {
            if (this.canConfirm) {
                request({
                    url: 'confirmHow',
                    method: 'get',
                    params: {
                        how_id: this.dimension.id
                    },
                    isShowLoading: true
                }).then(res => {
                    if (res) {
                        this.titleTxt = 'Confirm'
                        this.confirmTxt = 'Confirm successfully.'
                        this.isShowConfirm = true
                        this.$emit('confirmHow', true)
                        this.dimension.is_delegator_confirm = true
                    }
                })
            }
        }
    },
    mounted() {
        this.dimensionsList.forEach((item, index) => {
            item.description = this.dimension && this.dimension[`how${index + 1}`];
        })
    }
}
