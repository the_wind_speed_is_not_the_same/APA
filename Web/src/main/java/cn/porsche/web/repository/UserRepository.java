package cn.porsche.web.repository;

import cn.porsche.web.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    Integer countByStatus(Integer status);
    Page<User> findAllByStatus(Integer status, Pageable pageable);

    User findByCode(String code);
    User findByPositionId(Integer positionId);

    @Query("SELECT u from User u LEFT JOIN KPIUser kpi ON u.id=kpi.employeeId WHERE kpi.functionId=:functionId")
    Page<User> findAllByFunctionId(@Param("functionId")Integer functionId, Pageable page);

    Page<User> findAllByNeedKPI(Boolean yes, Pageable page);

    Integer countByNeedHands(Boolean yes);
    Page<User> findAllByNeedHands(Boolean yes, Pageable page);

    List<User> findAllByManagerId(Integer managerId);
}
