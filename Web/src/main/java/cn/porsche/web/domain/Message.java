package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "messages", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    Integer id;

    @ApiModelProperty(notes = "KPI主键")
    @JsonProperty("function_id")
    @Column(name = "function_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'KPI主键'")
    Integer functionId;

    @ApiModelProperty(notes = "消息拥有者主键")
    @JsonProperty("owner_id")
    @Column(name = "owner_id",                  columnDefinition = "INT UNSIGNED NOT NULL COMMENT '消息拥有者主键'")
    Integer ownerId;

    @ApiModelProperty(notes = "绩效主键")
    @JsonProperty("target_id")
    @Column(name = "target_id",                 columnDefinition = "INT UNSIGNED NULL COMMENT '绩效主键'")
    Integer targetId;

    @ApiModelProperty(notes = "What主键")
    @JsonProperty("what_id")
    @Column(name = "what_id",                   columnDefinition = "INT UNSIGNED NULL COMMENT '绩效主键'")
    Integer whatId;

    @ApiModelProperty(notes = "发送者主键")
    @JsonProperty("sender_id")
    @Column(name = "sender_id",                 columnDefinition = "INT UNSIGNED NOT NULL COMMENT '员工主键'")
    Integer senderId;

    @ApiModelProperty(notes = "发送者")
    @JsonProperty("sender")
    @Column(name = "sender",                    columnDefinition = "VARCHAR(255) NOT NULL COMMENT '发送者'")
    String sender;

    @ApiModelProperty(notes = "发送者")
    @JsonProperty("sender_mail")
    @Column(name = "sender_mail",               columnDefinition = "VARCHAR(255) NOT NULL COMMENT '发送者邮箱'")
    String senderEmail;

    @ApiModelProperty(notes = "接受者主键")
    @JsonProperty("receiver_id")
    @Column(name = "receiver_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT '委托人主键'")
    Integer receiverId;

    @ApiModelProperty(notes = "接受者")
    @JsonProperty("receiver")
    @Column(name = "receiver",                  columnDefinition = "VARCHAR(255) NOT NULL COMMENT '接受者'")
    String receiver;

    @ApiModelProperty(notes = "接受者邮箱")
    @JsonProperty("receiver_email")
    @Column(name = "receiver_email",            columnDefinition = "VARCHAR(255) NOT NULL COMMENT '接受者邮箱'")
    String receiverEmail;

    @ApiModelProperty(notes = "消息提示")
    @JsonProperty("subject")
    @Column(name = "subject",                   columnDefinition = "TEXT NULL DEFAULT NULL COMMENT '提示标题'")
    String subject;

    @ApiModelProperty(notes = "消息提示")
    @JsonProperty("message")
    @Column(name = "message",                   columnDefinition = "TEXT NULL DEFAULT NULL COMMENT '消息提示'")
    String message;

    @ApiModelProperty(notes = "消息提示")
    @JsonProperty("uri")
    @Column(name = "uri",                       columnDefinition = "VARCHAR(63) NULL DEFAULT NULL COMMENT '消息对应的页面地址'")
    String uri;

    @ApiModelProperty(notes = "是否已读")
    @JsonProperty("is_read")
    @Column(name = "is_read",                   columnDefinition = "TINYINT(1) NULL DEFAULT NULL COMMENT '是否已读'")
    boolean read;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @ApiModelProperty(notes = "发送时间")
    @JsonProperty("send_time")
    @Column(name = "send_time",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date sendTime;
}
