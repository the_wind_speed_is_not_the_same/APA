import { mapState } from 'vuex'
import Confirm from '../Confirm/page.vue'

export default {
    props:['isShowMenu'],
    data() {
        return {
            selected: null,
            activeIndex:null,
            isCollapse:false,
            isShowConfirm:false
        }
    },
    components:{
      Confirm
    },
    watch: {
        isShowMenu(val) {
            if (val) {
                this.initSelect();
            }
        }
    },
    computed: {
        ...mapState({
            menuList: state => state.allState.menuList
        })
    },
    methods:{
        cancel(){
            this.isShowConfirm = false
        },
        logout() {
            // 清理存储的数据
            localStorage.clear();
            if (process.env.NODE_ENV === 'production')
                // 清除cookie
                window.location.href = "/login/out";
        },
        closeMenu() {
            this.$emit('closeMenu');
        },
        switchMenu(menu) {
            if (menu.name === 'Setting') {
                this.isCollapse = !this.isCollapse
                return false;
            } else if (menu.name === 'Logout') {
                // 退出
                this.isShowConfirm = true
                return false
            }

            this.isCollapse = false

            if (this.activeIndex !== menu.index ) {
                if (menu.children && menu.children.length) {
                    // 有二级
                } else {
                    this.$router.push(menu.path);
                    this.$nextTick(() => {
                        this.closeMenu();
                    })
                }

              this.selected = menu.parent_id || menu.index;
              this.activeIndex = menu.index;
            }
        },
        initSelect() {
            // 默认选中menu
            let path = this.$route.path;
            let filter = [];

            this.menuList.filter(item => {
                if (item.path && item.path.split('?')[0] === path) {
                    filter.push(item);
                } else if (item.children && item.children.length) {
                    item.children.forEach(item=> {
                        if (item.path === path) {
                            filter.push(item);
                        }
                    })
                }
            });

            if (filter.length) {
                this.activeIndex = filter[0].index;
                this.selected    = filter[0].parent_id || filter[0].index;
                if (filter[0].name === 'Setting' || filter[0].parent_id) {
                  this.isCollapse = true
                }
            } else {
                this.selected = null;
            }
        }
    },
    created() {
        this.initSelect();
    },
    mounted() {
      document.addEventListener('click',this.closeMenu)
    }
}
