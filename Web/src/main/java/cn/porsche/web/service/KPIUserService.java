package cn.porsche.web.service;


import cn.porsche.web.domain.*;
import org.springframework.data.domain.Page;

import java.util.List;

public interface KPIUserService {
    void deleteByTarget(Target target);

    KPIUser findById(Integer id);
    KPIUser update(KPIUser kpiUser);

    KPIUser builder(Function function, User user, User delegator, Department department, Position position);
    List<KPIUser> builder(Function function, List<User> user);





    KPIUser findByTarget(Target target);
    KPIUser findByFunction(Function function, User user);


    List<KPIUser> findAllByManager(Function function, User manager);
    List<KPIUser> findAllByDelegator(Function function, User delegator);
    List<KPIUser> findAllByDepartment(Function function, Department department);

    Page<KPIUser> findAllByFunction(Function function, int page, int size);
    Page<KPIUser> findAllByFunction(Function function, Group group, int page, int size);
    Page<KPIUser> findAllByDelegator(Function function, User delegator, int page, int size);
    Page<KPIUser> findAllByDepartment(Function function, Department department, int page, int size);


    List<KPIUser> findAllByUser(User user);
}