package cn.porsche.compliance.task.parsing;

import cn.porsche.common.util.JsonUtils;
import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.Position;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.task.AbstractParsingApiTasks;
import cn.porsche.compliance.task.parsing.exception.ParsingException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ParsingHRISApiTask extends AbstractParsingApiTasks {
    @Override
    protected void parsing(final List<String[]> list, API api) throws ParsingException {
        String[] item;
        for (int i = 0; i < list.size(); i++) {
            item = list.get(i);
            switch (api) {
                case Department:
                    parsingDepartment(item, item.length);
                    break;
                case Employee:
                    parsingEmployee(item, item.length);
                    break;
                case Position:
                    parsingPosition(item, item.length);
                    break;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    //    DEPARTMENTID	DEPARTMENTNAME	                PARENTDEPARTMENDID	Company Code-Company
    //    PCN	        Porsche China	                PCN	                0091_PCN(v2 deleted)
    //    PCN-BD	    Strategy & Business Development	PCN	                0091_PCN(v2 deleted)
    //    PCN-CRM	    Customer Relations	            PCN	                0091_PCN(v2 deleted)
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void parsingDepartment(final String[] parts, int len) throws ParsingException {
        if (parts.length != len) {
            throw ParsingException.ItemsLengthException(len, parts.length, parts.toString());
        }

//        if (filter(parts[2])) {
//            return;
//        }

        logger.info("开始处理部门数据 {}", JsonUtils.toString(parts));

        boolean isUpdate = false;
        Department department = departmentService.findByCode(parts[0]);
        Department parent     = departmentService.findByCode(parts[2]);;

        // 0DEPARTMENTID 1DEPARTMENTNAME 2PARENTDEPARTMENDID 3Company Code-Company
        if (department == null) {
            if (null == parent) {
                if (parts[0].equals(parts[2])) {
                    // 父类就是自己本身
                    department = Department.builder()
                            .managerId(null)
                            .parentId(0)
                            .parentCode(null)
                            .companyCode(parts[3])
                            .code(parts[0])
                            .name(parts[1])
                            .count(0)
                            .build();
                } else {
                    // 需要创建一个父类
                    parent = Department.builder()
                            .managerId(null)
                            .parentId(0)
                            .parentCode(null)
                            .companyCode(parts[3])
                            .code(parts[2])
                            .name(parts[2])
                            .count(0)
                            .build();

                    departmentService.update(parent);
                }
            }

            if (null == department) {
                department = Department.builder()
                        .managerId(null)
                        .parentId(parent.getId())
                        .parentCode(parent.getCode())
                        .companyCode(parts[3])
                        .code(parts[0])
                        .name(parts[1])
                        .count(0)
                        .build();
            }

            isUpdate = true;
        }

        // 1DEPARTMENTNAME
        if (StringUtils.isNotEmpty(parts[1]) && !parts[1].equalsIgnoreCase(department.getName())) {
            department.setName(StringUtils.trim(parts[1]));

            isUpdate = true;
        }

        // 2PARENTDEPARTMENDID
        if (StringUtils.isNotEmpty(parts[2]) && !parts[2].equalsIgnoreCase(department.getParentCode())) {
            department.setParentCode(StringUtils.trim(parts[2]));

            isUpdate = true;
        }

        // 新版本无3 Company Code-Company，直接使用ID
        // 3Company Code-Company
        if (StringUtils.isNotEmpty(parts[3]) && !parts[3].equalsIgnoreCase(department.getCompanyCode())) {
            department.setCompanyCode(StringUtils.trim(parts[3]));

            isUpdate = true;
        }

        if (parent != null && !parent.getId().equals(department.getParentId()) && !parent.getCode().equalsIgnoreCase(department.getCode())) {
            department.setParentId(parent.getId());

            isUpdate = true;
        }

        if (isUpdate) {
            departmentService.update(department);
        }
    }

    //0UserID 1FIRSTNAMEEN 2LASTNAMEEN 3USERNAMECN 4USERCODE 5JOBCODEID 6DEPARTMENTID 7DEPARTMENTNAME 8COSTCENTERCODE 9EMAIL
    // 10ORGANIZATION 11MOBILENO 12TEL 13Preferred Name
    private void parsingEmployee(final String[] parts, int len) throws ParsingException {
        if (parts.length != len) {
            throw ParsingException.ItemsLengthException(len, parts.length, parts.toString());
        }

        logger.info("开始处理员工数据 {}", JsonUtils.toString(parts));

        String code = parts[4];
        if (null == code) {
            // 工号为空，无法处理
            logger.error("同步数据无工号，不做处理：{}", JsonUtils.toString(parts));
            return;
        }

        code = code.toUpperCase();

        if (filter(code)) {
            // 没有 sso系统的 CODE，或不是PCN的员工
            logger.error("员工数据不符合工号要求，不做处理：{}", JsonUtils.toString(parts));
            return;
        }

        // 实习生也不参与
        Integer id = Integer.parseInt(parts[0]);
        if (id == null) {
            logger.info("解析 员工文件出错，转换员工的外部主键失败，外部主键为： " + parts[0]);
            return;
        }

        // 5JOBCODEID
        Position position = null;
        if (StringUtils.isNotEmpty(parts[5])) {
            position = positionService.findById(Integer.parseInt(parts[5]));
            if (null == position) {
                logger.debug("职位信息：{} 在数据库不存在", parts[5]);
            }
        }


        // 6DEPARTMENTID
        Department department = null;
        if (StringUtils.isNotEmpty(parts[6])) {
            department = departmentService.findByCode(parts[6]);
            if (null == department) {
                logger.debug("部门信息：{} 在数据库不存在", parts[6]);

                throw ParsingException.DepartmentNotExistInDatabase(parts.toString());
            }
        }

        boolean isUpdate = false;
        User user = userService.findByCode(code);
        if (user == null) {
            // 系统中不存在
            user = User.builder()
                    .id(id)
                    .code(code)
                    .departmentId(null)
                    .avatar("")
                    .firstName("")
                    .lastName("")
                    .cnName("")
                    .enName("")
                    .title("")
                    .email("")
                    .mobile(null)
                    .tel(null)

                    .type(EmployeeTypeEnum.STAFF)

                    .status(0)
                    .build();

            isUpdate = true;
        }

        // 分解如下字段
        //0UserID 1FIRSTNAMEEN 2LASTNAMEEN 3USERNAMECN 4USERCODE 5JOBCODEID 6DEPARTMENTID 7DEPARTMENTNAME 8COSTCENTERCODE 9EMAIL
        // 10ORGANIZATION 11MOBILENO 12TEL 13Preferred Name

        // 1FIRSTNAMEEN
        if (StringUtils.isNotEmpty(parts[1]) && !parts[1].equalsIgnoreCase(user.getFirstName())) {
            user.setFirstName(StringUtils.trim(parts[1]));

            isUpdate = true;

            logger.info(" 更新员工数据FirstName {}", user.getFirstName());
        }

        // 2LASTNAMEEN
        if (StringUtils.isNotEmpty(parts[2]) && !parts[2].equalsIgnoreCase(user.getLastName())) {
            user.setLastName(StringUtils.trim(parts[2]));

            isUpdate = true;

            logger.info(" 更新员工数据LastName {}", user.getLastName());
        }

        // 3USERNAMECN
        if (StringUtils.isNotEmpty(parts[3]) && !parts[3].equalsIgnoreCase(user.getCnName())) {
            user.setCnName(StringUtils.trim(parts[3]));

            isUpdate = true;

            logger.info(" 更新员工数据CnName {}", user.getCnName());
        }

//        // 4USERCODE（SSO 系统使用的Code）
//        if (StringUtils.isNotEmpty(parts[4]) && !user.getCode().equalsIgnoreCase(parts[4])) {
//            user.setCode(parts[4]);
//
//            isUpdate = true;
//
//            logger.info(" 更新员工数据Code {}", user.getCode());
//        }

        // 5JOBCODEID（用于反应汇报线）
        if (position != null && !position.getId().equals(user.getPositionId())) {
            // 用户切换汇报线
            user.setPositionId(position.getId());
            user.setTitle(position.getFunction());

            isUpdate = true;

            logger.info(" 更新员工数据Position {}", user.getPositionId());
            logger.info(" 更新员工数据Function {}", position.getFunction());
        } else if (null == position) {
            // 用户没有汇报线
            user.setPositionId(0);
            user.setTitle("No Position");

            logger.debug(" 用户无职位数据，重置为 0 {}", JsonUtils.toString(parts));
        }

        // 6DEPARTMENTID
        if (department != null && !department.getId().equals(user.getDepartmentId())) {
            user.setDepartmentId(department.getId());

            isUpdate = true;

            logger.info(" 更新员工数据部门数据 {}", user.getDepartmentId());
        } else if (department == null) {
            user.setDepartmentId(0);

            logger.debug(" 用户无部门数据，重置为 0 {}", JsonUtils.toString(parts));
        }



        // 7DEPARTMENTNAME  ignore
        // 8COSTCENTERCODE  ignore
        // 9EMAIL
        if (StringUtils.isNotEmpty(parts[9]) && !parts[9].equals(user.getEmail())) {
            //  上线前删除
            if (profile.equalsIgnoreCase("prod")) {
                user.setEmail(parts[9]);
            } else {
                user.setEmail("pshen1@tureinfo.cn");
            }

            isUpdate = true;

            logger.info(" 更新员工数据Email {}", user.getEmail());
        }

        // 13Preferred Name
        if (StringUtils.isNotEmpty(parts[13]) && !parts[13].equals(user.getEnName())) {
            user.setEnName(StringUtils.trim(parts[13]));

            isUpdate = true;

            logger.info(" 更新员工数据EnName {}", user.getEnName());
        }

        if (isUpdate) {
            logger.info(" 更新员工数据到数据数据库 {}", parts);
            userService.update(user);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    //    JOBCODEID	JOBCODE     JOBFUNCTION	                            SUPERIORJOBCODEID	POSITIONOCCUPIED	Company Code
    //    3,        0091-PGCN,  President & CEO Porsche China,          ,                   1,                  PCN
    //    102029	            Specialist Fleet Steering	            101690	            TRUE	            PCN
    //    103384	            CEO Office Assistant (Intern)	        101302	            TRUE	            PCN
    //    105340	            Assistant Manager Quality Monitoring	101622	            TRUE	            PCN
    //    101528	PCN-NT4-6	Trainer Technical	                    101520	            FALSE	            PCN
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void parsingPosition(String[] parts, int len) throws ParsingException {
        if (parts.length != len) {
            throw ParsingException.ItemsLengthException(len, parts.length, parts.toString());
        }

        logger.debug("开始处理 职位数据 {}", JsonUtils.toString(parts));

        boolean isUpdate = false;
        Position position = positionService.findById(Integer.parseInt(parts[0]));
        if (position == null) {
            position = Position.builder()
                    .id(Integer.parseInt(parts[0]))
                    .parentId(null)
                    .hrisCode(null)
                    .function(null)
                    .occupied(false)
                    .build();

            isUpdate = true;
        }

        // 1JOBCODE
        if (StringUtils.isEmpty(position.getHrisCode())) {
            if (StringUtils.isNotEmpty(parts[1])) {
                position.setHrisCode(StringUtils.trim(parts[1]));

                isUpdate = true;
            }
        } else if (StringUtils.isNotEmpty(parts[1]) && !parts[1].equalsIgnoreCase(position.getHrisCode())) {
            position.setHrisCode(StringUtils.trim(parts[1]));

            isUpdate = true;
        }

        // 2JOBFUNCTION
        if (StringUtils.isEmpty(position.getFunction())) {
            if (StringUtils.isNotEmpty(parts[2])) {
                position.setFunction(StringUtils.trim(parts[2]));

                isUpdate = true;
            }
        } else if (StringUtils.isNotEmpty(parts[2]) && !parts[2].equalsIgnoreCase(position.getFunction())) {
            position.setFunction(StringUtils.trim(parts[2]));

            isUpdate = true;
        }

        // 3SUPERIORJOBCODEID（有可能为空）
        if (null == position.getParentId()) {
            if (StringUtils.isNotEmpty(parts[3])) {
                position.setParentId(Integer.parseInt(parts[3]));

                isUpdate = true;
            }
        } else if (StringUtils.isNotEmpty(parts[3]) && !parts[3].equalsIgnoreCase(position.getParentId().toString())) {
            position.setParentId(Integer.parseInt(parts[3]));

            isUpdate = true;
        }

        // 4POSITIONOCCUPIED
        if (Boolean.getBoolean(parts[4]) != position.isOccupied()) {
            position.setOccupied(Boolean.getBoolean(parts[4]));

            isUpdate = true;
        }

        // 5Company Code
        if (null == position.getCompanyCode()) {
            if (StringUtils.isNotEmpty(parts[5])) {
                position.setCompanyCode(StringUtils.trim(parts[5]));

                isUpdate = true;
            }
        } else if (StringUtils.isNotEmpty(parts[5]) && !parts[5].equalsIgnoreCase(position.getCompanyCode())) {
            position.setCompanyCode(StringUtils.trim(parts[5]));

            isUpdate = true;
        }

        if (isUpdate) {
            positionService.update(position);

            logger.debug("更新 {}", JsonUtils.toString(position));
        }
    }
}
