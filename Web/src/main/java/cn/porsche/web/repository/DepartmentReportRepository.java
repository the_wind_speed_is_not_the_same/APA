package cn.porsche.web.repository;

import cn.porsche.web.domain.DepartmentReport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentReportRepository extends CrudRepository<DepartmentReport, Integer> {
    DepartmentReport findByFunctionIdAndDepartmentId(Integer functionId, Integer departmentId);
    List<DepartmentReport> findAllByFunctionId(Integer functionId);
}
