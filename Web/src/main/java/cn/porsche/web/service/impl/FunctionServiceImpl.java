package cn.porsche.web.service.impl;

import cn.porsche.web.domain.Email;
import cn.porsche.web.domain.Function;
import cn.porsche.web.domain.User;
import cn.porsche.web.email.IEmail;
import cn.porsche.web.exceptions.FunctionException;
import cn.porsche.web.repository.FunctionRepository;
import cn.porsche.web.service.EmailService;
import cn.porsche.web.service.FunctionService;
import cn.porsche.web.service.KPIUserService;
import cn.porsche.web.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.util.*;
import java.util.concurrent.CountDownLatch;

@Log4j2
@Service
public class FunctionServiceImpl implements FunctionService {
    private static final int MaxTrackYear = 5;

    @Autowired
    FunctionRepository repository;

    @Autowired
    IEmail emailSerder;

    @Autowired
    EmailService emailService;

    @Autowired
    UserService userService;

    @Autowired
    KPIUserService kpiUserService;

    @Override
    public Function update(Function function) {
        return repository.save(function);
    }

    @Override
    public Function findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Function findByYear(Integer year) {
        return repository.findByYear(year);
    }

    @Override
    /*
    * 往前找五年，找最近一年开启的Function
    * */
    public Function getLastFunction() {
        int max = 0;
        Function function;
        Calendar calendar = Calendar.getInstance();
        do {
            calendar.add(Calendar.YEAR, max);
            if (null != ( function = findByYear(calendar.get(Calendar.YEAR)) )) {
                return function;
            }

        } while (++ max < MaxTrackYear);

        return null;
    }

    @Override
    public Function getLastOpeningFunction() {
        Function function = getLastFunction();
        if (null == function) {
            return null;
        }

        return function.getComplete() ? null : function;
    }

    @Override
    @Transactional(rollbackFor = FunctionException.class)
    public Function releaseTS(Integer year, User user) throws FunctionException {
        Function function = repository.findByYear(year);
        if (function == null) {
            // 未开启

            // 第一天和最后一天
            Calendar instance = Calendar.getInstance();
            Calendar calendar = Calendar.getInstance();
            calendar.clear();

            calendar.set(Calendar.YEAR, instance.get(Calendar.YEAR));
            Date fDate = calendar.getTime();

            calendar.clear();
            calendar.set(Calendar.YEAR, instance.get(Calendar.YEAR));
            calendar.roll(Calendar.DAY_OF_YEAR,-1);
            Date lDate = calendar.getTime();

            function = Function.builder()
                    .year(year)
                    .TS(true)
                    .TSTime(new Date())
                    .TSUserId(user.getId())
                    .lastTSEmailId(0)
                    .TSMailSuccess(true)
                    .APA(false)
                    .APATime(null)
                    .APAUserId(null)
                    .lastAPAEmailId(null)
                    .APAMailSuccess(false)
                    .complete(false)
                    .completeTime(null)
                    .firstDay(fDate)
                    .lastDay(lDate)
                    .build();

            if (null == ( function = repository.save(function) )) {
                throw FunctionException.ReleaseException();
            }
        }

        try {
            reOpenTS(function, user);
        } catch (FunctionException fe) {
            log.debug("开启TS异常");
            throw FunctionException.ReleaseException();
        }

        return function;
    }

    @Override
    @Transactional(rollbackFor = FunctionException.class)
    public Function reOpenTS(Function function, User user) throws FunctionException {
        float count = userService.count();
        if (count == 0f) {
            return function;
        }

        int page = Runtime.getRuntime().availableProcessors() + 1;
        int size = (int) Math.ceil(count / page);
        if (count < page) {
            page = 1;
            size = 1;
        }

        // 利用多核 CPU 加快速度
        log.debug("一共{}页，共{}人，准备启用 {} 个线程", page, count, page);
        CountDownLatch latch = new CountDownLatch(page);

        for (int i=1; i<=page; i++) {
            int finalPage = i;
            int finalSize = size;
            log.debug("一共{}页，准备启用 {} 个线程，当前第{}页", page, page, finalPage);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    List<User> users = userService.findAllByEnabled(finalPage, finalSize).getContent();
                    log.debug("一共{}页，当前 {} 线程，处理 {} 个", finalPage, finalPage, users.size());

                    kpiUserService.builder(function, users);
                    latch.countDown();
            }}).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            throw FunctionException.ReleaseException();
        }

        log.info("添加kpi user 线程完成工作");

        return function;
    }


    @Override
    public Function sendTSEmail(User user, Function function, String subject, String content) {
        Email email = Email.builder()
                .functionId(function.getId())
                .userId(user.getId())
                .subject(subject)
                .content(content)
                .attachment(null)
                .response(null)
                .build();

        try {
            Properties props = emailSerder.getProperties();
            email.setFrom(props.getProperty("spring.mail.username"));

            emailSerder.send(email);

            function.setTSMailSuccess(true);
        } catch (MessagingException e) {
            e.printStackTrace();

            function.setTSMailSuccess(false);
        } catch (Exception e) {
            e.printStackTrace();

            function.setTSMailSuccess(false);
        }

        if (emailService.update(email) != null) {
            // 邮件内容存储失败
            function.setLastTSEmailId(email.getId());
        }

        return repository.save(function);
    }


    @Override
    @Transactional(rollbackFor = FunctionException.class) // 需要开启事务支持
    public Function releaseAPA(Integer year, User user) throws FunctionException {
        Function function = repository.findByYear(year);
        if (function == null) {
            // KickOff 未开始
            throw FunctionException.APAhasBegun();
        }

        if (function.getAPA()) {
            // APA 之前已开始
            throw FunctionException.APAhasBegun();
        }

        // 设置开始标识和时间
        function.setAPA(true);
        function.setAPATime(new Date());
        function.setAPAUserId(user.getId());
        function.setAPAMailSuccess(true);
        function.setLastAPAEmailId(0);

        return repository.save(function);
    }

    @Override
    public Function sendAPAEmail(User user, Function function, String subject, String content) throws FunctionException {
        if (function.getAPAMailSuccess()) {
            // 已发送过邮件
            throw FunctionException.APAEmailSent();
        }

        Email email = Email.builder()
                .functionId(function.getId())
                .userId(user.getId())
                .from(user.getEmail())
                .subject(subject)
                .content(content)
                .attachment(null)
                .response(null)
                .build();

        // 当前所有的员工，提醒KPI开始了
        List<User> users = userService.findAllInKPIUsers(function);
        for (User item : users) {
            if (StringUtils.isNotEmpty(item.getEmail())) {
                email.addAddress(item.getEmail());
            }
        }

        try {
            Properties props = emailSerder.getProperties();
            email.setFrom(props.getProperty("spring.mail.username"));
            emailSerder.send(email);

            function.setAPAMailSuccess(true);
        } catch (MessagingException e) {
            e.printStackTrace();

            function.setAPAMailSuccess(false);
        }

        if (emailService.update(email) != null) {
            // 邮件内容存储失败
            function.setLastAPAEmailId(email.getId());
        }

        return repository.save(function);
    }

    @Override
    public List<Function> available() {
        return repository.findAllByOrderByIdDesc();
    }
}
