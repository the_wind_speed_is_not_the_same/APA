package cn.porsche.compliance.repository;

import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    @Modifying
    @Query(value = "UPDATE `users` SET `is_manager`=0 WHERE `is_manager`=1", nativeQuery = true)
    int removeManagerTag();

    Integer countByType(EmployeeTypeEnum type);

    Page<User> findAll(Pageable pageable);
    Page<User> findAllByType(EmployeeTypeEnum type, Pageable pageable);

    User findByCode(String code);
    User findByPositionId(Integer positionId);

    List<User> findAllByManagerId(Integer managerId);
    List<User> findAllByDepartmentId(Integer departmentId);
}
