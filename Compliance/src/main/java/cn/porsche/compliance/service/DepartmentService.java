package cn.porsche.compliance.service;


import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.DepartmentTree;

import java.util.List;

public interface DepartmentService {
    Department update(Department department);

    Department findById(Integer id);
    Department findByName(String name);

    Department findByCode(String code);

    Department findTopDepartment(String name);

    List<Department> findTopDepartment();
    List<Department> findSubDepartments(Department department);

    DepartmentTree buildTree(Department department);
}