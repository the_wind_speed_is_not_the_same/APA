package cn.porsche.compliance.service;


import cn.porsche.compliance.domain.Risk;
import cn.porsche.compliance.domain.User;

import java.util.List;

public interface RiskService {
    Risk update(Risk risk);
    Risk findByUser(User user);

    void deleteByYear(Integer year);

    List<Risk> findAll();
}
