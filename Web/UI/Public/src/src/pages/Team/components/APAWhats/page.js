import {request} from '../../../../common/request.js';
import ListSelect from '../../../../components/ListSelect/page.vue';
import {mapState, mapActions} from 'vuex';

export default {
    props: ['user', 'whats', 'whatIndex', 'currentIndex', 'componentType', 'dimension', 'tableData'],
    data() {
        return {
            tableHeight: 500,
            howData: [{
                name: 'Dimensions'
            }, {
                name: 'Overall Comments'
            }],
            selectIndex: 0,
            isShowListSelect: false,
            apaCommentsUnread: []
        }
    },
    watch: {
        whatIndex: {
            handler(n, o) {
                if (n > -1) {
                    this.$refs.whatTable.setCurrentRow(this.whats[n], true)
                    this.$refs.howTable.setCurrentRow()
                } else {
                    this.$refs.whatTable.setCurrentRow()
                    if (n == -1) {
                        this.$refs.howTable.setCurrentRow(this.howData[0], true)
                    } else {
                        this.$refs.howTable.setCurrentRow(this.howData[1], true)
                    }
                }
            }
        },
    },
    computed: {
        ...mapState({
            isApa: state => state.allState.isApa,
            allList: state => state.allState.allList,
            yearList: state => state.allState.yearList
        })
    },
    components: {
        ListSelect
    },
    methods: {
        ...mapActions(['setYearList']),
        close() {
            this.$emit('changeComponent', 'close-ts');
        },
        showList() {
            this.isShowListSelect = true;
        },
        confirmIndex(index) {
            this.selectIndex = index;
            this.isShowListSelect = false;
            this.$emit('getTargetList', this.allList[index]);
        },
        whatClick(what) {
            this.$emit('changeComponent', 'manage-apa-detail', what);
            this.$refs.howTable.setCurrentRow(null)

            let index = this.whats.indexOf(what);

            if (this.apaCommentsUnread[index]) {
                // 需要清除What上的红点提示（APA结构下）
                request({
                    url: 'readComment',
                    method: 'get',
                    params: {
                        topic_id: what['apa_comments'][0]['topic_id'],
                        type:'APA'
                    }
                }).then(res => {
                    if (!res || res.code !== 0) {
                        this.titleTxt = 'Error';
                        this.confirmTxt = res.message;
                        this.isShowButtonLeft = false;
                        this.isShowConfirm = true;

                        return false;
                    }

                    this.$set(this.apaCommentsUnread, index, false);
                })
            }
        },
        handleCurrentChange2(val) {
            let index = this.howData.indexOf(val);
            this.$refs.whatTable.setCurrentRow();
            if (index === 0) {
                this.$emit('changeComponent', 'manage-dimension');
            } else {
                this.$emit('changeComponent', 'manage-overall')
            }
        },
    },
    created() {
        if (this.whats) {
            this.whats.forEach((item, idx) => {
                this.apaCommentsUnread[idx] = false;

                let comments = this.whats[idx]['apa_comments'];
                if (comments && comments.length) {
                    this.apaCommentsUnread[idx] = comments.some(item => item['owner_id'] != this.user['id'] && item['is_read'] === false);
                }
            })
        }
    },
    mounted() {
        this.$nextTick(() => {
            this.tableHeight = this.$refs.tableTop.offsetHeight - 24;
        })

        this.$refs.whatTable.setCurrentRow(this.whats[0], true)
    }
}
