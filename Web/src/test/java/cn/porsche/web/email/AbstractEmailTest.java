package cn.porsche.web.email;

import cn.porsche.web.ApplicationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.MessagingException;

import static org.junit.jupiter.api.Assertions.*;

class AbstractEmailTest extends ApplicationTest {

    @Autowired
    IEmail email;

    @Test
    void send() {
        try {
            email.send("test", "hello", "byron.duan@tureinfo.cn");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}