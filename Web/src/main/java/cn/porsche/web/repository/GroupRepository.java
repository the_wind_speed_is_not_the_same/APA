package cn.porsche.web.repository;

import cn.porsche.web.domain.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, Integer> {
    Group findByName(String name);
    List<Group> findAllByIdInOrderByWidthDesc(List<Integer> ids);

    Group findByIsDefault(boolean isDefault);
}
