package cn.porsche.web.controller.params;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SwitchRoleRequest {
    @ApiModelProperty(notes = "阶段主键")
    @JsonProperty("function_id")
    Integer functionId;

    @ApiModelProperty(notes = "用户主键")
    @JsonProperty("employees")
    Integer[] employees;

    @ApiModelProperty(notes = "What主键")
    @JsonProperty("role")
    String role;
}
