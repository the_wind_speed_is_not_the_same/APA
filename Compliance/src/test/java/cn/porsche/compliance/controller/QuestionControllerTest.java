package cn.porsche.compliance.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.common.util.JsonUtils;
import cn.porsche.compliance.ApplicationTest;
import cn.porsche.compliance.emnu.SeasonEnum;
import cn.porsche.compliance.service.LoginService;
import javax.servlet.http.Cookie;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class QuestionControllerTest extends ApplicationTest {
    private static final String API = "/question";

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;

    private String token;
    
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    void bySeason(int year, int season) {
        final String api = API + "/by_season";

        Cookie cookie = new Cookie("auth_id", getEmployeeAuthorization());

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .cookie(cookie)
//                            .param("year", String.valueOf(year))
//                            .param("season", String.valueOf(season))
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void answers(int year, SeasonEnum season, String[] answers) {
        final String api = API + "/answer";

        Cookie cookie = new Cookie("auth_id", getEmployeeAuthorization());

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .cookie(cookie)
                            .param("answers", String.join(",", answers))
                            .param("year", String.valueOf(year))
                            .param("season", season.getName())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void bySeasonYearAndSeason() {
        bySeason(2021, 1);
    }

    @Test
    void byYearAndSeasonAnswers() {
        List<String> answers = new ArrayList<>();
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("B");
        answers.add("D");

        this.answers(2021, SeasonEnum.Q1, answers.toArray(new String[answers.size()]));
    }

    @Test
    void answers() {
    }
}