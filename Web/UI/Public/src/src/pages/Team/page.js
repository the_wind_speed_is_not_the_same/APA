import {request} from '../../common/request.js';
import DashBoard from './components/DashBoard/page.vue';
import TeamActivites from './components/TeamActivites/page.vue';
import TSWhatDetail from './components/TSWhatDetail/page.vue';
import TSWhats from './components/TSWhats/page.vue';
import TSComments from './components/TSComments/page.vue';
import APAWhatDetail from './components/APAWhatDetail/page.vue';
import APAWhats from './components/APAWhats/page.vue';
import Confirm from '../../components/Confirm/page.vue';
import {mapState, mapActions} from 'vuex';
import TSDimensions from './components/TSDimensions/page.vue';
import APADimensions from './components/APADimensions/page.vue'
import APAComments from './components/APAComments/page.vue';
import {formatDateTime} from '../../common/utils.js';

export default {
    data() {
        return {
            leftComponentName: 'DashBoard',
            rightComponentName: 'TeamActivites',
            isShowLeft: false,
            isShowRight: true,
            currentIndex: null,
            tableData: [],
            pieData: {},
            isShowLoading: false,
            whats: [],
            whatIndex: null,
            componentType: '',
            pieType: 'ts',
            isShowConfirm: false,
            titleTxt: '',
            confirmTxt: '',
            showTime: 0,
            comments: [],
            dimension: null,
            function_idx: null,
            isNeedUpdateData: false,
            user: {},
            pieIndex: 0
        }
    },
    components: {
        DashBoard,
        TeamActivites,
        Confirm,
        TSComments,
        TSWhats,
        TSWhatDetail,
        TSDimensions,

        APAWhats,
        APAWhatDetail,
        APAComments,
        APADimensions
    },
    computed: {
        ...mapState({
            currentYear: state => state.allState.currentYear
        }),
        function_id() {
            if (this.function_idx) return this.function_idx;
            return this.currentYear && this.currentYear.id || null;
        }
    },
    methods: {
        ...mapActions(['setYearList', 'setReleaseStatus']),
        cancel() {
            this.isShowConfirm = false;
        },
        confirm() {
            this.isShowConfirm = false;
            if (this.isNeedUpdateData) {
                this.getTargetsList();
            }
        },
        upDateScore() {
            let scoreList = this.tableData.filter(item => item.target.second_level);
            let list = [];
            scoreList.forEach(item => {
                list.push({
                    employee_id: item.target.employee_id,
                    level: item.target.second_level
                })
            });

            let params = {
                function_id: this.function_id,
                list
            };

            request({
                url: 'do_score_by_level',
                params,
                method: 'get',
                isShowLoading: true
            }).then(res => {
                if (res) {
                    this.confirmTxt = 'Update successfully';
                    this.titleTxt = 'Success';
                } else {
                    this.confirmTxt = 'Update Failed, Please try again';
                    this.titleTxt = 'Failed';
                }


                this.showTime = 0
                this.isShowConfirm = true;
                this.isNeedUpdateData = true
            })
        },
        sortStr(name) {
            this.tableData.sort((a, b) => {
                let c = a.target[name] && a.target[name].toLowerCase() || 'z';
                let d = a.target[name] && b.target[name].toLowerCase() || 'z';
                if (c < d) {
                    return -1;
                } else if (c > d) {
                    return 1;
                }

                return 0;
            });
        },
        getTargetList(info) {
            let employee_id = this.tableData[this.currentIndex].target.employee_id;
            if (!info.id) {
                return console.log("no params for info.id");
            }

            request({
                url: 'targetDetail',
                method: 'get',
                params: {
                    employee_id,
                    function_id: info.id
                },
                isShowLoading: true
            }).then(res => {
                if (res && res.data) {
                    this.whats = res.data.whats || [];
                }
            })
        },
        sortTableData(type) {
            if (type === 0) {
                this.sortStr('en_name');
            } else if (type === 1) {
                this.sortStr('first_score_level');
            } else if (type === 2) {
                this.sortStr('second_score_level');
            }
        },
        getTargetsList() {
            request({
                url: 'teamList',
                method: 'get',
                isShowLoading: true,
                params: {
                    function_id: this.function_id
                }
            }).then(res => {
                this.isShowLoading = false;
                if (res && res.list && res.list.length) {
                    let list = res.list;
                    list.forEach(item => {
                        Object.assign(item.target, item.user);
                    })
                    this.tableData = list;
                    this.setReleaseStatus(this.currentYear.year)

                    this.$nextTick(() => {
                        this.setPieData(this.pieType);
                    })
                } else {
                    this.tableData = [];
                }
            })
        },
        setPieData(type) {
            let notStart = [];
            let submitted = [];
            let reviewed = [];
            let confirmed = [];
            if (type == 'ts') {
                this.pieIndex = 0
                notStart = this.tableData.filter(item => {
                    return !item.target.is_ts_employee_submit;
                });
                submitted = this.tableData.filter(item => {
                    return item.target.is_ts_employee_submit && !item.target.is_ts_delegator_confirm;
                })
                reviewed = this.tableData.filter(item => {
                    return item.target.is_ts_delegator_confirm && !item.target.is_ts_employee_confirm;
                });
                confirmed = this.tableData.filter(item => {
                    return item.target.is_ts_delegator_confirm && item.target.is_ts_employee_confirm;
                });
            } else if (type == 'apa') {
                this.pieIndex = 1
                notStart = this.tableData.filter(item => {
                    return !item.target.is_apa_employee_submit;
                });
                submitted = this.tableData.filter(item => {
                    return item.target.is_apa_employee_submit && !item.target.is_apa_delegator_submit;
                })
                reviewed = this.tableData.filter(item => {
                    return !!item.target.is_apa_delegator_submit && !item.target.is_agree_score;
                });
                confirmed = this.tableData.filter(item => {
                    return !!item.target.is_agree_score;
                });
            }

            let data = {
                'Not Start': notStart.length,
                'Submitted': submitted.length,
                'Reviewed': reviewed.length,
                'Confirmed': confirmed.length
            };
            let pieData = {
                type,
                data: []
            };
            Object.keys(data).forEach(key => {
                let obj = {
                    itemStyle: {
                        color: ''
                    }
                };
                obj.name = key;
                obj.value = data[key] || 0;
                switch (key) {
                    case 'Confirmed':
                        obj.itemStyle.color = '#018A16';
                        break;
                    case 'Not Start':
                        obj.itemStyle.color = '#e5e5e5';
                        break;
                    case 'Reviewed':
                        obj.itemStyle.color = '#000';
                        break;
                    case 'Submitted':
                        obj.itemStyle.color = '#888';
                        break;
                    default:
                        obj.itemStyle.color = '#e5e5e5';
                        break;
                }
                pieData.data.push(obj);
            })
            this.$nextTick(() => {
                this.pieData = pieData;
            })
        },
        changeType(index) {
            if (index == 0) {
                this.setPieData('ts');
                this.pieType = 'ts';
                this.pieIndex = 0
            } else if (index == 1) {
                this.setPieData('apa');
                this.pieType = 'apa';
                this.pieIndex = 1
            }
        },
        setCurrentIndex(index) {
            this.currentIndex = index;
        },
        changeIndex(opt) {
            let {name, target, current} = opt

            if (name === 'ts') {
                if (target === 'pre') {
                    if (current === 'what') {
                        if (this.currentIndex === 0 && this.whatIndex == 0) {
                            return
                        } else if (this.currentIndex > 0 && this.whatIndex > 0) {
                            this.whatIndex--;
                        } else if (this.currentIndex > 0 && this.whatIndex === 0) {
                            if (!this.tableData[this.currentIndex - 1].target.is_ts_employee_submit) {
                                this.isShowConfirm = true
                                this.isNeedUpdateData = false
                                this.titleTxt = ' Warning'
                                this.confirmTxt = 'The previous employee had not submit performance.'
                                this.showTime = 0
                            } else {
                                this.currentIndex--;
                                this.changeComponent('overall')
                                this.whatIndex = -2
                                this.whats = this.tableData[this.currentIndex].whats
                            }
                             console.log(4)
                        }
                    } else if (current === 'how') {
                        let len = this.tableData[this.currentIndex].whats.length
                        this.whatIndex = len > 1 ? len - 1 : 0
                        this.changeComponent('manage-tr', this.tableData[this.currentIndex])
                    } else {
                        this.changeComponent('dimension')
                        this.whatIndex = -1
                    }
                } else if (target === 'next') {
                    if (current === 'what') {
                        if (this.whatIndex === this.whats.length - 1 || !this.tableData[this.currentIndex].whats.length) {
                            this.changeComponent('dimension')
                            this.whatIndex = -1
                        } else {
                            this.whatIndex++;
                        }
                    } else if (current === 'how') {
                        this.changeComponent('overall')
                        this.whatIndex = -2
                    } else {
                        if (this.currentIndex === this.tableData.length - 1) {
                            return
                        } else {
                            if (this.tableData[this.currentIndex + 1].target.is_ts_employee_submit) {
                                this.currentIndex++
                                this.whatIndex = 0
                                this.whats = this.tableData[this.currentIndex].whats
                                this.changeComponent('manage-tr', this.tableData[this.currentIndex])
                            } else {
                                this.isShowConfirm = true
                                this.isNeedUpdateData = false
                                this.titleTxt = ' Warning'
                                this.confirmTxt = 'The next employee had not submit performance.'
                                this.showTime = 0
                            }
                        }
                    }
                }
            } else {
                if (target === 'pre') {
                    if (current === 'what') {
                        if (this.currentIndex === 0 && this.whatIndex == 0) {
                            return
                        } else if (this.currentIndex === 0 && this.whatIndex > 0) {
                            this.whatIndex--;
                        } else if (this.currentIndex > 0 && this.whatIndex === 0) {
                            if (this.tableData[this.currentIndex - 1].target.is_apa_employee_submit) {
                                this.currentIndex--;
                                this.changeComponent('manage-overall')
                                this.whatIndex = -2
                                this.whats = this.tableData[this.currentIndex].whats
                            } else {
                                this.isShowConfirm = true
                                this.isNeedUpdateData = false
                                this.titleTxt = ' Warning'
                                this.confirmTxt = 'The previous employee had not submit self-appraise.'
                                this.showTime = 0
                            }
                        }
                    } else if (current === 'how') {
                        let len = this.tableData[this.currentIndex].whats.length
                        this.whatIndex = len > 1 ? len - 1 : 0,
                            this.changeComponent('manage-apa', this.tableData[this.currentIndex])
                    } else {
                        this.changeComponent('manage-dimension')
                        this.whatIndex = -1
                    }
                } else if (target === 'next') {
                    if (current === 'what') {
                        if (this.whatIndex === this.whats.length - 1 || !this.tableData[this.currentIndex].whats.length) {
                            this.changeComponent('manage-dimension')
                            this.whatIndex = -1
                        } else {
                            this.whatIndex++;
                        }
                    } else if (current === 'how') {
                        this.changeComponent('manage-overall')
                        this.whatIndex = -2
                    } else {
                        if (this.currentIndex === this.tableData.length - 1) {
                            return
                        } else {
                            if (this.tableData[this.currentIndex + 1].target.is_apa_employee_submit) {
                                this.currentIndex++
                                this.whatIndex = 0
                                this.whats = this.tableData[this.currentIndex].whats
                                this.changeComponent('manage-apa', this.tableData[this.currentIndex])
                            } else {
                                this.isShowConfirm = true
                                this.isNeedUpdateData = false
                                this.titleTxt = ' Warning'
                                this.confirmTxt = 'The next employee had not submit self-appraise.'
                                this.showTime = 0
                            }
                        }
                    }
                }
            }
        },
        changeComponent(type, val) {
            if (type === 'manage-apa') {
                this.componentType = type
                this.currentIndex = this.tableData.indexOf(val);
                this.whats = val.whats;
                this.whatIndex = this.whatIndex || 0
                this.leftComponentName = 'APAWhats';
                this.rightComponentName = 'APAWhatDetail';
                this.dimension = val.how
            } else if (type === 'manage-tr') {
                this.componentType = type
                this.currentIndex = this.tableData.indexOf(val);
                this.whats = val.whats;
                this.whatIndex = this.whatIndex || 0
                this.leftComponentName = 'TSWhats';
                this.rightComponentName = 'TSWhatDetail';
            } else if (type == 'ts') {
                this.componentType = 'ts';
                this.currentIndex = this.tableData.indexOf(val);
                this.whats = val.whats;
                this.leftComponentName = 'TeamActivites';
                this.rightComponentName = 'TargetSetting';
            } else if (type == 'apa') {
                this.componentType = 'apa';
                if (val) {
                    this.currentIndex = this.tableData.indexOf(val);
                    this.whats = val.whats;
                } else {
                    let currentIndex = this.currentIndex;
                    this.currentIndex = -1;
                    this.$nextTick(() => {
                        this.currentIndex = currentIndex;
                    })
                }
                this.leftComponentName = 'TeamActivites';
                this.rightComponentName = 'Apa';
            } else if (type == 'ts-detail') {
                this.componentType = 'ts';
                this.whatIndex = this.whats.indexOf(val);
                this.rightComponentName = 'TargetEdit';
                this.pieData = {};
            } else if (type == 'apa-detail') {
                this.componentType = 'apa';
                this.whatIndex = this.whats.indexOf(val);
                this.rightComponentName = 'ApaEdit';
                this.pieData = {}
            } else if (type == 'manage-ts-detail') {
                this.componentType = 'ts';
                this.whatIndex = this.whats.indexOf(val);
                this.leftComponentName = 'TSWhats';
                this.rightComponentName = 'TSWhatDetail';
                this.pieData = {};
            } else if (type === 'manage-apa-detail') {
                this.componentType = 'apa';
                this.whatIndex = this.whats.indexOf(val);
                this.leftComponentName = 'APAWhats';
                this.rightComponentName = 'APAWhatDetail';
                this.pieData = {};
            } else if (type == 'close-ts-detail') {
                this.componentType = 'ts';
                this.whatIndex = null;
                this.rightComponentName = 'TargetSetting';
            } else if (type == 'close-apa-detail') {
                this.componentType = 'apa';
                this.whatIndex = null;
                this.rightComponentName = 'Apa';
            } else if (type == 'close-ts' || type === 'close-manage-target-review' || type === 'close-manage-apa-commnet') {
                this.componentType = '';
                this.leftComponentName = 'DashBoard';
                this.rightComponentName = 'TeamActivites';
                this.currentIndex = null;
                this.whatIndex = 0;
                this.setPieData(this.pieType);
            } else if (type == 'dimension') {
                this.componentType = type
                this.rightComponentName = 'TSDimensions';
                this.dimension = this.tableData[this.currentIndex].how || {};
            } else if (type === 'manage-dimension') {
                this.componentType = type
                this.rightComponentName = 'APADimensions';
                this.dimension = this.tableData[this.currentIndex].how || {};
            } else if (type == 'overall') {
                this.componentType = type
                this.rightComponentName = 'TSComments';
                let overall = this.tableData[this.currentIndex].overall;
                if (overall && overall.length) {
                    this.setComments(overall)
                } else {
                    this.comments = [];
                }
            } else if (type === 'manage-overall') {
                this.componentType = type
                this.rightComponentName = 'APAComments';
                let overall = this.tableData[this.currentIndex].overall;
                if (overall && overall.length) {
                    this.setComments(overall)
                } else {
                    this.comments = [];
                }
            } else if (type == 'closeHow') {
                this.rightComponentName = 'TargetSetting';
                this.whatIndex = null;
            }
        },
        setComments(list) {
            list.forEach((item, index) => {
                if (item.owner_id == this.user.id) {
                    item.type = 'me';
                    item.name = item.delegator_name;
                } else {
                    item.type = 'other';
                    item.name = item.employee_name;
                }
                item.send_time = formatDateTime(item.send_time)
            });
            this.comments = list;
        },
        toSetTargetDelegatorSubmit(bool) {
            if (this.tableData.length > 0) {
                this.tableData.forEach((item) => {
                    item.target[bool] = true
                })
            }
        },
        delegatorScore(score) {
            if (!score) {
                return false;
            }

            this.tableData[this.currentIndex].whats[this.whatIndex]['first_score'] = score;
        },
        delegatorScoreHow(how) {
            if (!this.tableData || !this.tableData[this.currentIndex] || !this.tableData[this.currentIndex]['how']) {
                return false;
            }

            for (let key in how) {
                this.tableData[this.currentIndex]['how'][key] = how[key];
            }
        },
        changeYear(res) {
            if (res) {
                this.function_idx = res.id
                this.getTargetsList()
            }
        },
        confirmHow(res) {
            this.tableData[this.currentIndex].how.is_delegator_confirm = res
        },
        confirmDelegator(bool) {
            this.tableData[this.currentIndex].target.is_delegator_confirm = bool
        }
    },
    created() {
        let user = localStorage.getItem('user')
        if (user) {
            this.user = JSON.parse(user)
        }
    },
    mounted() {
        if (!this.currentYear) {
            this.setYearList().then(res => {
                this.getTargetsList();
            })
        } else {
            this.getTargetsList();
        }
    }
}
