package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.*;
import cn.porsche.web.controller.params.WhatParam;
import cn.porsche.web.domain.*;
import cn.porsche.web.email.IEmail;
import cn.porsche.web.exceptions.TargetException;
import cn.porsche.web.service.*;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@Log4j2
@RestController
@RequestMapping("/apa")
@ApiModel(value = "/apa", description = "员工相关的操作类")
public class APAController extends BaseController {

    @Autowired
    DepartmentService departmentService;

    @Autowired
    DepartmentReportService reportService;

    @Autowired
    MessageService messageService;

    @Autowired
    IEmail mailService;

    protected float checkScore(float score) {
        if (score < 1.0f) {
            return 1.0f;
        } else if (score > 5.0f) {
            return 5.0f;
        } else {
            return score;
        }
    }

    @ApiOperation(
            value = "【员工】在【APA】页 为每个【What】进行【自我评分】接口",
            notes = "GET参数<br/>" +
                    "what_id（What主键）<br />" +
                    "score（得分）<br />" +
                    "comment（评分原因）<br />" +
                    "在评分前，后端会检查每个【What】的状态，如：HR是否【release】APA阶段、员工是否【submit】所有的【what】、【员工】与【委托人】双方是否【confirm】、委托人是否打分等<br />" +
                    "只有所有的状态都通过之后，每个【What】才可能会对员工开放【评分】接口"
    )
    @PostMapping(value = "/e_score_what")
    public String eScoreWhat(
            @RequestBody WhatParam params
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (params.getWhatId() == null) {
            return ApiResult.isBad("Unable to find the What data, please contact your HR to fix it");
        }

        if (params.getEmployeeScore() == null || params.getEmployeeScore() < 0 || params.getEmployeeScore() > 5) {
            return ApiResult.isBad("Unable to find the Score data, please contact your HR to fix it");
        }

        if (params.getEmployeeComment() == null || params.getEmployeeComment().length() == 0) {
            return ApiResult.isBad("Unable to find the Comment data, please contact your HR to fix it");
        }

        TargetWhat what = targetService.findWhatById(params.getWhatId());
        if (null == what || !user.getId().equals(what.getEmployeeId())) {
            return ApiResult.isBad("Unable to find the What data, please contact your HR to fix it");
        }

        if (!user.getId().equals(what.getEmployeeId())) {
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }


        Function function = functionService.findById(what.getFunctionId());
        if (null == function) {
            return ApiResult.isBad("APA process has not started.");
        }

        if (!function.getAPA()) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        if (function.getComplete()) {
            return ApiResult.isBad("The APA has ended, unable to operate");
        }

        if (null == ( target = targetService.get(what.getTargetId()) )) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }
//
//        if (!target.isTSESubmit() || !target.isTSEConfirm()) {
//            return ApiResult.isBad("Your Target Data has not been confirmed, please confirm before operation");
//        }

        if (!target.isTSDConfirm()) {
            return ApiResult.isBad("Your Target Data manager has not confirmed, please confirm before operation");
        }

        if (target.isAPADSubmit()) {
            return ApiResult.isBad("The APA has ended, unable to operate");
        }

        if (!targetService.canPass(target)) {
            return ApiResult.isBad("Your performance data is not complete, please check it or contact your HR");
        }

        // 控制分数大小
        params.setEmployeeScore(checkScore(params.getEmployeeScore()));

        if (null == targetService.eAppraisalWhat(function, user, target, what, params)) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        return ApiResult.isOk();
    }



    @ApiOperation(
            value = "【员工】在【APA】页 为每个【What】进行【自我评分】接口",
            notes = "GET参数<br/>" +
                    "what_id（What主键）<br />" +
                    "score（得分）<br />" +
                    "comment（评分原因）<br />" +
                    "在评分前，后端会检查每个【What】的状态，如：HR是否【release】APA阶段、员工是否【submit】所有的【what】、【员工】与【委托人】双方是否【confirm】、委托人是否打分等<br />" +
                    "只有所有的状态都通过之后，每个【What】才可能会对员工开放【评分】接口"
    )
    @PostMapping(value = "/e_score_how")
    public String eScoreHow(
            @RequestBody TargetHow params
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (params.getFunctionId() == null) {
            return ApiResult.isBad("没有绩效考核数据");
        }

        if (params.getHow1EScore() == null
                || params.getHow2EScore() == null
                || params.getHow3EScore() == null
                || params.getHow4EScore() == null

                || params.getHow1EScore() < 0
                || params.getHow1EScore() > 5

                || params.getHow2EScore() < 0
                || params.getHow2EScore() > 5

                || params.getHow3EScore() < 0
                || params.getHow3EScore() > 5

                || params.getHow4EScore() < 0
                || params.getHow4EScore() > 5

//                || StringUtils.isEmpty(params.getHow1())
//                || StringUtils.isEmpty(params.getHow2())
//                || StringUtils.isEmpty(params.getHow3())
//                || StringUtils.isEmpty(params.getHow4())
        ) {
            return ApiResult.isBad("Please check all scores and keep the score between 1 and 5");
        }

        Function function = functionService.findById(params.getFunctionId());
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (!function.getAPA()) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        if (function.getComplete()) {
            return ApiResult.isBad("The APA has ended, unable to operate");
        }

        if (null == ( target = targetService.findByUser(function, user) )) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }


        if (!targetService.canPass(target)) {
            return ApiResult.isBad("Your performance data is not complete, please check it or contact your HR");
        }

        if (!target.isTSESubmit() || !target.isTSEConfirm()) {
            return ApiResult.isBad("Your Target Data has not been confirmed, please confirm before operation");
        }

        if (!target.isTSDConfirm()) {
            return ApiResult.isBad("Your Target Data manager has not confirmed, please confirm before operation");
        }

        if (target.isAPADSubmit()) {
            return ApiResult.isBad("The APA has ended, unable to operate");
        }

        if (null == targetService.eAppraisalHow(function, user, target, params)) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【员工】在【APA】页 点击【Submit】的接口",
            notes = "GET参数<br/>" +
                    "function_id（阶段主键）<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/submit")
    public String apaESubmit(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (!function.getAPA()) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        if (null == ( target = targetService.findByUser(function, user) )) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (!target.isTSESubmit() || !target.isTSEConfirm()) {
            return ApiResult.isBad("Your Target Data has not been confirmed, please confirm before operation");
        }

        if (!target.isTSDConfirm()) {
            return ApiResult.isBad("Your Target Data manager has not confirmed, please confirm before operation");
        }

        if (target.isAPADSubmit()) {
            return ApiResult.isBad("The APA has ended, unable to operate");
        }

        // 检查How是否打过分
        TargetHow how = targetService.findHowByTarget(target);
        if (null == how || null == how.getHow1EScore() || null == how.getHow2EScore() || null == how.getHow3EScore() || null == how.getHow4EScore()) {
            return ApiResult.isBad("You haven't self Appraisal on how, please do so first");
        }

        // 检查What评分
        List<TargetWhat> whats = targetService.findAllWhatByTarget(target);
        for (TargetWhat item : whats) {
            if (null == item.getEmployeeScore()) {
                return ApiResult.isBad("You haven't self Appraisal on what, please do so first");
            }
        }

        try {
            targetService.apaESubmit(function, user, true);
        } catch (TargetException e) {
            return ApiResult.exception(e);
        }

        Message message = messageService.employeeSubmitAPA(function, user, target);
        if (null != message) {
            try {
                mailService.sendHtml(message.getSubject(), message.getMessage(), message.getReceiverEmail());
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }

        reportService.refreshReporting(function, departmentService.findById(user.getDepartmentId()));

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【经理】在【APA】页 为每个【How】进行第一次打分接口",
            notes = "POST（JSON）参数<br/>" +
                    "employee_id（员工主键）<br />" +
                    "function_id（绩效主键）<br />" +
                    "how1_do_score（How1分数）<br />" +
                    "how2_do_score（How2分数）<br />" +
                    "how3_do_score（How3分数）<br />" +
                    "how4_do_score（How4分数）<br />"
    )
    @PostMapping(value = "/do_score_how")
    public String doScoreHow(
            @RequestBody TargetHow params
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (params.getFunctionId() == null) {
            return ApiResult.isBad("没有绩效考核数据");
        }

        if (params.getHow1DOScore() == null
                || params.getHow2DOScore() == null
                || params.getHow3DOScore() == null
                || params.getHow4DOScore() == null

                || params.getHow1DOScore() < 0
                || params.getHow1DOScore() > 5

                || params.getHow2DOScore() < 0
                || params.getHow2DOScore() > 5

                || params.getHow3DOScore() < 0
                || params.getHow3DOScore() > 5

                || params.getHow4DOScore() < 0
                || params.getHow4DOScore() > 5
        ) {
            return ApiResult.isBad("Please check all scores and keep the score between 1 and 5");
        }

        Function function = functionService.findById(params.getFunctionId());
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (!function.getAPA()) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        if (function.getComplete()) {
            return ApiResult.isBad("The APA has ended, unable to operate");
        }


        User employee;
        if (params.getEmployeeId() == null
                || null == ( employee = userService.findById(params.getEmployeeId()) )
        ) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (null == ( kpiUser = kpiUserService.findByFunction(function, employee) )
                || !user.getId().equals(kpiUser.getDelegatorId()) && !user.getId().equals(kpiUser.getManagerId())
        ) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (null == ( target = targetService.findByUser(function, employee) )) {
            return ApiResult.isBad("APA process has not started.");
        }

        if (!target.isTSDConfirm()) {
            return ApiResult.isBad("Your Target Data manager has not confirmed, please confirm before operation");
        }

        if (null == targetService.firstHowAppraisal(function, employee, target, params)) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        reportService.refreshReporting(function, departmentService.findById(employee.getDepartmentId()));

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "【经理】在【APA】页 为每个【How】进行第二次打分接口",
            notes = "POST（JSON）参数<br/>" +
                    "function_id（绩效主键）<br />" +
                    "function_id（绩效主键）<br />" +
                    "how1_dt_score（How1分数）<br />" +
                    "how2_dt_score（How2分数）<br />" +
                    "how3_dt_score（How3分数）<br />" +
                    "how4_dt_score（How4分数）<br />"
    )
    @PostMapping(value = "/dt_score_how")
    public String dtScoreHow(
            @RequestBody TargetHow params
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (params.getFunctionId() == null) {
            return ApiResult.isBad("没有绩效考核数据");
        }

        if (params.getHow1DTScore() == null
                || params.getHow2DTScore() == null
                || params.getHow3DTScore() == null
                || params.getHow4DTScore() == null

                || params.getHow1DTScore() < 0
                || params.getHow1DTScore() > 5

                || params.getHow2DTScore() < 0
                || params.getHow2DTScore() > 5

                || params.getHow3DTScore() < 0
                || params.getHow3DTScore() > 5

                || params.getHow4DTScore() < 0
                || params.getHow4DTScore() > 5
        ) {
            return ApiResult.isBad("Please check all scores and keep the score between 1 and 5");
        }

        Function function = functionService.findById(params.getFunctionId());
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (!function.getAPA()) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        if (function.getComplete()) {
            return ApiResult.isBad("The APA has ended, unable to operate");
        }


        User employee;
        if (params.getEmployeeId() == null
                || null == ( employee = userService.findById(params.getEmployeeId()) )
        ) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (null == ( kpiUser = kpiUserService.findByFunction(function, employee) )
                || !user.getId().equals(kpiUser.getDelegatorId()) && !user.getId().equals(kpiUser.getManagerId())
        ) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (null == ( target = targetService.findByUser(function, employee) )) {
            return ApiResult.isBad("APA process has not started.");
        }

        if (!target.isTSDConfirm()) {
            return ApiResult.isBad("Your Target Data manager has not confirmed, please confirm before operation");
        }

        if (null == targetService.secondHowAppraisal(function, employee, target, params)) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        reportService.refreshReporting(function, departmentService.findById(employee.getDepartmentId()));

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【委托人】首次在【APA】页 为每个【员工】进行【综合评分】接口",
            notes = "GET参数<br/>" +
                    "function_id（阶段主键）<br />" +
                    "employee_id（员工主键）<br />" +
                    "score（得分）<br />" +
                    "在评分前，检查每个【What】的状态，如：HR是否【release】APA阶段、员工是否【submit】所有的【what】、员工与委托人双方是否【confirm】、委托人是否打分等<br />" +
                    "只有所有的状态都通过之后，每个【What】才可能会对员工开放【评分】接口"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "what_id",     required = true, dataTypeClass = Integer.class, value = "What主键"),
            @ApiImplicitParam(name = "score",       required = true, dataTypeClass = Float.class,   value = "分数"),
    })
    @GetMapping(value = "/do_score_what")
    public String doScoreWhat(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "what_id") Integer whatId,
            @RequestParam(value = "score") Float score
    ) {
        if (noPromise(ModuleNameEnum.PreCalibrationScore, OperatorText.ADD, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (!function.getAPA()) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        if (function.getComplete()) {
            return ApiResult.isBad("The APA has ended, unable to operate");
        }

        TargetWhat what = targetService.findWhatById(whatId);
        if (null == what || null == what.getEmployeeId()) {
            return ApiResult.isBad("no what");
        }


        User employee = userService.findById(what.getEmployeeId());
        if (null == employee) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (null == ( kpiUser = kpiUserService.findByFunction(function, employee) )
                || !user.getId().equals(kpiUser.getDelegatorId()) && !user.getId().equals(kpiUser.getManagerId())
        ) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }

        if (null == ( target = targetService.findByUser(function, employee) )) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (!target.isTSESubmit() || !target.isTSEConfirm()) {
            return ApiResult.isBad("The employee's Target Data has not been confirmed, please confirm before operation");
        }

        if (!target.isTSDConfirm()) {
            return ApiResult.isBad("Your Target Data manager has not confirmed, please confirm before operation");
        }

        if (null == targetService.firstWhatAppraisal(function, employee, target, what, score)) {
            return ApiResult.isBad("Appraisal Fail");
        }

        reportService.refreshReporting(function, departmentService.findById(employee.getDepartmentId()));

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【委托人】第二次在【APA】页 为每个【员工】进行【综合评分】接口",
            notes = "GET参数<br/>" +
                    "function_id（阶段主键）<br />" +
                    "employee_id（员工主键）<br />" +
                    "score（得分）<br />" +
                    "在评分前，检查每个【What】的状态，如：HR是否【release】APA阶段、员工是否【submit】所有的【what】、员工与委托人双方是否【confirm】、委托人是否打分等<br />" +
                    "只有所有的状态都通过之后，每个【What】才可能会对员工开放【评分】接口"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "what_id",     required = true, dataTypeClass = Integer.class, value = "What主键"),
            @ApiImplicitParam(name = "score",       required = true, dataTypeClass = Float.class,   value = "分数"),
    })
    @GetMapping(value = "/dt_score_what")
    public String dtScoreWhat(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "what_id") Integer whatId,
            @RequestParam(value = "score") Float score
    ) {
        if (noPromise(ModuleNameEnum.PreCalibrationScore, OperatorText.ADD, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (!function.getAPA()) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        if (function.getComplete()) {
            return ApiResult.isBad("The APA has ended, unable to operate");
        }


        TargetWhat what = targetService.findWhatById(whatId);
        if (null == what) {
            return ApiResult.isBad("no what");
        }

        User employee = userService.findById(what.getEmployeeId());
        if (null == what.getEmployeeId() || null == employee) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (null == ( kpiUser = kpiUserService.findByFunction(function, employee) )
                || !user.getId().equals(kpiUser.getDelegatorId()) && !user.getId().equals(kpiUser.getManagerId())
        ) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }

        if (null == ( target = targetService.findByUser(function, employee) )) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (!target.isTSESubmit() || !target.isTSEConfirm()) {
            return ApiResult.isBad("The employee's Target Data has not been confirmed, please confirm before operation");
        }

        if (!target.isTSDConfirm()) {
            return ApiResult.isBad("Your Target Data manager has not confirmed, please confirm before operation");
        }


        if (null == targetService.secondWhatAppraisal(function, employee, target, what, score)) {
            return ApiResult.isBad("Appraisal Fail");
        }

        reportService.refreshReporting(function, departmentService.findById(employee.getDepartmentId()));

        return ApiResult.isOk();
    }




    @ApiOperation(
            value = "【经理】打完分之后，开始将分数上线，供 HR 查阅",
            notes = "GET参数<br/>" +
                    "function_id（阶段主键）<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/release_first_score")
    public String releaseFirstScore(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (!function.getAPA()) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        List<User> users = userService.findAllByManager(user);

        for (User item : users) {
            try {
                targetService.apaDOSubmit(function, item, true);
            } catch (TargetException e) {
                e.printStackTrace();
            }
        }

        if (null != user.getDepartmentId()) {
            Department department = departmentService.findById(user.getDepartmentId());
            if (null != department) {
                reportService.refreshReporting(function, department);
                reportService.releaseFirstAPA(function, department);

                User vp = user;
                // 查看部门所在VP，是否所有部门都已经完成第一次打分
                while (!groupService.inTopManagerGroup(vp)) {
                    // 用户不在TOP组中
                    if (null == ( vp = userService.getManager(vp) )) {
                        break;
                    }
                }

                if (vp != null) {
                    // 向VP发送邮件


                }
            }
        }



        return ApiResult.isOk();
    }



    @ApiOperation(
            value = "【经理】最终打分",
            notes = "GET参数<br/>" +
                    "function_id（阶段主键）<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/release_final_score")
    public String releaseFinalScore(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (!function.getAPA()) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        List<User> users = userService.findAllByManager(user);
        Iterator<User> iterator = users.iterator();
        while (iterator.hasNext()) {
            try {
                if (null == targetService.apaDTSubmit(function, iterator.next(), true)) {
                    // 没有评分完成
                    iterator.remove();
                }
            } catch (TargetException e) {
                e.printStackTrace();

                iterator.remove();
            }
        }

        List<Target> targets = targetService.findAllByManager(function, user);
        // 计算A,B对应的占有率
        int aTotal = 0, bTotal = 0;
        for (int i = 0; i < targets.size(); i++) {
            if (null == targets.get(i).getSecondScoreLevel()) {
                continue;
            }

            switch (targets.get(i).getSecondScoreLevel()) {
                case A: aTotal ++; break;
                case B: bTotal ++; break;
            }
        }

        double maxPeople;
        boolean isOver   = false;
        String message   = null;
        maxPeople = Math.floor( 0.07 * targets.size() );
        maxPeople = maxPeople < 1 ? 1 : maxPeople;
        if (maxPeople < aTotal) {
            // 大于 7%  的人数
            // 回滚确认的绩效

            message = String.format("Please keep the number of performance A below %d, now is %d", (int) maxPeople, aTotal);
            isOver = true;
        }

        maxPeople = Math.floor( 0.2 * targets.size() );
        maxPeople = maxPeople < 1 ? 1 : maxPeople;
        if (!isOver && maxPeople < bTotal) {
            // 大于 20%  的人数
            // 回滚确认的绩效

            message = String.format("Please keep the number of performance B below %d, now is %d", (int) maxPeople, bTotal);
            isOver = true;
        }

        if (isOver) {
            for (Target item : targets) {
                item.setReleaseDate(null);
                item.setReleaseSecond(false);

                targetService.update(item);
            }

            return ApiResult.isBad(message);
        }


        if (null != user.getDepartmentId()) {
            // 更新部门报表
            Department department = departmentService.findById(user.getDepartmentId());
            reportService.releaseSecondAPA(function, department);
        }

        batchSendEmail(function, users, target, 5);

        return ApiResult.isOk();
    }



    @ApiOperation(
            value = "【员工】同意经理对自己的绩效打分",
            notes = "GET参数<br/>" +
                    "function_id（阶段主键）<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/agree")
    public String agree(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (null == ( target = getTarget(function) )) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (false == target.isReleaseSecond()) {
            return ApiResult.isBad("经理未完成绩效考核");
        }

        target.setAgreeScore(true);
        if (null == targetService.update(target)) {
            return ApiResult.isBad("操作失败，请联系管理员");
        }

        reportService.refreshReporting(function, departmentService.findById(user.getDepartmentId()));

        return ApiResult.isOk();
    }

    private void batchSendEmail(Function function, List<User> users, Target target, int size) {
        int page = 0, max = Double.valueOf(Math.ceil(users.size() / size)).intValue();
        // 利用多核 CPU 加快速度
        int num = Runtime.getRuntime().availableProcessors() + 1;
        while (page < max) {
            int downNumber = (max - page) >= num ? num : max - page;
            log.debug("一共{}页，准备启用 {} 个线程", max, downNumber);
            CountDownLatch latch = new CountDownLatch(downNumber);

            for (int i=1; i <= downNumber && page < max; i++) {
                int finalPage = page + i;
                log.debug("一共{}页，准备启用 {} 个线程，当前第{}页", max, downNumber, finalPage);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // 使用多线程发邮件，否则非常慢
                        Message message;
                        for (int j = 0; j < size; j ++) {
                            message = messageService.noticeByReleaseFinalScore(function, users.get(( finalPage -1 ) * size + j), target);

                            if (null != message) {
                                try {
                                    mailService.sendHtml(message.getSubject(), message.getMessage(), message.getReceiverEmail());
                                } catch (MessagingException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        latch.countDown();
                    }}).start();
            }
            page += downNumber;

            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
