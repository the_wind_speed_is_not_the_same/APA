import {request} from '../../../../common/request.js';
import {deepClone, formatDateTime} from '../../../../common/utils.js';
import Mantle from '../../../../components/Mask/page.vue';
import Confirm from '../../../../components/Confirm/page.vue';
import ListSelect from '../../../../components/ListSelect/page.vue';

export default {
    props: {
        user: Object,
        whats: Array,
        whatIndex: {
            type: Number | String,
            default() {
                return 0;
            }
        },
        functionId: Number | String,
        tableData: Array,
        currentIndex: Number
    },
    data() {
        return {
            form: {
                finish_date: '2019-07-11',
                desc: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.',
                function_id: '',
                kpis: [{
                    desc: '',
                    index: 4,
                }, {
                    desc: '',
                    index: 3,
                }, {
                    desc: '',
                    index: 2,
                }, {
                    desc: '',
                    index: 1,
                }, {
                    desc: '',
                    index: 0,
                }],
                name: 'My first traget Teams set their own objectives',
                what_id: '',
                weight: '30%',
            },
            isShowKpiBox: false,
            comments: [],
            comment: '',
            showTime: 0,
            confirmType: '',
            isShowConfirm: false,
            confirmTxt: '',
            titleTxt: 'Warning',
            hasData: false
        }
    },
    components: {
        Mantle,
        Confirm,
        ListSelect
    },
    watch: {
        whatIndex: {
            handler(n, o) {
                this.initData(this.whats[n]);
            }
        }
    },
    computed: {
        targetInfo() {
            if (this.whats && this.whats.length) {
                return this.whats[this.whatIndex];
            }
            // return {};
        },
        delegatorInfo() {
            if (this.tableData && this.tableData.length && this.currentIndex !== null) {
                return this.tableData[this.currentIndex].target;
            }
            return {}
        }
    },
    methods: {
        rejectComment() {
            if (!this.functionId || !this.comment || !this.targetInfo.what_id) {
                return;
            }

            request({
                url: 'rejectTSComment',
                data: {
                    function_id: this.functionId,
                    reject_comment: this.comment,
                    what_id: this.targetInfo.what_id
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.isShowConfirm = true;
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;

                    return;
                }

                this.isShowConfirm = true;
                this.titleTxt = 'successfully';
                this.confirmTxt = 'Reject successfully';
                let message = {
                    send_time: formatDateTime(new Date()),
                    message: this.comment,
                    delegator_name: this.user.en_name
                };

                this.comments = [message, ...this.comments];
                this.targetInfo.ts_comments.unshift(message);
                this.comment = '';
            })
        },
        sendComment() {
            if (!this.functionId || !this.comment || !this.targetInfo.what_id) {
                return;
            }

            request({
                url: 'addTSComment',
                data: {
                    function_id: this.functionId,
                    message: this.comment,
                    topic_id: this.targetInfo.what_id
                },
                isShowLoading: true
            }).then(res => {
                if (res) {
                    this.isShowConfirm = true;
                    this.titleTxt = 'successfully';
                    this.confirmTxt = 'Send successfully';
                    let message = {
                        send_time: formatDateTime(new Date()),
                        message: this.comment,
                        delegator_name: this.user.en_name
                    };

                    this.comments = [message, ...this.comments];
                    this.targetInfo.ts_comments.unshift(message);
                    this.comment = '';
                } else {
                    this.isShowConfirm = true;
                    this.titleTxt = 'Error';
                    this.confirmTxt = 'Send Failed, Please try again';
                }
            })
        },
        confirm() {
            this.isShowConfirm = false;
        },
        close() {
            this.$emit('changeComponent', 'close-manage-target-review');
        },
        previousIndex() {
            this.$emit('changeIndex', {name: 'ts', target: 'pre', current: 'what'});
        },
        nextIndex() {
            this.$emit('changeIndex', {name: 'ts', target: 'next', current: 'what'});
        },
        spreadSelect() {
            this.isShowKpiBox = true;
        },
        closeBox() {
            this.isShowKpiBox = false;
        },
        initData(targetInfo) {
            if (targetInfo) {
                this.hasData = true
                let form = deepClone(targetInfo)
                if (!form.kpis.length) {
                    form.kpis = [{
                        desc: '',
                        index: 4,
                    }, {
                        desc: '',
                        index: 3,
                    }, {
                        desc: '',
                        index: 2,
                    }, {
                        desc: '',
                        index: 1,
                    }, {
                        desc: '',
                        index: 0,
                    }];
                } else {
                    form.kpis.sort((a, b) => {
                        return b.index - a.index
                    })
                }

                this.comments = form.ts_comments || [];
                this.form = form;
            } else {
                this.form = {}
                this.comments = []
                this.hasData = false
            }

            this.comment = ''
        }
    },
    created() {
    },
    mounted() {
        this.initData(this.targetInfo);
    }
}
