import {request} from '../../../../common/request.js';
import ListSelect from '../../../../components/ListSelect/page.vue';
import Confirm from '../../../../components/Confirm/page.vue';
import {deepClone} from '../../../../common/utils.js';

export default {
    props: ['dimension', 'isBtnDisabled', 'func'],
    data() {
        return {
            dimensionsListConst: null,
            selectIndex: -1,
            list: ['1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0'],
            isShowList: false,
            scoreIndex: -1,
            dimensionTop: 0,
            selectScoreTop: 0,
            isShowConfirm: false,
            titleTxt: '',
            confirmTxt: '',
            showTime: 0,
            dimensionsList: [
                {
                    name: 'Herzblut',
                    subTxt: '1. Live Passionately <br /> 2. Know where you come from',
                    weight: '25%',
                    description: '',
                    tootip: 'Herzblut<br/>' +
                        '<p style="text-indent:1em">1.  Live Passionately</p>' +
                        '<p style="text-indent:2em">I inspire others for our work </p>' +
                        '<p style="text-indent:2em">I have high standards for my work </p>' +
                        '<p style="text-indent:2em">I celebrate successes with others </p>' +
                        '<p style="text-indent:2em">We work with commitment and with conviction</p>' +
                        '<p style="text-indent:1em">2.	Know where you come from </p>' +
                        '<p style="text-indent:2em">I do not take the given things for granted</p>' +
                        '<p style="text-indent:2em">I utilize our resources intelligently </p>' +
                        '<p style="text-indent:2em">We know for whom we work - for our customers </p>' +
                        '<p style="text-indent:2em">We foster social partnerships and promote a fair balance </p>'+
                        '<p style="text-indent:2em">between employee and company interests',
                    placeholder: "Please describe how you are going to perform and realize the objectives by living the value of \"Herzblut\"."
                }, {
                    name: "Pioniergeist",
                    subTxt: '1. Look into the future with courage<br />2. Think outside the box',
                    weight: '25%',
                    tootip: 'Pioniergeist<br/>' +
                        '<p style="text-indent:1em">1.  Look into the future with courage</p>' +
                        '<p style="text-indent:2em">I respond flexibly to changing framework conditions</p>' +
                        '<p style="text-indent:2em">I keep the goal in mind when there is uncertainty </p>' +
                        '<p style="text-indent:2em">I am willing to take risks </p>' +
                        '<p style="text-indent:2em">We consider new technologies to be a chance <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; and drive forward future-oriented ideas (regardless of their origin)</p>' +
                        '<p style="text-indent:1em">2.	Think outside the box </p>' +
                        '<p style="text-indent:2em">I ensure, that our objectives and backgrounds are transparent</p>' +
                        '<p style="text-indent:2em">I analyze challenges quickly, reduce complexity and derive specific conclusions </p>' +
                        '<p style="text-indent:2em">I question the status quo and bring in solutions </p>' +
                        '<p style="text-indent:2em">We build networks to achieve common objectives',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize the objectives by living the value of \"Pioniergeist\".'
                }, {
                    name: 'Sportlichkeit',
                    subTxt: '1. Fight fair <br/>2. Stay hungry',
                    weight: '25%',
                    tootip: 'Sportlichkeit<br/>' +
                        '<p style="text-indent:1em">1.	Fight fair</p>' +
                        '<p style="text-indent:2em">I voice my opinion and am prepared to disagree with others</p>' +
                        '<p style="text-indent:2em">I discuss other’s opinions in a constructive way</p>' +
                        '<p style="text-indent:2em">I remain objective and tolerant, even when conflict arises </p>' +
                        '<p style="text-indent:2em">Together, we fight for the best result</p>' +
                        '<p style="text-indent:1em">2.	Stay hungry </p>' +
                        '<p style="text-indent:2em">I set myself ambitious goals</p>' +
                        '<p style="text-indent:2em">I seek and provide feedback and examine my own conduct </p>' +
                        '<p style="text-indent:2em">I update and expand my knowledge, learn quickly from mistakes and ' +
                        '<p style="text-indent:2em">acknowledge them openly</p>' +
                        '<p style="text-indent:2em">We strive to find the optimum solution</p>',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize\r\n the objectives by living the value of \"Sportlichkeit\".'
                }, {
                    name: 'One Family',
                    subTxt: '1. Take responsibility<br />' + '2. Respect each other',
                    weight: '25%',
                    tootip: 'One Family<br/>' +
                        '<p style="text-indent:1em">1.	Take responsibility</p>' +
                        '<p style="text-indent:2em">I keep my word and behave with integrity</p>' +
                        '<p style="text-indent:2em">I take responsibility for common objectives</p>' +
                        '<p style="text-indent:2em">I prioritize topics and create leeway for responsibility to be handed over </p>' +
                        '<p style="text-indent:2em">We ensure that objectives are achieved for our topics </p>' +
                        '<p style="text-indent:1em">2.	Respect each other </p>' +
                        '<p style="text-indent:2em">I develop individuals based on their strengths and weaknesses</p>' +
                        '<p style="text-indent:2em">I develop my team cross functionally and encourage its diversity </p>' +
                        '<p style="text-indent:2em">I am respectful and considerate of others and myself</p>' +
                        '<p style="text-indent:2em">We encourage interaction with each other</p>',
                    description: '',
                    placeholder: 'Please describe how you are going to perform and realize the objectives by living the value of \"One Family\".'
                }
            ],
            watchBtnDisabled: true,
            isClickType: null,
            isShowButtonLeft: false
        }
    },
    watch: {
        dimensionsList: {
            handler(n, o) {
                if (JSON.stringify(n) !== JSON.stringify(this.dimensionsListConst)) {
                    this.watchBtnDisabled = false;
                } else {
                    this.watchBtnDisabled = true;
                }
            },
            deep: true
        }
    },
    components: {
        ListSelect,
        Confirm
    },
    computed: {
        isSaveDisabled() {
            return JSON.stringify(this.dimensionsList) === JSON.stringify(this.dimensionsListConst);
        }
    },
    methods: {
        selectScore(index) {
            this.scoreIndex = index;
            this.selectIndex = this.list.indexOf(this.dimensionsListConst[index].score);
            if (index < 3) {
                this.selectScoreTop = document.getElementsByClassName('score-wrapper')[index].offsetTop - document.getElementsByClassName('score-wrapper')[index].scrollTop;
            } else {
                this.selectScoreTop = document.getElementsByClassName('score-wrapper')[index].offsetTop - document.getElementsByClassName('score-wrapper')[index].scrollTop - 224;
            }
            this.isShowList = !this.isShowList;
        },
        confirmIndex(index) {
            let dimensionsList = deepClone(this.dimensionsListConst);

            if (index == this.selectIndex) {
                this.isShowList = false;
                return;
            }

            this.selectIndex = index;
            this.isShowList = false;
            dimensionsList[this.scoreIndex].score = this.list[this.selectIndex] ? this.list[this.selectIndex] : '';
            this.dimensionsListConst = dimensionsList;
        },
        confirm() {
            if (this.isClickType === 'prev') {
                this.$emit('previousIndex');
            } else if (this.isClickType === 'next') {
                this.$emit('nextIndex');
            }
            this.isShowConfirm = false;
        },
        cancel() {
            this.isShowConfirm = false;
        },
        previous() {
            if (!this.isSaveDisabled) {
                this.titleTxt = 'Warning'
                this.confirmTxt = 'There are some changes not saved，are you sure to leave？?'
                this.isShowConfirm = true
                this.showTime = 0
                this.isClickType = 'prev'
                this.isShowButtonLeft = true
                return false
            }
            this.$emit('previousIndex');
        },
        next() {
            if (!this.isSaveDisabled) {
                this.titleTxt = 'Warning'
                this.confirmTxt = 'There are some changes not saved，are you sure to leave？'
                this.isShowConfirm = true
                this.showTime = 0
                this.isClickType = 'next'
                this.isShowButtonLeft = true
                return false
            }
            this.$emit('nextIndex');
        },
        closeComponent() {
            this.$emit('closeComponent');
        },
        reloadDetail() {
            this.$emit('refreshTargetDetail');
        },
        saveHow() {
            if (this.isSaveDisabled) {
                return;
            }

            let scoreData = {
                how1_e_score: '1.0',
                how2_e_score: '1.0',
                how3_e_score: '1.0',
                how4_e_score: '1.0',
                function_id: this.func['id'],
                how1: '',
                how2: '',
                how3: '',
                how4: ''
            }

            this.dimensionsListConst.forEach((item, index) => {
                scoreData[`how${index+1}_e_score`] = item.score;
                scoreData[`how${index+1}`] = item.description
            });

            request({
                url: 'eScoreHow',
                data: scoreData,
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;

                    return;
                }

                this.titleTxt = 'Success';
                this.confirmTxt = 'Save Successfully';
                this.isShowButtonLeft = false;
                // this.dimensionsListConst = deepClone(this.dimensionsList);
                this.watchBtnDisabled = true;

                this.isShowConfirm = true;
                this.reloadDetail();
            })
        },
        filterScore(score) {
            if (!score) return '';

            score = String(score);
            if (score.length === 1) {
                return score + '.0';
            } else {
                return score;
            }
        }
    },
    mounted() {
        this.dimensionsList.forEach((item, index) => {
            item.description = this.dimension && this.dimension[`how${index + 1}`] || '';
            item.score = this.dimension && this.filterScore(this.dimension[`how${index + 1}_e_score`])
        });

        this.dimensionTop = this.$refs.dimension.offsetTop;
        this.dimensionsListConst = deepClone(this.dimensionsList);
    }
}
