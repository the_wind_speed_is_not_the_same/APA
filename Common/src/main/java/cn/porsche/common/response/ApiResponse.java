package cn.porsche.common.response;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@AllArgsConstructor
public abstract class ApiResponse {
    private transient final Logger logger = LoggerFactory.getLogger(this.getClass());
    public transient static final int      RootSuccessCode     = 0;
    public transient static final String   RootSuccessMessage  = "success";

    public transient static final int      RootErrorCode       = 100000;
    public transient static final String   RootErrorMessage    = "Something gone wrong";


    public int      code;
    public String   message;
}
