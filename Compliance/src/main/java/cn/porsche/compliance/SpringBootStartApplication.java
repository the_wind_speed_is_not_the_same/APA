package cn.porsche.compliance;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


public class SpringBootStartApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Application.class);
    }


//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//        return builder.sources(Application.class);
//    }
//
//    @SpringBootApplication
//    public static class Application {
//        static ApplicationContext context;
//        public static void main(String[] args) {
//            context = SpringApplication.run(Application.class, args);
//        }
//
//        public static ApplicationContext getContext() {
//            return context;
//        }
//    }
}
