package cn.porsche.compliance.domain;

import cn.porsche.compliance.emnu.TrainingTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "statistical", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
}
    , uniqueConstraints = {
        @UniqueConstraint(name = "UNQ_Year_User_Type", columnNames = {"user_id", "year", "type"})
    }
)
public class Statistical implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @JsonIgnore
    @ApiModelProperty(notes = "部门主锓")
    @Column(name = "department_id",             columnDefinition = "INT UNSIGNED NOT NULL COMMENT '部门主键'")
    private Integer departmentId;

    @JsonIgnore
    @ApiModelProperty(notes = "用户主锓")
    @Column(name = "user_id",                   columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer userId;

    @ApiModelProperty(notes = "培训类别")
    @Enumerated(EnumType.STRING)
    @JsonProperty("type")
    @Column(name = "type",                      columnDefinition = "CHAR(32) NOT NULL COMMENT '培训类别'")
    private TrainingTypeEnum type;

    @ApiModelProperty(notes = "参与年份")
    @JsonProperty("year")
    @Column(name = "year",                      columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '参与年份'")
    private Integer year;

    @ApiModelProperty(notes = "参与类别总数")
    @JsonProperty("total")
    @Column(name = "total",                     columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '参与类别总数'")
    private Integer total;

    @ApiModelProperty(notes = "未参与类别总数")
    @JsonProperty("attend")
    @Column(name = "attend",                    columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '未参与类别总数'")
    private Integer attend;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    private Date createAt;
}
