/**

 @Name： 用户登入和注册等
 @Author：Rose
 @Site：OK代练
 @License: LPPL
  */
layui.define(['table', 'form', 'laydate'], function (exports) {
  var $ = layui.$
    , layer = layui.layer
    , laytpl = layui.laytpl
    , setter = layui.setter
    , view = layui.view
    , admin = layui.admin
    , form = layui.form
    , router = layui.router()
    , table = layui.table
    , search = router.search
    , show_num = [];

  //自定义验证
  form.verify({
    nickname: function (value, item) { //value：表单的值、item：表单的DOM对象
      if (!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)) {
        return '用户名不能有特殊字符';
      }
      if (/(^\_)|(\__)|(\_+$)/.test(value)) {
        return '用户名首尾不能出现下划线\'_\'';
      }
      if (/^\d+\d+\d$/.test(value)) {
        return '用户名不能全为数字';
      }
    },
    vercode: function (value, item) {
      var num = show_num.join("");
      var msg = "";
      if (value == "") {
        msg = "验证码不能为空";
      }
      else if (value.toLowerCase() != num) {

        msg = "验证码错误！请重新输入！";
        active.draw(show_num);
      }
      return msg;
    }

    //我们既支持上述函数式的方式，也支持下述数组的形式
    //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
    , pass: [
      /^[\S]{6,12}$/
      , '密码必须6到12位，且不能出现空格'
    ]
  });

  /*************************************用户登陆*************************************/

  $(document).keydown(function (e) {
    if (e.keyCode === 13) {
      active.login(admin.events.formDataSerizal("form1"));
    }
  });
  //登陆提交
  form.on('submit(LAY-user-login-submit)', function (obj) {
    active.login(obj.field);
    return false;
  });



  /*************************************用户管理*************************************/

  //事件集合
  var active = {
    draw: function () {

      var canvas_width = $('#canvas').width();
      var canvas_height = $('#canvas').height();
      canvas_width = canvas_width == 0 ? 100 : canvas_width;
      canvas_height = canvas_height == 0 ? 35 : canvas_height;
      var canvas = document.getElementById("canvas");//获取到canvas的对象，演员
      // console.log("canvas_width:"+canvas_width+"                canvas_height:"+canvas_height+"           canvas:"+canvas);
      var context = canvas.getContext("2d");//获取到canvas画图的环境，演员表演的舞台
      canvas.width = canvas_width;
      canvas.height = canvas_height;
      var sCode = "A,B,C,E,F,G,H,J,K,L,M,N,P,Q,R,S,T,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0";
      var aCode = sCode.split(",");
      var aLength = aCode.length;//获取到数组的长度

      for (var i = 0; i <= 3; i++) {
        var j = Math.floor(Math.random() * aLength);//获取到随机的索引值
        var deg = Math.random() * 30 * Math.PI / 180;//产生0~30之间的随机弧度
        var txt = aCode[j];//得到随机的一个内容
        show_num[i] = txt.toLowerCase();
        var x = 10 + i * 20;//文字在canvas上的x坐标
        var y = 20 + Math.random() * 8;//文字在canvas上的y坐标
        context.font = "bold 23px 微软雅黑";

        context.translate(x, y);
        context.rotate(deg);

        context.fillStyle = active.randomColor();
        context.fillText(txt, 0, 0);

        context.rotate(-deg);
        context.translate(-x, -y);
      }
      for (var i = 0; i <= 5; i++) { //验证码上显示线条
        context.strokeStyle = active.randomColor();
        context.beginPath();
        context.moveTo(Math.random() * canvas_width, Math.random() * canvas_height);
        context.lineTo(Math.random() * canvas_width, Math.random() * canvas_height);
        context.stroke();
      }
      for (var i = 0; i <= 30; i++) { //验证码上显示小点
        context.strokeStyle = active.randomColor();
        context.beginPath();
        var x = Math.random() * canvas_width;
        var y = Math.random() * canvas_height;
        context.moveTo(x, y);
        context.lineTo(x + 1, y + 1);
        context.stroke();
      }

    },
    randomColor: function () {
      var r = Math.floor(Math.random() * 256);
      var g = Math.floor(Math.random() * 256);
      var b = Math.floor(Math.random() * 256);
      return "rgb(" + r + "," + g + "," + b + ")";
    },
    checkLogin: function () {
      return layui.data(setter.tableName).access_token != undefined;
    },
    login: function (oData) {
      $.ajax({
        url: setter.apiUrl + 'Login/Login',
        contentType: "application/json-patch+json",
        data: JSON.stringify(oData),
        type: "POST",
        crossDomain: true,
        dataType: "json",
        success: function (data) {
          if (data.Success) {
            layer.msg('登入成功', {
              offset: '15px'
              , icon: 1
              , time: 1000
            }, function () {
              //请求成功后，写入 access_token
              layui.data(setter.tableName, {
                key: setter.request.tokenName
                , value: data.ResData.AccessToKen
              });

              //写入Cookies
              $.cookie("accessToKen", data.ResData.AccessToKen);
              //将最新资料存储到公共表中okTable
              layui.data(setter.tableName, { key: "userinfo", value: data.ResData });
              //  //获取用户最新基础资料
              //     var responseData = admin.events.AjaxGet(-1, "Security/Account/GetById?id=" + data.ResData.AccountInfoId, "", false, false);
              location.hash = router.search.redirect ? decodeURIComponent(router.search.redirect) : '/';
            });
          }
          else {
            layer.msg(data.ErrMsg);
            //window.location.reload();
          }

        }
      });
    },
    initUserInfoTpl: function () {

      var temp = userinfotemp.innerHTML;
      var tempView = document.getElementById("li_userInfo");
      var userinfo = layui.data(layui.setter.tableName).userinfo;
      if (userinfo == null || userinfo == undefined) {
        this.loginStateAndLogOut();
      } else {
        laytpl(temp).render(userinfo, function (html) {
          tempView.innerHTML = html;

        });
      }

    },
    modifymyinfo: function () {
      admin.popup({
        title: '修改基本资料'
        , area: ['450px', '380px']
        , id: 'LAY-popup-useradmin-modify'
        , success: function (layero, index) {
          view(this.id).render('user/userform').done(function () {

            form.render(null, 'layuiadmin-form-useradmin');
            //监听提交
            form.on('submit(LAY-user-modify-submit)', function (data) {

              admin.events.AjaxPost(index, "Security/Account/ModifyAccountInfo?accountId=" + layui.data(setter.tableName).userinfo.AccountInfoId, JSON.stringify(data.field), false, true, "");
              //获取用户最新基础资料
              var responseData = admin.events.AjaxGet(-1, "Security/Account/GetById?id=" + layui.data(setter.tableName).userinfo.AccountInfoId, "", false, false, "");
              //将最新资料存储到公共表中okTable
              layui.data(setter.tableName, { key: "userinfo", value: responseData });
            });
          });
        }, done: function () {

        }
      });

    },
    modifypassword: function () {
      admin.popup({
        title: '修改密码'
        , area: ['520px', '380px']
        , id: 'LAY-popup-useradmin-modify'
        , success: function (layero, index) {
          view(this.id).render('user/password').done(function () {
            form.render(null, 'layui-form-password');
            //监听提交
            form.on('submit(LAY_user_password_submit)', function (data) {

              var param = admin.events.quertParams(data.field);
              admin.events.AjaxPostQuery(index, "Security/Account/ModifyPassword", param, false, true, "");

            });
          });
        }
      });
    },

    loginStateAndLogOut: function () {
      if (layui.data(layui.setter.tableName).userinfo == null) {

        admin.exit();
      }

    }
    , initEvent: function () {
      $("#canvas").on('click', function () {
        active.draw();
      })
      //事件绑定
      $('.hasevent').on('click', function () {
        var type = layui.$(this).data('type');
        active[type] ? active[type].call(this) : '';

        form.render();
      });
    }
  };


  //对外暴露的接口
  exports('user', active);
});
