package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.constant.ScoreLevelEnum;
import cn.porsche.web.controller.params.EmployeeGroupRequest;
import cn.porsche.web.controller.params.ImportParam;
import cn.porsche.web.controller.params.SwitchRoleRequest;
import cn.porsche.web.domain.*;
import cn.porsche.web.email.IEmail;
import cn.porsche.web.service.*;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Log4j2
@RestController
@RequestMapping("/employee")
@Api(value = "/employee", description = "HR对员工操作类")
public class EmployeeController extends BaseController {

    @Autowired
    MessageService messageService;

    @Autowired
    IEmail mailService;

    @Autowired
    UserViewService userViewService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    DepartmentReportService reportService;

    @Autowired
    PositionService positionService;


    @ApiOperation(
            value = "【HR】按页获取全公司列表",
            notes = "GET（KPIUser）参数：<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",            required = false, dataTypeClass = Integer.class, value = "页数"),
            @ApiImplicitParam(name = "size",            required = false, dataTypeClass = Integer.class, value = "条量"),
            @ApiImplicitParam(name = "key",             required = false, dataTypeClass = String.class, value = "关键字搜索"),
            @ApiImplicitParam(name = "group",           required = false, dataTypeClass = GroupNameEnum.class, value = "组名称"),
            @ApiImplicitParam(name = "department_id",   required = false, dataTypeClass = GroupNameEnum.class, value = "部门主键")
    })
    @GetMapping(value = "/list")
    public String list(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "25") Integer size,
            @RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "group", required = false) GroupNameEnum groupEnum,
            @RequestParam(value = "only",  required = false) String only,
            @RequestParam(value = "department_id", required = false) Integer departmentId
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.ADD, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (groupEnum != null) {
            return ApiResult.builder().list(userViewService.searchByGroup(groupService.findByName(groupEnum.getName()), checkPage(page), checkSize(size))).toJson();
        }

        if (departmentId != null && !StringUtils.isEmpty(key)) {
            return ApiResult.builder().list(userViewService.searchByDepartmentAndName(departmentService.findById(departmentId), key, checkPage(page), checkSize(size))).toJson();
        }

        if (departmentId != null && !StringUtils.isEmpty(only)) {
            if (only.equals("HireDate")) {
                return ApiResult.builder().list(userViewService.searchByDepartmentAndHireDateIsNull(departmentService.findById(departmentId), checkPage(page), checkSize(size))).toJson();
            }

            if (only.equals("Manager")) {
                return ApiResult.builder().list(userViewService.searchByDepartmentAndNeedHands(departmentService.findById(departmentId), true, checkPage(page), checkSize(size))).toJson();
            }
        }

        if (departmentId != null) {
            return ApiResult.builder().list(userViewService.searchByDepartment(departmentService.findById(departmentId), checkPage(page), checkSize(size))).toJson();
        }

        if (StringUtils.isNotEmpty(only)) {
            if (only.equals("HireDate")) {
                return ApiResult.builder().list(userViewService.searchByHireDateIsNull(checkPage(page), checkSize(size))).toJson();
            }

            if (only.equals("Manager")) {
                return ApiResult.builder().list(userViewService.searchByNeedHands(checkPage(page), checkSize(size))).toJson();
            }
        }

        if (!StringUtils.isEmpty(key)) {
            return ApiResult.builder().list(userViewService.searchByName(key, checkPage(page), checkSize(size))).toJson();
        }

        return ApiResult.builder().list(userViewService.findByPage(checkPage(page), checkSize(size))).toJson();
    }

    @ApiOperation(
            value = "【经理】或【委托人】返回【所有的员工】的接口（返回的数据包括员工数据和目标数据）",
            notes = "返回数据参数：<br />" +
                    "参考：Schemas中的：KPIUser"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = KPIUser.class, responseContainer = "List"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键")
    })
    @GetMapping(value = "/list_by_delegator")
    public String listByDelegator(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.CHECK, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        boolean isVP = groupService.inTopManagerGroup(user);

        if (!isVP && null == ( kpiUser = getKPIUser(function) )) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }

//        Map<String, Object> json = new HashMap<>();
//        json.put("employee", kUser.getIsDelegator() ? kpiUserService.findAllByDelegator(function, user) : kpiUserService.findAllByManager(function, user));
//        json.put("targets",  kUser.getIsDelegator() ? kpiUserService.findAllByDelegator(function, user) : targetService.findAllByManager(function, user));

        return ApiResult.builder().list(null != kpiUser && kpiUser.getIsDelegator() ? kpiUserService.findAllByDelegator(function, user) : kpiUserService.findAllByManager(function, user)).toJson();
    }


    @ApiOperation(
            value = "【HR】将员工移动到某个组",
            notes = "GET参数：" +
                    "function_id（阶段主键）<br />" +
                    "employees（搜索名称）<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "employees",   required = true, dataTypeClass = List.class,    value = "员工主键列表"),
            @ApiImplicitParam(name = "group",       required = true, dataTypeClass = GroupNameEnum.class,    value = "目标组"),
    })
    @PostMapping(value = "/move")
    public String move(
            @RequestBody EmployeeGroupRequest param
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.ADD, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        List<User> users = new ArrayList<>();
        for (Integer employeeId : param.getEmployees()) {
            User employee = userService.findById(employeeId);
            if (null != employee) {
                users.add(employee);
            }
        }

        // 直接从绩效系统中删除
        boolean isRemoveTarget  = false;

        // 恢复绩效系统中的数据
        boolean isRestoreTarget = false;

        // 重置绩效数据为C
        boolean isAppraisal     = false;
        switch (param.getGroup()) {
            case HRPerformanceTeam:
                groupService.moveToHRPerformanceGroup(users, user);

                // 恢复绩效数据
                isRestoreTarget = true;
                break;

            case TopManagement:
                groupService.moveToTopManagerGroup(users, user);

                // 不参与考勤
                isRemoveTarget = true;
                break;

            case InProbation:
                groupService.moveToInProbationGroup(users, user);

                // 恢复绩效数据并自动打分为C
                isAppraisal     = true;
                isRestoreTarget = true;
                break;

            case LongLeaves:
                groupService.moveToLongLevelGroup(users, user);

                // 不参与考勤
                isRemoveTarget = true;
                break;
            case Employee:
                groupService.addToEmployeeGroup(users, user);

                // 恢复绩效数据
                isRestoreTarget = true;
                break;
        }

        // 检查当前的Target是否需要重新调整
        Function function;
        if (null != ( function = functionService.getLastFunction() )) {
            Set<Integer> refresh = new HashSet();
            if (isRemoveTarget) {
                // 删除设置的绩效数据
                for (User item : users) {
                    target = targetService.findByUser(function, item);

                    if (null == target) {
                        continue;
                    }

                    // 直接不在绩效系统中存在
                    targetService.reset(target);
                    targetService.delete(target);

                    // 重新计算部门的统计数据
                    if (null != item.getDepartmentId()) {
                        refresh.add(item.getDepartmentId());
                    }
                }
            }

            if (isRestoreTarget) {
                // 还原之前的绩效数据
                for (User item : users) {
                    target = targetService.findByUser(function, item);
                    if (null != target) {
                        continue;
                    }

                    try {
                        target = targetService.autoTargetSetting(function, item);

                        if (item.getNeedHands()) {
                            targetService.reset(target);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (null != item.getDepartmentId()) {
                        refresh.add(item.getDepartmentId());
                    }
                }
            }

            if (isAppraisal) {
                // 非Manager组，才能自动评分
                // 还原之前的绩效数据
                for (User item : users) {
                    if (item.getNeedHands()) {
                        continue;
                    }

                    target = targetService.findByUser(function, item);
                    if (null == target) {
                        try {
                            target = targetService.autoTargetSetting(function, item);
                        } catch (Exception e) {
                            e.printStackTrace();

                            continue;
                        }
                    }

                    if (!item.getNeedHands()) {
                        targetService.autoAppraisal(function, item, ScoreLevelEnum.C);
                    }
                }
            }

            if (!refresh.isEmpty()) {
                Department department;
                Iterator<Integer> iterator = refresh.iterator();

                while (iterator.hasNext()) {
                    department = departmentService.findById(iterator.next());
                    if (null != department) {
                        reportService.refreshReporting(function, department);
                    }
                }
            }
        }

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "【HR】获取整个公司的部门列表"
    )
    @GetMapping(value = "/departments")
    public String departments() {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Department department = departmentService.findByCode("1");
        DepartmentTree tree   = departmentService.buildTree(department);

        return ApiResult.builder().data(tree).toJson();
    }

    @ApiOperation(
            value = "【HR】获取整个公司的部门列表"
    )
    @GetMapping(value = "/company")
    public String company() {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        List<DepartmentTree> trees = new ArrayList<>();
        departmentService.findTopDepartments().forEach(item -> trees.add(departmentService.buildTree(item)));

        return ApiResult.builder().data(trees).toJson();
    }

    @ApiOperation(
            value = "【HR】更新员工的入职日期接口",
            notes = "POST（KPIUser）参数：<br />" +
                    "employee_id（员工主键）<br />" +
                    "hire_date（入职日期）2010-09-10<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employee_id",     required = true, dataTypeClass = Integer.class, value = "员工主键"),
            @ApiImplicitParam(name = "hire_date",       required = true, dataTypeClass = Date.class,    value = "入职日期", example = "2010-09-10"),
    })
    @PostMapping(value = "/hire_date")
    public String hireDate(@RequestBody KPIUser params) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.ADD, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        User employee = userService.findById(params.getEmployeeId());
        if (null == employee) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }

        if (null == params.getHireDate()) {
            return ApiResult.isBad("Please set the hire date of the employee and try again");
        }

        if (params.getHireDate().equals(employee.getHireDate())) {
            return ApiResult.isOk();
        }

        if (null == userService.updateHireDate(employee, params.getHireDate())) {
            return ApiResult.isBad("Please set the hire date of the employee and try again");
        }

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        // 回到年初
        calendar.clear();
        calendar.setTime(params.getHireDate());

        if (year == calendar.get(Calendar.YEAR)) {
            groupService.moveToInProbationGroup(employee, getUser());

            if (employee.getNeedHands()) {
                Function function = functionService.getLastOpeningFunction();
                if (null != function) {
                    if (null != ( target = targetService.findByUser(function, employee) )) {
                        if (target.isTSEConfirm()) {
                            // 自动设置为C绩效
                            targetService.autoAppraisal(function, employee, ScoreLevelEnum.C);
                        }
                    }
                }
            }
        }

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【HR】批量员工的入职日期接口",
            notes = "POST（KPIUser）参数：<br />" +
                    "employee_id（员工主键）<br />" +
                    "hire_date（入职日期）2010-09-10<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id",     required = true, dataTypeClass = Integer.class, value = "与之关联的Function组件"),
            @ApiImplicitParam(name = "text",            required = true, dataTypeClass = String.class,  value = "Code与hire date对应关系的字符串"),
    })
    @PostMapping(value = "/import_hire_dates")
    public String importHireDates(
            @RequestBody ImportParam params
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.ADD, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (StringUtils.isEmpty(params.getText())) {
            return ApiResult.isOk();
        }

        String[] lines = params.getText().split("\n");
        if (lines.length == 0) {
            return ApiResult.isOk();
        }


        String json;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < lines.length; i++) {
            // 依次获取对应的行
            String line = lines[i];
            if (StringUtils.isEmpty(line)) {
                continue;
            }

            String code, date;
            String[] parts = line.split("\t");
            if (parts.length != 2
                || StringUtils.isEmpty( code = StringUtils.trim(parts[0]) )
                || StringUtils.isEmpty( date = StringUtils.trim(parts[1]) )
            ) {
                return ApiResult.isBad(String.format("Content [%s] format error, please copy from Excel Template.", line));
            }

            Date hireDate;
            try {
                hireDate = format.parse(date);
            } catch (ParseException e) {
                return ApiResult.isBad(String.format("Content [%s]'s date [%s] is error, like this['2021-01-01'].", line, parts[1]));
            }

            log.debug("开始设置{} 的入职日期", code);
            User employee = userService.findByCode(code);
            if (null == employee) {
                continue;
            }

            json = hireDate(KPIUser.builder().employeeId(employee.getId()).hireDate(hireDate).build());
            if (json.indexOf(ApiResult.RootSuccessMessage) == -1) {
                return json;
            }
        }

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "HR将员工调换对应的组",
            notes = "Post参数<br/>" +
                    "function_id（阶段主键）<br />" +
                    "employee_id（员工主键）<br />" +
                    "role（角色描述：employee / manager）<br />"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = String.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employee_id", required = true,   dataTypeClass = Integer.class, value = "员工主键"),
            @ApiImplicitParam(name = "role",        required = false,  dataTypeClass = String.class,  value = "所属组名称"),
    })
    @PostMapping(value = "/switch_role")
    public String switchRole(
            @RequestBody SwitchRoleRequest params
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam)) {
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (params.getEmployees().length == 0) {
            return ApiResult.isOk();
        }

        User employee;
        Function function = functionService.getLastFunction();
        if (function== null || !function.getTS() || function.getComplete()) {
            // 未开始或已结束
            function = null;
        }

        for (int i = 0; i < params.getEmployees().length; i++) {
            if (null == ( employee = userService.findById(params.getEmployees()[i]) )) {
                continue;
            }

            log.info("准备处理 角色切换 {} {}", employee.getEnName(), params.getRole());
            if (employee.getNeedHands() == true && "manager".equals(params.getRole())
                || employee.getNeedHands() == false && "employee".equals(params.getRole())
            ) {
                continue;
            }

            employee.setNeedHands("manager".equals(params.getRole()));
            userService.update(employee);

            // 删除绩效
            // 1、之前的绩效考核
            if (function != null) {
                Target target = targetService.findByUser(function, employee);
                if (target != null) {
                    // 重置Target数据
                    targetService.reset(target);
                }

                if (!employee.getNeedHands() && !groupService.inTopManagerGroup(employee)) {
                    // 非手动，直接填充
                    try {
                        targetService.autoTargetSetting(function, employee);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // 创建一个新的KpiUser 和
                    kpiUserService.builder(function, employee,
                            null == employee.getManagerId() ? null : userService.findById(employee.getManagerId()),
                            null == employee.getDepartmentId() ? null : departmentService.findById(employee.getDepartmentId()),
                            null == employee.getPositionId() ? null : positionService.findById(employee.getPositionId())
                    );

                    // 发送邮件通知
                    Message message = messageService.tsReleaseNotice(function, employee);
                    try {
                        mailService.sendHtml(message.getSubject(), message.getMessage(), message.getReceiverEmail());
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【HR】批量员工的是管理员"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "text",            required = true, dataTypeClass = String.class,  value = "Code与hire date对应关系的字符串"),
    })
    @PostMapping(value = "/import_manager")
    public String importManager(
            @RequestBody ImportParam params
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.ADD, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (StringUtils.isEmpty(params.getText())) {
            return ApiResult.isOk();
        }

        String[] lines = params.getText().split("\n");
        if (lines.length == 0) {
            return ApiResult.isOk();
        }

        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < lines.length; i++) {
            // 依次获取对应的行
            String code = lines[i];
            if (StringUtils.isEmpty(code)) {
                continue;
            }

            if (StringUtils.isEmpty( code = StringUtils.trim(code) )) {
                return ApiResult.isBad(String.format("Content [%s] format error, please copy from Excel Template.", code));
            }

            log.debug("开始设置{} 的Manager属性", code);
            User employee = userService.findByCode(code);
            if (null == employee) {
                continue;
            }

            integers.add(employee.getId());
        }

        SwitchRoleRequest request = new SwitchRoleRequest();
        request.setEmployees(integers.toArray(new Integer[0]));
        request.setRole("manager");

        String json = switchRole(request);
        if (json.indexOf(ApiResult.RootSuccessMessage) == -1) {
            return json;
        }

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【HR】批量员工的是VP"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "text",            required = true, dataTypeClass = String.class,  value = "Code与hire date对应关系的字符串"),
    })
    @PostMapping(value = "/import_vps")
    public String importVP(
            @RequestBody ImportParam params
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.ADD, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (StringUtils.isEmpty(params.getText())) {
            return ApiResult.isOk();
        }

        String[] lines = params.getText().split("\n");
        if (lines.length == 0) {
            return ApiResult.isOk();
        }

        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < lines.length; i++) {
            // 依次获取对应的行
            String code = lines[i];
            if (StringUtils.isEmpty(code)) {
                continue;
            }

            if (StringUtils.isEmpty( code = StringUtils.trim(code) )) {
                return ApiResult.isBad(String.format("Content [%s] format error, please copy from Excel Template.", code));
            }

            log.debug("开始设置{} 的VP属性", code);
            User employee = userService.findByCode(code);
            if (null == employee) {
                continue;
            }

            integers.add(employee.getId());
        }

        EmployeeGroupRequest request = new EmployeeGroupRequest();
        request.setEmployees(integers);
        request.setGroup(GroupNameEnum.TopManagement);

        String json = move(request);
        if (json.indexOf(ApiResult.RootSuccessMessage) == -1) {
            return json;
        }

        return ApiResult.isOk();
    }
}
