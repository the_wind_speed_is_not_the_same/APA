package cn.porsche.web.service;


import cn.porsche.web.constant.CommentTypeEnum;
import cn.porsche.web.domain.Comment;
import cn.porsche.web.domain.Function;
import cn.porsche.web.domain.User;

import java.util.List;


public interface CommentService {
    Comment findById(Integer id);
    Comment add(Function function, User delegator, User employee, User sender, CommentTypeEnum typeEnum, Integer topicId, String message);

    Comment update(Comment comment);

    void read(User user, CommentTypeEnum type, Integer topicId);

    List<Comment> findByTopic(CommentTypeEnum type, Integer topicId);
    List<Comment> findByTopicByIdAsc(CommentTypeEnum type, Integer topicId);
}
