package cn.porsche.web.repository;

import cn.porsche.web.domain.GroupUser;
import cn.porsche.web.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupUserRepository extends CrudRepository<GroupUser, Integer> {
    List<GroupUser> findAllByGroupId(Integer groupId);

    @Query("SELECT u from User u LEFT JOIN GroupUser gu ON u.id=gu.userId WHERE gu.groupId=:groupId")
    List<User> listByGroupId(Integer groupId);

    List<GroupUser> findByUserId(Integer userId);
    GroupUser findByGroupIdAndUserId(Integer groupId, Integer userId);
}
