package cn.porsche.compliance.service.impl;

import cn.porsche.compliance.domain.DepartmentEmployee;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.repository.DepartmentEmployeeRepository;
import cn.porsche.compliance.service.DepartmentEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class DepartmentEmployeeServiceImpl implements DepartmentEmployeeService {
    @Autowired
    DepartmentEmployeeRepository repository;

    @Override
    public DepartmentEmployee update(DepartmentEmployee data) {
        return repository.save(data);
    }

    @Override
    public DepartmentEmployee findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public DepartmentEmployee findByEmployee(User employee) {
        return repository.findByEmployeeId(employee.getId());
    }
}
