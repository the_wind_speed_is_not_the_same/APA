package cn.porsche.web.service;


import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.GroupRole;

import java.util.List;

public interface GroupRoleService {
    GroupRole update(GroupRole role);

    GroupRole findByGroupAndMoudle(Group group, ModuleNameEnum moudle);

    List<GroupRole> findByGroup(Group group);
}
