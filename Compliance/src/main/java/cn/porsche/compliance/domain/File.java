package cn.porsche.compliance.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "files", indexes = {
//        @Index(name = "UNQ_NAME", columnList = "name", unique = true)
    }
    , uniqueConstraints = {
        @UniqueConstraint(name = "UNQ_NAME", columnNames = "md5")
    }
)
public class File implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @ApiModelProperty(notes = "文件名称")
    @JsonProperty("user_id")
    @Column(name = "user_id",                   columnDefinition = "INT UNSIGNED NOT NULL COMMENT '上传人'")
    Integer userId;

    @ApiModelProperty(notes = "文件名称")
    @JsonProperty("md5")
    @Column(name = "md5",                       columnDefinition = "CHAR(32) NOT NULL COMMENT '文件名称的 MD5值，用于唯一索引'")
    String md5;

    @ApiModelProperty(notes = "文件名称")
    @JsonProperty("name")
    @Column(name = "name",                      columnDefinition = "VARCHAR(1023) NOT NULL COMMENT '文件名称'")
    String name;

    @ApiModelProperty(notes = "培训主题")
    @JsonProperty("path")
    @Column(name = "path",                      columnDefinition = "VARCHAR(2013) NOT NULL COMMENT '下载路径'")
    String path;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
