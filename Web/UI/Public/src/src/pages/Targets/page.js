import Introduction from './components/Introduction/page.vue';
import SettingWhat from './components/What/page.vue';
import Dimensions from './components/Dimensions/page.vue';
import OverallComments from './components/Comments/page.vue';
import ListSelect from '../../components/ListSelect/page.vue';
import Confirm from '../../components/Confirm/page.vue';
import {request} from '../../common/request.js';
import {formatDateTime} from '../../common/utils.js';
import {mapState, mapActions} from 'vuex';

export default {
    data() {
        return {
            target: {},
            whats: [],
            tableRows: [{ name: 'Dimensions' }, { name: 'Overall Comments' }],
            currentIndex: null,
            currentRow: null,
            tableHeight: 412,
            componentName: 'Introduction',
            tsState: 'add',
            is_ts: false,
            is_apa: false,
            is_complete: false,
            from: new Date().getFullYear() - 1,
            to: new Date().getFullYear(),
            isShowYearSelectUI: false,
            targetType: null,
            targetInfo: null,
            dimensions: null,
            confirmTxt: '',
            confirmType: null,
            titleTxt: '',
            isShowConfirm: false,
            function_id: null,
            comments: [],
            showTime: 0,
            selectYearIndex: 0,
            isShowButtonLeft: false,
            refreshStatus: false,
            is_agree_score: false,
            user: {},
            loginUser: null,

            func:null,

            // 是否有编辑动作
            hasEdit: false,
            // 能否被编辑，双方未Confirm
            canEdit: false,
            hasCommentsUnread: false
        }
    },
    computed: {
        ...mapState({
            allList: state => state.allState.allList,
            yearList: state => state.allState.yearList
        }),
        yearVal() {
            return this.allList && this.allList[this.selectYearIndex] || {};
        },
        isAPAStarted() {
            return this.is_apa;
        },
        isSaveBtnDisabled() {
            // Todo : 看 What 的状态是否为 confirm，如果是旧的What，但未Confirm,那么要让用户保存
            return this.is_apa && this.target && this.target['is_ts_employee_confirm'] && this.target['is_ts_delegator_confirm'];
        },
        // 返回 Submit 按钮是否禁用状态
        canSubmit() {
            if (!this.whats || this.whats.length === 0) {
                // 没有任何绩效数据
                return false;
            }

            let total = 0;
            this.whats.forEach(item => total += (typeof item['weight'] == "undefined" ? 0 : parseInt(item['weight'])));

            // 绩效数据总和不为100
            if (total !== 100) {
                return false;
            }

            if (this.target['is_ts_employee_submit']) {
                // 用户已提交
                return false;
            }

            return true;
        },
        // 返回 Add Targets 按钮的状态
        // https://gitee.com/tongteng/APABAOSHIJIE/issues/I1XLDX
        canAddTS() {
            if (!this.func) {
                return false;
            }

            if (this.func['is_complete']) {
                // Function 已完成
                return false;
            }

            if (!this.func['is_ts']) {
                // Function 未开启
                return false;
            }

            if (this.target && this.target && this.target['is_ts_employee_confirm'] && this.target['is_ts_delegator_confirm']) {
                return false;
            }

            return true;
        },
        statusTxt() {
            if (!this.func) {
                return `Not Started`;
            } else if (this.func['is_complete']) {
                return `Complete`;
            } else if (this.func['is_apa'] || this.func['is_ts']) {
                return `In Progress`;
            } else {
                return `Not Started`;
            }
        },
        tsStatus() {
            return `${this.to} STATUS: ${this.statusTxt}`
        },
        tableDisabled() {
            return !this.yearVal.is_ts;
        },
        isDelegatorConfirmStatus() {
            return this.target.is_ts_employee_submit && this.target.is_ts_delegator_confirm
        },
        isEmployeeConfirmStatus() {
            return !(this.target.is_ts_delegator_confirm && !this.target.is_ts_employee_confirm);
        },
        isRejected() {
            if (this.whats) {
                return this.whats.some((item) => {
                    return item && !!item.reject_comment_id
                })
            } else {
                return false
            }
        }
    },
    components: {
        Introduction,
        SettingWhat,
        Dimensions,
        OverallComments,
        ListSelect,
        Confirm
    },
    methods: {
        ...mapActions(['setYearList']),
        initYear() {
            let checkItem = this.allList.filter(item => {
                return item.is_ts || item.is_apa;
            })

            if (checkItem.length) {
                this.selectYearIndex = this.allList.indexOf(checkItem[0]);
                this.to = checkItem[0].year;
                this.func = checkItem[0];
            }
        },
        showYearsUI(index) {
            this.isShowYearSelectUI = false;
            if (index !== this.selectYearIndex) {
                this.selectYearIndex = index;
                this.to = this.yearList[this.selectYearIndex];
                this.from = this.to - 1;
                this.$nextTick(() => {
                    this.initData();
                })
            }
        },
        willSubmitTarget() {
            if (!this.canSubmit) {
                return false;
            }

            this.checkData().then(res => {
                this.submitTarget();
            });
        },
        confirm() {
            this.isShowConfirm = false;
            if (this.confirmType) {
                this.deleteWhatRequest();
            }
            if (this.refreshStatus) {
                this.getTarget();
                this.refreshStatus = false;
            }
        },
        cancel() {
            this.isShowConfirm = false;
            if (this.confirmType) {
                this.confirmType = null;
            }
            if (this.refreshStatus) {
                this.refreshStatus = false;
            }
        },
        checkData() {
            return new Promise((resolve, reject) => {
                request({
                    url: 'checkData',
                    method: 'get',
                    isShowLoading: true,
                    params: {
                        function_id: this.func['id']
                    }
                }).then(res => {
                    if (res && res.data) {
                        if (res.data.weight === null) {
                            this.isShowConfirm = true;
                            this.isShowButtonLeft = false;
                            this.confirmTxt = 'The total value of the goal setting must be 100%, please check to resubmit after modification';
                            this.titleTxt = 'Warning';
                            reject();
                        } else {
                            resolve();
                        }
                    }
                })
            })
        },
        submitTarget() {
            request({
                url: 'submitTarget',
                method: 'get',
                isShowLoading: true,
                params: {
                    function_id: this.func['id']
                }
            }).then(res => {
                if (res) {
                    this.isShowConfirm = true;
                    this.isShowButtonLeft = false;
                    this.titleTxt = 'Success';
                    this.confirmTxt = 'Submit successfully';

                    this.refreshStatus = true;

                } else {
                    this.isShowConfirm = true;
                    this.isShowButtonLeft = false;
                    this.titleTxt = 'Warning';
                    this.confirmTxt = 'Submit Fail, Please try again'
                }
            })
        },
        getStatus() {
            return new Promise((resolve, reject) => {
                if (!this.to) {
                    return;
                }

                if (this.func && this.func['year'] && this.to == this.func['year']) {
                    return resolve();
                }

                request({
                    url: 'getFRStatus',
                    method: 'get',
                    params: {
                        // start_year: this.from,
                        year: this.to
                    },
                    isShowLoading: true
                }).then(res => {
                    if (!res || res.code !== 0) {
                        return console.log(res.message);
                    }

                    if (res && res.data) {
                        this.is_apa = res.data.is_apa;
                        this.is_ts = res.data.is_ts;
                        this.is_complete = res.data.is_complete;
                        if (res.data.id) {
                            this.function_id = res.data.id;
                            localStorage.setItem('function_id', res.data.id);

                            this.func = res.data;
                            resolve();
                        } else {
                            reject();
                        }
                    }
                });
            });
        },
        confirmAppraise() {
            if (this.isEmployeeConfirmStatus || this.isRejected) return false;
            if (!this.isEmployeeConfirmStatus) {
                request({
                    url: 'setTsEmployeeConfirm',
                    method: 'get',
                    params: {
                        function_id: this.func['id'],
                        employee_id: this.user.employee_id
                    },
                    isShowLoading: true
                }).then(res => {
                    if (res) {
                        this.target.is_ts_employee_confirm = true

                        this.isShowConfirm = true
                        this.titleTxt = "Success"
                        this.confirmTxt = "Confirm successfully."
                    }
                })
            }
        },
        employeeSave(res) {
            this.target.is_ts_employee_submit = false
            this.target.is_ts_delegator_confirm = false
            this.target.is_ts_employee_confirm = false
            if (this.whats[this.currentIndex] && this.whats[this.currentIndex].reject_comment_id) {
                this.whats[this.currentIndex].reject_comment_id = null
                this.whats[this.currentIndex].reject_comment = null
            }


            this.getTarget();
        },
        deleteWhatRequest() {
            return new Promise((resolve, reject) => {
                request({
                    url: 'deleteTarget',
                    method: 'get',
                    params: {
                        function_id: this.func['id'],
                        what_id: this.confirmType
                    },
                    isShowLoading: true
                }).then(res => {
                    if (res) {
                        this.titleTxt = 'Success';
                        this.confirmTxt = 'Delete successfully';
                        this.isShowButtonLeft = false;
                        this.isShowConfirm = true;
                        this.refreshStatus = true;
                        resolve();
                    } else {
                        this.titleTxt = 'Fail';
                        this.confirmTxt = 'Delete Fail, Please try again.'
                        this.isShowButtonLeft = false;
                        this.isShowConfirm = true;
                        reject();
                    }
                })
            })
        },
        deleteWhat(whatId) {
            this.confirmType = whatId;
            this.titleTxt = 'Confirm';
            this.confirmTxt = 'Are you sure you want to delete this target?'
            this.isShowButtonLeft = true;
            this.isShowConfirm = true;
        },
        getTarget() {
            request({
                url: 'targetDetail',
                method: 'get',
                params: {
                    function_id:this.func['id']
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;

                    return false;
                }

                const {what_id, how_id} = this.$route.query;

                let data     = res.data;
                this.overall = data.overall
                this.whats   = data.whats;
                this.target  = data.target;
                this.user    = data.user
                this.is_agree_score = data.target.is_agree_score;

                if (this.currentIndex !== null) {
                    if (this.targetType == 'what') {
                        this.$refs.table1.setCurrentRow(this.whats[this.currentIndex]);
                    } else if (this.targetType == 'how') {
                        this.$refs.table2.setCurrentRow(this.tableRows[this.currentIndex]);
                    }
                }

                if (what_id && data.whats && data.whats.length) {
                    if (data.whats.length == 1) {
                        this.$refs.table1.setCurrentRow(data.whats[0])
                    }
                    data.whats.forEach((item, idx) => {
                        if (item.what_id == what_id) {
                            this.$refs.table1.setCurrentRow(data.whats[idx])
                        }
                    })
                } else if (how_id && data.how) {
                    this.$refs.table2.setCurrentRow(this.tableRows[0]);
                }

                if (!data.overall) {
                    data.overall = [];
                }

                this.setComments(data.overall);

                this.dimensions = data.how || {};
            })
        },
        setComments(list) {
            list.forEach((item, index) => {
                if (item.owner_id == item.employee_id) {
                    item.type = 'me';
                    item.name = item.employee_name;
                } else {
                    item.type = 'other';
                    item.name = item.delegator_name;
                }

                item.send_time = formatDateTime(item.send_time);

            });

            if (!this.hasCommentsUnread) {
                this.hasCommentsUnread = list.some(item => {
                    return item['is_read'] === false && item['owner_id'] != this.loginUser['id'];
                });
            }

            this.comments = list;
        },
        selectYear() {
            this.isShowYearSelectUI = true;
        },
        confirmDate(from, to) {
            this.from = from + '';
            this.to = to + '';
            this.isShowYearSelectUI = false;
            this.$nextTick(() => {
                this.initData();
            })
        },
        getRowClass() {

        },
        rowClick(row, event, column) {
            return;
        },
        closeComponent() {
            this.$nextTick(() => {
                this.componentName = 'Introduction';
                this.currentIndex = null;
                this.targetType = null;
                this.$refs.table1.setCurrentRow(null);
                this.$refs.table2.setCurrentRow(null);
            })
        },
        previous() {
            this.settingHandle('previous');
        },
        next() {
            this.settingHandle('next');
        },
        addSetting() {
            if (!this.canAddTS) return;
            if (this.componentName == 'SettingWhat') {
                if (this.tsState === 'add') return;
                this.$nextTick(() => {
                    this.componentName = 'SettingWhat';
                    this.tsState = 'add';
                    this.targetInfo = null;
                    this.$refs.table1.setCurrentRow(null);
                    this.$refs.table2.setCurrentRow(null);
                })
            } else {
                this.componentName = 'SettingWhat';
                this.tsState = 'add';
                this.$refs.table1.setCurrentRow(null);
                this.$refs.table2.setCurrentRow(null);
            }
        },
        scrollTop() {
            let tableWrapper = document.getElementsByClassName('el-table__body-wrapper')[0];
            let index = this.currentIndex;
            tableWrapper.scrollTop = 46 * this.currentIndex;
        },
        settingHandle(type) {
            if (type === 'next') {
                if (this.targetType === 'what') {
                    if (this.currentIndex === this.whats.length - 1) {
                        if (this.tableDisabled) return;
                        this.$refs.table1.setCurrentRow(null);
                        this.currentIndex = 0;
                        this.targetType = 'how';
                        this.$refs.table2.setCurrentRow(this.tableRows[this.currentIndex]);
                    } else {
                        this.$refs.table1.setCurrentRow(this.whats[++this.currentIndex]);
                        this.scrollTop();
                    }
                } else if (this.targetType === 'how') {
                    this.$refs.table2.setCurrentRow(this.tableRows[++this.currentIndex]);
                }
            } else if (type === 'previous') {
                if (this.targetType === 'how') {
                    if (this.currentIndex === 0) {
                        this.currentIndex = this.whats.length - 1;
                        this.$refs.table1.setCurrentRow(this.whats[this.currentIndex]);
                        this.scrollTop();
                    } else {
                        this.$refs.table2.setCurrentRow(this.tableRows[--this.currentIndex]);
                    }
                } else if (this.targetType === 'what') {
                    if (this.currentIndex === 0) return;
                    this.$refs.table1.setCurrentRow(this.whats[--this.currentIndex]);
                    this.scrollTop();
                }
            }
        },
        handleCurrentChange1(val) {
            if (val) {
                this.targetType = 'what';
                this.currentIndex = this.whats.indexOf(val);
                this.$refs.table2.setCurrentRow(null);
                this.tsState = 'edit';
                this.targetInfo = val;
                this.$nextTick(() => {
                    this.componentName = 'SettingWhat';
                })

                if (val['employee_red']) {
                    request({
                        url: 'readComment',
                        method: 'get',
                        params: {
                            topic_id:val.what_id,
                            type:'TS'
                        }
                    }).then(res=>{
                        if(res && res.code === 0){
                            this.whats[this.currentIndex]['employee_red'] = false;
                        }
                    })
                }
            }
        },
        howHandle() {
            this.$nextTick(() => {
                this.componentName = this.currentIndex == 0 ? 'Dimensions' : 'OverallComments';
            })
        },
        handleHowItemChange(val) {
            if (this.hasCommentsUnread && val.name == "Overall Comments") {
                request({
                    url: 'readComment',
                    method: 'get',
                    params: {
                        topic_id: this.comments[0]['topic_id'],
                        type:'OV'
                    }
                }).then(res => {
                    if (!res || res.code !== 0) {
                        this.titleTxt = 'Error';
                        this.confirmTxt = res.message;
                        this.isShowButtonLeft = false;
                        this.isShowConfirm = true;

                        return false;
                    }

                    this.hasCommentsUnread = false;
                    this.comments.forEach(item => {
                        if (item['owner_id'] != this.user['id']) {
                            item['is_read'] = true;
                        }
                    });
                })
            }

            if (val && !this.tableDisabled) {
                this.targetType = 'how';
                this.currentIndex = this.tableRows.indexOf(val);
                this.$refs.table1.setCurrentRow(null);
                this.howHandle();
            } else {
                this.$refs.table2.setCurrentRow(null);
            }
        },
        initData() {
            this.targetType = '';
            this.componentName = 'Introduction';
            this.$refs.table1.setCurrentRow(null);
            this.$refs.table2.setCurrentRow(null);
            this.getStatus()
                .then(res => {
                    this.getTarget();
                })
                .catch(err => {
                    this.whats = [];
                    this.dimensions = {};
                })
        },
        anyChange() {
            this.hasEdit = true;
        }
    },
    created() {
        const {function_id} = this.$route.query;

        let json = localStorage.getItem('user')
        if (json) {
            this.loginUser = JSON.parse(json);
        }

        this.setYearList().then(res => {
            if (function_id) {
                this.function_id = function_id;
                res.forEach((item, idx) => {
                    if (item.id == function_id) {
                        this.to = item.year;
                        this.selectYearIndex = idx;
                    }
                })
            } else {
                this.initYear()
            }

            this.initData();
        })
    },
    mounted() {
        this.$nextTick(() => {
            this.tableHeight = this.$refs.tableTop.offsetHeight - 50;
        })
    }
}
