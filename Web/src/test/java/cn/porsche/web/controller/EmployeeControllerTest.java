package cn.porsche.web.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.common.util.JsonUtils;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.controller.params.ImportParam;
import cn.porsche.web.domain.KPIUser;
import cn.porsche.web.service.LoginService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class EmployeeControllerTest extends ApplicationTest {
    private static final String API = "/employee";

    private String token;

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;


    @Test
    void list() {
        final String api = API + "/list";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void listByDelegator() {
        final String api = API + "/list_by_delegator";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void listByManager() {
        final String api = API + "/list_by_delegator";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void searchByName() {
        final String api = API + "/search_by_name";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("name", "Zhou")
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void hireDate() {
        final String api = API + "/hire_date";

        KPIUser params = KPIUser.builder()
                .employeeId(getEmployeeId())
                .hireDate(new Date())
                .build();


        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(params, true))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void move() {
        final String api = API + "/move";

//
//        try {
//            mvcResult = mvc.perform(
//                    post(api)
//                            .header(LoginService.AUTH_NAME, getHRAuthorization())
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .content(JsonUtils.toString(params, true))
//                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
//            ).andExpect(
//                    status().is2xxSuccessful()
//            ).andExpect(
//                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
//            ).andDo(
//                    print()
//            ).andReturn();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Test
    void departments() {
        final String api = API + "/departments";



        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void importHireDates() {
        final String api = API + "/import_hire_dates";

        String text = "PCN724\t2021-01-01\nPCN750\t2020-01-01";

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .param("text", text)
                            .param("function_id", "3")
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void importVP() {
        final String api = API + "/import_vps";

        String text = "PCN114\n" +
                "PCN559\n" +
                "PCN203\n" +
                "PCN467\n" +
                "PCN156\n" +
                "PCN085\n" +
                "PCN502\n" +
                "PCN378\n" +
                "PCN198\n" +
                "PCN566\n" +
                "PCN478\n" +
                "PCN382\n" +
                "PCN421\n" +
                "PCN499\n" +
                "PCN576\n" +
                "PCN618\n" +
                "PCN696\n" +
                "PCN681\n" +
                "PCN918\n" +
                "PCN782\n" +
                "PCN850\n" +
                "PCN905\n" +
                "PCN922\n" +
                "PCN932";

        ImportParam param = new ImportParam();
        param.setText(text);

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(param))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}