package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "positions", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Position implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    Integer id;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    //    JOBCODEID	JOBCODE     JOBFUNCTION	                            SUPERIORJOBCODEID	POSITIONOCCUPIED	Company Code
    //    102029	            Specialist Fleet Steering	            101690	            TRUE	            PCN
    //    103384	            CEO Office Assistant (Intern)	        101302	            TRUE	            PCN
    //    105340	            Assistant Manager Quality Monitoring	101622	            TRUE	            PCN
    //    101528	PCN-NT4-6	Trainer Technical	                    101520	            FALSE	            PCN
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @ApiModelProperty(notes = "HRIS系统中上级主键")
    @JsonProperty("parent_id")
    @Column(name = "parent_id",                 columnDefinition = "INT UNSIGNED NULL COMMENT 'HRIS系统中上级主键'")
    Integer parentId;

    @ApiModelProperty(notes = "HRIS Company Code")
    @JsonProperty("company_code")
    @Column(name = "company_code",              columnDefinition = "VARCHAR(63) NULL COMMENT 'HRIS Company Code'")
    String companyCode;

    @ApiModelProperty(notes = "HRIS系统代码")
    @JsonProperty("hris_code")
    @Column(name = "hris_code",                 columnDefinition = "VARCHAR(63) NULL COMMENT 'HRIS系统代码'")
    String hrisCode;

    @ApiModelProperty(notes = "显示抬头")
    @JsonProperty("function")
    @Column(name = "`function`",                columnDefinition = "VARCHAR(511) NULL COMMENT '显示抬头'")
    String function;

    @ApiModelProperty(notes = "是否已占用完毕")
    @JsonProperty("occupied")
    @Column(name = "occupied",                  columnDefinition = "TINYINT(1) UNSIGNED NULL COMMENT '是否已占用完毕'")
    boolean occupied;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
