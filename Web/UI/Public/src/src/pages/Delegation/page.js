import {request} from '../../common/request.js';
import {mapState, mapActions} from 'vuex'
import Confirm from '../../components/Confirm/page.vue';

export default {
    data() {
        return {
            currentIndex: null,
            keyWord: '',
            tableData: [],
            function_id: null,
            tableHeight: 412,
            personType: null,
            delegatorInfo: {},
            users: [],
            isShowConfirm: false,
            titleTxt: 'Confirm',
            confirmTxt: ''
        }
    },
    components: {
        Confirm
    },
    computed: {
        ...mapState({
            currentYear: state => state.allState.currentYear
        })
    },
    methods: {
        ...mapActions(['setDelegatorInfo', 'setYearList']),
        getRoleList() {
            request({
                url: 'employee',
                method: 'get',
                params: {
                    function_id: this.function_id
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;

                    return;
                }

                if (res && res.list && res.list.length) {
                    let resu = [], users = [];
                    let delegators = [];

                    // 筛选小组长
                    res.list.forEach((item) => {
                        if (item.is_delegator) {
                            delegators.push(item.employee_id);
                            resu.push(item);
                        }
                    });

                    if (delegators.length > 0) {
                        res.list.forEach((item) => {
                            if (!item.is_delegator) {
                                let idx = delegators.indexOf(item.delegator_id)
                                if (idx > -1) {
                                    if (!resu[idx].members) {
                                        resu[idx].members = []
                                    }
                                    resu[idx].members.push(item)
                                } else {
                                    // 全部数据
                                    resu.push(item);
                                    // 非小组长非成员数据
                                    users.push(item);
                                }
                            }
                        })

                        this.users = users;
                        this.tableData = resu;
                    } else {
                        this.tableData = res.list
                        this.users = res.list
                    }
                }
            })
        },
        confirm() {
            this.isShowConfirm = false
        },
        // 获取当前行信息
        setCurrentIndex(row, column, event) {
            this.personType = row.is_delegator
            this.currentIndex = true
            let users = []
            if (row.members) {
                users = [...this.users, ...row.members]
            } else {
                users = this.users
            }

            this.delegatorInfo = {
                delegator: row,
                persons: users.filter((item) => row.employee_id !== item.employee_id)
            }
        },
        rowClick(row, column, event) {
            this.tableData.forEach((item) => {
                if (item.employee_id === row.delegator_id) {
                    this.$refs.table.setCurrentRow(item, true)
                    this.setCurrentIndex(item)
                }
            })
        },
        // 设置小组长、成员
        modifyDelegator() {
            // tableData
            if (Object.keys(this.delegatorInfo).length > 0) {
                this.setDelegatorInfo(this.delegatorInfo)
                this.$router.push('/delegator')
            }
        }
    },
    created() {
        // localStorage.setItem('function_id',1)
        let function_id = localStorage.getItem('function_id');
        if (function_id) {
            this.function_id = function_id;
            this.getRoleList();
        } else {
            if (this.currentYear) {
                this.function_id = this.currentYear.id
                localStorage.setItem('function_id', this.currentYear.id);
                this.getRoleList();
            } else {
                this.setYearList().then(res => {
                    this.function_id = res[0].id;
                    localStorage.setItem('function_id', res[0].id);
                    this.getRoleList();
                })
            }
        }
    },
    mounted() {
        // this.$nextTick(() => {
        //   this.tableHeight = this.$refs.tableWrapper.offsetHeight - 28;
        // })
    }
}
