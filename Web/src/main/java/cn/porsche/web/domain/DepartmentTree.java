package cn.porsche.web.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class DepartmentTree implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "部门信息")
    @JsonProperty("department")
    Department department = null;

    @ApiModelProperty(notes = "子部门信息")
    @JsonProperty("sub_tree")
    List<DepartmentTree> subTree;
}