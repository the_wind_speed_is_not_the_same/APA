package cn.porsche.compliance;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTest {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected String getEmployeeAuthorization() {
        return "d03d03c1c573c076436318db60f5e0a05bce56a262bcb8cd179b9a08fa1305d6e618d265809bc156v641mMQ490qSpNCE3wD1937P6Y42C6p9J99CQ349i1NgH7e7py26eM2GB203SKH7E98aLLpXP61kW53yo16f2T50B1Yk7176x5149511Y2vwmfuJc5j5adb3d8ce46668dd040c286afd43cef21vHH236uc0x6UG7Z92g3245860W80";
    }

    protected String getHRAuthorization() {
        return "ee6328c78fc0eb0802ea572b5703140b4393e92a15396b0efcb024184d7a4664746c2fd812cde4d5OK25Mm1tpR7ZI7Tl8Jl74m568B60CD74MC671RzgMP8IO9tJnV9534U35cfPnG1BIg6Lk0X8boH468s808njaCJ339n319I83t87zEJ09i1778ihw2j11fc66788a2a80d365b80eb3654bb7dc90Z8y2a6RF5929Y56EzB374174280";
    }
}