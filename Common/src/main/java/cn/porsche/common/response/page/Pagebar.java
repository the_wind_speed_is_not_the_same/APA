package cn.porsche.common.response.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
@Builder
@ApiModel(value = "页码基础类")
public class Pagebar {
    @ApiModelProperty(value = "第一页")
    boolean first;

    @ApiModelProperty(value = "最后一页")
    boolean last;

    @ApiModelProperty(value = "每页个数")
    int size;

    @ApiModelProperty(value = "当前页码")
    int current;

    @ApiModelProperty(value = "当前页对应的页码")
    int nums[];

    @ApiModelProperty(value = "结果集总数")
    long total;

    @ApiModelProperty(value = "总页数")
    @JsonProperty("total_page")
    long totalPage;

    public static Pagebar fromPageable(Page page) {
        return Pagebar.builder()
                .current(page.getNumber() + 1)
                .size(page.getSize())
                .total(page.getTotalElements())
                .totalPage(page.getTotalPages())
                .first(page.isFirst())
                .last(page.isLast())
                .build();
    }
}
