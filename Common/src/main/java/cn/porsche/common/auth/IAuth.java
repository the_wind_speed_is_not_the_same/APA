package cn.porsche.common.auth;


public interface IAuth {
    AccessToken getAccessToken(String code);
    AccessToken refreshAccessToken(String code);

    App getApp();

    String redirect();
}
