package cn.porsche.web.service;

import cn.porsche.web.ApplicationTest;
import cn.porsche.web.domain.Function;
import cn.porsche.web.domain.Target;
import cn.porsche.web.domain.TargetWhat;
import cn.porsche.web.domain.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class MessageServiceTest extends ApplicationTest {
    @Autowired
    MessageService messageService;

    @Autowired
    UserService userService;

    @Autowired
    TargetService targetService;

    @Autowired
    FunctionService functionService;


    private User employee;
    private User delegator;
    private Function function;

    private Target target;
    private TargetWhat what;


    @BeforeEach
    void setUp() {
        employee    =  userService.findById(getEmployeeId());
        delegator   = userService.findById(getManagerId());


        function    = functionService.findById(getFunctionId());
        target      = targetService.findByUser(function, employee);
        what        = targetService.findWhatById(getWhatId());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void employeeSubmitTarget() {
        messageService.employeeSubmitOnTS(function, employee, target);
    }

    @Test
    void delegatorCommentWhat() {
        messageService.delegatorRejectOnTS(function, employee, target, what);
    }

    @Test
    void readMessage() {
    }

    @Test
    void findUnReadMessages() {
    }
}