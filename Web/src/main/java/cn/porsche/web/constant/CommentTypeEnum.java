package cn.porsche.web.constant;

public enum CommentTypeEnum {
    TS("TS"),
    OV("OV"),
    APA("APA"),
    REJECT("REJECT"),
    ;

    private String type;

    CommentTypeEnum(String type) {
        this.type = type;
    }

    public String getName() {
        return type;
    }

    public static CommentTypeEnum from(String type) {
        try {
            return CommentTypeEnum.valueOf(type);
        } catch (IllegalArgumentException exception) {
            return null;
        }
    }
}
