package cn.porsche.web.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.common.util.JsonUtils;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.domain.Email;
import cn.porsche.web.domain.Function;
import cn.porsche.web.service.LoginService;
import net.minidev.json.JSONUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class FunctionControllerTest extends ApplicationTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String API = "/function";

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;


    @Test
    public void available() {
        final String api = API + "/available";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStatus() {
        final String api = API + "/status";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("start_year", "2019")
                            .param("finish_year", "2020")
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void tsRelease() {
        final String api = API + "/ts_release";

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .param("year", getFunctionYear())
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void tsReOpen() {
        final String api = API + "/ts_reopen";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getHRAuthorization())
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void tsSendEmail() {
        final String api = API + "/ts_email";

        Email email = Email.builder()
                .functionId(2)
                .subject("hello")
                .content("let's begin!")
                .build();

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(email, true))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void apaRelease() {
        final String api = API + "/apa_release";


        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .param("year", getFunctionYear())
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void apaSendEmail() {
        final String api = API + "/apa_email";

        Email email = Email.builder()
                .functionId(getFunctionId())
                .subject("hello")
                .content("let's begin!")
                .build();

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getEmployeeAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(email, true))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}