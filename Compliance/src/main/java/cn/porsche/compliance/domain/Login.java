package cn.porsche.compliance.domain;


import cn.porsche.compliance.constant.LoginTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "login", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Login implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    //用户主键
    @ApiModelProperty(notes = "用户登录的主键")
    @JsonProperty("user_id")
    @Column(name = "user_id",                   columnDefinition = "INT UNSIGNED NOT NULL COMMENT '用户登录的主键'")
    Integer userId;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "登陆的方式")
    @JsonProperty("by")
    @Column(name = "`by`",                      columnDefinition = "VARCHAR(256) NOT NULL COMMENT '用户登录的方式'")
    LoginTypeEnum loginBy;

    @ApiModelProperty(notes = "用户登录的密钥")
    @JsonProperty("access_token")
    @Column(name = "access_token",              columnDefinition = "VARCHAR(1024) NOT NULL COMMENT '用户登录的密钥'")
    String accessToken;

    @ApiModelProperty(notes = "登陆的过期时间")
    @JsonProperty("access_token_expire_time")
    @Column(name = "access_token_expire_time",  columnDefinition = "DATETIME NULL COMMENT '登陆的过期时间'")
    Date accessTokenExpireTime;

    @ApiModelProperty(notes = "用户登录的密钥")
    @JsonProperty("refresh_token")
    @Column(name = "refresh_token",             columnDefinition = "VARCHAR(1024) NOT NULL COMMENT '用户登录的密钥'")
    String refreshToken;

    @ApiModelProperty(notes = "登陆的过期时间")
    @JsonProperty("refresh_token_expire_time")
    @Column(name = "refresh_token_expire_time", columnDefinition = "DATETIME NULL COMMENT '登陆的过期时间'")
    Date refreshTokenExpireTime;

    @ApiModelProperty(notes = "第三方凭证")
    @JsonProperty("ticket")
    @Column(name = "ticket",                    columnDefinition = "TEXT NULL COMMENT '第三方凭证'")
    String ticket;

    @ApiModelProperty(notes = "最后登录时间")
    @JsonProperty("last_time")
    @Column(name = "last_time",                 columnDefinition = "DATETIME NULL COMMENT '最后登录时间'")
    Date lastTime;

    @ApiModelProperty(notes = "最后登录的流程器信息")
    @JsonProperty("last_ua")
    @Column(name = "last_ua",                   columnDefinition = "TEXT NULL COMMENT '最后登录的流程器信息'")
    String lastUA;

    @ApiModelProperty(notes = "最后登录的设备")
    @JsonProperty("last_dev")
    @Column(name = "last_dev",                   columnDefinition = "TEXT NULL COMMENT '最后登录的UA'")
    String lastDEV;

    @ApiModelProperty(notes = "登录之后的Cookie标识")
    @JsonProperty("auth_id")
    @Column(name = "auth_id",                   columnDefinition = "VARCHAR(256) NULL COMMENT '登录之后的Cookie标识'")
    String authId;

    @ApiModelProperty(notes = "是否已登出")
    @JsonProperty("is_logout")
    @Column(name = "is_logout",                 columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否已登出'")
    boolean Logout;

    @ApiModelProperty(notes = "登出时间")
    @JsonProperty("logout_time")
    @Column(name = "logout_time",               columnDefinition = "DATETIME NULL COMMENT '登出时间'")
    Date logoutTime;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
