package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "emails", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Email {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    Integer id;

    @ApiModelProperty(notes = "阶段主键")
    @JsonProperty("function_id")
    @Column(name = "function_id",               columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '阶段主键'")
    Integer functionId;

    @ApiModelProperty(notes = "发送者主键")
    @JsonProperty("user_id")
    @Column(name = "user_id",                   columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '发送者主键'")
    Integer userId;

    @ApiModelProperty(notes = "发送者邮箱地址")
    @JsonProperty("from")
    @Column(name = "`from`",                    columnDefinition = "TEXT NULL COMMENT '发送者邮箱地址'")
    String from;

    @ApiModelProperty(notes = "发送者邮箱地址")
    @JsonProperty("to")
    @Column(name = "`to`",                      columnDefinition = "TEXT NULL COMMENT '发送者邮箱地址'")
    String to;

    @ApiModelProperty(notes = "邮件主题")
    @JsonProperty("subject")
    @Column(name = "subject",                   columnDefinition = "TEXT NULL DEFAULT NULL COMMENT '邮件主题'")
    String subject;

    @ApiModelProperty(notes = "邮件内容")
    @JsonProperty("content")
    @Column(name = "content",                   columnDefinition = "TEXT NULL DEFAULT NULL COMMENT '邮件内容'")
    String content;

    @ApiModelProperty(notes = "邮件附件")
    @JsonProperty("attachment")
    @Column(name = "attachment",                columnDefinition = "TEXT NULL DEFAULT NULL COMMENT '邮件附件'")
    String attachment;

    @ApiModelProperty(notes = "邮件反馈")
    @JsonProperty("response")
    @Column(name = "response",                  columnDefinition = "TEXT NULL DEFAULT NULL COMMENT '邮件反馈'")
    String response;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;


    public void addAddress(String address) {
        to = StringUtils.isEmpty(to)
                ? ""
                : to.concat(";");

        to = to.concat(address);
    }

    public void addAddresses(List<String> addresses) {
        if (addresses == null) {
            throw new IllegalArgumentException();
        }

        for (String address : addresses) {
            addAddress(address);
        }
    }

    public void addAttachment(String path) {
        attachment = StringUtils.isEmpty(attachment)
                ? ""
                : attachment.concat(";");

        attachment = attachment.concat(path);
    }

    public void addAttachments(List<String> paths) {
        if (paths == null) {
            throw new IllegalArgumentException();
        }

        for (String path : paths) {
            addAttachment(path);
        }
    }
}
