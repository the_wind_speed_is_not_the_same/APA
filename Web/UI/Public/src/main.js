import 'element-ui/lib/theme-chalk/index.css';
import './src/css/font.less';
import './src/css/base.less';
import './src/css/common.less';
import store from './store/index.js'
import Vue from 'vue';
// import Router from 'vue-router';
import 'babel-polyfill';

import { Message, Input, Checkbox, Button, DatePicker, TimePicker, CheckboxGroup, Table, TableColumn, Pagination, Dialog, Select, Option, Radio, RadioGroup, Row, Col, Form, FormItem, Tooltip, Upload, Popover, Carousel, CarouselItem, Alert, Tabs, TabPane, Tag, Switch, Loading, InputNumber, Menu, Submenu, MenuItem, MenuItemGroup, Card, Steps, Step, Container, Main, Aside, Header, Footer, MessageBox, Badge, Collapse, CollapseItem} from 'element-ui';
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

locale.use(lang);

import Filter from './src/common/filter.js'

import APP from './src/pages/app/page.vue';
import router from './config/router.js';
import { getCookie, setCookie } from './src/common/utils.js';
// import $ from 'n-zepto';

// 直接判断用户登录状态
(function() {
//let userId = getCookie('user_id');
//let user = localStorage.getItem('user')
//if(user && JSON.parse(user).id != userId){
//	
//}
	
  let auth = getCookie("auth_id");
  
  if (auth && auth.length === 256) {
    return ;
  }

  if (window.location.hostname === 'localhost') {
    // 开发环境
    return ;
  }

  // 用户未登录，跳转SSO登录地址
  let sso = "/login/sso";
  if (window.location.hash) {
    sso += "?uri=/" + encodeURIComponent(window.location.hash);
  }

  window.location.href = sso;
})();


Vue.use(Filter)

Vue.use(Input);
Vue.use(InputNumber);
Vue.use(Checkbox);
Vue.use(Button);
Vue.use(DatePicker);
Vue.use(TimePicker);
Vue.use(CheckboxGroup);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Pagination);
Vue.use(Dialog);
Vue.use(Select);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(Option);
Vue.use(Row);
Vue.use(Col);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Tooltip);
Vue.use(Upload);
Vue.use(Popover);
Vue.use(Carousel);
Vue.use(CarouselItem);
Vue.use(Alert);
Vue.use(Tabs);
Vue.use(TabPane);
Vue.use(Tag);
Vue.use(Switch);
Vue.use(Menu);
Vue.use(Submenu);
Vue.use(MenuItem);
Vue.use(MenuItemGroup);
Vue.use(Card);
Vue.use(Steps);
Vue.use(Step);
Vue.use(Container);
Vue.use(Main);
Vue.use(Aside);
Vue.use(Header);
Vue.use(Footer);
Vue.use(Badge);
Vue.use(Collapse);
Vue.use(CollapseItem);

Vue.use(Loading.directive);
// Vue.use(Router);

Vue.prototype.$loading = Loading.service
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
Vue.prototype.$prompt = MessageBox.prompt
Vue.prototype.$message = function(option){
    Message(option);
};


// let router = new Router({ routes: pages,mode:'hash'});

router.afterEach((to, from, next) => {
   window.scrollTo(0, 0);
   document.title = to.name;
   window.pathName = to.name;
});


let app = new Vue({
    router,
    store,
    render: h => h(APP)
}).$mount('#app');


