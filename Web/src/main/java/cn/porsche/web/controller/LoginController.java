package cn.porsche.web.controller;

import cn.porsche.auth.AccessToken;
import cn.porsche.common.response.ApiResult;
import cn.porsche.common.util.RandomUtils;
import cn.porsche.common.util.http.HttpException;
import cn.porsche.common.util.http.HttpUtils;
import cn.porsche.web.domain.KPIUser;
import cn.porsche.web.domain.Login;
import cn.porsche.web.domain.User;
import cn.porsche.web.service.KPIUserService;
import cn.porsche.web.service.LoginService;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;


@Log4j2
@ApiIgnore
@Controller
@RequestMapping("/login")
public class LoginController extends BaseController {
    private final static String UTF8                = "UTF-8";
    private final static String TEST_DOMAIN_TOKEN   = "test.";
    private final static String COOKIE_AUTH_TOKEN = "auth_id";
    private final static String COOKIE_USER_TOKEN   = "user_id";
    private final static String REDIRECT_TOKEN      = "redirect:";

    private final static String AVATAR_DOMAIN       = "https://iporsche.porsche-pcn.com";

    protected final static String SSO_LOGIN         = "https://sso-prod.porsche-pcn.com/sso/login?service=%s";
    protected final static String SSO_LOGIN_TEST    = "https://sso-test.porsche-pcn.com/sso/login?service=%s";
    protected final static String SSO_LOGOUT        = "https://sso-prod.porsche-pcn.com/sso/v1/tickets/%s";
    protected final static String SSO_LOGOUT_TEST   = "https://sso-test.porsche-pcn.com/sso/v1/tickets/%s";
    protected final static String SSO_VALIDATE      = "https://sso-prod.porsche-pcn.com/sso/p3/serviceValidate?format=json&ticket=%s&service=%s";
    protected final static String SSO_VALIDATE_TEST = "https://sso-test.porsche-pcn.com/sso/p3/serviceValidate?format=json&ticket=%s&service=%s";
    protected final static String SSO_CALLBACK      = "/login/sso_validate?uri=%s";
    protected final static String SSO_CALLBACK_TEST = "/login/sso_validate?uri=%s";
    protected final static String HOST_DETAIL       = "%s://%s:%d";

    @Value("${spring.profiles.active}")
    protected String active;

    @Autowired
    protected LoginService loginService;

    @Autowired
    protected KPIUserService kpiUserService;

    @ApiOperation(
            value = "使用SSO登录账号",
            notes = "通过SSO登录系统"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功", response = ApiResult.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uri",         required = true,    value = "授权成功之后跳转的地址"),
            @ApiImplicitParam(name = "pound",       required = true,    value = "进行SSO对应的pound信息"),
    })
    @GetMapping(value = "/sso")
    public String sso(
            HttpServletRequest request,
            @RequestParam(value = "uri", required = false, defaultValue = "/") String uri
    ) {
        // 查看当前有没有Cookie，如果没有者需要根据当前支持的App进行跳转
        User user = loginService.getUserFromCookie();
        if (null != user) {
            // 用户已登录
            return "redirect:"
                    + String.format(HOST_DETAIL, request.getScheme(), request.getServerName(), request.getServerPort())
                    + uri;
        }

        // 检查是否为测试环境
        boolean isTest = request.getServerName().indexOf(TEST_DOMAIN_TOKEN) != -1;

        // 生成真实的回调地址
        String back = String.format(HOST_DETAIL, request.getScheme(), request.getServerName(), request.getServerPort())
                + String.format(isTest ? SSO_CALLBACK_TEST : SSO_CALLBACK, encode(uri));

        if (isTest) {
            back += "&test=true";
        }

        log.debug(back);

        back = encode(back);

        return REDIRECT_TOKEN + String.format(isTest ? SSO_LOGIN_TEST : SSO_LOGIN, back);
    }

    @ApiIgnore
    @ApiOperation(
            value = "SSO登录环境回调地址"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功", response = ApiResult.class)
    })
    @GetMapping(value = "/sso_validate")
    public String ssoBack(
            HttpServletRequest request,
            HttpServletResponse httpResponse,
            @RequestParam(value = "ticket") String ticket,
            @RequestParam(value = "uri", required = false, defaultValue = "/") String uri,
            @RequestParam(value = "test", required = false, defaultValue = "false") boolean isTest
    ) {
        // 生成真实的回调地址
        String back = String.format(HOST_DETAIL, request.getScheme(), request.getServerName(), request.getServerPort())
                        + String.format(isTest ? SSO_CALLBACK_TEST : SSO_CALLBACK, encode(uri));

        if (isTest) {
            back += "&test=true";
        }

        log.debug(back);

        back = encode(back);

        String response;
        JSONObject json;

        String userCode, userSmallAvatar;

        try {
            response = HttpUtils.get(String.format(isTest ? SSO_VALIDATE_TEST : SSO_VALIDATE, ticket, back));
        } catch (HttpException e) {
            e.printStackTrace();

            return redirectToLogin(uri, request);
        }

        json = new JSONObject(response);
        try {
            json = json.getJSONObject("serviceResponse");
            json = json.getJSONObject("authenticationSuccess");
            json = json.getJSONObject("attributes");
            JSONArray array = json.getJSONArray("eid");

            if (array.length() == 0) {
                // 最重要的信息在里面
                return redirectToLogin(uri, request);
            }

            userCode = array.getString(0);

            json = json.getJSONObject("avatar");
            userSmallAvatar = (String) json.get("avatar_s");
            userSmallAvatar = AVATAR_DOMAIN + "/public/images/avatar/s.jpg";

            log.debug("工号：{}", userCode);

        } catch (JSONException e) {
            return redirectToLogin(uri, request);
        }

        if (null == userCode
            || null == userSmallAvatar
        ) {
            return redirectToLogin(uri, request);
        }

        User user = userService.findByCode(userCode.toUpperCase());
        if (null == user) {
            return redirectToLogin(uri, request);
        }

        if (null == user.getAvatar() || !user.getAvatar().contentEquals(userSmallAvatar)) {
            user.setAvatar(userSmallAvatar);

            userService.update(user);

            UpdateAvatar(user, user.getAvatar());
        }

        // 组装一个AccessToken
        AccessToken token = new AccessToken();

        // 第三方凭证
        token.setTicket(ticket);
        token.setAccessToken(RandomUtils.randomString(32));
        token.setAccessTokenExpiresTime(DateUtils.addDays(new Date(), 10));
        token.setRefreshToken(RandomUtils.randomString(32));
        token.setRefreshTokenExpiresTime(DateUtils.addDays(new Date(), 70));

        Login login = loginService.loginBySSO(user, token);
        if (null == login.getAuthId()) {
            return redirectToLogin(uri, request);
        }

//        cn.porsche.web.util.HttpUtils.setCookie(httpResponse, "auth_id", login.getAuthId(), 10 * 86400, "/");
        // cookie 关闭浏览器即失效
        // -1 关闭即失效
        //  0 删除Cookie
        cn.porsche.web.util.HttpUtils.setCookie(httpResponse, COOKIE_AUTH_TOKEN, login.getAuthId(), -1, "/");
        cn.porsche.web.util.HttpUtils.setCookie(httpResponse, COOKIE_USER_TOKEN, login.getUserId().toString(), -1, "/");


        return REDIRECT_TOKEN + String.format(HOST_DETAIL, request.getScheme(), request.getServerName(), request.getServerPort()) + uri;
    }



    @ApiOperation(
            value = "退出登录",
            notes = "清除用户的Cookie信息，以到达退出系统。同时退出动作记录在案"
    )
    @GetMapping("/out")
    public String logout(
            HttpServletRequest request,
            HttpServletResponse httpResponse,
            @RequestParam(value = "uri", required = false, defaultValue = "/") String uri
    ) {
        user = loginService.getUserFromCookie();
        String href = REDIRECT_TOKEN
                + String.format(HOST_DETAIL, request.getScheme(), request.getServerName(), request.getServerPort())
                + uri;

        if (null == user) {
            return href;
        }

        Login login = loginService.logout(user);
        if (null == login || null == login.getTicket() || login.getTicket().length() == 0) {
            return href;
        }

        if (null != login.getTicket()) {
            try {
                boolean isTest = request.getServerName().indexOf(TEST_DOMAIN_TOKEN) != -1;
                String response = HttpUtils.get(String.format(isTest ? SSO_LOGOUT_TEST : SSO_LOGOUT, login.getTicket()));
            } catch (HttpException e) {
                e.printStackTrace();
            }
        }


        cn.porsche.web.util.HttpUtils.delCookie(httpResponse, COOKIE_AUTH_TOKEN);

        return href;
    }

    private String redirectToLogin(String uri, HttpServletRequest request) {
        return REDIRECT_TOKEN
                + String.format(HOST_DETAIL, request.getScheme(), request.getServerName(), request.getServerPort())
                + "/login/sso?uri=" + uri;
    }

    private void UpdateAvatar(User user, String avatar) {
        List<KPIUser> users = kpiUserService.findAllByUser(user);

        for (KPIUser item : users) {
            if (avatar.equals(item.getAvatar())) {
                continue;
            }

            item.setAvatar(avatar);
            kpiUserService.update(item);
        }
    }
}