package cn.porsche.compliance.constant;

// 员工类型
public enum EmployeeTypeEnum {
    OUTER("outer"),
    STAFF("staff"),
    VP("vp"),
    ;

    private String type;
    EmployeeTypeEnum(String type) {
        this.type = type;
    }
}