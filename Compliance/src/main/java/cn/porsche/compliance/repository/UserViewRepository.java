package cn.porsche.compliance.repository;

import cn.porsche.compliance.domain.view.UserView;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserViewRepository extends CrudRepository<UserView, Integer> {
    UserView findByCode(String code);
    UserView findByPositionId(Integer positionId);
    UserView findByManagerId(Integer managerId);

    List<UserView> findAllByManagerId(Integer managerId);
}
