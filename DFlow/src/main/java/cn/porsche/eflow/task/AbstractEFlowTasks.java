package cn.porsche.eflow.task;

import cn.porsche.eflow.task.exception.ParsingException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.*;
import java.util.regex.Pattern;

@Log4j2
public abstract class AbstractEFlowTasks {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${porsche.dflow.api.filter:false}")
    protected boolean isFilter;

    @Value("${porsche.dflow.api.isTest:false}")
    protected boolean isTest;

    protected final static String PCNToken          = "P(S)?CN\\d+";

    // departmentKeys
    //  DEPARTMENTID DEPARTMENTNAME PARENTDEPARTMENDID Company Code-Company
    protected final static String[] dpKeys = {"DEPARTMENTID", "DEPARTMENTNAME", "PARENTDEPARTMENDID", "Company_Code_Company"};


    // employeeKeys
    //  0UserID 1FIRSTNAMEEN 2LASTNAMEEN 3USERNAMECN 4USERCODE 5JOBCODEID 6DEPARTMENTID 7DEPARTMENTNAME 8COSTCENTERCODE 9EMAIL
    //      10ORGANIZATION 11MOBILENO 12TEL 13Preferred Name
    protected final static String[] emKeys = {"UserID", "FIRSTNAMEEN", "LASTNAMEEN", "USERNAMECN", "USERCODE",
            "JOBCODEID", "DEPARTMENTID", "DEPARTMENTNAME", "COSTCENTERCODE", "EMAIL", "ORGANIZATION", "MOBILENO", "TEL", "Preferred_Name"};

    // employeeKeys
    //  JOBCODEID JOBCODE JOBFUNCTION SUPERIORJOBCODEID	POSITIONOCCUPIED Company Code
    protected final static String[] poKeys = {"JOBCODEID", "JOBCODE", "JOBFUNCTION", "SUPERIORJOBCODEID", "POSITIONOCCUPIED", "Company_Code"};


    protected enum DateTypeEnum {
        Department(),
        Position(),
        Employee(),
        ;
    }


    // API 调用前需要实现的方法
    protected abstract void before();

    // API 调用前需要实现的方法
    protected abstract void after();

    // 将字符串解析成对象
    protected abstract <T extends Object> T[] parseToObjects(String line[]) throws ParsingException;
    protected abstract void doingDepartment(T[] employees) throws ParsingException;
    protected abstract void doingEmployees(T[] employees) throws ParsingException;
    protected abstract void doingPosition(T[] employees) throws ParsingException;

    protected abstract String[] getDataProvider(DateTypeEnum type);


    // 每天最后一秒执行文件
    @Scheduled(cron = "01 01 22 1/1 * ?")
    public void cron() {
        try {
            running();
        } catch (ParsingException e) {
            e.printStackTrace();

            logger.debug(e.getMessage());
        }
    }

    protected boolean filter(String code) {
        if (StringUtils.isEmpty(code)) {
            return true;
        }

        return isFilter && !Pattern.compile(PCNToken).matcher(code).matches();
    }

    protected void running() throws ParsingException {
        before();

        List[] list;
        for (DateTypeEnum item : DateTypeEnum.values()) {
            list = parseToObjects(getDataProvider(item));
            switch (item) {
                case Employee: doingDepartment();      break;
                case Position: list.add(parsePosition(lines));      break;
                case Department: list.add(parseDepartment(lines));  break;
            }
        }

        after();
    }
}
