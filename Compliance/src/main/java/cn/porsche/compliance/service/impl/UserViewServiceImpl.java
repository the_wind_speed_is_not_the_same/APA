package cn.porsche.compliance.service.impl;

import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.domain.view.UserView;
import cn.porsche.compliance.repository.UserViewRepository;
import cn.porsche.compliance.service.UserViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserViewServiceImpl implements UserViewService {
    @Autowired
    UserViewRepository repository;

    @Override
    public UserView findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public UserView findByCode(String code) {
        return repository.findByCode(code);
    }

    @Override
    public Integer count() {
        return null;
    }

    @Override
    public List<UserView> findEmployeesByManager(User manager) {
        return repository.findAllByManagerId(manager.getId());
    }

    @Override
    public UserView getManager(User user) {
        return findById(user.getManagerId());
    }
}
