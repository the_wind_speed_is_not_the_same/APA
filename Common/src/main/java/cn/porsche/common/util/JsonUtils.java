package cn.porsche.common.util;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

public final class JsonUtils {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String toString (Object object){
        return toString(object, false);
    }


    public static String toString (Object object, boolean notNull){
        StringWriter writer = new StringWriter();
        if (notNull) {
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        } else {
            mapper.setSerializationInclusion(JsonInclude.Include.USE_DEFAULTS);
        }

        try {
            mapper.writeValue(writer, object);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            System.out.print(e);

            return null;
        }

        return writer.toString();
    }

    public static <T> T fromString (String json, Class klass){
        T object;

        try {
            object = (T) mapper.readValue(json, klass);
        } catch (RuntimeException e) {
            //log.error("Runtime exception during deserializing " + klass.getSimpleName() + " from " + StringUtils.abbreviate(json, 80));
            System.out.print(e);
            throw e;
        } catch (Exception e) {
            //log.error("Exception during deserializing " + klass.getSimpleName() + " from " + StringUtils.abbreviate(json, 80));
            System.out.print(e);
            return null;
        }

        return object;
    }
}
