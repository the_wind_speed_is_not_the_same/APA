package cn.porsche.web.constant;

public enum ScoreLevelEnum {
    A("A"),
    B("B"),
    C("C"),
    D("D"),
    E("E"),
    N("N"),
    ;

    private String level;

    ScoreLevelEnum(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }
    public ScoreLevelEnum setLevel(String level) {
        return from(level);
    }

    public static ScoreLevelEnum from(String level) {
        try {
            return ScoreLevelEnum.valueOf(level);
        } catch (IllegalArgumentException exception) {
            return null;
        }
    }
}
