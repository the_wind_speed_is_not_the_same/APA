package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "target_how", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class TargetHow implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @ApiModelProperty(notes = "KPI主键")
    @JsonProperty("function_id")
    @Column(name = "function_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'KPI主键'")
    Integer functionId;

    @ApiModelProperty(notes = "TS主键")
    @JsonProperty("target_id")
    @Column(name = "target_id",                 columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'TS主键'")
    Integer targetId;

    @ApiModelProperty(notes = "用户主键")
    @JsonProperty("employee_id")
    @Column(name = "employee_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT '用户主键'")
    Integer employeeId;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how1_weight")
    @Column(name = "how1_weight",               columnDefinition = "TINYINT UNSIGNED NULL COMMENT '如何做权重'")
    Integer how1Weight;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how1")
    @Column(name = "how1",                      columnDefinition = "TEXT NULL COMMENT '如何做'")
    String how1;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how1_e_score")
    @Column(name = "how1_e_score",              columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how1EScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how1_do_score")
    @Column(name = "how1_do_score",             columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how1DOScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how1_dt_score")
    @Column(name = "how1_dt_score",             columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how1DTScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how2_weight")
    @Column(name = "how2_weight",               columnDefinition = "TINYINT UNSIGNED NULL COMMENT '如何做权重'")
    Integer how2Weight;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how2")
    @Column(name = "how2",                      columnDefinition = "TEXT NULL COMMENT '如何做'")
    String how2;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how2_e_score")
    @Column(name = "how2_e_score",              columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how2EScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how2_do_score")
    @Column(name = "how2_do_score",             columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how2DOScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how2_dt_score")
    @Column(name = "how2_dt_score",             columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how2DTScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how3_weight")
    @Column(name = "how3_weight",               columnDefinition = "TINYINT UNSIGNED NULL COMMENT '如何做权重'")
    Integer how3Weight;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how3")
    @Column(name = "how3",                      columnDefinition = "TEXT NULL COMMENT '如何做'")
    String how3;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how3_e_score")
    @Column(name = "how3_e_score",              columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how3EScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how3_do_score")
    @Column(name = "how3_do_score",             columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how3DOScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how3_dt_score")
    @Column(name = "how3_dt_score",             columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how3DTScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how4_weight")
    @Column(name = "how4_weight",               columnDefinition = "TINYINT UNSIGNED NULL COMMENT '如何做权重'")
    Integer how4Weight;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how4")
    @Column(name = "how4",                      columnDefinition = "TEXT NULL COMMENT '如何做'")
    String how4;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how4_e_score")
    @Column(name = "how4_e_score",              columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how4EScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how4_do_score")
    @Column(name = "how4_do_score",             columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how4DOScore;

    @ApiModelProperty(notes = "如何做")
    @JsonProperty("how4_dt_score")
    @Column(name = "how4_dt_score",             columnDefinition = "FLOAT(2,1) UNSIGNED NULL COMMENT '如何做权重'")
    Float how4DTScore;

    @ApiModelProperty(notes = "员工是否确认")
    @JsonProperty("is_employee_confirm")
    @Column(name = "is_employee_confirm",       columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '员工是否确认'")
    boolean EmployeeConfirm;

    @ApiModelProperty(notes = "员工是否确认")
    @JsonProperty("is_delegator_confirm")
    @Column(name = "is_delegator_confirm",      columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '代理人是否确认'")
    boolean DelegatorConfirm;

    @ApiModelProperty(notes = "向小组长展示的红点")
    @JsonProperty("delegator_red")
    @Column(name = "delegator_red",             columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '向小组长展示的红点'")
    boolean delegatorRed;

    @ApiModelProperty(notes = "向员工展示的红点")
    @JsonProperty("employee_red")
    @Column(name = "employee_red",             columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '向员工展示的红点'")
    boolean employeeRed;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
