package cn.porsche.common.util;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


/**
 * 随机数辅助类
 *
 *
 */
public class RandomUtils {
    public static final int randomInt(int max) {
        return ThreadLocalRandom.current().nextInt(max);
    }

    public static String randomString(int length) {
        StringBuilder string = new StringBuilder();
        for (int i = 0; i < length; i++) {
            // 输出字母还是数字
            boolean isChar = (randomInt(2) % 2 == 0);
            if (isChar) {
                // 取得大写字母还是小写字母
                int choice = randomInt(2) % 2 == 0 ? 65 : 97;
                string.append((char) (choice + randomInt(26)));
            } else {
                // 数字
                string.append(randomInt(10));
            }
        }

        return string.toString();
    }
}
