package cn.porsche.web.repository;

import cn.porsche.web.domain.TargetWhat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TargetWhatRepository extends CrudRepository<TargetWhat, Integer> {
    void deleteByTargetId(Integer targetId);
    List<TargetWhat> findAllByFunctionIdAndEmployeeId(Integer functionId, Integer userId);

    List<TargetWhat> findAllByTargetIdOrderByIdAsc(Integer targetId);
}
