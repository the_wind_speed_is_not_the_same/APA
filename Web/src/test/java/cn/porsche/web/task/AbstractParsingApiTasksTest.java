package cn.porsche.web.task;

import cn.porsche.web.ApplicationTest;
import cn.porsche.web.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

@Log4j2
class AbstractParsingApiTasksTest extends ApplicationTest {
    protected final static String PCNToken          = "P(S)?CN\\d+";


    @Test
    void filter() {
        String code1 = "PCN550";
        String code2 = "PSCN009";
        String code3 = "PCNX550";

        log.info(Pattern.compile(PCNToken).matcher(code1).matches());
        log.info(Pattern.compile(PCNToken).matcher(code2).matches());
        log.info(Pattern.compile(PCNToken).matcher(code3).matches());
    }
}