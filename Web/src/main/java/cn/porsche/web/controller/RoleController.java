package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.GroupRole;
import cn.porsche.web.group.IGroup;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/role")
@Api(value = "/role", description = "绩效团队操作模块权限类")
public class RoleController extends BaseController {
    @Autowired
    IGroup groupFactory;

    @ApiOperation(
            value = "【HR】在【Setting】对组权限做调整",
            notes = "GET参数<br />" +
                    "group：操作组名称<br />" +
                    "module：模块名称<br />" +
                    "operator：操作类别<br />" +
                    "turn：true，增加；false，关闭<br />"
    )
    @ApiResponses({

            @ApiResponse(code = 200, message = "成功", response = ApiResult.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "group",       required = true, dataTypeClass = GroupNameEnum.class,   value = "权限组名"),
            @ApiImplicitParam(name = "module",      required = true, dataTypeClass = ModuleNameEnum.class,  value = "操作模块"),
            @ApiImplicitParam(name = "operator",    required = true, dataTypeClass = OperatorText.class,    value = "接口操作"),
            @ApiImplicitParam(name = "turn",        required = true, dataTypeClass = Boolean.class,         value = "取消或增加，默认取消"),
    })
    @GetMapping(value = "/update")
    public String update(
            @RequestParam(value = "group") GroupNameEnum groupName,
            @RequestParam(value = "module") ModuleNameEnum moduleName,
            @RequestParam(value = "operator") OperatorText operator,
            @RequestParam(value = "turn", defaultValue = "false")   Boolean turn
    ) {
        if (noPromise(ModuleNameEnum.SystemSetting, OperatorText.ADD, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Group group = groupFactory.findByName(groupName.getName());
        if (null == group) {
            ApiResult.isBad("Unable to find the Group data, please contact your HR to fix it");
        }

        boolean isUpdate = false;
        GroupRole role = groupFactory.findByGroupAndModule(group, moduleName.getName());
        if (null == role) {
            // 权限没有，需要重新添加
            role = GroupRole.builder()
                    .groupId(group.getId())
                    .name(groupName.getName())
                    .module(moduleName.getName())
                    .viewer(false)
                    .anonymous(false)
                    .add(false)
                    .edit(false)
                    .check(false)
                    .download(false)
                    .delegation(false)
                    .build();

            isUpdate = true;
        }

        switch (operator) {
            case ADD:           role.setAdd(turn);          isUpdate = true; break;
            case EDIT:          role.setEdit(turn);         isUpdate = true; break;
            case CHECK:         role.setCheck(turn);        isUpdate = true; break;
            case DOWNLOAD:      role.setDownload(turn);     isUpdate = true; break;
            case DELEGATION:    role.setDelegation(turn);   isUpdate = true; break;
        }

        if (!isUpdate) {
            return ApiResult.isOk();
        }

        role.setGroupIdx(groupName.ordinal() + 1);
        role.setModuleIdx(moduleName.ordinal() + 1);

        return null == groupFactory.update(role) ? ApiResult.isBad() : ApiResult.isOk();
    }

    @ApiOperation(
            value = "【HR】在【Setting】返回所有权限的接口列表"
    )
    @ApiResponses({

            @ApiResponse(code = 200, message = "成功", response = GroupRole.class, responseContainer = "List")
    })
    @GetMapping(value = "/list")
    public String list() {
        if (noPromise(ModuleNameEnum.SystemSetting, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        List<GroupRole> list = new ArrayList<>();
        for (GroupNameEnum name : GroupNameEnum.values()) {
            list.addAll(groupFactory.getRoles(groupFactory.findByName(name.getName())));
        }

        Iterator<GroupRole> iterator = list.listIterator();
        while (iterator.hasNext()) {
            GroupRole item = iterator.next();

            if (null == item.getGroupIdx() || item.getGroupIdx() > 5
                    || null == item.getModuleIdx() || item.getModuleIdx() > 8
            ) {
                iterator.remove();
            }
        }

        return ApiResult.builder().list(list).toJson();
    }
}
