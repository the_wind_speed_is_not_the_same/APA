package cn.porsche.compliance.service;


import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.Statistical;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.emnu.TrainingTypeEnum;

import java.util.List;

public interface StatisticalService {
    Statistical update(Statistical summary);

    void refresh(User user);
    void refreshQuestion(User user);
    void refreshViewer(User user);

    Statistical findByYearAndUserAndType(Integer year, User user, TrainingTypeEnum type);
    Statistical findByUserAndType(User user, TrainingTypeEnum type);
    List<Statistical> findAllEmployeesByYearAndDepartment(Integer year, Department department, TrainingTypeEnum type);
}
