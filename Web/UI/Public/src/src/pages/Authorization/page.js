import {request} from '../../common/request.js';
import {Message, TimeSelect} from 'element-ui';
import SwitchBar from '../../components/Switch/page.vue'
import {mapState, mapActions} from 'vuex'
import Confirm from '../../components/Confirm/page.vue';

const tableData = [{
    type: 'split',
    title1: 'Standard Group'
}, {
    id: 1,
    title1: 'HR',
    title2: 'Performance',
    title3: 'Team',
    delegatePermission: false,
}, {
    id: 2,
    title1: 'Disciplinary',
    title2: 'Manager',
    title3: '',
    delegatePermission: true,
}, {
    id: 3,
    title1: 'Employee',
    title2: '',
    title3: '',
    delegatePermission: false,
},
    {
        type: 'split',
        title1: 'Special Group'
    },
    {
        id: 4,
        title1: 'Top Manager',
        title2: '',
        title3: '',
        delegatePermission: false,
    }, {
        id: 5,
        title1: 'New Employees',
        title2: 'In Probation',
        title3: '',
        delegatePermission: false,
    }]

export default {
    data() {
        return {
            currentIndex: null,
            keyWord: '',
            tableData: JSON.parse(JSON.stringify(tableData)),
            function_id: null,
            switchShow: true,
            isShowConfirm: false,
            titleTxt: 'Confirm',
            confirmTxt: ''
        }
    },
    components: {
        SwitchBar,
        Confirm
    },
    computed: {
        ...mapState({
            currentYear: state => state.allState.currentYear
        })
    },
    methods: {
        ...mapActions(['setYearList']),
        getRoleList() {
            request({
                url: 'roleList',
                method: 'get',
                params: {
                    _t: +new Date()
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;

                    return;
                }

                this.tableData = JSON.parse(JSON.stringify(tableData))

                if (res && res.list && res.list.length) {
                    this.tableData.forEach((v, i) => {
                        res.list.forEach((item, idx) => {
                            if (v.id === item.group_idx && item.group_idx && item.module_idx) {
                                v['check_' + v.id + '_' + item.module_idx] = item.check
                                v['edit_' + v.id + '_' + item.module_idx] = item.edit
                                v['download_' + v.id + '_' + item.module_idx] = item.download
                                v['delegation_' + v.id + '_' + item.module_idx] = item.delegation
                            }
                        })
                    })
                }

                this.switchShow = false
                this.$nextTick(() => {
                    this.switchShow = true
                })
            })
        },
        confirm() {
            this.isShowConfirm = false
        },
        setCurrentIndex(index) {
            this.currentIndex = index;
        },
        tableCalc({row, column, rowIndex, columnIndex}) {
            if (rowIndex === 0 || rowIndex === 4) {
                return 'split-row'
            } else {
                return [0, 0]
            }
        },
        setCell({row, column, rowIndex, columnIndex}) {
            if (rowIndex === 0 || rowIndex === 4) {
                if (columnIndex > 0) {
                    return {
                        border: '0 none',
                        background: '#F4F4F4'
                    }
                } else {
                    return {
                        lineHeight: '33px',
                        fontSize: '18px',
                        color: '#000',
                        fontWeight: 700,
                        border: '0 none',
                        background: '#F4F4F4'
                    }
                }
            } else {
                return {}
            }
        }
    },
    created() {
        let function_id = localStorage.getItem('function_id');
        if (function_id) {
            this.function_id = function_id;
            this.getRoleList();
        } else {
            if (this.currentYear) {
                this.function_id = this.currentYear.id
                localStorage.setItem('function_id', this.currentYear.id);
                this.getRoleList();
            } else {
                this.setYearList().then(res => {
                    this.function_id = res[0].id
                    localStorage.setItem('function_id', res[0].id);
                    this.getRoleList();
                })
            }
        }
    },
    mounted() {

    }
}
