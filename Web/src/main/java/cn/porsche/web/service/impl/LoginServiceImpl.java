package cn.porsche.web.service.impl;

import cn.porsche.auth.AccessToken;
import cn.porsche.common.util.CryptoUtils;
import cn.porsche.common.util.MD5Utils;
import cn.porsche.common.util.RandomUtils;
import cn.porsche.web.constant.LoginTypeEnum;
import cn.porsche.web.domain.Login;
import cn.porsche.web.domain.User;
import cn.porsche.web.repository.LoginRepository;
import cn.porsche.web.service.LoginService;
import cn.porsche.web.service.UserService;
import cn.porsche.web.util.HttpUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
@Log4j2
public class LoginServiceImpl implements LoginService {
    private final static int AUTH_LENGTH                 = 256;
    private final static int AUTH_START_INDEX            =   0;

    private final static int ENCRYPT_STRING_LENGTH       =  32;
    private final static int ENCRYPT_INDEX_MAX_LENGTH    =   2;

    private final static int KEY_LENGTH                  =  26;
    private final static int KEY_INDEX                   = AUTH_LENGTH - KEY_LENGTH - ENCRYPT_INDEX_MAX_LENGTH;


    @Autowired
    LoginRepository repository;

    @Autowired
    UserService userService;

    @Override
    public User getUserFromHeader() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String auth = request.getHeader(AUTH_NAME);
        if (null == auth || auth.length() != AUTH_LENGTH) {
            return null;
        }

        String des = decryptAuthentication(auth, null);
        if (null == des) {
            return null;
        }

        int idx = des.indexOf(AUTH_TOKEN);
        if (-1 == idx) {
            // 没有分隔标识
            return null;
        }

        int userId  = Integer.parseInt(des.substring(0, idx));
        if (0 == userId) {
            return null;
        }

        return userService.findById(userId);
    }

    @Override
    public User getUserFromCookie() {
        String auth = HttpUtils.getCookie(AUTH_NAME);
        if (null == auth || auth.length() != AUTH_LENGTH) {
            return null;
        }

        String des = decryptAuthentication(auth, null);
        if (null == des) {
            return null;
        }

        int idx = des.indexOf(AUTH_TOKEN);
        if (-1 == idx) {
            // 没有分隔标识
            return null;
        }

        int userId  = Integer.parseInt(des.substring(0, idx));
        if (0 == userId) {
            return null;
        }

        return userService.findById(userId);
    }


    @Override
    public Login loginByPassword(User user) {
        return null;
    }

    @Override
    public Login loginBySSO(User user, AccessToken token) {
        String encrpty = encryptAuthentication(
                user.getId().toString()
                        + AUTH_TOKEN
                        + RandomUtils.randomString(ENCRYPT_STRING_LENGTH - user.getId().toString().length() -1),
                null);

        Login login = Login.builder()
                .userId(user.getId())
                .accessToken(token.getAccessToken())
                .accessTokenExpireTime(token.getAccessTokenExpiresTime())
                .refreshToken(token.getRefreshToken())
                .refreshTokenExpireTime(token.getRefreshTokenExpiresTime())
                .ticket(token.getTicket())
                .lastUA(null)
                .lastDEV(null)
                .loginBy(LoginTypeEnum.SSO)
                .authId(encrpty)
                .Logout(false)
                .logoutTime(null)
                .build();

        return repository.save(login);
    }

    @Override
    public String encryptPassword(String salt, String password) {
        password = MD5Utils.md5(password).concat(salt).concat(AUTH_NAME);
        password = MD5Utils.md5(MD5Utils.md5(password));

        return password;
    }

    @Override
    public Login update(Login login) {
        return null;
    }

    private String encryptAuthentication(String encrypt, String other) {
        // 加密方式：
        //   +----------------++-------------++-----------------++-------------++-------------+
        //   |_Authentication_||__[(x+1)-y]__||_[(y+1)-(y+32))]_||_[100 - 126]_||_[107 - 128]_+
        //            |              |                |                 |              |_______ 127   -    120（x）
        //            |              |                |                 |______________________ 100   -    126 位加密公钥
        //            |              |                |________________________________________ (y+1) - (y+32) 32位md5字符串
        //            |              |_________________________________________________________ (x+1) -      y（随机字符串）
        //            |________________________________________________________________________ 0     -      x（Authentication）


        //  随机产生的匹配的公钥
        String key = RandomUtils.randomString(KEY_LENGTH);
        String ens = CryptoUtils.encrypt(encrypt, key.getBytes(), CryptoUtils.DES);
        String md5 = MD5Utils.md5(ens + key.toUpperCase());

        log.debug("Key:{}, Encrypt:{}, String:{}", key, ens, encrypt);
        log.debug("MD5:{}", md5);

        int len = key.length() + ens.length() + md5.length() + ENCRYPT_INDEX_MAX_LENGTH;
        String rnd = RandomUtils.randomString(AUTH_LENGTH - len);

        StringBuffer sb = new StringBuffer(AUTH_LENGTH);
        sb.append(ens).append(rnd).append(md5).append(key);

        return ens.length() < 10
                ? sb.append(0).append(ens.length()).toString()
                : sb.append(ens.length()).toString();
    }

    private String decryptAuthentication(String encrypt, String other) {
        //   +----------------++-------------++-----------------++-------------++-------------+
        //   |_Authentication_||__[(x+1)-y]__||_[(y+1)-(y+32))]_||_[100 - 126]_||_[107 - 128]_+
        //            |              |                |                 |              |_______ 127   -    120（x）
        //            |              |                |                 |______________________ 100   -    126 位加密公钥
        //            |              |                |________________________________________ (y+1) - (y+32) 32位md5字符串
        //            |              |_________________________________________________________ (x+1) -      y（随机字符串）
        //            |________________________________________________________________________ 0     -      x（Authentication）
        if (null == encrypt || encrypt.length() != AUTH_LENGTH) {
            return null;
        }

        // 最后面十位
        String key  = encrypt.substring(KEY_INDEX, KEY_INDEX + KEY_LENGTH);
        String ens  = encrypt.substring(AUTH_START_INDEX, Integer.parseInt(encrypt.substring(KEY_INDEX + KEY_LENGTH)));
        String des  = CryptoUtils.decrypt(encrypt.substring(AUTH_START_INDEX, Integer.parseInt(encrypt.substring(KEY_INDEX + KEY_LENGTH))), key.getBytes(), CryptoUtils.DES);
        log.debug("Key:{}, Encrypt:{}, String:{}", key, ens, des);

        if (null == des) {
            return null;
        }

        String md5 = encrypt.substring(KEY_INDEX -32, KEY_INDEX);
        log.debug("MD5:{}", md5);

        if (!md5.equals(MD5Utils.md5(ens + key.toUpperCase()))) {
            return null;
        }

        return des;
    }

    @Override
    public Login logout(User user) {
        Page<Login> list = repository.findByUserId(user.getId(), PageRequest.of(0, 1, Sort.by(Sort.Direction.DESC, "id")));
        if (!list.hasContent()) {
            return null;
        }

        Login login = list.iterator().next();
        if (login.isLogout()) {
            return login;
        }

        login.setLogout(true);
        login.setLogoutTime(new Date());

        return repository.save(login);
    }
}
