package cn.porsche.web.service.impl;

import cn.porsche.web.domain.*;
import cn.porsche.web.repository.DepartmentReportRepository;
import cn.porsche.web.service.DepartmentReportService;
import cn.porsche.web.service.TargetService;
import cn.porsche.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


@Service
public class DepartmentReportServiceImpl implements DepartmentReportService {
    @Autowired
    DepartmentReportRepository repository;

    @Autowired
    UserService userService;

    @Autowired
    TargetService targetService;


    @Override
    public DepartmentReport findByDepartment(Integer functionId, Integer departmentId) {
        return repository.findByFunctionIdAndDepartmentId(functionId, departmentId);
    }

    @Override
    public DepartmentReport initReporting(Function function, Department department) {
        DepartmentReport report = findByDepartment(function.getId(), department.getId());
        if (null != report) {
            return report;
        }

        User manager = null;
        if (department.getManagerId() != null) {
            manager = userService.findById(department.getManagerId());
        }


        List<Target> targets = targetService.findAllByDepartment(function, department);

        report = DepartmentReport.builder()
                .functionId(function.getId())
                .departmentId(department.getId())
                .departmentName(department.getName())
                .departmentManager(manager == null ? null : manager.getEnName())
                .total(targets.size())
                
                .notStartTS(0)
                .submittedTS(0)
                .reviewedTS(0)
                .confirmedTS(0)
                .notStartAPA(0)
                .submittedAPA(0)
                .reviewedAPA(0)
                .confirmedAPA(0)
                
                .preATotal(0)
                .preARating(0.0f)
                .preBTotal(0)
                .preBRating(0.0f)
                .preCTotal(0)
                .preCRating(0.0f)
                .preDTotal(0)
                .preDRating(0.0f)
                .preETotal(0)
                .preERating(0.0f)

                .finalATotal(0)
                .finalARating(0.0f)
                .finalBTotal(0)
                .finalBRating(0.0f)
                .finalCTotal(0)
                .finalCRating(0.0f)
                .finalDTotal(0)
                .finalDRating(0.0f)
                .finalETotal(0)
                .finalERating(0.0f)
                .build();

        return repository.save(report);
    }

    @Override
    public DepartmentReport refreshReporting(Function function, Department department) {
        List<Target> targets = targetService.findAllByDepartment(function, department);
        int noStartTS    = 0;
        int submittedTS  = 0;
        int reviewedTS   = 0;
        int confirmedTS  = 0;

        int noStartAPA   = 0;
        int submittedAPA = 0;
        int reviewedAPA  = 0;
        int confirmedAPA = 0;
        for (Target item : targets) {
            if (item.isTSEConfirm()) {
                confirmedTS ++;
            } else if (item.isTSDConfirm()) {
                reviewedTS ++;
            } else if (item.isTSESubmit()) {
                submittedTS ++;
            } else {
                noStartTS ++;
            }

            if (item.isAgreeScore()) {
                confirmedAPA ++;
            } else if (item.isAPADSubmit()) {
                reviewedAPA ++;
            } else if (item.isAPAESubmit()) {
                submittedAPA ++;
            } else {
                noStartAPA ++;
            }
        }

        DepartmentReport report = findByDepartment(function.getId(), department.getId());
        if (report == null) {
            return null;
        }

        report.setTotal(targets.size());
        report.setNotStartTS(noStartTS);
        report.setSubmittedTS(submittedTS);
        report.setReviewedTS(reviewedTS);
        report.setConfirmedTS(confirmedTS);

        report.setNotStartAPA(noStartAPA);
        report.setSubmittedAPA(submittedAPA);
        report.setReviewedAPA(reviewedAPA);
        report.setConfirmedAPA(confirmedAPA);

        report.setDepartmentName(department.getName());

        return repository.save(report);
    }


    @Override
    public DepartmentReport releaseFirstAPA(Function function, Department department) {
        DepartmentReport report = findByDepartment(function.getId(), department.getId());
        if (null == report) {
            report = initReporting(function, department);

            if (report == null) {
                return null;
            }
        }

        int aTotal  = 0;
        int bTotal  = 0;
        int cTotal  = 0;
        int dTotal  = 0;
        int eTotal  = 0;

        List<Target> targets = targetService.findAllByDepartment(function, department);

        for (Target target : targets) {
            if (target.getFirstScoreLevel() != null) {
                switch (target.getFirstScoreLevel()) {
                    case A:
                        aTotal ++;
                        break;
                    case B:
                        bTotal ++;
                        break;
                    case C:
                        cTotal ++;
                        break;
                    case D:
                        dTotal ++;
                        break;
                    case E:
                        eTotal ++;
                        break;
                }
            }
        }

        report.setTotal(targets.size());
        report.setPreATotal(aTotal);
        report.setPreBTotal(bTotal);
        report.setPreCTotal(cTotal);
        report.setPreDTotal(dTotal);
        report.setPreETotal(eTotal);

        BigDecimal bd100 = new BigDecimal(report.getTotal());

        BigDecimal bdA = new BigDecimal(aTotal);
        BigDecimal bdB = new BigDecimal(bTotal);
        BigDecimal bdC = new BigDecimal(cTotal);
        BigDecimal bdD = new BigDecimal(dTotal);
        BigDecimal bdE = new BigDecimal(eTotal);

        report.setPreARating(bdA.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());
        report.setPreBRating(bdB.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());
        report.setPreCRating(bdC.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());
        report.setPreDRating(bdD.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());
        report.setPreERating(bdE.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());

        return repository.save(report);
    }

    @Override
    public DepartmentReport releaseSecondAPA(Function function, Department department) {
        DepartmentReport report = findByDepartment(function.getId(), department.getId());
        if (null == report) {
            report = initReporting(function, department);

            if (report == null) {
                return null;
            }
        }


        int aTotal  = 0;
        int bTotal  = 0;
        int cTotal  = 0;
        int dTotal  = 0;
        int eTotal  = 0;

        List<Target> targets = targetService.findAllByDepartment(function, department);

        for (Target target : targets) {
            if (target.getSecondScoreLevel() != null) {
                switch (target.getSecondScoreLevel()) {
                    case A:
                        aTotal ++;
                        break;
                    case B:
                        bTotal ++;
                        break;
                    case C:
                        cTotal ++;
                        break;
                    case D:
                        dTotal ++;
                        break;
                    case E:
                        eTotal ++;
                        break;
                }
            }
        }

        report.setTotal(targets.size());
        report.setFinalATotal(aTotal);
        report.setFinalBTotal(bTotal);
        report.setFinalCTotal(cTotal);
        report.setFinalDTotal(dTotal);
        report.setFinalETotal(eTotal);

        BigDecimal bd100 = new BigDecimal(report.getTotal());

        BigDecimal bdA = new BigDecimal(aTotal);
        BigDecimal bdB = new BigDecimal(bTotal);
        BigDecimal bdC = new BigDecimal(cTotal);
        BigDecimal bdD = new BigDecimal(dTotal);
        BigDecimal bdE = new BigDecimal(eTotal);

        report.setFinalARating(bdA.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());
        report.setFinalBRating(bdB.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());
        report.setFinalCRating(bdC.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());
        report.setFinalDRating(bdD.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());
        report.setFinalERating(bdE.divide(bd100, 2, RoundingMode.HALF_UP).floatValue());

        return repository.save(report);
    }

    @Override
    public List<DepartmentReport> findAllByFunction(Function function) {
        return repository.findAllByFunctionId(function.getId());
    }
}
