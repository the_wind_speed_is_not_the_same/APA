package cn.porsche.web.service;


import cn.porsche.web.domain.*;

import java.util.List;
import java.util.Map;

public interface TargetWhatService {
    TargetWhat get(Integer id);
    TargetWhat update(TargetWhat what);
    void deleteByTarget(Target target);
    void clearRedDot(User user, TargetWhat what);
    TargetWhat deleteByTarget(TargetWhat what);
    TargetWhat update(Target target, TargetWhat what);
    TargetWhat update(TargetWhat targetWhat, List<TargetKPI> kpi);

    TargetWhat tsEConfirm(Target target, TargetWhat what);
    TargetWhat tsDConfirm(Target target, TargetWhat what);

    TargetWhat eAppraisal(Target target, TargetWhat what, float score, String comment);
    TargetWhat firstAppraisal(Target target, TargetWhat what, float score);
    TargetWhat secondAppraisal(Target target, TargetWhat what, float score);

    float totalEmployeeScore(List<TargetWhat> whats);
    float totalFirstScore(List<TargetWhat> whats);
    float totalSecondScore(List<TargetWhat> whats);

    List<TargetWhat> findByUser(Function function, User user);
    List<TargetWhat> findAllByTarget(Target target);

    Map<String, Map<String, String>> check(List<TargetWhat> whats);
}
