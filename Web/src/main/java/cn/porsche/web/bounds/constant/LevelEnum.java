package cn.porsche.web.bounds.constant;

public enum LevelEnum {
    TOP("TOP"),
    ABOVE("ABOVE"),
    AVERAGE("AVERAGE"),
    BLOW("BLOW"),
    ;

    private String level;

    LevelEnum(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }
    public LevelEnum setLevel(String level) {
        return from(level);
    }

    public static LevelEnum from(String level) {
        try {
            return LevelEnum.valueOf(level);
        } catch (IllegalArgumentException exception) {
            return null;
        }
    }
}
