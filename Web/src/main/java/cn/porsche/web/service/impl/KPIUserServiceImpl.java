package cn.porsche.web.service.impl;

import cn.porsche.web.domain.*;
import cn.porsche.web.repository.KPIUserRepository;
import cn.porsche.web.service.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Log4j2
@Service
public class KPIUserServiceImpl implements KPIUserService {
    @Autowired
    KPIUserRepository repository;


    @Autowired
    UserService userService;

    @Autowired
    TargetService targetService;

    @Autowired
    PositionService positionService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    GroupService groupService;

    @Override
    @Transactional
    public void deleteByTarget(Target target) {
        repository.deleteByTargetId(target.getId());
    }

    @Override
    public KPIUser findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public KPIUser update(KPIUser kpiUser) {
        return repository.save(kpiUser);
    }

    @Override
    public KPIUser builder(Function function, User user, User manager, Department department, Position position) {
        Target target = targetService.open(function, user);
        if (null == target) {
            return null;
        }

        boolean isUpdate = false;
        KPIUser kpiUser = findByFunction(function, user);
        if (kpiUser == null) {
            kpiUser = KPIUser.builder()
                    .employeeId(user.getId())
                    .functionId(function.getId())
                    .targetId(target.getId())
                    .isDelegator(false)
                    .delegatorId(manager == null ? null : manager.getId())
                    .delegator(manager == null ? null : manager.getEnName())
                    .code(user.getCode())
                    .cnName(user.getCnName())
                    .enName(user.getEnName())
                    .avatar(null)
                    .position(position == null ? null : position.getFunction())
                    .email(user.getEmail())
                    .telephone(user.getTel())
                    .build();

            isUpdate = true;
        }

        if (manager != null && !manager.getId().equals(kpiUser.getManagerId())) {
            kpiUser.setManagerId(manager.getId());
            kpiUser.setManager(manager.getEnName());

            isUpdate = true;
        }

        if (department != null && !department.getId().equals(kpiUser.getDepartmentId())) {
            kpiUser.setDepartmentId(department.getId());
            kpiUser.setDepartment(department.getName());

            isUpdate = true;
        }

        if (user.getHireDate() != null) {
            kpiUser.setHireDate(user.getHireDate());

            isUpdate = true;
        }

        // 员工是否是新员工
        if (user.getHireDate() != null) {
            Calendar instance = Calendar.getInstance();
            int year = instance.get(Calendar.YEAR);

            instance.clear();
            instance.setTime(user.getHireDate());

            boolean inProbation = groupService.inProbationGroup(user);

            log.debug("员工ID：{}, 有入职日期： {},是否在新员工组：{}, {}",  user.getId(), user.getHireDate(), inProbation);
            if (inProbation) {
                if (year != instance.get(Calendar.YEAR)) {
                    // 在新员工组，需要移出来
                    log.debug("从新员工中移出：{}, {}, {}", kpiUser.getDepartment(), kpiUser.getEnName(), user.getId());

                    groupService.removeFromProbationGroup(user, user);
                }
            } else if (year == instance.get(Calendar.YEAR)) {
                // 在当前年，却又不在新员工组，添加到新员工组中
                log.debug("新增未满一年员工：{}, {}, {}", kpiUser.getDepartment(), kpiUser.getEnName(), user.getId());

                groupService.moveToInProbationGroup(user, user);
            }
        }

        if (!isUpdate) return kpiUser;
        log.debug("新增年度考核员工：{}, {}, {}",  kpiUser.getDepartment(), kpiUser.getEnName(), user.getId());

        return repository.save(kpiUser);
    }

    @Override
    public List<KPIUser> builder(Function function, List<User> users) {
        List<KPIUser> kpiList = new ArrayList<>();
        for (User item : users) {
            // 将每个员工都添加到年度考抨系统中
            kpiList.add(builder(function, item,
                    null == item.getManagerId() ? null : userService.findById(item.getManagerId()),
                    null == item.getDepartmentId() ? null : departmentService.findById(item.getDepartmentId()),
                    null == item.getPositionId() ? null : positionService.findById(item.getPositionId())
            ));
        }

        return kpiList;
    }

    @Override
    public KPIUser findByTarget(Target target) {
        return repository.findByTargetId(target.getId());
    }

    @Override
    public KPIUser findByFunction(Function function, User user) {
        return repository.findByFunctionIdAndEmployeeId(function.getId(), user.getId());
    }

    @Override
    public List<KPIUser> findAllByManager(Function function, User manager) {
        return repository.findAllByFunctionIdAndManagerId(function.getId(), manager.getId());
    }

    @Override
    public List<KPIUser> findAllByDelegator(Function function, User delegator) {
        return repository.findAllByFunctionIdAndDelegatorId(function.getId(), delegator.getId());
    }

    @Override
    public List<KPIUser> findAllByDepartment(Function function, Department department) {
        return repository.findAllByFunctionIdAndDepartmentId(function.getId(), department.getId());
    }

    @Override
    public Page<KPIUser> findAllByFunction(Function function, int page, int size) {
        return repository.findAllByFunctionId(function.getId(), PageRequest.of(page -1, size));
    }

    @Override
    public Page<KPIUser> findAllByFunction(Function function, Group group, int page, int size) {
        return repository.findAllByFunctionIdAndGroupId(function.getId(), group.getId(), PageRequest.of(page -1, size));
    }

    @Override
    public Page<KPIUser> findAllByDelegator(Function function, User delegator, int page, int size) {
        return repository.findAllByFunctionIdAndDelegatorId(function.getId(), delegator.getId(), PageRequest.of(page -1, size));
    }

    @Override
    public Page<KPIUser> findAllByDepartment(Function function, Department department, int page, int size) {
        return repository.findAllByFunctionIdAndDepartmentId(function.getId(), department.getId(), PageRequest.of(page -1, size));
    }

    @Override
    public List<KPIUser> findAllByUser(User user) {
        return repository.findAllByEmployeeId(user.getId());
    }
}
