package cn.porsche.web.service;


import cn.porsche.web.constant.CommentTypeEnum;
import cn.porsche.web.domain.*;

import java.util.List;


public interface MessageService {
    Message findById(Integer id);


    Message tsReleaseNotice(Function function, User employee);

    Message addEmployeeOVComment(Function function, Target target, User employee, User delegator);
    Message addDelegatorOVComment(Function function, Target target, User delegator, User employee,CommentTypeEnum type);

    Message employeeSubmitOnTS(Function function, User employee, Target target);

    Message employeeModifyOnTS(Function function, User employee, Target target);

    Message employeeConfirmOnTS(Function function, User delegator, User employee, Target target);
    Message delegatorConfirmOnTS(Function function, User delegator, User employee, Target target);


    // 委托人确认员工的绩效的What
    Message delegatorRejectOnTS(Function function, User employee, Target target, TargetWhat what);

    // 员工自评完 APA 后提交的提示信息
    Message employeeSubmitAPA(Function function, User employee, Target target);

    // 经理释放完最终结果通知员工的提示信息
    Message noticeByReleaseFinalScore(Function function, User employee, Target target);

    Message noticeVPByFirstScore(Function function, User vp);


    Message readMessage(Message message, User user);
    List<Message> findUnReadMessages(User owner);
}
