import {request} from '../../../../common/request.js';
import {mapState, mapActions} from 'vuex'
import {downloadFile} from '../../../../common/utils.js';
import DashBoard from '../DashBoard2/page.vue';

export default {
    props: {
        query: {
            type: Object
        },
        function_id: {
            type: Number | String,
            required: true
        },
        department_code: {
            type: String,
            required: true
        }
    },
    data() {
        return {
            tableData: [],
            tableHeight: 412,
            currentRow: null,
            confirmTxt: '',
            titleTxt: 'Error',
            isShowConfirm: false,
        }
    },
    components: {
        DashBoard
    },
    watch: {
        'function_id': function (val) {
            localStorage.removeItem('reportingStatus')
            localStorage.removeItem('totalCount')
            localStorage.removeItem('currentData')
            this.getData()
        },
        'department_code': function (val) {
            localStorage.removeItem('reportingStatus')
            localStorage.removeItem('currentData')
            this.currentData = {}
            this.currentRow = null
            this.getData();
        }
    },
    methods: {
        ...mapActions(['setReportingDepartmentId', 'setReportingDepartment']),
        toEmployeeList(row) {
            setTimeout(() => {
                this.setReportingDepartmentId(row.department_id)
                this.setReportingDepartment(row.name)
                this.$emit('changeComponent', {page: 'child', currentRow: row})
            }, 300)
        },
        setClass({row, rowIndex}) {
            if (row.id % 5) {
                return 'hide-tow-column'
            }
        },
        confirm() {
            this.isShowConfirm = false
        },
        getData() {
            request({
                url: 'getReportingResult',
                method: 'get',
                params: {
                    code: this.department_code,
                    function_id: this.function_id
                },
                isShowLoading: true
            }).then((res) => {
                if (!res || res.code !== 0) {
                    this.confirmTxt = res.message;
                    this.titleTxt = 'Error';
                    this.isShowConfirm = true;

                    return;
                }

                let results = [];
                let rate_group = {};
                res.list.forEach((item, idx) => {
                    let rate_group_index = results.length;
                    let final_rate_group = {
                        'A': 1,
                        'B': 2,
                        'C': 3,
                        'D': 4,
                        'E': 4,
                    };
                    let pre_rate_group = {
                        'A': 1,
                        'B': 2,
                        'C': 3,
                        'D': 4,
                        'E': 5,
                    };

                    results.push({
                            id: idx * 5,
                            department_id: item.department_id,
                            name: item.department_name,
                            manager: item.manager,
                            level: 'A',
                            final_total: item.final_a_total,
                            final_rate: ~~(item.final_a_rating * 100),
                            pre_rate: ~~(item.pre_a_rating * 100),
                            pre_total: item.pre_a_total,
                            final_rate_group: final_rate_group,
                            pre_rate_group: pre_rate_group,
                        }, {
                            id: idx * 5 + 1,
                            name: item.department_name,
                            manager: item.manager,
                            level: 'B',
                            final_total: item.final_b_total,
                            final_rate: ~~(item.final_b_rating * 100),
                            pre_rate: ~~(item.pre_b_rating * 100),
                            pre_total: item.pre_b_total,
                            final_rate_group: final_rate_group,
                            pre_rate_group: pre_rate_group,
                        }, {
                            id: idx * 5 + 2,
                            name: item.department_name,
                            manager: item.manager,
                            level: 'C',
                            final_total: item.final_c_total,
                            final_rate: ~~(item.final_c_rating * 100),
                            pre_rate: ~~(item.pre_c_rating * 100),
                            pre_total: item.pre_c_total,
                            final_rate_group: final_rate_group,
                            pre_rate_group: pre_rate_group,
                        }, {
                            id: idx * 5 + 3,
                            name: item.department_name,
                            manager: item.manager,
                            level: 'D',
                            final_total: item.final_d_total,
                            final_rate: ~~(item.final_d_rating * 100),
                            pre_rate: ~~(item.pre_d_rating * 100),
                            pre_total: item.pre_d_total,
                            final_rate_group: final_rate_group,
                            pre_rate_group: pre_rate_group,
                        }, {
                            id: idx * 5 + 4,
                            name: item.department_name,
                            manager: item.manager,
                            level: 'E',
                            final_total: item.final_e_total,
                            final_rate: ~~(item.final_e_rating * 100),
                            pre_rate: ~~(item.pre_e_rating * 100),
                            pre_total: item.pre_e_total,
                            final_rate_group: final_rate_group,
                            pre_rate_group: pre_rate_group,
                        }
                    )

                    rate_group[rate_group_index] = {
                        final_rate_group: {
                            'A': item.final_a_total,
                            'B': item.final_b_total,
                            'C': item.final_c_total,
                            'D': item.final_d_total,
                            'E': item.final_e_total,
                        },
                        pre_rate_group: {
                            'A': item.pre_a_total,
                            'B': item.pre_b_total,
                            'C': item.pre_c_total,
                            'D': item.pre_d_total,
                            'E': item.pre_e_total,
                        },
                    }
                })

                this.tableData = results

                setTimeout(()=>{
                    this.initDashBoard(rate_group);
                })
            }, (err) => console.log(err))
        },
        exportAllFile() {
            request({
                url: 'downloadAPAReporting',
                method: 'download',
                params: {
                    function_id: this.function_id
                }
            }).then((res) => {
                if (!res || res.code !== 0) {
                    this.confirmTxt = res.message;
                    this.titleTxt = 'Error';
                    this.isShowConfirm = true;

                    return;
                }

                if (res && res.data) {
                    downloadFile(res.data)
                }
            })
        },
        arraySpanMethod({ row, column, rowIndex, columnIndex }) {
            if (columnIndex === 4||columnIndex === 7) {
               if (rowIndex % 5 === 0) {
                    return {
                        rowspan: 5,
                        colspan: 1
                    };
                } else {
                    return {
                        rowspan: 0,
                        colspan: 0
                    };
                }
            }
        },
        initDashBoard(data){
            for(let i in data){
                let v = data[i];
                this.tableData[i].pre_rate_group = v.pre_rate_group
                this.tableData[i].final_rate_group = v.final_rate_group
            }

        },
    },
    created() {
        // let result = localStorage.getItem('result')
        // if(result){
        //   this.tableData = JSON.parse(result)
        // }else{
        this.getData()
        // }
    },
    mounted() {
        // this.$nextTick(()=>{
        //   this.tableHeight = this.$refs.tableWrapper.offsetHeight
        // })
        if (this.query && this.query.query) {
            this.currentRow = this.query.query.currentRow
        }
    }
}
