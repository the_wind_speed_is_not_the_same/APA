package cn.porsche.compliance.service.impl;


import cn.porsche.compliance.domain.Position;
import cn.porsche.compliance.repository.PositionRepository;
import cn.porsche.compliance.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class PositionServiceImpl implements PositionService {
    @Autowired
    PositionRepository repository;

    @Override
    public Integer count() {
        return Integer.valueOf((int) repository.count());
    }

    @Override
    public Page<Position> getListByPage(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page -1, size));
    }

    @Override
    public Position update(Position job) {
        return repository.save(job);
    }

    @Override
    public Position findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Position findByParentId(Integer parentId) {
        return repository.findByParentId(parentId);
    }

    @Override
    public List<Position> subordinates(Position position) {
        List<Position> list = new ArrayList<>();

        List<Position> list0 = repository.findAllByParentId(position.getId());

        list.addAll(list0);
        for (Position item1 : list0) {
            List<Position> list1 = repository.findAllByParentId(item1.getId());

            list.addAll(list1);
            for (Position item2 : list1) {
                List<Position> list2 = repository.findAllByParentId(item2.getId());

                list.addAll(list2);
                for (Position item3 : list2) {
                    List<Position> list3 = repository.findAllByParentId(item3.getId());

                    list.addAll(list3);
                    for (Position item4 : list3) {
                        List<Position> list4 = repository.findAllByParentId(item4.getId());
                        list.addAll(list4);
                    }
                }
            }
        }

        return list;
    }
}