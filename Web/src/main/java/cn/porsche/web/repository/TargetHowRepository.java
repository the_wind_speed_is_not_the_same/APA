package cn.porsche.web.repository;

import cn.porsche.web.domain.TargetHow;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TargetHowRepository extends CrudRepository<TargetHow, Integer> {
    void deleteAllByTargetId(Integer targetId);

    TargetHow findByTargetId(Integer targetId);
}
