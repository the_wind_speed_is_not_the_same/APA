import {mapState} from 'vuex';
import {request} from '../../../../common/request.js';
import {deepClone, formatDate, formatDateTime} from '../../../../common/utils.js';
// import { Message } from 'element-ui';
import Mantle from '../../../../components/Mask/page.vue';
import Confirm from '../../../../components/Confirm/page.vue';
import th from "element-ui/src/locale/lang/th";

export default {
    props: ['func', 'tsState', 'targetInfo', 'isSaveBtnDisabled', 'tableData', 'targetDetail'],
    data() {
        return {
            formConst: null,
            form: null,
            isShowKpiBox: false,
            comments: [],
            isShowConfirm: false,
            confirmTxt: '',
            titleTxt: 'Warning',
            isTargetNameRed: false,
            isDescriptionRed: false,
            isKPIRed: false,
            isWeightRed: false,
            isDateRed: false,
            showTime: 0,
            confirmType: '',
            isEdit: false,
            finish_date: '',
            isClickType: null,
            isShowButtonLeft: false
        }
    },
    watch: {
        targetInfo: {
            handler(n, o) {
                this.initData(n);
            },
            deep: true
        },
        form: {
            handler(n, o) {
                // console.log(n, o);
                // console.log(JSON.stringify(n) == JSON.stringify(this.formConst));
                this.isEdit = !(JSON.stringify(n) == JSON.stringify(this.formConst));
            },
            deep: true
        }
    },
    components: {
        Mantle,
        Confirm
    },
    computed: {
        ...mapState({
            currentYear: state => state.allState.currentYear
        }),
        pickerOptions() {
            let {year} = this.currentYear
            return {
                disabledDate: (time) => {
                    return (time.getTime() > new Date(year, 11, 31).getTime()) || (time.getTime() < new Date(year, 0, 1).getTime())
                }
            }
        },
        isApa: state => state.allState.isApa,
        colorBlack() {
            let result = this.form.kpis.filter(item => {
                if (item.desc) return item;
            })

            if (result.length) {
                return true;
            }

            return false;
        },
        isBtnDisabled() {
            // console.log("disabled", this.inputDisabled, this.isEdit);
            // 未修改 并且可以编辑
            return !this.isEdit;
        },
        inputDisabled() {
            // apa阶段开启，已确定的ts不能再修改
            return this.func['is_apa'] && this.targetDetail['is_ts_employee_confirm']&& this.targetDetail['is_ts_delegator_confirm'];
        }
    },
    methods: {
        validateForm() {
            return new Promise((resolve, reject) => {
                let validate = true;
                let form = this.form;

                if (!form.name) {
                    this.isTargetNameRed = true;
                    validate = false;
                } else {
                    this.isTargetNameRed = false;
                }

                if (!form.desc) {
                    this.isDescriptionRed = true;
                    validate = false;
                } else {
                    this.isDescriptionRed = false;
                }

                if (!(form.kpis[0].desc && form.kpis[1].desc && form.kpis[2].desc)) {
                    this.isKPIRed = true;
                    validate = false;
                } else {
                    this.isKPIRed = false;
                }

                if (!form.weight) {
                    this.isWeightRed = true;
                    validate = false;
                } else {
                    this.isWeightRed = false;
                }

                if (!form.finish_date) {
                    this.isDateRed = true;
                    validate = false;
                } else {
                    this.isDateRed = false;
                }

                if (validate) {
                    resolve();
                } else {
                    this.confirmTxt = 'Please fill in the part marked with a red background';
                    this.titleTxt = 'Warning';
                    this.isShowConfirm = true;
                    this.isShowButtonLeft = false;

                    this.confirmType = 'comment';
                    reject();
                }
            })
        },
        closeComponent() {
            this.$emit('closeComponent');
        },
        showKPIUI() {
            this.isShowKpiBox = true;
        },
        closeBox() {
            this.isShowKpiBox = false;
        },
        saveAdd() {
            this.validateForm().then(res => {
                let totalWeight = 0;

                this.form.finish_date = formatDate(this.form.finish_date);
                if (this.form.ts_comments) {
                    delete this.form.ts_comments;
                }

                if (this.form.apa_comments) {
                    delete this.form.apa_comments;
                }

                // 判断是否权重超之和100
                if (this.tsState == 'add') {
                    if (this.tableData && this.tableData.length) {
                        this.tableData.forEach((item) => {
                            totalWeight += item.weight * 1
                        })
                    }

                    totalWeight += this.form.weight * 1
                } else {
                    if (this.tableData && this.tableData.length) {
                        this.tableData.forEach((item) => {
                            // 排除本身的数据
                            if (item.what_id != this.form.what_id) {
                                console.log(item.weight, 2)
                                totalWeight += item.weight * 1
                            }
                        })
                    }

                    totalWeight += this.form.weight * 1
                }

                if (totalWeight > 100) {
                    this.isShowConfirm = true;
                    this.isShowButtonLeft = false
                    this.titleTxt = 'Warning';
                    this.confirmTxt = 'The sum of weights cannot exceed 100.';
                    this.showTime = 0;
                    return false
                }

                this.form['function_id'] = this.func['id'];

                request({
                    url: 'updateWhat',
                    data: this.form,
                    isShowLoading: true,
                }).then(res => {
                    if (!res || res.code !== 0) {
                        this.titleTxt = 'Error';
                        this.confirmTxt = res.message ? res.message : 'Save Failed, Please try again.';
                        this.isShowConfirm = false;
                        this.isShowButtonLeft = false;

                        return;
                    }


                    this.isEdit = false;
                    this.formConst = deepClone(this.form);
                    this.isShowConfirm = true;
                    this.isShowButtonLeft = false
                    this.confirmType = 'save';
                    this.titleTxt = 'Success';
                    this.confirmTxt = 'Saved successfully';
                    this.showTime = 1000;

                    this.$emit('employeeSave')
                });
            });
        },
        selfConfirm() {
            if (this.form.isDelegatorConfirm && !this.form.isEmployeeConfirm) {
                request({
                    url: 'confirmByDelegator',
                    method: 'get',
                    params: {
                        // comment: this.comment,
                        what_id: this.form.what_id
                    },
                    isShowLoading: true
                }).then(res => {
                    if (res) {
                        this.isShowConfirm = true;
                        this.isShowButtonLeft = false
                        this.titleTxt = 'Success';
                        this.confirmTxt = 'Confirm successfully';
                        this.formConst.isEmployeeConfirm = true;
                        this.form.isEmployeeConfirm = true;
                        this.showTime = 0
                    } else {
                        this.isShowConfirm = true;
                        this.isShowButtonLeft = false
                        this.titleTxt = 'Error';
                        this.confirmTxt = 'Confirm Failed, Please try again';
                        this.showTime = 0
                    }
                })
            }
        },
        callBack() {
            if (this.confirmType === 'save') {
                this.$emit('refreshList');
                if (this.tsState === 'add') {
                    this.initData();
                }
            }
            this.confirmType = '';
        },
        confirm() {
            if (this.isClickType === 'prev') {
                this.$emit('previousIndex');
            } else if (this.isClickType === 'next') {
                this.$emit('nextIndex');
            }
            this.isShowConfirm = false;
        },
        cancel() {
            this.isShowConfirm = false;
        },
        previous() {
            if (this.isEdit) {
                this.titleTxt = 'Warning'
                this.confirmTxt = 'There are some changes not saved，are you sure to leave？?'
                this.isShowConfirm = true
                this.showTime = 0
                this.isClickType = 'prev'
                this.isShowButtonLeft = true
                return false
            }
            this.$emit('previousIndex');
        },
        next() {
            if (this.isEdit) {
                this.titleTxt = 'Warning'
                this.confirmTxt = 'There are some changes not saved，are you sure to leave？'
                this.isShowConfirm = true
                this.showTime = 0
                this.isClickType = 'next'
                this.isShowButtonLeft = true
                return false
            }
            this.$emit('nextIndex');
        },
        initData(targetInfo) {
            let function_id = localStorage.getItem('function_id');
            this.isDescriptionRed   = false;
            this.isTargetNameRed    = false;
            this.isKPIRed           = false;
            this.isWeightRed        = false;
            this.isDateRed          = false;
            let oDate = new Date();

            if (this.tsState === 'add') {
                this.form = {
                    finish_date: this.currentYear ? new Date(this.currentYear.year, 11, 31) : new Date(oDate.getFullYear(), 11, 31),
                    desc: '',
                    function_id: function_id,
                    kpis: [{
                        desc: '',
                        index: 4,
                    }, {
                        desc: '',
                        index: 3,
                    }, {
                        desc: '',
                        index: 2,
                    }, {
                        desc: '',
                        index: 1,
                    }, {
                        desc: '',
                        index: 0,
                    }],
                    name: '',
                    what_id: '',
                    weight: '',
                    isEmployeeConfirm: false,
                    isDelegatorConfirm: false,
                };

                this.comments = [];
            } else if (this.tsState === 'edit') {
                let form = deepClone(targetInfo);
                if (!form.kpis.length) {
                    form.kpis = [{
                        desc: '',
                        index: 4,
                    }, {
                        desc: '',
                        index: 3,
                    }, {
                        desc: '',
                        index: 2,
                    }, {
                        desc: '',
                        index: 1,
                    }, {
                        desc: '',
                        index: 0,
                    }];
                }
                ;

                if (form.ts_comments && form.ts_comments.length) {
                    form.ts_comments.forEach((item, idx) => {
                        form.ts_comments[idx].send_time = formatDateTime(item.send_time);
                    });
                }

                form.isEmployeeConfirm = form.is_employee_confirm;
                form.isDelegatorConfirm = form.is_delegator_confirm;

                delete form.is_employee_confirm;
                delete form.is_delegator_confirm;

                this.comments = form.ts_comments;
                this.form = form;
                this.formConst = deepClone(form);
            }
        }
    },
    mounted() {
        this.initData(this.targetInfo);
    }
}
