package cn.porsche.web.service;


import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.GroupUser;
import cn.porsche.web.domain.User;

import java.util.List;

public interface GroupUserService {
    void delete(GroupUser groupUser);
    GroupUser update(GroupUser groupUser);

    GroupUser add(Group group, User user);

    GroupUser add(Group group, User user, User hands);

    // 返回用户所拥有的组
    List<GroupUser> findByUser(User user);

    List<User> findAllByGroup(Group group);

    // 根据组信息和员工信息，查找对应的组
    GroupUser findByGroupAndUser(Group group, User user);

    Boolean isExist(Group group, User user);
    void remove(Group group, User user);
}
