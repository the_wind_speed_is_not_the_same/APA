package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "kpi_users", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class KPIUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'", updatable = false)
    private Integer id;

    @ApiModelProperty(notes = "员工主键")
    @JsonProperty("employee_id")
    @Column(name = "employee_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT '员工主键'", updatable = false)
    private Integer employeeId;

    @ApiModelProperty(notes = "阶段主键")
    @JsonProperty("function_id")
    @Column(name = "function_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT '阶段主键'", updatable = false)
    private Integer functionId;

    @ApiModelProperty(notes = "当前的KPI类主键")
    @JsonProperty("target_id")
    @Column(name = "target_id",                 columnDefinition = "INT UNSIGNED NOT NULL COMMENT '当前的KPI类主键'", updatable = false)
    private Integer targetId;

    @ApiModelProperty(notes = "经理主键")
    @JsonProperty("manager_id")
    @Column(name = "manager_id",                columnDefinition = "INT UNSIGNED NULL COMMENT '直线经理主键'")
    private Integer managerId;

    @ApiModelProperty(notes = "委托人主键")
    @JsonProperty("delegator_id")
    @Column(name = "delegator_id",              columnDefinition = "INT UNSIGNED NULL COMMENT '委托人主键'")
    private Integer delegatorId;

    @ApiModelProperty(notes = "部门主键")
    @JsonProperty("department_id")
    @Column(name = "department_id",             columnDefinition = "INT UNSIGNED NULL COMMENT '部门主键'")
    private Integer departmentId;

    @ApiModelProperty(notes = "是否为小组长")
    @JsonProperty("is_delegator")
    @Column(name = "is_delegator",              columnDefinition = "TINYINT(1) UNSIGNED NULL COMMENT '是否为委托人'")
    private Boolean isDelegator;

    @ApiModelProperty(notes = "外部系统中的员工代码")
    @JsonProperty("code")
    @Column(name = "code",                      columnDefinition = "CHAR(32) NOT NULL COMMENT '外部系统中的员工代码'")
    private String code;

    @ApiModelProperty(notes = "英文名称")
    @JsonProperty("en_name")
    @Column(name = "en_name",                   columnDefinition = "VARCHAR(1024) NULL COMMENT '英文名称'")
    private String enName;

    @ApiModelProperty(notes = "中文名称")
    @JsonProperty("cn_name")
    @Column(name = "cn_name",                   columnDefinition = "VARCHAR(1023) NULL COMMENT '中文名称'")
    private String cnName;

    @ApiModelProperty(notes = "中文名称")
    @JsonProperty("avatar")
    @Column(name = "avatar",                    columnDefinition = "VARCHAR(1023) NULL COMMENT '头像'")
    private String avatar;

    @ApiModelProperty(notes = "部门名称")
    @JsonProperty("department")
    @Column(name = "department",                columnDefinition = "VARCHAR(1024) NULL COMMENT '部门名称'")
    private String department;

    @ApiModelProperty(notes = "小组长名称")
    @JsonProperty("delegator")
    @Column(name = "delegator",                 columnDefinition = "VARCHAR(1024) NULL COMMENT '委托人'")
    private String delegator;

    @ApiModelProperty(notes = "经理名称")
    @JsonProperty("manager")
    @Column(name = "manager",                   columnDefinition = "VARCHAR(1024) NULL COMMENT '委托人'")
    private String manager;

    @ApiModelProperty(notes = "职位信息")
    @JsonProperty("position")
    @Column(name = "position",                  columnDefinition = "VARCHAR(1024) NULL COMMENT '职位信息'")
    private String position;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(notes = "入职时间")
    @JsonProperty("hire_date")
    @Column(name = "hire_date",                 columnDefinition = "DATE NULL COMMENT '用户 APA 结束时间'")
    private Date hireDate;

    @ApiModelProperty(notes = "邮件地址")
    @JsonProperty("email")
    @Column(name = "email",                     columnDefinition = "VARCHAR(254) NULL COMMENT '邮件地址'")
    private String email;

    @ApiModelProperty(notes = "电话号码")
    @JsonProperty("telephone")
    @Column(name = "telephone",                 columnDefinition = "VARCHAR(254) NULL COMMENT '电话号码'")
    private String telephone;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    private Date createAt;
}
