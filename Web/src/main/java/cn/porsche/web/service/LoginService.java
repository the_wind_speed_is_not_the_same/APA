package cn.porsche.web.service;


import cn.porsche.auth.AccessToken;
import cn.porsche.web.domain.Login;
import cn.porsche.web.domain.User;

public interface LoginService {
    Integer EXPIRE_DATES        = 3;
    String AUTH_NAME            = "Authorization";
    char AUTH_TOKEN             = '|';

    User getUserFromHeader();
    User getUserFromCookie();
    Login loginByPassword(User user);
    Login loginBySSO(User user, AccessToken token);

    String encryptPassword(String salt, String password);

    Login update(Login login);

    Login logout(User user);
}
