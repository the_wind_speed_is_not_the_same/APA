package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @ApiModelProperty(notes = "主键")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @ApiModelProperty(notes = "职位主键")
    @JsonProperty("position_id")
    @Column(name = "position_id",               columnDefinition = "INT UNSIGNED NULL COMMENT '职位描述'")
    Integer positionId;

    @JsonIgnore
    @ApiModelProperty(notes = "部门主键")
    @JsonProperty("department_id")
    @Column(name = "department_id",             columnDefinition = "INT UNSIGNED NULL DEFAULT NULL COMMENT '部门主键'")
    Integer departmentId;

    @JsonIgnore
    @ApiModelProperty(notes = "上级主管主键")
    @Column(name = "manager_id",                columnDefinition = "INT UNSIGNED NULL DEFAULT NULL COMMENT '上级主管主键'")
    Integer managerId;

    @ApiModelProperty(notes = "外部系统中的员工代码")
    @JsonProperty("code")
    @Column(name = "code",                      columnDefinition = "CHAR(32) NOT NULL COMMENT '外部系统中的员工代码'")
    String code;

    @ApiModelProperty(notes = "用户头像")
    @JsonProperty("avatar")
    @Column(name = "avatar",                    columnDefinition = "VARCHAR(1023) NULL DEFAULT NULL COMMENT '用户头像'")
    String avatar;

    @ApiModelProperty(notes = "姓")
    @JsonProperty("first_name")
    @Column(name = "first_name",                columnDefinition = "VARCHAR(255) NOT NULL COMMENT '姓'")
    String firstName;

    @ApiModelProperty(notes = "名")
    @JsonProperty("last_name")
    @Column(name = "last_name",                 columnDefinition = "VARCHAR(255) NOT NULL COMMENT '名'")
    String lastName;

    @ApiModelProperty(notes = "中文名称")
    @JsonProperty("cn_name")
    @Column(name = "cn_name",                   columnDefinition = "VARCHAR(255) NOT NULL COMMENT '中文名称'")
    String cnName;

    @ApiModelProperty(notes = "英文名称")
    @JsonProperty("en_name")
    @Column(name = "en_name",                   columnDefinition = "VARCHAR(255) NOT NULL COMMENT '英文名称'")
    String enName;

    @ApiModelProperty(notes = "用户职称")
    @JsonProperty("title")
    @Column(name = "title",                     columnDefinition = "VARCHAR(255) NULL COMMENT '用户职称'")
    String title;

    @JsonIgnore
    @ApiModelProperty(notes = "用户邮箱")
    @Column(name = "email",                     columnDefinition = "VARCHAR(255) NULL COMMENT '用户邮箱'")
    String email;

    @JsonIgnore
    @ApiModelProperty(notes = "手机号码")
    @Column(name = "mobile",                    columnDefinition = "VARCHAR(255) NULL COMMENT '手机号码'")
    String mobile;

    @JsonIgnore
    @ApiModelProperty(notes = "电话号码")
    @Column(name = "tel",                       columnDefinition = "VARCHAR(255) NULL COMMENT '电话号码'")
    String tel;

    @ApiModelProperty(notes = "用户入职日期")
    @JsonProperty("hire_date")
    @Column(name = "hire_date",                 columnDefinition = "DATE NULL DEFAULT NULL COMMENT '用户入职日期'")
    Date hireDate;

    @JsonIgnore
    @ApiModelProperty(notes = "用户是否需要进行 KPI 考核。有以下情况不需要纳入 KPI考核：高管、未满一年的新人")
    @Column(name = "need_kpi",                  columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户是否需要进行 KPI 考核。有以下情况不需要纳入 KPI考核：高管、未满一年的新人'")
    Boolean needKPI;

    @JsonIgnore
    @ApiModelProperty(notes = "用户是否手动输入KPI")
    @Column(name = "need_hands",                columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户是否手动输入KPI'")
    Boolean needHands;

    @JsonIgnore
    @ApiModelProperty(notes = "数据状态")
    @Column(name = "status",                    columnDefinition = "SMALLINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '数据状态'", insertable = false)
    Integer status;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
