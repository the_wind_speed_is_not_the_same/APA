package cn.porsche.web.service.impl;


import cn.porsche.web.domain.*;
import cn.porsche.web.repository.TargetWhatRepository;
import cn.porsche.web.service.TargetWhatService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Log4j2
public class TargetWhatServiceImpl implements TargetWhatService {
    @Autowired
    TargetWhatRepository repository;


    @Override
    public TargetWhat get(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public TargetWhat update(TargetWhat what) {
        return repository.save(what);
    }

    @Override
    @Transactional
    public void deleteByTarget(Target target) {
        repository.deleteByTargetId(target.getId());
    }

    @Override
    public void clearRedDot(User user, TargetWhat what) {
        if (what.getEmployeeId().equals(user.getId())) {
            // 员工读取信息
            what.setEmployeeRed(false);
        } else {
            what.setDelegatorRed(false);
        }

        update(what);
    }

    @Override
    public TargetWhat deleteByTarget(TargetWhat what) {
        repository.delete(what);

        return what;
    }

    @Override
    public TargetWhat update(Target target, TargetWhat what) {
        what.setTargetId(target.getId());
        what.setFunctionId(target.getFunctionId());

        return update(what);
    }

    @Override
    public TargetWhat update(TargetWhat targetWhat, List<TargetKPI> kpis) {
        if (null == targetWhat || null == targetWhat.getId()) {
            return null;
        }

        for (TargetKPI kpi : kpis) {
            // index
            if (kpi.getIndex() > 1) {

            }
        }

        return null;
    }

    @Override
    public TargetWhat tsEConfirm(Target target, TargetWhat what) {
        if (what.isTSEConfirm()) {
            return what;
        }

        what.setTSEConfirm(true);

        return update(what);
    }

    @Override
    public TargetWhat tsDConfirm(Target target, TargetWhat what) {
        // 先经理Confirm后，员工才能Confirm
        what.setTSDConfirm(true);
        what.setTSEConfirm(false);

        // 将之前经理的拒绝信息设置为空，同时将对应的消息置为已读
        what.setEmployeeRed(false);

        return update(what);
    }


    @Override
    public TargetWhat eAppraisal(Target target, TargetWhat what, float score, String comment) {
        what.setEmployeeScore(score);
        what.setEmployeeComment(comment);

        return repository.save(what);
    }

    @Override
    public TargetWhat firstAppraisal(Target target, TargetWhat what, float score) {
        what.setFirstScore(score);
        what.setEmployeeRed(true);

        return repository.save(what);
    }

    @Override
    public TargetWhat secondAppraisal(Target target, TargetWhat what, float score) {
        what.setSecondScore(score);
        what.setEmployeeRed(true);


        return repository.save(what);
    }



    @Override
    public List<TargetWhat> findByUser(Function function, User user) {
        return repository.findAllByFunctionIdAndEmployeeId(function.getId(), user.getId());
    }

    @Override
    public List<TargetWhat> findAllByTarget(Target target) {
        return repository.findAllByTargetIdOrderByIdAsc(target.getId());
    }

    @Override
    public Map<String, Map<String, String>> check(List<TargetWhat> whats) {
        Map<String, Map<String, String>> notices = new HashMap<>();
        if (null == whats || 0 == whats.size()) {
            notices.put("whats", null);

            return notices;
        }

        int weight = 0;
        for (TargetWhat what : whats) {
            Map<String, String> fields = new HashMap<>();
            if (StringUtils.isEmpty(what.getName())) {
                fields.put("name", "绩效名称未填写");
            }

            if (what.getDesc() == null) {
                fields.put("desc", "描述未填写");
            }

            if (what.getWeight() == null || what.getWeight() == 0) {
                fields.put("weight", "权重未填写");
            }

            if (what.getFinishDate() == null) {
                fields.put("finish_date", "结束时间未填写");
            }

            if (what.getWeight() != null) {
                weight += what.getWeight();
            }

            if (!fields.isEmpty()) {
                notices.put("what:" + what.getId(), fields);
            }
        }

        if (weight != 100) {
            // 权重不中100
            notices.put("percent", null);
        }

        return notices;
    }


    @Override
    public float totalEmployeeScore(List<TargetWhat> whats) {
        float score = 0f;
        for (TargetWhat what : whats) {
            score += what.getEmployeeScore() * Double.valueOf(what.getWeight());
        }

        return score;
    }

    @Override
    public float totalFirstScore(List<TargetWhat> whats) {
        float score = 0f;
        for (TargetWhat what : whats) {
            score += what.getFirstScore() * Double.valueOf(what.getWeight());
        }

        return score;
    }

    @Override
    public float totalSecondScore(List<TargetWhat> whats) {
        float score = 0f;
        for (TargetWhat what : whats) {
            score += what.getSecondScore() * Double.valueOf(what.getWeight());
        }

        return score;
    }
}
