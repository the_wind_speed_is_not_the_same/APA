package cn.porsche.compliance.service.impl;


import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.Question;
import cn.porsche.compliance.emnu.SeasonEnum;
import cn.porsche.compliance.repository.QuestionRepository;
import cn.porsche.compliance.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    QuestionRepository repository;

    @Override
    public Question insert(Question question) {
        return repository.save(question);
    }

    @Override
    public List<Question> findAllByYear(Integer year) {
        return repository.findAllByYear(year);
    }


    public List<Question> findListByYearAndSeasonAndEmployeeType(Integer year, SeasonEnum season, EmployeeTypeEnum type) {
        return repository.findAllByYearAndSeasonAndType(year, season, type);
    }

    @Override
    public void removeByYearAndSeasonAndEmployeeType(Integer year, SeasonEnum season, EmployeeTypeEnum type) {
        for (Question item : findListByYearAndSeasonAndEmployeeType(year, season, type)) {
            repository.delete(item);
        }
    }
}