package cn.porsche.web.repository;

import cn.porsche.web.domain.GroupRole;
import cn.porsche.web.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRoleRepository extends CrudRepository<GroupRole, Integer> {
    GroupRole findByGroupIdAndModule(Integer groupId, String module);

    List<GroupRole> findAllByGroupId(Integer groupId);

}
