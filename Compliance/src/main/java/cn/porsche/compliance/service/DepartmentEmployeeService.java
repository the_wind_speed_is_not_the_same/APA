package cn.porsche.compliance.service;


import cn.porsche.compliance.domain.DepartmentEmployee;
import cn.porsche.compliance.domain.User;

public interface DepartmentEmployeeService {
    DepartmentEmployee update(DepartmentEmployee data);

    DepartmentEmployee findById(Integer id);
    DepartmentEmployee findByEmployee(User employee);
}