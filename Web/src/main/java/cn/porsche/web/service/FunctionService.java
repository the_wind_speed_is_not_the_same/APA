package cn.porsche.web.service;


import cn.porsche.web.domain.Function;
import cn.porsche.web.domain.User;
import cn.porsche.web.exceptions.FunctionException;

import java.util.List;

public interface FunctionService {
    Function update(Function function);

    Function findById(Integer id);
    Function findByYear(Integer year);

    Function getLastFunction();
    Function getLastOpeningFunction();

    // 开启TargetSetting
    Function releaseTS(Integer year, User user) throws FunctionException;

    Function reOpenTS(Function function, User user) throws FunctionException;

    // 设置KickOff邮件内容
    Function sendTSEmail(User user, Function function, String subject, String content) throws FunctionException ;


    // 开启TargetSetting
    Function releaseAPA(Integer year, User user) throws FunctionException;

    // 设置KickOff邮件内容
    Function sendAPAEmail(User user, Function function, String subject, String content) throws FunctionException ;

    List<Function> available();

}