package cn.porsche.compliance.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.Position;
import cn.porsche.compliance.domain.Risk;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.service.*;
import io.swagger.annotations.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
@ApiModel(value = "/user", description = "员工相关的操作类")
public class UserController extends BaseController {

    @Autowired
    PositionService positionService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    RiskService riskService;

    @Autowired
    UserViewService userViewService;

    @Autowired
    TrainingService trainingService;

    @Autowired
    StatisticalService reportService;

    @ApiOperation(
            value = "获取当前员工的个人信息",
            notes = "只显示当前员工的个人信息"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/me")
    public String me() {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }
        Map<String, Object> data = new HashMap<>();

        // 用户信息
        data.put("user", user);
        // 消息通知
        data.put("notice", userService.findById(user.getManagerId()));
        // 审批者
        data.put("delegator", userService.findById(user.getManagerId()));
        // 职位信息
        data.put("position", positionService.findById(user.getPositionId()));
        // 部门信息
        data.put("department", departmentService.findById(user.getDepartmentId()));

        return ApiResult.builder().data(data).toJson();
    }

    @ApiOperation(
            value = "设置整个系统的VP用户",
            notes = "设置整个系统的VP用户"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @PostMapping(value = "/vps")
    public String vps(
            @RequestParam("content") String content
    ) {
        if (noPromise("Human Resources")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (StringUtils.isEmpty(content)) {
            return ApiResult.isBad("内容为空，设置失败");
        }

        String[] codes = content.split("\n");
        for (int i=0; i<codes.length; ++i) {
            if (null == ( user = userService.findByCode(StringUtils.trim(codes[i])) )) {
                continue;
            }

            if (user.getType().equals(EmployeeTypeEnum.VP)) {
                continue;
            }

            user.setType(EmployeeTypeEnum.VP);
            userService.update(user);
        }

        int year = Calendar.getInstance().get(Calendar.YEAR);
        for (int i=0; i<codes.length; ++i) {
            if (null == ( user = userService.findByCode(StringUtils.trim(codes[i])) )) {
                continue;
            }

            trainingService.resetTrainingByQuestion(year, user);
            reportService.refreshQuestion(user);
        }

        // 由于之前设置过 VP，需要将不是 VP 的用户设置为普通用户
        Page<User> pageable = userService.findAllByTypeAndPage(EmployeeTypeEnum.VP, 1, 500);
        List<String> oldVPs = pageable.stream().map(User::getCode).collect(Collectors.toList());
        Collection<String> noVPs = CollectionUtils.subtract(Arrays.asList(codes), oldVPs);

        for (String code : noVPs) {
            if (null == ( user = userService.findByCode(StringUtils.trim(code)) )) {
                continue;
            }

            trainingService.resetTrainingByQuestion(year, user);
            reportService.refreshQuestion(user);
        }

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "下属的风险列表",
            notes = "只看下级的风险信息，自己不可看自己的风险数据"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/risks")
    public String risks() {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        Map<String, Object> json = new HashMap<>();

        List<Risk> risks = new ArrayList<>();
        json.put("risks", risks);
        if (null == user.getPositionId()) {
            // 用户没有职位信息
            return ApiResult.builder().data(json).toJson();
        }

        Position position = positionService.findById(user.getPositionId());
        if (null == position) {
            // 用户没有职位信息
            return ApiResult.builder().data(json).toJson();
        }

        // 当前用户添加到返回结果
        json.put("user", userViewService.findById(user.getId()));
        json.put("risk", riskService.findByUser(user));

        User subUser;
        Risk subRisk;
        List<Position> subordinates = positionService.subordinates(position);
        for (Position item : subordinates) {
            if (null != ( subUser = userService.findByPositionId(item.getId()) )) {
                if (null != ( subRisk = riskService.findByUser(subUser)) ) {
                    risks.add(subRisk);
                }
            }
        }

        return ApiResult.builder().data(json).toJson();
    }
}
