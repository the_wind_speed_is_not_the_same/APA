package cn.porsche.web.service;


import cn.porsche.web.domain.Department;
import cn.porsche.web.domain.DepartmentTree;

import java.util.List;

public interface DepartmentService {
    Department findById(Integer id);
    Department findByName(String name);

    Department update(Department department);

    Department findByCode(String code);


    Department findTopDepartment();
    Department findTopDepartment(String code);
    List<Department> findSubDepartments(Department department);
    List<Department> findAllDepartments();
    List<Department> findTopDepartments();

    DepartmentTree buildTree(Department department);
}