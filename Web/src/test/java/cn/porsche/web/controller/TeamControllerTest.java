package cn.porsche.web.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.service.LoginService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class TeamControllerTest extends ApplicationTest {
    private static final String API = "/team";

    private String token;

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    private void employeeByDelegator(String order) {
        final String api = API + "/employee_by_delegator";

        if (order == null) {
            order = "name";
        }

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("order", order)
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void list() {
        employeeByDelegator("name");
        employeeByDelegator("sa");
        employeeByDelegator("dt");
    }

    @Test
    void tagets() {
        final String api = API + "/targets";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Test
    void usersByManager() {
        final String api = API + "/employee_by_manager";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void statistic() {
        final String api = API + "/statistic";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("section", "ts")
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("section", "apa")
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void departmentByManager() {
        final String api = API + "/department_by_manager";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void employees() {
        final String api = API + "/employees";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}