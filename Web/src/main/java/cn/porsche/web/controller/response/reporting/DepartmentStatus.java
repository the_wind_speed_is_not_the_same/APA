package cn.porsche.web.controller.response.reporting;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DepartmentStatus {
    @ApiModelProperty(notes = "部门的主键")
    @JsonProperty("id")
    Integer id;

    @ApiModelProperty(notes = "部门的名称")
    @JsonProperty("name")
    String name;

    @ApiModelProperty(notes = "部门TS的状态数据")
    @JsonProperty("ts_status")
    Status tsStatus;

    @ApiModelProperty(notes = "部门APA的状态数据")
    @JsonProperty("apa_status")
    Status apaStatus;

    @ApiModelProperty(notes = "部门的员工数据")
    @JsonProperty("sub_status")
    List<DepartmentStatus> subStatus;

    public void setTSStatus(int notStarted, int inProgress, int finished) {
        if (null == tsStatus) {
            tsStatus = new Status();
        }

        tsStatus.setNotStarted(notStarted);
        tsStatus.setInProgress(inProgress);
        tsStatus.setFinished(finished);
    }

    public void setAPAStatus(int notStarted, int inProgress, int finished) {
        if (null == apaStatus) {
            apaStatus = new Status();
        }

        apaStatus.setNotStarted(notStarted);
        apaStatus.setInProgress(inProgress);
        apaStatus.setFinished(finished);
    }

    @Setter
    @Getter
    private class Status {
        @JsonProperty("not_started")
        private Integer notStarted;

        @JsonProperty("in_progress")
        private Integer inProgress;

        @JsonProperty("finished")
        private Integer finished;
    }
}
