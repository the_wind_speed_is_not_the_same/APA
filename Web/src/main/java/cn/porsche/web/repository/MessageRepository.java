package cn.porsche.web.repository;

import cn.porsche.web.domain.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends CrudRepository<Message, Integer> {
    List<Message> findAllByReceiverIdAndRead(Integer receiverId, boolean isRead);

    List<Message> findAllByReceiverIdAndUriAndRead(Integer receiverId, String uri, boolean isRead);
}
