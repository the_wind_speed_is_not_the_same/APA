package cn.porsche.compliance.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "risks", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
}
        , uniqueConstraints=@UniqueConstraint(columnNames={"user_id"})
)
public class Risk implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'", insertable = false, updatable = false)
    private Integer id;

    @ApiModelProperty(notes = "风险对应年份")
    @JsonProperty("year")
    @Column(name = "year",                      columnDefinition = "SMALLINT UNSIGNED NOT NULL COMMENT '风险对应年份'")
    Integer year;

    @ApiModelProperty(notes = "用户主键")
    @JsonProperty("user_id")
    @Column(name = "user_id",                   columnDefinition = "INT UNSIGNED NOT NULL COMMENT '用户主键'")
    Integer userId;

    @ApiModelProperty(notes = "用户昵称")
    @JsonProperty("nickname")
    @Column(name = "nickname",                  columnDefinition = "VARCHAR(125) NOT NULL COMMENT '用户昵称'")
    String nickname;

    @ApiModelProperty(notes = "风险数值")
    @JsonProperty("department_id")
    @Column(name = "department_id",             columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '风险数值'")
    Integer departmentId;

    @ApiModelProperty(notes = "部门名称")
    @JsonProperty("department_name")
    @Column(name = "department_name",           columnDefinition = "VARCHAR(125) NULL DEFAULT NULL COMMENT '部门名称'")
    String departmentName;

    @ApiModelProperty(notes = "职位主键")
    @JsonProperty("position_id")
    @Column(name = "position_id",               columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '职位主键'")
    Integer positionId;

    @ApiModelProperty(notes = "职位名称")
    @JsonProperty("position")
    @Column(name = "position",                  columnDefinition = "VARCHAR(125) NULL DEFAULT NULL COMMENT '职位名称'")
    String position;

    @ApiModelProperty(notes = "风险数值")
    @JsonProperty("number1")
    @Column(name = "number1",                   columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' NOT NULL COMMENT '风险数值'")
    Integer number1;

    @ApiModelProperty(notes = "风险数值")
    @JsonProperty("number2")
    @Column(name = "number2",                   columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' NOT NULL COMMENT '风险数值'")
    Integer number2;

    @ApiModelProperty(notes = "风险数值")
    @JsonProperty("number3")
    @Column(name = "number3",                   columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' NOT NULL COMMENT '风险数值'")
    Integer number3;

    @ApiModelProperty(notes = "风险数值")
    @JsonProperty("number4")
    @Column(name = "number4",                   columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' NOT NULL COMMENT '风险数值'")
    Integer number4;

    @ApiModelProperty(notes = "风险数值")
    @JsonProperty("number5")
    @Column(name = "number5",                   columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' NOT NULL COMMENT '风险数值'")
    Integer number5;

    @ApiModelProperty(notes = "创建时间")
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
