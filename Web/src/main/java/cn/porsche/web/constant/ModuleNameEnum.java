package cn.porsche.web.constant;

public enum ModuleNameEnum {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///（千万不能移动会出问题的）
    ///

    TargetSettingAndUpdate("Target Setting & Update"),
    TargetsReview("Targets Review"),
    MarkSelfScoreAndComments("Mark Self-Score and Comments"),
    PreCalibrationScore ("Pre-calibration Score"),
    CommentsAndFinalScoreUpdates ("Comments & Final Score Updates"),
    StatusTrackingOverallAndDetail("Status Tracking Overall & Detail"),
    FinalScore ("Final Score"),
    Dashboard ("Dashboard"),

    ///
    ///（千万不能移动会出问题的）
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ///（以下枚举随便移）
    AuthRole("Auth Role"),
    Delegate("Delegate"),
    SystemSetting("System Setting"),
    FunctionSetting("Function Setting"),
    ;

    private String moudle;

    ModuleNameEnum(String moudle) {
        this.moudle = moudle;
    }

    public String getName() {
        return moudle;
    }

    public static ModuleNameEnum from(String moudle) {
        try {
            return ModuleNameEnum.valueOf(moudle);
        } catch (IllegalArgumentException exception) {
            return null;
        }
    }
}
