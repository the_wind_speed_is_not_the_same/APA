package cn.porsche.web.service.impl;

import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.domain.*;
import cn.porsche.web.repository.GroupRepository;
import cn.porsche.web.service.GroupService;
import cn.porsche.web.service.GroupUserService;
import cn.porsche.web.service.TargetService;
import cn.porsche.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {
    protected static Group TMGroup;
    protected static Group DMGroup;
    protected static Group DelegatorGroup;
    protected static Group HRGroup;
    protected static Group LongLeavesGroup;
    protected static Group EmployeeGroup;
    protected static Group ProbationGroup;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    GroupUserService groupUserService;

    @Autowired
    UserService userService;

    @Autowired
    TargetService targetService;


    @PostConstruct
    public void init() {
        HRGroup         = findByName(GroupNameEnum.HRPerformanceTeam.getName());
        TMGroup         = findByName(GroupNameEnum.TopManagement.getName());
        DMGroup         = findByName(GroupNameEnum.DisciplinaryManager.getName());
        DelegatorGroup  = findByName(GroupNameEnum.Delegator.getName());
        LongLeavesGroup = findByName(GroupNameEnum.LongLeaves.getName());
        EmployeeGroup   = findByName(GroupNameEnum.Employee.getName());
        ProbationGroup  = findByName(GroupNameEnum.InProbation.getName());
    }


    @Override
    public Group update(Group group) {
        return groupRepository.save(group);
    }

    @Override
    public Group findById(Integer groupId) {
        return groupRepository.findById(groupId).orElse(null);
    }

    @Override
    public Group findByName(String name) {
        return groupRepository.findByName(name);
    }

    @Override
    public void delete(Group group, User user) {
        GroupUser groupUser = groupUserService.findByGroupAndUser(group, user);
        if (null == groupUser) {
            return;
        }

        groupUserService.delete(groupUser);
    }

    @Override
    public Group getDefaultGroup() {
        return groupRepository.findByIsDefault(true);
    }

    @Override
    public List<Group> findAll() {
        List<Group> list = new ArrayList<>();

        Iterator<Group> iterator = groupRepository.findAll().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }

        return list;
    }

    @Override
    public List<Group> findByUser(User user) {
        List<Group> groups = new ArrayList<>();

        List<GroupUser> groupUsers = groupUserService.findByUser(user);
        for (GroupUser groupUser : groupUsers) {
            groups.add(findById(groupUser.getGroupId()));
        }

        return groups;
    }

    @Override
    public List<User> findByGroup(Group group) {
        return groupUserService.findAllByGroup(group);
    }

    @Override
    public void addToEmployeeGroup(List<User> users, User by) {
        for (User user : users) {
            addToEmployeeGroup(user, by);
        }

        // 从HR组、新员工组、长假组和最高管理组中移除
        removeHRPerformanceGroup(users, by);
        removeFromLongLevelGroup(users, by);
        removeFromProbationGroup(users, by);
        removeFromTopManagerGroup(users, by);
    }

    @Override
    public void addToEmployeeGroup(User user, User by) {
        groupUserService.add(EmployeeGroup, user, by);
    }

    @Override
    public void removeFromEmployeeGroup(User user, User by) {
        delete(EmployeeGroup, user);
    }

    @Override
    public void removeFromEmployeeGroup(List<User> users, User by) {
        for (User item : users) {
            removeFromEmployeeGroup(item, by);
        }
    }

    @Override
    public void moveToHRPerformanceGroup(List<User> users, User by) {
        for (User user : users) {
            moveToHRPerformanceGroup(user, by);
        }

        // 从新员工组和长假组中移除
        removeFromProbationGroup(users, by);
        removeFromLongLevelGroup(users, by);
    }

    @Override
    public void moveToHRPerformanceGroup(User user, User by) {
        groupUserService.add(HRGroup, user, by);
    }

    @Override
    public void removeHRPerformanceGroup(User user, User by) {
        delete(HRGroup, user);
    }

    @Override
    public void removeHRPerformanceGroup(List<User> users, User by) {
        for (User item : users) {
            removeHRPerformanceGroup(item, by);
        }
    }

    @Override
    public void moveToLongLevelGroup(List<User> users, User by) {
        for (User user : users) {
            moveToLongLevelGroup(user, by);
        }


        // 从HR组、新员工组和最高管理组中移除
        removeHRPerformanceGroup(users, by);
        removeFromProbationGroup(users, by);
        removeFromTopManagerGroup(users, by);
    }

    @Override
    public void moveToLongLevelGroup(User user, User by) {
        groupUserService.add(LongLeavesGroup, user, by);

        user.setNeedKPI(false);
        userService.update(user);
    }

    @Override
    public void removeFromLongLevelGroup(User user, User by) {
        delete(LongLeavesGroup, user);
    }

    @Override
    public void removeFromLongLevelGroup(List<User> users, User by) {
        for (User user : users) {
            removeFromLongLevelGroup(user, by);
        }
    }

    @Override
    public void moveToTopManagerGroup(List<User> users, User by) {
        for (User user : users) {
            moveToTopManagerGroup(user, by);
        }

        // 从新员工组和长假组中移除
        removeFromProbationGroup(users, by);
        removeFromLongLevelGroup(users, by);
    }

    @Override
    public void moveToTopManagerGroup(User user, User by) {
        groupUserService.add(TMGroup, user, by);

        user.setNeedKPI(false);
        userService.update(user);
    }

    @Override
    public void removeFromTopManagerGroup(User user, User by) {
        delete(TMGroup, user);
    }

    @Override
    public void removeFromTopManagerGroup(List<User> users, User by) {
        for (User user : users) {
            removeFromTopManagerGroup(user, by);
        }
    }

    @Override
    public void moveToManagerGroup(List<User> users, User by) {
        for (User user : users) {
            moveToManagerGroup(user, by);
        }
    }

    @Override
    public void moveToManagerGroup(User user, User by) {
        user.setNeedKPI(false);
        addToEmployeeGroup(user, by);
        groupUserService.add(DMGroup, user, by);
    }

    @Override
    public void removeFromManagerGroup(User user, User by) {
        delete(DMGroup, user);
    }

    @Override
    public void removeFromManagerGroup(List<User> users, User by) {
        for (User user : users) {
            removeFromManagerGroup(user, by);
        }
    }

    @Override
    public void moveToInProbationGroup(List<User> users, User by) {
        for (User user : users) {
            moveToInProbationGroup(user, by);
        }

        // 从HR组、新员工组和长假组中移除
        removeHRPerformanceGroup(users, by);
        removeFromLongLevelGroup(users, by);
        removeFromTopManagerGroup(users, by);
    }

    @Override
    public void moveToInProbationGroup(User user, User by) {
        addToEmployeeGroup(user, by);
        groupUserService.add(ProbationGroup, user);
    }

    @Override
    public void removeFromProbationGroup(User user, User by) {
        delete(ProbationGroup, user);
    }

    @Override
    public void removeFromProbationGroup(List<User> users, User by) {
        for (User user : users) {
            removeFromProbationGroup(user, by);
        }
    }

    @Override
    public void moveToDelegatorGroup(List<User> users, User by) {
        for (User user : users) {
            moveToDelegatorGroup(user, by);
        }
    }

    @Override
    public void moveToDelegatorGroup(User user, User by) {
        addToEmployeeGroup(user, by);
        groupUserService.add(DelegatorGroup, user, by);
    }

    @Override
    public void removeFromDelegatorGroup(User user, User by) {
        delete(DelegatorGroup, user);
    }

    @Override
    public void removeFromDelegatorGroup(List<User> users, User by) {
        for (User user : users) {
            removeFromDelegatorGroup(user, by);
        }
    }


    // 查看某个成员是否在某个组中
    private GroupUser exist(Group group, User user) {
        if (null == group) {
            return null;
        }

        GroupUser gUser = groupUserService.findByGroupAndUser(group, user);
        if (null == gUser) {
            return null;
        }

        return gUser;
    }

    @Override
    public boolean inHRTeamGroup(User user) {
        GroupUser groupUser = exist(HRGroup, user);
        if (null == groupUser) {
            return false;
        }

        return true;
    }

    @Override
    public boolean inEmployeeGroup(User user) {
        return exist(EmployeeGroup, user) != null;
    }

    @Override
    public boolean inManagerGroup(User user) {
        GroupUser groupUser = exist(DMGroup, user);
        if (null == groupUser) {
            return false;
        }

        return true;
    }

    @Override
    public boolean inLongLevelGroup(User user) {
        GroupUser groupUser = exist(LongLeavesGroup, user);
        if (null == groupUser) {
            return false;
        }

        return true;
    }

    @Override
    public boolean inTopManagerGroup(User user) {
        GroupUser groupUser = exist(TMGroup, user);
        if (null == groupUser) {
            return false;
        }

        return true;
    }

    @Override
    public boolean inIDelegatorGroup(User user) {
        GroupUser groupUser = exist(DelegatorGroup, user);
        if (null == groupUser) {
            return false;
        }

        return true;
    }

    @Override
    public boolean inProbationGroup(User user) {
        GroupUser groupUser = exist(ProbationGroup, user);
        if (null == groupUser) {
            return false;
        }

        return true;
    }

}
