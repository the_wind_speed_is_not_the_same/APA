package cn.porsche.web.controller.params;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ImportParam {
    @ApiModelProperty(notes = "所有的列表")
    @JsonProperty("text")
    String text;
}
