package cn.porsche.web.service.impl;

import cn.porsche.web.domain.Department;
import cn.porsche.web.domain.DepartmentTree;
import cn.porsche.web.repository.DepartmentRepository;
import cn.porsche.web.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    DepartmentRepository repository;

    @Override
    public Department findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Department findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Department update(Department department) {
        return repository.save(department);
    }

    @Override
    public Department findByCode(String code) {
        return repository.findByCode(code);
    }

    @Override
    public Department findTopDepartment() {
        return findTopDepartment("PCN");
    }

    @Override
    public Department findTopDepartment(String code) {
        return repository.findByCode(code);
    }

    @Override
    public List<Department> findSubDepartments(Department department) {
        return repository.findAllByParentId(department.getId());
    }

    @Override
    public List<Department> findAllDepartments() {
        List<Department> departments = new ArrayList<>();

        for (Department item : findTopDepartments()) {
            DepartmentTree tree = buildTree(item);
            departments.add(tree.getDepartment());

            List<DepartmentTree> subTree = tree.getSubTree();
            for (int i = 0; i < subTree.size(); i++) {
                DepartmentTree sub = subTree.get(i);
                if (null == sub.getDepartment()) {
                    continue;
                }

                departments.add(sub.getDepartment());
                List<DepartmentTree> subTreej = sub.getSubTree();
                for (int j = 0; j <subTreej.size(); j++) {
                    departments.add(subTreej.get(j).getDepartment());
                }
            }
        }

        return departments;
    }

    @Override
    public List<Department> findTopDepartments() {
        return repository.findTopDepartments();
    }

    @Override
    public DepartmentTree buildTree(Department department) {
        if (null == department) {
            department = findTopDepartment();
            if (department == null) {
                return null;
            }
        }

        List<Department> departments = findSubDepartments(department);
        List<DepartmentTree> subTree = new ArrayList<>();
        if (departments.size() > 0) {
            for (Department item : departments) {
                subTree.add(buildTree(item));
            }
        }

        DepartmentTree tree = new DepartmentTree();
        tree.setDepartment(department);
        tree.setSubTree(subTree);

        return tree;
    }
}
