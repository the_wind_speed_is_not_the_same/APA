package cn.porsche.web.task;

import cn.porsche.common.util.JsonUtils;
import cn.porsche.common.util.http.HttpException;
import cn.porsche.common.util.http.HttpUtils;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ScoreLevelEnum;
import cn.porsche.web.domain.*;
import cn.porsche.web.service.*;
import cn.porsche.web.task.parsing.exception.ParsingException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;

@Log4j2
public abstract class AbstractParsingApiTasks {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${spring.profiles.active}")
    protected String profile;

    protected final static String PCNToken          = "P(S)?CN\\d+";

    private final static String TestDate            = "20200830";
    private final static String DateFormatString    = "yyyMMdd";

    private final static String ApiHeaderKey        = "apikey";
    private final static String ApiHeaderValue      = "LmdkMuu5bavHDQ2";
//    private final static String ApiHeaderValue      = "yI6X025ERcgvFOeFDpzbrIraP9Qn2yGd";

    private final static String ParamPageIndexKey   = "pageIndex";
    private final static String ParamSizeKey        = "pageSize";
    private final static String ParamPageKey        = "page";

    private final static int ParamPageValue         = 1;
    private final static int ParamSizeValue         = 50;

    private final static int HttpStatusCode         = 200;

    private final static String HttpStatusKey       = "code";
    private final static String HttpItemsKey        = "items";
    private final static String HttpMaxPageKey      = "totalPage";


    protected final static String ProdEmployeeURI   = "http://apigwaws.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/EmployeeApi?date=%s";
    protected final static String ProdPositionURI   = "http://apigwaws.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/PositionApi?date=%s";
    protected final static String ProdDepartmentURI = "http://apigwaws.porsche-cloudservice.com/env-101/Eflow/eflow/hris/eflow/DepartmentApi?date=%s";


    // departmentKeys
    //  DEPARTMENTID DEPARTMENTNAME PARENTDEPARTMENDID Company Code-Company
    protected final static String[] dpKeys = {"DEPARTMENTID", "DEPARTMENTNAME", "PARENTDEPARTMENDID", "Company_Code_Company"};


    // employeeKeys
    //  0UserID 1FIRSTNAMEEN 2LASTNAMEEN 3USERNAMECN 4USERCODE 5JOBCODEID 6DEPARTMENTID 7DEPARTMENTNAME 8COSTCENTERCODE 9EMAIL
    //      10ORGANIZATION 11MOBILENO 12TEL 13Preferred Name
    protected final static String[] emKeys = {"UserID", "FIRSTNAMEEN", "LASTNAMEEN", "USERNAMECN", "USERCODE",
            "JOBCODEID", "DEPARTMENTID", "DEPARTMENTNAME", "COSTCENTERCODE", "EMAIL", "ORGANIZATION", "MOBILENO", "TEL", "Preferred_Name"};

    // employeeKeys
    //  JOBCODEID JOBCODE JOBFUNCTION SUPERIORJOBCODEID	POSITIONOCCUPIED Company Code
    protected final static String[] poKeys = {"JOBCODEID", "JOBCODE", "JOBFUNCTION", "SUPERIORJOBCODEID", "POSITIONOCCUPIED", "Company_Code"};


    protected enum API {
        First(null),
        Department(ProdDepartmentURI),
        Position(ProdPositionURI),
        Employee(ProdEmployeeURI),
        ;

        API(String uri) {
            this.uri = uri;
        }

        protected String uri;

        public String getUri() {
            return this.uri;
        }

        public API first()  {
            if (this.ordinal() < 0) {
                return null;
            }

            return values()[0];
        }

        public API next() {
            int idx = this.ordinal() + 1;
            API[] urls = values();
            if (idx >= urls.length) {
                return null;
            }

            return urls[idx];
        }

        public boolean hasNext() {
            return this.ordinal() + 1 < values().length;
        }
    }

    @Autowired
    protected UserService userService;

    @Autowired
    protected PositionService positionService;

    @Autowired
    protected GroupService groupService;

    @Autowired
    protected GroupUserService groupUserService;

    @Autowired
    protected DepartmentService departmentService;

    @Autowired
    protected DepartmentEmployeeService departmentEmployeeService;

    @Autowired
    protected KPIUserService kpiUserService;

    @Autowired
    protected TargetService targetService;

    @Autowired
    protected FunctionService functionService;

    @Autowired
    protected DepartmentReportService reportService;


    // 解析外部系统提供过来的文件
    protected abstract void parsing(List<String[]> list, API api) throws ParsingException;


    // 每天最后一秒执行文件
    @Scheduled(cron = "01 01 22 1/1 * ?")
    public void cron() {
        try {
            running();
        } catch (ParsingException e) {
            e.printStackTrace();

            logger.debug(e.getMessage());
        }
    }

    protected boolean filter(String code) {
        if (StringUtils.isEmpty(code)) {
            return true;
        }

        return !Pattern.compile(PCNToken).matcher(code).matches();
    }

    @Transactional(rollbackFor = ParsingException.class) // 需要开启事务支持
    protected void running() throws ParsingException {
        logger.debug("{} -------------> 准备解析 API 接口", new Date());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);

        String date = new SimpleDateFormat(DateFormatString).format(calendar.getTime());

        List<Header> headers = new ArrayList<>();
        headers.add(new BasicHeader(ApiHeaderKey, ApiHeaderValue));

        Map<String, Integer> params = new HashMap<>();



        API api = API.First;
        BasicHttpEntity entity = new BasicHttpEntity();


        String[] keys;
        String[] values;
        JSONArray list;
        JSONObject item;
        JSONObject json;


        List<String[]> data;
        Object value;
        String response = null;
        do {
            int page = ParamPageValue;
            int size = ParamSizeValue;
            int max  = ParamPageValue;

            api  = api.next();
            data = new ArrayList<>();

            logger.debug("访问 -------------> {}", api.getUri());
            switch (api) {
                case Employee: keys = emKeys; break;
                case Position: keys = poKeys; break;
                case Department: keys = dpKeys; break;
                default: keys = null;
            }

            while (page <= max) {
                params.clear();

                params.put(ParamPageIndexKey, page);
                params.put(ParamSizeKey, size);


                try {
                    entity.setContent(new ByteArrayInputStream(JsonUtils.toString(params).getBytes("UTF-8")));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return;
                }

                try {
                    response = HttpUtils.post(String.format(api.getUri(), date), entity, headers);
                    logger.debug("返回 -------------> {}", response);

                    json = new JSONObject(response);
                } catch (HttpException e) {
                    e.printStackTrace();

                    throw ParsingException.ParsingApiException(response);
                } catch (Exception e) {
                    e.printStackTrace();

                    throw ParsingException.ParsingApiException(response);
                }

                if (HttpStatusCode != json.getInt(HttpStatusKey)) {
                    log.error("获取数据失败，接口地址：{}，返回状态码：{}，错误内容：{}", api.uri, json.getInt(HttpStatusKey), json.getString("msg"));

                    throw ParsingException.ParsingApiException(response);
                }

                if (keys == null) {
                    break;
                }

                list = json.getJSONArray(HttpItemsKey);
                for (int i = 0; i < list.length(); i++) {
                    if (null == ( item = list.getJSONObject(i) )) {
                        continue;
                    }

                    values = new String[keys.length];
                    for (int j = 0; j < keys.length; j++) {
                        try {
                            value = item.get(keys[j]);
                        } catch (JSONException e) {
                            value = JSONObject.NULL;
                        }

                        values[j] = value.equals(JSONObject.NULL) ? null : String.valueOf(value);
                    }

                    data.add(values);
                }

                page = json.getInt(ParamPageKey);
                size = json.getInt(ParamSizeKey);
                max  = json.getInt(HttpMaxPageKey);

                page ++;
            }

            parsing(data, api);
            logger.debug("完成 -------------> {}", api.getUri());

        } while (api.hasNext());


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ///


        ///
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        logger.debug("{} =============> API EFlow 解析完毕", new Date());



        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// 更新数据，因为有依赖关系
        ///

        logger.debug("{} -------------> 开始更新用户的上级信息", new Date());
        // 更新用户的上线信息，依赖
        updateUserManager();
        logger.debug("{} =============> 更新用户的上级信息完毕", new Date());

        // 更新用户组信息，VP，DM，员工组信息
        logger.debug("{} -------------> 更新用户的组信息", new Date());
        updateUserGroup();
        logger.debug("{} =============> 更新用户的组信息完毕", new Date());

        logger.debug("{} -------------> 开始更新用户部门信息", new Date());
        updateUserDepartment();
        logger.debug("{} =============> 更新用户部门信息完毕", new Date());

        logger.debug("{} -------------> 开始更新用户的绩效数据", new Date());
        updateUserKpi();
        logger.debug("{} =============> 更新用户的绩效数据完毕", new Date());

        logger.debug("{} -------------> 开始更新部门报表", new Date());
        updateDepartmentReport();
        logger.debug("{} =============> 更新部门报表完毕", new Date());

        ///
        /// 解析文件的顺序千万不要变，因为有依赖关系
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }


    // 更新用户信息表的User.ManagerId，同时修改绩效表中经理数据
    protected void updateUserManager() {
        double count = Double.valueOf(userService.count());
        if (count == 0f) {
            return;
        }

        Set<Integer> managerIds = Collections.synchronizedSet(new HashSet<>());

        // 开始设置用户的部门数据
        int page = Runtime.getRuntime().availableProcessors() + 1;
        int size = Double.valueOf(Math.ceil(count / page)).intValue();

        CountDownLatch latch = new CountDownLatch(page);
        for (int i = 1; i <= page; i++) {
            new Thread(new SyncProcessUserManager(latch, managerIds, i, size)).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 根据管理员列表，设置对应部门的主管
        for (Integer id : managerIds) {
            User manager = userService.findById(id);
            if (null == manager || null == manager.getDepartmentId()) {
                continue;
            }

            Department department = departmentService.findById(manager.getDepartmentId());
            if (null == department || id.equals(department.getManagerId())) {
                continue;
            }

            if (!id.equals(department.getManagerId())) {
                department.setManagerId(id);

                departmentService.update(department);
            }
        }
    }


    // 处理直线信息数据
    //  检查用户是否在直线经理或VP中，做相应的处理
    protected void updateUserGroup() {
        double count = Double.valueOf(userService.count());
        if (count == 0f) {
            return;
        }

        Set<Integer> vpIDs = Collections.synchronizedSet(new HashSet<>());
        Set<Integer> dmIDs = Collections.synchronizedSet(new HashSet<>());

        // 开始设置用户的部门数据
        int page = Runtime.getRuntime().availableProcessors() + 1;
        int size = Double.valueOf(Math.ceil(count / page)).intValue();


        CountDownLatch latch = new CountDownLatch(page);
        for (int i = 1; i <= page; i++) {
            new Thread(new SyncProcessUserGroup(latch, vpIDs, dmIDs, i, size)).start();;
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // 根据VP设置
        for (Integer id : vpIDs) {
            User user = userService.findById(id);

            groupService.moveToTopManagerGroup(user, user);
        }


        Group managerGroup  = groupService.findByName(GroupNameEnum.DisciplinaryManager.getName());
        List<User> managers = groupUserService.findAllByGroup(managerGroup);
        // 将旧的管理员数据先清除
        for (User item : managers) {
            if (!dmIDs.contains(item.getId())) {
                groupUserService.remove(managerGroup, item);
            } else {

                // 已经存在新的经理列表中，删除掉
                dmIDs.remove(item.getId());
            }
        }

        for (Integer id : dmIDs) {
            // 不在管理组的添加到经理级中
            User manager = userService.findById(id);
            if (null == manager) {
                continue;
            }

            // 添加到管理组信息中去
            groupService.moveToManagerGroup(manager, manager);
            logger.debug("新增 {} {} 到管理组中", manager.getId(), manager.getCnName());
        }
    }


    // 更新用户所有的部门信息
    protected void updateUserDepartment() {
        double count = Double.valueOf(userService.count());
        if (count == 0f) {
            return;
        }

        Integer departmentId;
        Department department;

        // 生成部门树
        Map<Integer, List<Department>> tree = new HashMap<>();
        List<Department> departments = departmentService.findAllDepartments();
        for (Department item : departments) {
            List<Department> list = new ArrayList<>();

            list.add(item);

            departmentId = item.getParentId();
            while (departmentId != null || departmentId != 0) {
                // 找到父级
                department = departmentService.findById(departmentId);

                if (null == department) {
                    break;
                }

                if (department != null) {
                    list.add(department);

                    if (department.getParentId() == null || department.getParentId() == 0) {
                        break;
                    }

                    departmentId = department.getParentId();
                }
            }

            Collections.reverse(list);

            while (list.size() < 5) {
                // 补齐总数
                list.add(null);
            }

            tree.put(item.getId(), list);
        }

        // 开始设置用户的部门数据
        int page = Runtime.getRuntime().availableProcessors() + 1;
        int size = Double.valueOf(Math.ceil(count / page)).intValue();

        CountDownLatch latch = new CountDownLatch(page);
        for (int i = 1; i <= page; i++) {
            new Thread(new SyncProcessDepartmentInformation(latch, tree, i, size)).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // 处理员工的绩效数据，有可能因为变换部门之后，绩效数据出问题了
    protected void updateUserKpi() {
        Function function = functionService.getLastOpeningFunction();
        if (null == function) {
            return;
        }

        double count = Double.valueOf(userService.count());
        if (count == 0f) {
            return;
        }

        // 开始设置用户的部门数据
        int page = Runtime.getRuntime().availableProcessors() + 1;
        int size = Double.valueOf(Math.ceil(count / page)).intValue();

        CountDownLatch latch = new CountDownLatch(page);
        for (int i = 1; i <= page; i++) {
            new Thread(new SyncProcessKpiUser(latch, function, i, size)).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // 更新所有的部门报表
    protected void updateDepartmentReport() {
        Function function = functionService.getLastOpeningFunction();
        if (null == function) {
            return;
        }

        for (Department item : departmentService.findAllDepartments()) {
            if (null == reportService.findByDepartment(function.getId(), item.getId())) {
                reportService.initReporting(function, item);
            }

            reportService.refreshReporting(function, item);
        }
    }

    // 更新用户的部门数据
    private class SyncProcessDepartmentInformation implements Runnable {
        int page;
        int size;

        CountDownLatch latch;
        Map<Integer, List<Department>> tree;

        public SyncProcessDepartmentInformation(CountDownLatch latch, final Map<Integer, List<Department>> tree, final int page, final int size) {
            this.page = page;
            this.size = size;

            this.tree = tree;

            this.latch = latch;
        }

        @Override
        public void run() {
            Iterator<User> iterator = userService.findAllByPage(page, size).getContent().iterator();

            User item;
            Integer departmentId;

            // 用于生成员工的部门层级
            List<Department> list;
            while (iterator.hasNext()) {
                item = iterator.next();
                if (null == (departmentId = item.getDepartmentId())) {
                    continue;
                }

                if (null == (list = tree.get(departmentId))) {
                    continue;
                }

                DepartmentEmployee data = departmentEmployeeService.findByEmployee(item);
                if (data == null) {
                    data = DepartmentEmployee.builder().employeeId(item.getId())
                            .department1(0)
                            .department2(0)
                            .department3(0)
                            .department4(0)
                            .department5(0)
                            .build();
                }

                logger.debug(" =============> 开始更新 {} {} 的部门数据", item.getId(), item.getCnName());


                boolean isUpdate = false;
                if (list.get(0) != null && !list.get(0).getId().equals(data.getDepartment1())) {
                    data.setDepartment1(list.get(0).getId());

                    isUpdate = true;
                }

                if (list.get(1) != null && !list.get(1).getId().equals(data.getDepartment2())) {
                    data.setDepartment2(list.get(1).getId());

                    isUpdate = true;
                }

                if (list.get(2) != null && !list.get(2).getId().equals(data.getDepartment3())) {
                    data.setDepartment3(list.get(2).getId());

                    isUpdate = true;
                }

                if (list.get(3) != null && !list.get(3).getId().equals(data.getDepartment4())) {
                    data.setDepartment4(list.get(3).getId());

                    isUpdate = true;
                }

                if (list.get(4) != null && !list.get(4).getId().equals(data.getDepartment5())) {
                    data.setDepartment5(list.get(4).getId());

                    isUpdate = true;
                }

                if (isUpdate) {
                    logger.debug(" =============> 更新的部门信息：{} {}, {} {} {} {} {}",item.getId(), item.getCnName(),
                            data.getDepartment1(),
                            data.getDepartment2(),
                            data.getDepartment3(),
                            data.getDepartment4(),
                            data.getDepartment5()
                    );

                    departmentEmployeeService.update(data);
                }
            }

            latch.countDown();
        }
    }

    // 更新用户的汇报线
    private class SyncProcessUserManager implements Runnable {
        int page;
        int size;
        CountDownLatch latch;

        Set<Integer> managers;

        public SyncProcessUserManager(CountDownLatch latch, Set<Integer> managers, final int page, final int size) {
            this.page = page;
            this.size = size;

            this.latch = latch;

            this.managers = managers;
        }

        @Override
        public void run() {
            Iterator<User> iterator = userService.findAllByPage(page, size).getContent().iterator();

            User item;
            User delegator;
            while (iterator.hasNext()) {
                item = iterator.next();
                delegator = null;

                logger.debug("开始查找上级 {}", JsonUtils.toString(item));
                Integer positionId = item.getPositionId();

                if (null == positionId || positionId == 0) {
                    logger.debug("没有职位信息 {}", JsonUtils.toString(item));
                    continue;
                }

                // 根据 PositionId 查看对应的上级 User 数据
                Position position;
                while (positionId != null) {
                    position  = positionService.findById(positionId);
                    if (null == position || null == position.getParentId()) {
                        // 没有上级信息
                        delegator = null;

                        logger.debug("用户{} {} 没有职位信息，职位数据中无上级数据", item.getId(), item.getCnName());
                        break;
                    }

                    delegator = userService.findByPositionId(position.getParentId());
                    if (delegator != null) {
                        logger.debug("用户{} {} 的直属上级", item.getId(), item.getCnName(), JsonUtils.toString(delegator));

                        break;
                    }

                    if (null == ( positionId = position.getParentId() )) {
                        logger.debug("用户{} {} 没有职位信息，职位数据中无上级数据，开始查找上级的上级 Id:{}, ParentId:{}", item.getId(), item.getCnName(), position.getId(), position.getParentId());

                        break;
                    }
                }

                synchronized(managers) {
                    if (delegator != null && managers.add(delegator.getId())) {
                        logger.debug("{} 的直线经理 {} {}", item.getCnName(), delegator.getId(), delegator.getCnName());
                    }
                }

                if (delegator != null && !delegator.getId().equals(item.getManagerId())) {
                    item.setManagerId(delegator.getId());

                    // 将KPI中的汇报线也一并改为新的管理员
                    // 绩效考核已经完成的，忽略，未完成的，切换为新的管理员
                    List<KPIUser> kpiUsers = kpiUserService.findAllByUser(item);
                    for (KPIUser kpiuser : kpiUsers) {
                        Target target = targetService.findByKPIUser(kpiuser);
                        if (target == null || target.isAgreeScore()) {
                            // 已经结束的绩效记录不必修改
                            continue;
                        }

                        logger.debug(" -------------> 更新绩效数据中的汇报数据 {} 的汇报经理为 {}", item.getCnName(), delegator.getCnName());

                        kpiuser.setDelegatorId(delegator.getId());
                        kpiuser.setDelegator(delegator.getEnName());

                        kpiuser.setManager(delegator.getEnName());
                        kpiuser.setManagerId(delegator.getManagerId());

                        kpiUserService.update(kpiuser);
                    }

                    logger.debug(" --------------------------> 更新 {} 的汇报经理为 {} 已完成", item.getCnName(), delegator.getCnName());
                    userService.update(item);
                }
            }

            latch.countDown();
        }
    }


    // 更新用户的组信息，同时自动TS
    private class SyncProcessUserGroup implements Runnable {
        int page;
        int size;

        CountDownLatch latch;

        Set<Integer> vpIDs;
        Set<Integer> dmIDs;


        public SyncProcessUserGroup(CountDownLatch latch, final Set<Integer> vpIDs, final Set<Integer> dmIDs, final int page, final int size) {
            this.page = page;
            this.size = size;

            this.latch  = latch;

            this.vpIDs  = vpIDs;
            this.dmIDs  = dmIDs;
        }

        @Override
        public void run() {
            Iterator<User> iterator = userService.findAllByPage(page, size).getContent().iterator();

            User item;

            Function function = functionService.getLastFunction();
            Group defaultGroup = groupService.getDefaultGroup();
            while (iterator.hasNext()) {
                item = iterator.next();

                // 查看用户是那类角色
                boolean isVP = false;

                // 检查是否为VP
                if (null != item.getPositionId()) {
                    Position position = positionService.findById(item.getPositionId());
                    if (null != position && null != position.getFunction() && position.getFunction().indexOf("Vice President") == 0) {
                        synchronized(vpIDs) {
                            vpIDs.add(item.getId());
                        }

                        isVP = true;
                    }
                }

                // 检查是否为DM（直线经理）
                if (null != item.getManagerId()) {
                    synchronized (dmIDs) {
                        dmIDs.add(item.getManagerId());
                    }
                }

                if (isVP || groupService.inLongLevelGroup(item) || groupService.inProbationGroup(item)) {
                    // 是VP, 或在长假组，或在实习生组
                    continue;
                }


                if (!groupService.inEmployeeGroup(item)) {
                    // 不在员工组，需要创建Target
                    if (null == function) {
                        continue;
                    }

                    groupUserService.add(defaultGroup, item);

                    if (null != function) {
//                        Department department = null;
//                        if (null != item.getDepartmentId()) {
//                            department = departmentService.findById(item.getDepartmentId());
//                        }
//
//                        Position position = null;
//                        if (null != item.getPositionId()) {
//                            position = positionService.findById(item.getPositionId());
//                        }
//
//                        User delegator = null;
//                        if (null != item.getManagerId()) {
//                            delegator = userService.findById(item.getManagerId());
//                        }

                        try {
                            targetService.autoTargetSetting(function, item);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

//                        targetService.autoAppraisal(function, item, ScoreLevelEnum.C);

                        Department department;
                        if (null != item.getDepartmentId() && null != ( department = departmentService.findById(item.getDepartmentId()) )) {
                            log.debug("开始更新此部门对应的数据报表");

                            reportService.initReporting(function, department);
                            reportService.refreshReporting(function, department);
                        }
                    }

                    logger.debug("用户{} {} 已经添加到 {} 组", item.getId(), item.getCnName(), defaultGroup.getName());
                }
            }

            latch.countDown();
        }
    }


    // 更新用户的组信息，同时自动TS
    private class SyncProcessKpiUser implements Runnable {
        int page;
        int size;

        Function function;
        CountDownLatch latch;

        public SyncProcessKpiUser(CountDownLatch latch, Function function, final int page, final int size) {
            this.page = page;
            this.size = size;

            this.latch  = latch;
            this.function = function;
        }

        @Override
        public void run() {
            Iterator<User> iterator = userService.findAllByPage(page, size).getContent().iterator();

            User item;
            User manager;
            Department department;

            KPIUser kpi;
            Boolean isUpdate;
            while (iterator.hasNext()) {
                isUpdate    = false;
                manager     = null;
                department  = null;
                item = iterator.next();

                // 查看用户是否更换部门
                if (null == ( kpi = kpiUserService.findByFunction(function, item) )) {
                    continue;
                }

                if (null == item.getManagerId() || null == ( manager = userService.findById(item.getManagerId()) )) {
                    // 未找到用户的上级信息
                    isUpdate = true;
                } else if (!item.getDepartmentId().equals(kpi.getDepartmentId())) {
                    // 换了部门
                    isUpdate = true;
                } else if (!item.getManagerId().equals(kpi.getManagerId())) {
                    // 换了经理
                    isUpdate = true;
                }

                if (isUpdate) {
                    department = departmentService.findById(item.getDepartmentId());

                    kpi.setManagerId(null == manager ? 0 : manager.getId());
                    kpi.setManager(null == manager ? "" : manager.getEnName());
                    kpi.setDelegatorId(null == manager ? 0 : manager.getId());
                    kpi.setDelegator(null == manager ? "" : manager.getEnName());

                    kpi.setDepartmentId(null == department ? 0 : department.getId());
                    kpi.setDepartment(null == department ? "" : department.getName());

                    kpiUserService.update(kpi);
                }
            }

            latch.countDown();
        }
    }
}
