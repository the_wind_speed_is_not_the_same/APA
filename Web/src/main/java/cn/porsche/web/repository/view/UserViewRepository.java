package cn.porsche.web.repository.view;

import cn.porsche.web.domain.view.UserView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserViewRepository extends CrudRepository<UserView, Integer> {
    Page<UserView> findAll(Pageable pageable);
    Page<UserView> findAllByNeedHands(boolean needHands, Pageable pageable);


    @Query("SELECT uv from UserView uv LEFT JOIN GroupUser gu ON uv.id=gu.userId WHERE gu.groupId=:groupId")
    Page<UserView> findAllByGroupId(Integer groupId, Pageable pageable);

    Page<UserView> findAllByDepartmentId(Integer departmentId, Pageable pageable);

    Page<UserView> findAllByEnNameLike(String name, Pageable pageable);
    Page<UserView> findAllByDepartmentIdAndEnNameLike(Integer departmentId, String name, Pageable pageable);
    Page<UserView> findAllByDepartmentIdAndNeedHands(Integer departmentId, boolean needHands, Pageable pageable);

    Page<UserView> findAllByHireDateIsNull(Pageable pageable);
    Page<UserView> findAllByDepartmentIdAndHireDateIsNull(Integer departmentId, Pageable pageable);
}
