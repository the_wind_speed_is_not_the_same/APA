package cn.porsche.web.controller.params;

import cn.porsche.web.domain.Comment;
import cn.porsche.web.domain.KPIUser;
import cn.porsche.web.domain.Target;
import cn.porsche.web.domain.TargetHow;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KPIParam {
    @ApiModelProperty(notes = "员工当前的各种状态：是否提交、是否确认等等")
    @JsonProperty("target")
    Target target;

    @ApiModelProperty(notes = "Target Setting中的How数据")
    @JsonProperty("how")
    TargetHow how;

    @ApiModelProperty(notes = "用户详细说明")
    @JsonProperty("user")
    KPIUser user;

    @ApiModelProperty(notes = "每个员工的Target列表，包括其中的Comment")
    @JsonProperty("whats")
    List<WhatParam> whats;

    @ApiModelProperty(notes = "员工与经理的对话列表")
    @JsonProperty("overall")
    List<Comment> overall;
}
