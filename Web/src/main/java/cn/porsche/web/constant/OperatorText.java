package cn.porsche.web.constant;

public enum OperatorText {
    ADD(),
    EDIT(),
    CHECK(),
    DOWNLOAD(),
    DELEGATION(),
}
