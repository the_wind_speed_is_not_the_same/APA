package cn.porsche.compliance.domain.view;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "user_views", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class UserView implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @JsonIgnore
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @ApiModelProperty(notes = "外部系统中的员工代码")
    @JsonProperty("code")
    @Column(name = "code")
    String code;

    @ApiModelProperty(notes = "中文名称")
    @JsonProperty("cn_name")
    @Column(name = "cn_name")
    String cnName;

    @ApiModelProperty(notes = "英文名称")
    @JsonProperty("en_name")
    @Column(name = "en_name")
    String enName;

    @ApiModelProperty(notes = "用户职称")
    @JsonProperty("title")
    @Column(name = "title")
    String title;

    @ApiModelProperty(notes = "职位主键")
    @JsonProperty("position_id")
    @Column(name = "position_id")
    Integer positionId;

    @JsonIgnore
    @ApiModelProperty(notes = "部门主键")
    @Column(name = "department_id")
    Integer departmentId;

    @JsonIgnore
    @ApiModelProperty(notes = "上级主管主键")
    @Column(name = "manager_id")
    Integer managerId;

    @JsonIgnore
    @ApiModelProperty(notes = "是否为经理")
    @Column(name = "is_manager")
    Boolean isManager;

    @ApiModelProperty(notes = "经理名称")
    @JsonProperty("manager")
    @Column(name = "manager")
    String manager;

    @ApiModelProperty(notes = "部门名称")
    @JsonProperty("department")
    @Column(name = "department")
    String department;

    @ApiModelProperty(notes = "职位描述")
    @JsonProperty("position")
    @Column(name = "position")
    String position;

    @ApiModelProperty(notes = "用户头像")
    @JsonProperty("avatar")
    @Column(name = "avatar")
    String avatar;

    @JsonIgnore
    @ApiModelProperty(notes = "用户邮箱")
    @Column(name = "email")
    String email;

    @ApiModelProperty(notes = "手机号码")
    @JsonProperty("mobile")
    @Column(name = "mobile")
    String mobile;

    @ApiModelProperty(notes = "电话号码")
    @JsonProperty("tel")
    @Column(name = "tel")
    String tel;
}
