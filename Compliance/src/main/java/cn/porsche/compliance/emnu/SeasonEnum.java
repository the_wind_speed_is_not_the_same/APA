package cn.porsche.compliance.emnu;

public enum SeasonEnum {
    Q1("Q1"),
    Q2("Q2"),
    Q3("Q3"),
    Q4("Q4");

    private String name;

    SeasonEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    // 将字符串转换为季度对象
    public static SeasonEnum from(String string) {
        return SeasonEnum.valueOf(string.toUpperCase());
    }

    public static SeasonEnum from(int index) {
        if (-- index < 0) {
            index = 0;
        }

        SeasonEnum[] values = SeasonEnum.values();

        return index > values.length ? null : values[index];
    }
}
