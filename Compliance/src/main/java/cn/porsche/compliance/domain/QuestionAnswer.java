package cn.porsche.compliance.domain;

import cn.porsche.compliance.emnu.SeasonEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "question_answer", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class QuestionAnswer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'", insertable = false, updatable = false)
    private Integer id;

    @ApiModelProperty(notes = "员工主键")
    @JsonProperty("user_id")
    @Column(name = "user_id",                   columnDefinition = "INT UNSIGNED NULL DEFAULT NULL COMMENT '员工主键'")
    Integer userId;

    @ApiModelProperty(notes = "题目年份")
    @JsonProperty("year")
    @Column(name = "year",                      columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '部门主键'")
    Integer year;

    @Enumerated(EnumType.STRING)
    @JsonProperty("season")
    @Column(name = "season",                    columnDefinition = "CHAR(2) NOT NULL COMMENT '一、二、三、四复季度'")
    SeasonEnum season;

    @ApiModelProperty(notes = "问题主键")
    @JsonProperty("question_id")
    @Column(name = "question_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT '问题主键'")
    Integer questionId;

    @ApiModelProperty(notes = "选择答案")
    @JsonProperty("answer")
    @Column(name = "answer",                    columnDefinition = "CHAR(32) NOT NULL COMMENT '选择答案'")
    String answer;

    @ApiModelProperty(notes = "回答是否正确")
    @JsonProperty("right")
    @Column(name = "`right`",                    columnDefinition = "TINYINT(1) NOT NULL DEFAULT '0' COMMENT '回答是否正确'")
    boolean Right;

    @ApiModelProperty(notes = "回答正确之后的分数")
    @JsonProperty("score")
    @Column(name = "score",                    columnDefinition = "TINYINT(2) NOT NULL DEFAULT '0' COMMENT '回答正确之后的分数'")
    Integer score;

    @ApiModelProperty(notes = "创建时间")
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
