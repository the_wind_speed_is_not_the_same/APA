package cn.porsche.compliance.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.domain.Position;
import cn.porsche.compliance.domain.Risk;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.service.PositionService;
import cn.porsche.compliance.service.RiskService;
import cn.porsche.compliance.service.UserViewService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/risk")
@ApiModel(value = "/risk", description = "风险的操作类")
public class RiskController extends BaseController {
    @Autowired
    PositionService positionService;

    @Autowired
    RiskService riskService;

    @Autowired
    UserViewService userViewService;

    @ApiOperation(
            value = "部门的风险列表",
            notes = "可看到自己属下及下级部门人员的风险"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/list")
    public String list() {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        Map<String, Object> json = new HashMap<>();

        List<User> list = new ArrayList<>();
        json.put("users", list);
        if (null == user.getPositionId()) {
            // 用户没有职位信息
            return ApiResult.builder().data(json).toJson();
        }

        Position position = positionService.findById(user.getPositionId());
        if (null == position) {
            // 用户没有职位信息
            return ApiResult.builder().data(json).toJson();
        }

        // 当前用户添加到返回结果
        json.put("user", userViewService.findById(user.getId()));

        User subUser;
        List<Position> subordinates = positionService.subordinates(position);
        for (Position item : subordinates) {
            if (null != ( subUser = userService.findByPositionId(item.getId()) )) {
                if (null != ( riskService.findByUser(subUser)) ) {
                    list.add(subUser);
                }
            }
        }

        return ApiResult.builder().data(json).toJson();
    }

    @ApiOperation(
            value = "部门的风险列表",
            notes = "查看属下员工对应的风险数据"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/detail")
    public String detail(
            @RequestParam(value = "user_id") Integer userId
    ) {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        Map<String, Object> json = new HashMap<>();
        Position position = positionService.findById(user.getPositionId());
        if (null == position) {
            // 用户没有职位信息
            return ApiResult.builder().data(json).toJson();
        }

        User subUser = userService.findById(userId);;
        Risk subRisk = riskService.findByUser(subUser);
        json.put("user", subUser);
        json.put("risk", subRisk);
//
//        User subUser;
//        Risk subRisk;
//        List<Position> subordinates = positionService.subordinates(position);
//        for (Position item : subordinates) {
//            if (null == ( subUser = userService.findByPositionId(item.getId()) )
//                || !subUser.getId().equals(userId)
//                || null == ( subRisk = riskService.findByUser(subUser) )
//            ) {
//                continue;
//            }
//
//            json.put("user", subUser);
//            json.put("risk", subRisk);
//
//            break;
//        }

        return ApiResult.builder().data(json).toJson();
    }
}
