package cn.porsche.common.util.http;

import cn.porsche.common.util.JsonUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;

import java.util.List;

public class HttpJsonUtils extends HttpUtils {
    static public <T> T getJson(Class clz, String uri) throws HttpException {
        return JsonUtils.fromString(get(uri), clz);
    }

    static public <T> T getJson(Class clz, String uri, List<NameValuePair> params) throws HttpException {
        return JsonUtils.fromString(get(uri, params, null), clz);
    }

    static public <T> T getJson(Class clz, String url, List<NameValuePair> params, List<Header> headers) throws HttpException {
        return JsonUtils.fromString(get(url, params, headers), clz);
    }

    static public <T> T postJson(Class clz, String url) throws HttpException {
        return JsonUtils.fromString(post(url), clz);
    }

    static public <T> T postJson(Class clz, String url, List<NameValuePair> params) throws HttpException {
        return JsonUtils.fromString(post(url, params), clz);
    }

    static public <T> T postJson(Class clz, String url, List<NameValuePair> params, List<Header> headers) throws HttpException {
        return JsonUtils.fromString(post(url, params, headers), clz);
    }

    static public <T> T json(Class clz, String url) throws HttpException {
        return JsonUtils.fromString(post(url), clz);
    }
}