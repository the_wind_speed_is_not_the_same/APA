package cn.porsche.web.controller.response.reporting;

import cn.porsche.web.domain.KPIUser;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ResultRating {
    @ApiModelProperty(notes = "部门主键")
    @JsonProperty("id")
    String id;

    @ApiModelProperty(notes = "部门名称")
    @JsonProperty("department")
    String department;

    @ApiModelProperty(notes = "经理名称")
    @JsonProperty("manager")
    String manager;

    @ApiModelProperty(notes = "预打分数据")
    @JsonProperty("pre_calibration_ratings")
    List<Rating> preCalibrationRating;


    @ApiModelProperty(notes = "最终打分数据")
    @JsonProperty("final_performance_ratings")
    List<Rating> finalPerformanceRating;

    @ApiModelProperty(notes = "部门员工考核数据")
    @JsonProperty("employee")
    List<KPIUser> employee;

    public void addPreCalibrationRation(String level, int percent, int total) {
        if (null == preCalibrationRating) {
            preCalibrationRating = new ArrayList<>();
        }

        Rating rating = new Rating();
        rating.setLevel(level);
        rating.setDistributionTotal(total);
        rating.setDistributionPercent(percent);

        preCalibrationRating.add(rating);
    }


    public void addFinalPerformanceRation(String level, int percent, int total) {
        if (null == finalPerformanceRating) {
            finalPerformanceRating = new ArrayList();
        }

        Rating rating = new Rating();
        rating.setLevel(level);
        rating.setDistributionTotal(total);
        rating.setDistributionPercent(percent);

        finalPerformanceRating.add(rating);
    }

    @Setter
    @Getter
    private class Rating {
        @JsonProperty("level")
        private String level;

        @JsonProperty("distribution_percent")
        private Integer distributionPercent;

        @JsonProperty("distribution_total")
        private Integer distributionTotal;
    }
}
