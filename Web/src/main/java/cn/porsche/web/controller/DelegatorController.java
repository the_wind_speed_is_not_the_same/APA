package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.controller.params.DelegatorParam;
import cn.porsche.web.domain.*;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/delegator")
@Api(value = "/delegator", description = "委托人相关的Target操作类")
public class DelegatorController extends BaseController {

    @ApiOperation(
            value = "【经理】在【委托授权】界面指定委托数据的接口",
            notes = "POST参数<br/>" +
                    "function_id（阶段主键）<br />" +
                    "delegator_id（小组长主键）<br />" +
                    "employee_ids（成员长主键）:employee_ids[]=123&employee_ids[]=456<br />"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = ApiResult.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id",  required = true, dataTypeClass = Integer.class,  value = "阶段主键", example = "1"),
            @ApiImplicitParam(name = "delegator_id", required = true, dataTypeClass = Integer.class,  value = "小组长主键", example = "504169"),
            @ApiImplicitParam(name = "employee_ids", required = true, dataTypeClass = List.class,     value = "员工主键列表", example = "employee_ids[]=123&employee_ids[]=456"),
    })
    @PostMapping(value = "/delegator_group")
    public String delegatorGroup(
            @RequestBody DelegatorParam param
    ) {
        if (noPromise(ModuleNameEnum.Delegate, OperatorText.ADD, GroupNameEnum.DisciplinaryManager, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (null == param.getEmployeeIds() || param.getEmployeeIds().size() == 0) {
            return ApiResult.isBad("被委托的员工数据为空");
        }

        if (null == param.getDelegatorId()) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }


        Function function = functionService.getLastFunction();
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        User delegator = userService.findById(param.getDelegatorId());
        if (null == delegator || !delegator.getManagerId().equals(user.getId())) {
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///  修改Delegator的属性
        ///

        boolean isVP = groupService.inTopManagerGroup(user);
        kpiUser = kpiUserService.findByFunction(function, delegator);
        if (!isVP && null == kpiUser) {
            return ApiResult.isBad("The function not open yet, please operate after opening by HR");
        }

        if (null != kpiUser) {
            kpiUser.setIsDelegator(true);
            kpiUserService.update(kpiUser);
        }

        ///
        ///  修改Delegator的属性
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///  删除旧时指派的成员未在新指定的组中
        ///


        List<KPIUser> items = kpiUserService.findAllByDelegator(function, delegator);
        for (KPIUser item : items) {
            if (!param.getEmployeeIds().contains(item.getId())) {
                // 旧的员工并不存在于新的委托数据中
                item.setDelegatorId(user.getId());
                item.setDelegator(user.getEnName());

                kpiUserService.update(item);
            }
        }

        ///
        ///  删除旧时指派的成员未在新指定的组中
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///  将成员指派到组中
        ///

        for (Integer kId : param.getEmployeeIds()) {
            User kItem = userService.findById(kId);
            if (null == kItem) {
                continue;
            }

            kpiUser = kpiUserService.findByFunction(function, kItem);
            if (kpiUser == null || !user.getId().equals(kpiUser.getDelegatorId())) {
                continue;
            }

            if (!delegator.getId().equals(kpiUser.getDelegatorId())) {
                kpiUser.setDelegatorId(delegator.getId());
                kpiUser.setDelegator(delegator.getEnName());

                kpiUserService.update(kpiUser);
            }
        }

        ///
        ///  将成员指派到组中
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        // 将用户添加为Delegator组中
        groupService.moveToDelegatorGroup(delegator, user);


        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【经理】在【委托授权】解散委托小组接口",
            notes = "GET参数<br/>" +
                    "function_id（阶段主键）<br />" +
                    "delegator_id（小组长主键）<br />"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = ApiResult.class)
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id",  required = true, dataTypeClass = Integer.class,  value = "阶段主键"),
            @ApiImplicitParam(name = "delegator_id", required = true, dataTypeClass = Integer.class,  value = "小组长主键"),
    })
    @GetMapping(value = "/delegator_dismiss")
    public String delegateDismiss(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "delegator_id") Integer delegatorId
    ) {
        if (noPromise(ModuleNameEnum.Delegate, OperatorText.ADD, GroupNameEnum.DisciplinaryManager, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        User delegator = userService.findById(delegatorId);
        if (null == delegator || !delegator.getManagerId().equals(user.getId())) {
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///  解散小组成员，恢复之前的委托信息
        ///

        List<KPIUser> items = kpiUserService.findAllByDelegator(function, delegator);
        for (KPIUser item : items) {
            // 旧的员工并不存在于新的委托数据中
            item.setDelegatorId(user.getId());
            item.setDelegator(user.getEnName());

            kpiUserService.update(item);
        }


        // 取消小组长标识t
        if (null != ( kpiUser = kpiUserService.findByFunction(function, delegator) )) {
            kpiUser.setIsDelegator(false);

            kpiUserService.update(kpiUser);
        }

        ///
        ///  解散小组成员，恢复之前的委托信息
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // 将员工从Delegator级中移出
        groupService.removeFromDelegatorGroup(delegator, user);

        return ApiResult.isOk();
    }
}
