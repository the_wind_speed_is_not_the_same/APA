import Mantle from '../Mask/page.vue';
export default {
    props: ['from', 'to'],
    data() {
        return {
            fromYear: '',
            toYear: '',
            isShowMask: true,
            isShowMaskColor: false,
        }
    },
    components: {
        Mantle
    },
    computed: {
        isShowFromAdd() {
            return this.toYear - this.fromYear > 1;
        },
        isShowToAdd() {
            return this.toYear < new Date().getFullYear();
        },
    },
    methods: {
        changeYear(type, paramType) {
            if (type === 'add') {
                this.fromYear ++;
                this.toYear ++;
            } else if (type === 'minus') {
                this.fromYear --;
                this.toYear --;
            }
        },
        confirmDate() {
            this.$emit('confirmDate', this.fromYear, this.toYear);
        }
    },
    mounted() {
        this.fromYear = this.from;
        this.toYear = this.to;
    }
}