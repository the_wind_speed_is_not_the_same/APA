package cn.porsche.web.domain.view;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users_view", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class UserView implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",            insertable = false, updatable = false)
    private Integer id;

    @JsonProperty("code")
    @Column(name = "code",          insertable = false, updatable = false)
    private String code;

    @JsonProperty("department_id")
    @Column(name = "department_id", insertable = false, updatable = false)
    private Integer departmentId;

    @ApiModelProperty(notes = "部门名称")
    @JsonProperty("department")
    @Column(name = "department",    insertable = false, updatable = false)
    private String department;

    @ApiModelProperty(notes = "主管名称")
    @JsonProperty("manager")
    @Column(name = "manager",       insertable = false, updatable = false)
    private String manager;

    @ApiModelProperty(notes = "英文名称")
    @JsonProperty("en_name")
    @Column(name = "en_name",       insertable = false, updatable = false)
    private String enName;

    @ApiModelProperty(notes = "中文名称")
    @JsonProperty("cn_name")
    @Column(name = "cn_name",       insertable = false, updatable = false)
    private String cnName;
//
//    @ApiModelProperty(notes = "职位信息")
//    @JsonProperty("position")
//    @Column(name = "position",      insertable = false, updatable = false)
//    private String position;

    @ApiModelProperty(notes = "用户职称")
    @JsonProperty("title")
    @Column(name = "title",                     columnDefinition = "VARCHAR(255) NULL COMMENT '用户职称'")
    String title;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(notes = "入职时间")
    @JsonProperty("hire_date")
    @Column(name = "hire_date",     insertable = false, updatable = false)
    private Date hireDate;

    @ApiModelProperty(notes = "邮件地址")
    @JsonProperty("email")
    @Column(name = "email",         insertable = false, updatable = false)
    private String email;

    @ApiModelProperty(notes = "是否需要手动设置KPI")
    @JsonProperty("need_hands")
    @Column(name = "need_hands",    insertable = false, updatable = false)
    private boolean needHands;

    @ApiModelProperty(notes = "电话号码")
    @JsonProperty("tel")
    @Column(name = "tel",           insertable = false, updatable = false)
    private String tel;
}
