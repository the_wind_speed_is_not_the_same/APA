package cn.porsche.compliance.domain;

import cn.porsche.compliance.constant.EmployeeTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Table(name = "users", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @ApiModelProperty(notes = "主键")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @JsonIgnore
    @ApiModelProperty(notes = "职位主键")
    @Column(name = "position_id",               columnDefinition = "INT UNSIGNED NULL COMMENT '职位描述'")
    Integer positionId;

    @JsonIgnore
    @ApiModelProperty(notes = "部门主键")
    @Column(name = "department_id",             columnDefinition = "INT UNSIGNED NULL DEFAULT NULL COMMENT '部门主键'")
    Integer departmentId;

    @JsonIgnore
    @ApiModelProperty(notes = "上级主管主键")
    @Column(name = "manager_id",                columnDefinition = "INT UNSIGNED NULL DEFAULT NULL COMMENT '上级主管主键'")
    Integer managerId;

    @ApiModelProperty(notes = "员工类别：VP，STAFF，OUTER")
    @Enumerated(EnumType.STRING)
    @JsonProperty("type")
    @Column(name = "type",                      columnDefinition = "CHAR(10) NOT NULL COMMENT '员工类别：VP，STAFF，OUTER'")
    EmployeeTypeEnum type;

    @JsonIgnore
    @ApiModelProperty(notes = "是否为经理")
    @Column(name = "is_manager",                columnDefinition = "TINYINT(1) UNSIGNED NULL COMMENT '是否为经理'")
    Boolean isManager;

    @JsonIgnore
    @ApiModelProperty(notes = "外部系统中的员工代码")
    @Column(name = "code",                      columnDefinition = "CHAR(32) NOT NULL COMMENT '外部系统中的员工代码'")
    String code;

    @ApiModelProperty(notes = "用户头像")
    @JsonProperty("avatar")
    @Column(name = "avatar",                    columnDefinition = "VARCHAR(1023) NULL DEFAULT NULL COMMENT '用户头像'")
    String avatar;

    @JsonIgnore
    @ApiModelProperty(notes = "姓")
    @Column(name = "first_name",                columnDefinition = "VARCHAR(255) NOT NULL COMMENT '姓'")
    String firstName;

    @JsonIgnore
    @ApiModelProperty(notes = "名")
    @JsonProperty("last_name")
    @Column(name = "last_name",                 columnDefinition = "VARCHAR(255) NOT NULL COMMENT '名'")
    String lastName;

    @ApiModelProperty(notes = "中文名称")
    @JsonProperty("cn_name")
    @Column(name = "cn_name",                   columnDefinition = "VARCHAR(255) NOT NULL COMMENT '中文名称'")
    String cnName;

    @ApiModelProperty(notes = "英文名称")
    @JsonProperty("en_name")
    @Column(name = "en_name",                   columnDefinition = "VARCHAR(255) NOT NULL COMMENT '英文名称'")
    String enName;

    @ApiModelProperty(notes = "用户职称")
    @JsonProperty("title")
    @Column(name = "title",                     columnDefinition = "VARCHAR(255) NULL COMMENT '用户职称'")
    String title;

    @JsonIgnore
    @ApiModelProperty(notes = "用户邮箱")
    @Column(name = "email",                     columnDefinition = "VARCHAR(255) NULL COMMENT '用户邮箱'")
    String email;

    @JsonIgnore
    @ApiModelProperty(notes = "手机号码")
    @Column(name = "mobile",                    columnDefinition = "VARCHAR(255) NULL COMMENT '手机号码'")
    String mobile;

    @JsonIgnore
    @ApiModelProperty(notes = "电话号码")
    @Column(name = "tel",                       columnDefinition = "VARCHAR(255) NULL COMMENT '电话号码'")
    String tel;

    @JsonIgnore
    @ApiModelProperty(notes = "数据状态")
    @Column(name = "status",                    columnDefinition = "SMALLINT UNSIGNED NOT NULL DEFAULT '0' COMMENT '数据状态'", insertable = false)
    Integer status;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
