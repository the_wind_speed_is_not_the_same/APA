package cn.porsche.web.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.common.util.JsonUtils;
import cn.porsche.web.ApplicationTest;
import cn.porsche.web.controller.params.DelegatorParam;
import cn.porsche.web.domain.Comment;
import cn.porsche.web.service.LoginService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class DelegatorControllerTest extends ApplicationTest {
    private static final String API = "/delegator";

    private String token;

    @Autowired
    private MockMvc mvc;

    private MvcResult mvcResult;


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {

    }
//
//    @Test
//    void updateWhat() {
//        final String api = API + "/update_what";
//
//        WhatParams params = new WhatParams();
//        params.setFunctionId(getFunctionId());
//        params.setEmployeeId(getEmployeeId());
//
//        params.setName("My first traget  Teams set their own objectives");
//        params.setDesc("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. ");
//        params.setWeight(40);
//
//        try {
//            mvcResult = mvc.perform(
//                    post(api)
//                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
//                            .contentType(MediaType.APPLICATION_JSON)
//                            .content(JsonUtils.toString(params, true))
//                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
//            ).andExpect(
//                    status().is2xxSuccessful()
//            ).andExpect(
//                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
//            ).andDo(
//                    print()
//            ).andReturn();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Test
    void getWhats() {
        final String api = API + "/get_whats";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("employee_id", String.valueOf(getEmployeeId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void confirm() {
        final String api = API + "/confirm";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("employee_id", String.valueOf(getEmployeeId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void addOAComment() {
        final String api = API + "/add_oa_comment";

        Comment comment;
        comment = Comment.builder()
                .functionId(getFunctionId())
                .employeeId(getEmployeeId())
                .message("Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.")
                .build();

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(JsonUtils.toString(comment, true))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void getOAComments() {
        final String api = API + "/get_oa_comments";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getDelegatorAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("employee_id", String.valueOf(getEmployeeId()))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void delegatorGroup() {
        final String api = API + "/delegator_group";

        List<Integer> ids = new ArrayList<>();
        ids.add(505098);
        ids.add(501099);
        ids.add(501176);
        ids.add(501265);

        DelegatorParam param = new DelegatorParam();
        param.setDelegatorId(501110);
        param.setFunctionId(getFunctionId());
        param.setEmployeeIds(ids);

        try {
            mvcResult = mvc.perform(
                    post(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonUtils.toString(param))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void delegateDismiss() {
        final String api = API + "/delegator_dismiss";

        try {
            mvcResult = mvc.perform(
                    get(api)
                            .header(LoginService.AUTH_NAME, getManagerAuthorization())
                            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                            .param("function_id", String.valueOf(getFunctionId()))
                            .param("delegator_id", String.valueOf(501110))
                            .accept(MediaType.APPLICATION_JSON)                 // 接受的格式为JSON
            ).andExpect(
                    status().is2xxSuccessful()
            ).andExpect(
                    MockMvcResultMatchers.jsonPath("code").value(ApiResult.RootSuccessCode)
            ).andDo(
                    print()
            ).andReturn();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}