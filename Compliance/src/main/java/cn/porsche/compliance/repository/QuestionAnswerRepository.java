package cn.porsche.compliance.repository;

import cn.porsche.compliance.domain.QuestionAnswer;
import cn.porsche.compliance.emnu.SeasonEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionAnswerRepository extends CrudRepository<QuestionAnswer, Integer> {
    List<QuestionAnswer> findAllByYearAndSeason(Integer year, Integer season);

    List<QuestionAnswer> deleteAllByUserIdAndYearAndSeason(Integer userId, Integer year, SeasonEnum season);

    @Query(nativeQuery = true, value = "SELECT SUM(qa.score) FROM question_answer as qa WHERE qa.user_id=:userId AND qa.year=:year AND qa.season=:season AND qa.right=true")
    Integer sumScoreByUserAndYearAndSeason(@Param("userId")Integer userId, @Param("year")int year, @Param("season")String season);

    List<QuestionAnswer> findAllByUserIdAndYearAndSeason(Integer userId, Integer year, SeasonEnum season);
}
