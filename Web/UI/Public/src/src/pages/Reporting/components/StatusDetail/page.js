import { request } from '../../../../common/request.js';
import {mapState} from 'vuex'
import { downloadFile } from '../../../../common/utils.js';
import Confirm from '../../../../components/Confirm/page.vue';

export default {
  props:{
    query:{
      type:Object
    },
    function_id:{
      type: Number | String,
      required: true
    }
  },
  data() {
    return {
      tableData:[],
      tableHeight:412,
      departmentent:'',
      totalRecord:0,
      pageSize:25,
      pageIndex:1,
      totalPage:0,
      confirmTxt : '',
      titleTxt : 'Error',
      isShowConfirm: false
    }
  },
  watch:{
    'function_id':function(val){
      localStorage.removeItem('reportingStatus')
      localStorage.removeItem('totalCount')
      localStorage.removeItem('currentData')
      this.getData()
    }
  },
  components:{
      Confirm
  },
  computed:{
    ...mapState({
      department_id: (state) => state.allState.reportingDepartmentId
    }),
    pageNumbers(){
      return this.totalRecord?Math.ceil(this.totalRecord/this.pageSize):0
    }
  },
  methods: {
    goBack(){
      this.$emit('changeComponent',{page:'parent',query:this.query})
    },
    confirm(){
      this.isShowConfirm = false
    },
    getData(){
      request({
        url:'getEmployeeByDepartment',
        method:'get',
        params:{
          department_id:this.department_id,
          function_id:this.function_id,
          page:this.pageIndex,
          size:this.pageSize
        },
        isShowLoading:true
      }).then(res=>{
		  if (!res || res.code !== 0) {
		      this.confirmTxt =  res.message;
		      this.titleTxt = 'Error';
		      this.isShowConfirm = true;

		      return ;
		  }

        if(res && res.data){
          let data = res.data
          this.departmentent = data.department.name
          let tableData = []
          if(data.employee.length > 0){
            data.employee.forEach((emp,idx)=>{
              tableData[idx] = emp
              data.targets.forEach((tar)=>{
                if(emp.target_id === tar.id){
                  tableData[idx] = Object.assign(emp,tar)
                }
              })
            })
          }

          this.tableData = tableData
          this.totalRecord = data.total_size
        }

      })
    },
    pageChange(e){
      this.pageIndex = e
      this.getData()
    },
    exportAllFile(){
        request({
            url:'downloadTSByDReporting',
            method:'download',
            params:{
                function_id:this.function_id,
                department_id:this.department_id
            }
        }).then((res)=>{
			if (!res || res.code !== 0) {
			    this.confirmTxt =  res.message;
			    this.titleTxt = 'Error';
			    this.isShowConfirm = true;

			    return ;
			}

            if(res && res.data){
                downloadFile(res.data)
            }
        })
    }
  },
  created() {
    // console.log(this.function_id)
    this.getData()
  },
  mounted(){
    // this.$nextTick(()=>{
    //   this.tableHeight = this.$refs.tableWrapper.offsetHeight - 10
    // })
  }
}
