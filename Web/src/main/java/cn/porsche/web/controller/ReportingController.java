package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.controller.response.RatingReporting;
import cn.porsche.web.controller.response.StatusReporting;
import cn.porsche.web.controller.response.reporting.DepartmentStatus;
import cn.porsche.web.domain.*;
import cn.porsche.web.service.DepartmentReportService;
import cn.porsche.web.service.DepartmentService;
import cn.porsche.web.service.FunctionService;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/reporting")
@ApiModel(value = "/reporting", description = "员工相关的Target操作类")
public class ReportingController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${app.company.code}")
    String defaultCode;

    @Autowired
    FunctionService functionService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    DepartmentReportService reportService;


    @ApiOperation(
            value = "【HR】在【Reporting】Status 的接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = StatusReporting.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/ts_status")
    public String tsStatus(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "code", required = false) String code
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (StringUtils.isEmpty(code)) {
            code = defaultCode;
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        Map<Integer, DepartmentReport> reports = new HashMap<>();
        Department parent, department;

        // 得到全局的统计数据
        int topId;
        boolean isHR = groupService.inHRTeamGroup(user);
        parent       = isHR ? departmentService.findByCode(code) : departmentService.findById(user.getDepartmentId());
        reports.put(topId = parent.getId(), reportService.findByDepartment(function.getId(), parent.getId()));

        DepartmentTree tree = departmentService.buildTree(parent);
        Iterator<DepartmentTree> iterator = tree.getSubTree().iterator();
        while (iterator.hasNext()) {
            DepartmentTree subTree = iterator.next();
            department = subTree.getDepartment();

            reports.put(department.getId(), reportService.findByDepartment(function.getId(), department.getId()));

            for (DepartmentTree subItem : subTree.getSubTree()) {
                department = subItem.getDepartment();

                reports.put(department.getId(), reportService.findByDepartment(function.getId(), department.getId()));
            }
        }

        Map<String, Object> json = new HashMap<>();
        json.put("departments", tree.getSubTree());
        json.put("parent", parent);
        json.put("statistic", reports);
        json.put("top_id", topId);

        return ApiResult.builder().data(json).toJson();
    }


    @ApiOperation(
            value = "【HR】或【Top Manager】在【Reporting】Status 的下载接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = StatusReporting.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/ts_status_down")
    public void tsStatusDown(
            HttpServletResponse response,
            @RequestParam(value = "function_id") Integer functionId

    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ;
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ;
        }

        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFCellStyle border = workbook.createCellStyle();
        border.setBorderBottom(BorderStyle.THIN);
        border.setBorderLeft(BorderStyle.THIN);
        border.setBorderTop(BorderStyle.THIN);
        border.setBorderRight(BorderStyle.THIN);

        Row row;
        Cell cell;
        DepartmentReport report;
        boolean isHR = groupService.inHRTeamGroup(user);
        List<Department> tops = departmentService.findTopDepartments();
        for (Department department : tops) {
            int rowNum = 0, colNum = 0;
            XSSFSheet sheet = workbook.createSheet(department.getName());

            row = sheet.createRow(rowNum ++);
            cell = row.createCell(colNum ++);
            cell.setCellValue("Department");
            cell.setCellStyle(border);


            cell = row.createCell(colNum ++);
            cell.setCellValue("Targets");
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue("APA");
            cell.setCellStyle(border);


            report = reportService.findByDepartment(functionId, department.getId());
            if (null == report) {
                report = DepartmentReport.builder()
                        .total(0)
                        .confirmedTS(0)
                        .confirmedAPA(0)
                        .build();
            }

            // Department       Targets     APA
            // Porsche China    1/4         0/4（生成此列）
            row = sheet.createRow(rowNum ++);

            colNum = 0;

            // 部门名称
            cell = row.createCell(colNum ++);
            cell.setCellValue(department.getName());
            cell.setCellStyle(border);

            // Targets
            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(report.getConfirmedTS() + "/" + report.getTotal());

            // APA
            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(report.getConfirmedAPA() + "/" + report.getTotal());


            DepartmentTree tree = departmentService.buildTree(department);
            Iterator<DepartmentTree> iterator = tree.getSubTree().iterator();
            while (iterator.hasNext()) {
                DepartmentTree subTree = iterator.next();
                department = subTree.getDepartment();
                if (!isHR && !department.getId().equals(user.getDepartmentId())) {
                    iterator.remove();

                    continue;
                }

                report = reportService.findByDepartment(functionId, department.getId());
                if (null == report) {
                    report = DepartmentReport.builder()
                            .total(0)
                            .confirmedTS(0)
                            .confirmedAPA(0)
                            .build();
                }

                // Department       Targets     APA
                // Porsche China    1/4         0/4
                //      PCN CFO     3/4         0/4（生成此列）
                row = sheet.createRow(rowNum ++);

                colNum = 0;

                // 部门名称
                cell = row.createCell(colNum ++);
                cell.setCellValue("      " + department.getName());
                cell.setCellStyle(border);

                // Targets
                cell = row.createCell(colNum ++);
                cell.setCellStyle(border);
                cell.setCellValue(report.getConfirmedTS() + "/" + report.getTotal());

                // APA
                cell = row.createCell(colNum ++);
                cell.setCellStyle(border);
                cell.setCellValue(report.getConfirmedAPA() + "/" + report.getTotal());


                for (DepartmentTree subItem : subTree.getSubTree()) {
                    // 有子部门
                    department = subItem.getDepartment();
                    report = reportService.findByDepartment(functionId, department.getId());
                    if (null == report) {
                        report = DepartmentReport.builder()
                                .total(0)
                                .confirmedTS(0)
                                .confirmedAPA(0)
                                .build();
                    }

                    // Department                               Targets     APA
                    // Porsche China                            1/4         0/4
                    //      PCN CFO                             3/4         0/4
                    //           Market Controlling Unit        3/9         0/9（生成此列）
                    row = sheet.createRow(rowNum ++);

                    colNum = 0;

                    // 部门名称
                    cell = row.createCell(colNum ++);
                    cell.setCellValue("            " + department.getName());
                    cell.setCellStyle(border);

                    // Targets
                    cell = row.createCell(colNum ++);
                    cell.setCellStyle(border);
                    cell.setCellValue(report.getConfirmedTS() + "/" + report.getTotal());

                    // APA
                    cell = row.createCell(colNum ++);
                    cell.setCellStyle(border);
                    cell.setCellValue(report.getConfirmedAPA() + "/" + report.getTotal());
                }
            }

            // 设置样式
            sheet.setColumnWidth(0, 30 * 256);
            sheet.setColumnWidth(1, 12 * 256);
            sheet.setColumnWidth(2, 12 * 256);
        }

        response.setHeader("Content-Disposition", "attachment; filename=" + function.getYear() + " TargetSetting Status.xls");

        OutputStream out = null;
        try{
            out = new BufferedOutputStream(response.getOutputStream());
            workbook.write(out);

        } catch (IOException e) {
            System.out.println("excel导出有误");
        } finally {
            try {
                out.close();
                workbook.close();
            } catch (IOException e) {
                System.out.println("读取内容有误");
            }
        }
    }

    @ApiOperation(
            value = "【HR】在【Reporting】按部门查看用户的状态数据的接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = KPIUser.class, responseContainer = "List"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id",     required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "department_id",   required = true, dataTypeClass = Integer.class, value = "部门主键"),
            @ApiImplicitParam(name = "page",            required = false, dataTypeClass = Integer.class, value = "页数：默认1"),
            @ApiImplicitParam(name = "size",            required = false, dataTypeClass = Integer.class, value = "条数：默认25"),
    })
    @GetMapping(value = "/ts_by_department")
    public String department(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "department_id") Integer departmentId,
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "25") Integer size
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        // VP 只有看自己部门的数据
        if (!groupService.inHRTeamGroup(user)) {
            departmentId = user.getDepartmentId();
        }

        Department department = departmentService.findById(departmentId);
        if (null == department) {
            return ApiResult.isBad("Unable to find the Department data, please contact your HR to fix it");
        }

        Page<KPIUser> pageable = kpiUserService.findAllByDepartment(function, department, checkPage(page), checkSize(size));

        Map<String, Object> map = new HashMap<>();
        map.put("employee",     pageable.getContent());
        map.put("targets",      targetService.findAllByDepartment(function, department, checkPage(page), checkSize(size)).getContent());
        map.put("department",   department);
        map.put("page_size",    size);
        map.put("current_page", page);
        map.put("total_page",   pageable.getTotalPages());
        map.put("total_size",   pageable.getTotalElements());

        return ApiResult.builder().data(map).toJson();
    }



    @ApiOperation(
            value = "【HR】或【Top Manager】在【Reporting】按部门查看用户的状态数据的接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = KPIUser.class, responseContainer = "List"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id",     required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "department_id",   required = true, dataTypeClass = Integer.class, value = "部门主键"),
            @ApiImplicitParam(name = "page",            required = false, dataTypeClass = Integer.class, value = "页数：默认1"),
            @ApiImplicitParam(name = "size",            required = false, dataTypeClass = Integer.class, value = "条数：默认25"),
    })
    @GetMapping(value = "/ts_by_department_down")
    public void department(
            HttpServletResponse response,
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "department_id") Integer departmentId
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ;
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ;
        }

        // VP 只有看自己部门的数据
        if (!groupService.inHRTeamGroup(user)) {
            departmentId = user.getDepartmentId();
        }

        Department department = departmentService.findById(departmentId);
        if (null == department) {
            return ;
        }


        int rowNum = 0, colNum = 0;
        Row row;
        Cell cell;
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Department TargetSetting Detail");

        XSSFCellStyle border = workbook.createCellStyle();
        border.setBorderBottom(BorderStyle.THIN);
        border.setBorderLeft(BorderStyle.THIN);
        border.setBorderTop(BorderStyle.THIN);
        border.setBorderRight(BorderStyle.THIN);


        //     Development Name    Final Performance Rating
        row = sheet.createRow(rowNum ++);
        cell = row.createCell(0);
        cell.setCellValue(department.getName());
        cell.setCellStyle(border);

        cell = row.createCell(3);
        cell.setCellValue("Targets");
        cell.setCellStyle(border);

        cell = row.createCell(6);
        cell.setCellValue("Annual Performance Appraisal");
        cell.setCellStyle(border);

        row = sheet.createRow(rowNum ++);
        cell = row.createCell(colNum ++);
        cell.setCellValue("Employee ID");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Employee Name");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Disciplinary Manager");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Submitted by E");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Confirmed by M");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Confirmed by E");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Submitted by E");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Submitted by M");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Released by M");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Read by E");
        cell.setCellStyle(border);


        List<KPIUser> kUsers = kpiUserService.findAllByDepartment(function, department);
        List<Target> targets = targetService.findAllByDepartment(function, department);
        Map<Integer, Target> mapTargets = new HashMap<>();
        for (Target item : targets) {
            mapTargets.put(item.getId(), item);
        }


        final String Yes = "Y";
        final String No  = "N";
        for (KPIUser item : kUsers) {
            target = mapTargets.get(item.getTargetId());
            if (null == target) {
                continue;
            }

            colNum = 0;
            row = sheet.createRow(rowNum ++);

            cell = row.createCell(colNum ++);
            cell.setCellValue(item.getCode());
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue(item.getEnName());
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue(item.getManager());
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue(target.isTSESubmit() ? Yes : No);
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue(target.isTSDConfirm() ? Yes : No);
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue(target.isTSEConfirm() ? Yes : No);
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue(target.isAPAESubmit() ? Yes : No);
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue(target.isAPADSubmit() ? Yes : No);
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue(target.isReleaseSecond() ? Yes : No);
            cell.setCellStyle(border);

            cell = row.createCell(colNum ++);
            cell.setCellValue(target.isAgreeScore() ? Yes : No);
            cell.setCellStyle(border);
        }

        // 合并头两列
        sheet.addMergedRegion(new CellRangeAddress(0,0,0,2));
        sheet.addMergedRegion(new CellRangeAddress(0,0,3,5));
        sheet.addMergedRegion(new CellRangeAddress(0,0,6,9));

        sheet.setColumnWidth(0, 12 * 256);
        sheet.setColumnWidth(1, 25 * 256);
        sheet.setColumnWidth(2, 25 * 256);
        sheet.setColumnWidth(3, 14 * 256);
        sheet.setColumnWidth(4, 14 * 256);
        sheet.setColumnWidth(5, 14 * 256);
        sheet.setColumnWidth(6, 14 * 256);
        sheet.setColumnWidth(7, 14 * 256);
        sheet.setColumnWidth(8, 14 * 256);
        sheet.setColumnWidth(9, 14 * 256);

        response.setHeader("Content-Disposition", "attachment; filename=" + department.getName() + " Department TargetSetting Detail.xls");

        OutputStream out = null;
        try{
            out = new BufferedOutputStream(response.getOutputStream());
            workbook.write(out);

        } catch (IOException e) {
            System.out.println("excel导出有误");
        } finally {
            try {
                out.close();
                workbook.close();
            } catch (IOException e) {
                System.out.println("读取内容有误");
            }
        }
    }


    @ApiOperation(
            value = "【HR】在【Reporting】APA Result 的接口",
            notes = "统计每个部门的的分数"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = RatingReporting.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/apa_status")
    public String apaStatus(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "code", required = false) String code
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (StringUtils.isEmpty(code)) {
            code = defaultCode;
        }

        List<DepartmentReport> reports = new ArrayList<>();
        Department parent, department;

        // 得到全局的统计数据
        boolean isHR = groupService.inHRTeamGroup(user);
        parent = isHR ? departmentService.findByCode(code) : departmentService.findById(user.getDepartmentId());
        DepartmentTree tree = departmentService.buildTree(parent);
        reports.add(reportService.findByDepartment(function.getId(), tree.getDepartment().getId()));

        Iterator<DepartmentTree> iterator = tree.getSubTree().iterator();
        while (iterator.hasNext()) {
            DepartmentTree subTree = iterator.next();
            department = subTree.getDepartment();

            reports.add(reportService.findByDepartment(function.getId(), department.getId()));

            for (DepartmentTree subItem : subTree.getSubTree()) {
                department = subItem.getDepartment();

                reports.add(reportService.findByDepartment(function.getId(), department.getId()));
            }
        }

        return ApiResult.builder().list(reports).toJson();
    }

    @Deprecated
    @ApiOperation(
            value = "【HR】在【Reporting】APA Result 的接口",
            notes = "统计每个部门的的分数"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = RatingReporting.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/apa_status1")
    public String apaStatus1(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        List<DepartmentReport> reports = reportService.findAllByFunction(function);
        if (!groupService.inHRTeamGroup(user)) {
            // VP 只有看自己部门的数据
            Iterator<DepartmentReport> iterator = reports.iterator();
            while (iterator.hasNext()) {
                DepartmentReport item = iterator.next();

                if (!item.getDepartmentId().equals(user.getDepartmentId())) {
                    iterator.remove();
                }
            }
        }

        return ApiResult.builder().list(reports).toJson();
    }

    @ApiOperation(
            value = "【HR】或【Top Manager】在【Reporting】APA Result 的下载接口",
            notes = "统计每个部门的的分数"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = RatingReporting.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/apa_status_down")
    public void apaStatusDown(
            HttpServletResponse response,
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ;
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ;
        }

        List<Department> tops = departmentService.findTopDepartments();

        int rowNum = 0, colNum = 0;
        Row row;
        Cell cell;
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Department APA Status");

        XSSFCellStyle border = workbook.createCellStyle();
        border.setBorderBottom(BorderStyle.THIN);
        border.setBorderLeft(BorderStyle.THIN);
        border.setBorderTop(BorderStyle.THIN);
        border.setBorderRight(BorderStyle.THIN);

        //     Pre-calibration Rating    Final Performance Rating
        row = sheet.createRow(rowNum ++);
        cell = row.createCell(2);
        cell.setCellValue("Pre-calibration Rating");
        cell.setCellStyle(border);

        cell = row.createCell(5);
        cell.setCellValue("Final Performance Rating");
        cell.setCellStyle(border);

        row = sheet.createRow(rowNum ++);
        cell = row.createCell(colNum ++);
        cell.setCellValue("Department");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Disciplinary Manager");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Performance Level");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Distribution%");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Distribution#");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Performance Level");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Distribution%");
        cell.setCellStyle(border);

        cell = row.createCell(colNum ++);
        cell.setCellValue("Distribution#");
        cell.setCellStyle(border);


        final String TOP        = "A";
        final String Above      = "B";
        final String Average    = "C";
        final String Below      = "D";
        final String Fail       = "E";

        List<DepartmentReport> list = reportService.findAllByFunction(function);
        if (!groupService.inHRTeamGroup(user)) {
            // VP 只有看自己部门的数据
            Iterator<DepartmentReport> iterator = list.iterator();
            while (iterator.hasNext()) {
                DepartmentReport item = iterator.next();

                if (!item.getDepartmentId().equals(user.getDepartmentId())) {
                    iterator.remove();
                }
            }
        }


        for (DepartmentReport item : list) {
            // TOP
            colNum = 0;
            row = sheet.createRow(rowNum ++);
            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getDepartmentName());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getDepartmentManager());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(TOP);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreARating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreBRating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(TOP);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalARating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalATotal());

            // Above
            colNum = 2;
            row = sheet.createRow(rowNum ++);
            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(Above);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreBRating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreBTotal());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(Above);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalBRating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalBTotal());


            // Average
            colNum = 2;
            row = sheet.createRow(rowNum ++);
            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(Average);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreCRating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreCTotal());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(Average);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalCRating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalCTotal());

            // Below
            colNum = 2;
            row = sheet.createRow(rowNum ++);
            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(Below);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreDRating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreDTotal());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(Below);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalDRating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalDTotal());


            // Fail
            colNum = 2;
            row = sheet.createRow(rowNum ++);
            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(Fail);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreERating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getPreETotal());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(Fail);

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalERating());

            cell = row.createCell(colNum ++);
            cell.setCellStyle(border);
            cell.setCellValue(item.getFinalETotal());
        }


        // 合并头两列
        sheet.addMergedRegion(new CellRangeAddress(0,0,0,1));
        sheet.addMergedRegion(new CellRangeAddress(0,0,2,4));
        sheet.addMergedRegion(new CellRangeAddress(0,0,5,7));

        sheet.setColumnWidth(0, 25 * 256);
        sheet.setColumnWidth(1, 25 * 256);
        sheet.setColumnWidth(2, 20 * 256);
        sheet.setColumnWidth(3, 12 * 256);
        sheet.setColumnWidth(4, 12 * 256);
        sheet.setColumnWidth(5, 20 * 256);
        sheet.setColumnWidth(6, 12 * 256);
        sheet.setColumnWidth(7, 12 * 256);

        response.setHeader("Content-Disposition", "attachment; filename=" + function.getYear() + " Department APA Status.xls");

        OutputStream out = null;
        try{
            out = new BufferedOutputStream(response.getOutputStream());
            workbook.write(out);

        } catch (IOException e) {
            System.out.println("excel导出有误");
        } finally {
            try {
                out.close();
                workbook.close();
            } catch (IOException e) {
                System.out.println("读取内容有误");
            }
        }
    }

    @ApiOperation(
            value = "【HR】在【Reporting】按部门查看用户的状态数据的接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = KPIUser.class, responseContainer = "List"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id",     required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "department_id",   required = true, dataTypeClass = Integer.class, value = "部门主键"),
            @ApiImplicitParam(name = "page",            required = false, dataTypeClass = Integer.class, value = "页数：默认1"),
            @ApiImplicitParam(name = "size",            required = false, dataTypeClass = Integer.class, value = "条数：默认25"),
    })
    @GetMapping(value = "/apa_by_department")
    public String apaByDepartment(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "department_id") Integer departmentId,
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "25") Integer size
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        // VP 只有看自己部门的数据
        if (!groupService.inHRTeamGroup(user)) {
            departmentId = user.getDepartmentId();
        }

        Department department = departmentService.findById(departmentId);
        if (null == department) {
            return ApiResult.isBad("Unable to find the Department data, please contact your HR to fix it");
        }

        List employees = new ArrayList<>();
        List<KPIUser> kUsers = kpiUserService.findAllByDepartment(function, department);
        for (KPIUser kUser : kUsers) {
            target = targetService.get(kUser.getTargetId());

            Map<String, Object> employee = new HashMap<>();
            employee.put("employee_id", kUser.getCode());
            employee.put("employee_name", kUser.getEnName());
            employee.put("manager", kUser.getManager());


            Map<String, Object> calibration;
            calibration = new HashMap<>();
            calibration.put("level", target.getFirstScoreLevel());
            calibration.put("score", target.getFirstTotalScore());
            calibration.put("what_rate", target.getFirstWhatRate());

            employee.put("pre_calibration", calibration);

            calibration = new HashMap<>();
            calibration.put("level", target.getSecondScoreLevel());
            calibration.put("score", target.getSecondTotalScore());
            calibration.put("what_rate", target.getSecondWhatRate());

            employee.put("final_calibration", calibration);

            employees.add(employee);
        }

        return ApiResult.builder().list(employees).toJson();
    }

    @ApiOperation(
            value = "【HR】或【Top Manager】在【Reporting】按部门下载用户的状态数据的接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = KPIUser.class, responseContainer = "List"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id",     required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "department_id",   required = true, dataTypeClass = Integer.class, value = "部门主键"),
    })
    @GetMapping(value = "/apa_by_department_down")
    public void apaByDepartmentDown(
            HttpServletResponse response,
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "department_id") Integer departmentId
    ) {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ;
        }

//        Function function = functionService.findById(functionId);
//        if (null == function) {
//            return ;
//        }
//
//        // VP 只有看自己部门的数据
//        if (!groupService.inHRTeamGroup(user)) {
//            departmentId = user.getDepartmentId();
//        }
//
//        Department department = departmentService.findById(departmentId);
//        if (null == department) {
//            return ;
//        }
//
//
//        int rowNum = 0, colNum = 0;
//        Row row;
//        Cell cell;
//        XSSFWorkbook workbook = new XSSFWorkbook();
//        XSSFSheet sheet = workbook.createSheet("Department APA Detail");
//
//        XSSFCellStyle border = workbook.createCellStyle();
//        border.setBorderBottom(BorderStyle.THIN);
//        border.setBorderLeft(BorderStyle.THIN);
//        border.setBorderTop(BorderStyle.THIN);
//        border.setBorderRight(BorderStyle.THIN);
//
//        //     Pre-calibration Rating    Final Performance Rating
//        row = sheet.createRow(rowNum ++);
//        cell = row.createCell(0);
//        cell.setCellValue(department.getName());
//        cell.setCellStyle(border);
//
//        cell = row.createCell(3);
//        cell.setCellValue("Pre-calibration Rating");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(7);
//        cell.setCellValue("Final Performance Rating");
//        cell.setCellStyle(border);
//
//        row = sheet.createRow(rowNum ++);
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Employee ID");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Employee Name");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Disciplinary Manager");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Performance Level（How）");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Appraisal Result");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Individual Bonus Rate");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Performance Level（How）");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Appraisal Result");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Achievement Rate（What）");
//        cell.setCellStyle(border);
//
//        cell = row.createCell(colNum ++);
//        cell.setCellValue("Individual Bonus Rate");
//        cell.setCellStyle(border);
//
//
//
//        List<KPIUser> kUsers = kpiUserService.findAllByDepartment(function, department);
//        List<Target> targets = targetService.findAllByDepartment(function, department);
//        Map<Integer, Target> mapTargets = new HashMap<>();
//        for (Target item : targets) {
//            mapTargets.put(item.getId(), item);
//        }
//
//
//        for (KPIUser item : kUsers) {
//            target = mapTargets.get(item.getTargetId());
//            if (null == target) {
//                continue;
//            }
//
//            colNum = 0;
//            row = sheet.createRow(rowNum ++);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(item.getEmployeeId());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(item.getEnName());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(item.getManager());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(null == target.getFirstHowLevel() ? "" : target.getFirstHowLevel().toString());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(null == target.getFirstWhatScore() ? 0f : target.getFirstWhatScore());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(null == target.getFirstWhatBonusRate() ? 0f : target.getFirstWhatBonusRate());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(null == target.getFirstFinalBonusRate() ? 0f : target.getFirstFinalBonusRate());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(null == target.getSecondHowLevel() ? "" : target.getSecondHowLevel().toString());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(null == target.getSecondWhatScore() ? 0f : target.getSecondWhatScore());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(null == target.getSecondWhatBonusRate() ? 0f : target.getSecondWhatBonusRate());
//            cell.setCellStyle(border);
//
//            cell = row.createCell(colNum ++);
//            cell.setCellValue(null == target.getSecondFinalBonusRate() ? 0f : target.getSecondFinalBonusRate());
//            cell.setCellStyle(border);
//        }
//
//        // 合并头两列
//        sheet.addMergedRegion(new CellRangeAddress(0,0,0,2));
//        sheet.addMergedRegion(new CellRangeAddress(0,0,3,6));
//        sheet.addMergedRegion(new CellRangeAddress(0,0,7,10));
//
//        sheet.setColumnWidth(0, 25 * 256);
//        sheet.setColumnWidth(1, 25 * 256);
//        sheet.setColumnWidth(2, 20 * 256);
//        sheet.setColumnWidth(3, 22 * 256);
//        sheet.setColumnWidth(4, 12 * 256);
//        sheet.setColumnWidth(5, 20 * 256);
//        sheet.setColumnWidth(6, 22 * 256);
//        sheet.setColumnWidth(7, 12 * 256);
//        sheet.setColumnWidth(8, 12 * 256);
//        sheet.setColumnWidth(9, 12 * 256);
//        sheet.setColumnWidth(10, 12 * 256);
//
//        response.setHeader("Content-Disposition", "attachment; filename=" + function.getYear() + " Department APA Detail.xls");
//
//        OutputStream out = null;
//        try{
//            out = new BufferedOutputStream(response.getOutputStream());
//            workbook.write(out);
//
//        } catch (IOException e) {
//            System.out.println("excel导出有误");
//        } finally {
//            try {
//                out.close();
//                workbook.close();
//            } catch (IOException e) {
//                System.out.println("读取内容有误");
//            }
//        }
    }

    @ApiOperation(
            value = "【HR】在【Reporting】显示多个所属部门的接口",
            notes = "多个部门的接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = RatingReporting.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/companies")
    public String getCompany() {
        if (noPromise(ModuleNameEnum.Dashboard, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        boolean isHR = groupService.inHRTeamGroup(user);
        List<Department> list = departmentService.findTopDepartments().stream().filter(item -> isHR || item.getId() == 10).collect(Collectors.toList());

        return ApiResult.builder().list(list).toJson();
    }
}
