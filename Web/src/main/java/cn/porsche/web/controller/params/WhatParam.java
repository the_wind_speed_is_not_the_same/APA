package cn.porsche.web.controller.params;

import cn.porsche.web.domain.Comment;
import cn.porsche.web.domain.TargetKPI;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class WhatParam {
    @ApiModelProperty(notes = "阶段主键")
    @JsonProperty("function_id")
    Integer functionId;

    @ApiModelProperty(notes = "TS主键")
    @JsonProperty("target_id")
    Integer targetId;

    @ApiModelProperty(notes = "用户主键")
    @JsonProperty("empolyee_id")
    Integer employeeId;

    @ApiModelProperty(notes = "What主键")
    @JsonProperty("what_id")
    Integer whatId;

    @ApiModelProperty(notes = "名称")
    @JsonProperty("name")
    String name;

    @ApiModelProperty(notes = "描述")
    @JsonProperty("desc")
    String desc;

    @ApiModelProperty(notes = "权重")
    @JsonProperty("weight")
    Integer weight;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(notes = "结束时间")
    @JsonProperty("finish_date")
    Date finishDate;

    @ApiModelProperty(notes = "得分")
    @JsonProperty("employee_score")
    Float employeeScore;

    @ApiModelProperty(notes = "打分时的评语")
    @JsonProperty("employee_comment")
    String employeeComment;

    @ApiModelProperty(notes = "拒绝时的评语")
    @JsonProperty("reject_comment")
    String rejectComment;

    @ApiModelProperty(notes = "第一次得分")
    @JsonProperty("first_score")
    Float firstScore;

    @ApiModelProperty(notes = "第二次得分")
    @JsonProperty("second_score")
    Float secondScore;

    @ApiModelProperty(notes = "员工是否确认")
    @JsonProperty("is_employee_confirm")
    boolean employeeConfirm;

    @ApiModelProperty(notes = "经理是否确认")
    @JsonProperty("is_delegator_confirm")
    boolean delegatorConfirm;

    @ApiModelProperty(notes = "员工红点")
    @JsonProperty("employee_red")
    boolean employeeRed;

    @ApiModelProperty(notes = "代理人红点")
    @JsonProperty("delegator_red")
    boolean delegatorRed;

    @ApiModelProperty(notes = "KPI列表")
    @JsonProperty("kpis")
    List<TargetKPI> kpis;

    @ApiModelProperty(notes = "KPI列表")
    @JsonProperty("ts_comments")
    List<Comment> TSComments;

    @ApiModelProperty(notes = "KPI列表")
    @JsonProperty("apa_comments")
    List<Comment> apaComments;
}
