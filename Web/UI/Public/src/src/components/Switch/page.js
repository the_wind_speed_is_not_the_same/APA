import { request } from '../../common/request.js';
import Confirm from '../Confirm/page.vue';

export default {
    props: {
      query:{
        type:Object,
        required:true
      },
      active:{
        type:Boolean,
        default(){return false}
      },
      className:{
        type:String
      },
      width:{
        type:Number | String,
        default(){return '100%'}
      },
      height:{
        type:Number | String,
        default(){return '6px'}
      },
      activeColor:{
        type:String,
        default(){return '#90CD99'}
      },
      inactiveColor:{
        type:String,
        default(){return '#D2D2D2'}
      }
    },
    data() {
        return {
          activeBar:this.active,
          confirmTxt:'success to set the authorization!',
          confirmTitle:'success',
          isShowConfirm:false,
        }
    },
    components:{
      Confirm
    },
    computed: {

    },
    methods: {
      async changeValue(){
        let { group_id, module_id, type } = this.query
        const groups = ['HRPerformanceTeam','DisciplinaryManager','Employee','TopManagement','NewEmployeesInProbation']
        const modules = ['TargetSettingAndUpdate','TargetsReview','MarkSelfScoreAndComments','PreCalibrationScore','CommentsAndFinalScoreUpdates','StatusTrackingOverallAndDetail','FinalScore','Dashboard']
        try{
          const res = await request({
              url: 'roleUpdate',
              method: 'get',
              params:{
                group:groups[group_id - 1],
                module:modules[module_id-1],
                operator:type.toUpperCase(),
                turn:!this.activeBar
              },
              isShowLoading: true
          });

          if(res.code === 0){
            this.activeBar = !this.activeBar;
            this.confirmTxt = 'success to set the authorization!';
            this.confirmTitle = 'success';
            this.isShowConfirm = true;
            setTimeout(()=>{
              this.isShowConfirm = false
            },1500)
          }else{
            this.confirmTxt = 'Fail to set the authorization!';
            this.confirmTitle = 'Failed';
            this.isShowConfirm = true
            setTimeout(()=>{
              this.isShowConfirm = false
            },1500)
          }
        }catch(e){
          console.log(e)
        }
      },
      confirm(){
        this.isShowConfirm = false
      },
      cancel(){

      }
    },
    mounted() {

    }
}
