import Qs from 'qs'
import { hostUrl} from '../../config/request.config'

export default{
	url: '', //是用于请求的服务器 URL
	baseURL: hostUrl,
	// method:'POST',
	// `transformRequest` 允许在向服务器发送前，修改请求数据
	// 只能用在 'PUT', 'POST' 和 'PATCH' 这几个请求方法
	// 后面数组中的函数必须返回一个字符串，或 ArrayBuffer，或 Stream
	transformRequest:function(param){
		return JSON.stringify(param)
	},
	// `transformResponse` 在传递给 then/catch 前，允许修改响应数据
	transformResponse: [function (data) {
		return data;
	}],
  	// `headers` 是即将被发送的自定义请求头
	headers: {'Content-Type':'application/json'}, 
	// `params` 是即将与请求一起发送的 URL 参数
  	// 必须是一个无格式对象(plain object)或 URLSearchParams 对象
	params:{},
	// `paramsSerializer` 是一个负责 `params` 序列化的函数
	paramsSerializer:function(params){
		return Qs.stringify(params)
	},
	// `data` 是作为请求主体被发送的数据
	// 只适用于这些请求方法 'PUT', 'POST', 和 'PATCH'
	data:{}, 
	// `timeout` 指定请求超时的毫秒数(0 表示无超时时间)
	// 如果请求话费了超过 `timeout` 的时间，请求将被中断
	timeout: 10000,
	// `withCredentials` 表示跨域请求时是否需要使用凭证
	withCredentials: false, // default
	// `responseType` 表示服务器响应的数据类型，
	responseType: 'json', // default
	// `validateStatus` 定义对于给定的HTTP 响应状态码是 resolve 或 reject  promise 。
	validateStatus: function (status) {
	    return status >= 200 && status < 300; // default
	}
}
