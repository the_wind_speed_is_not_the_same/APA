package cn.porsche.web.domain;

import cn.porsche.web.bounds.constant.LevelEnum;
import cn.porsche.web.constant.ScoreLevelEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "targets", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Target implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'", updatable = false)
    private Integer id;

    @ApiModelProperty(notes = "KPI主键")
    @JsonProperty("function_id")
    @Column(name = "function_id",               columnDefinition = "INT UNSIGNED NOT NULL COMMENT 'KPI主键'", updatable = false)
    Integer functionId;

    @ApiModelProperty(notes = "员工是否已提交KPI")
    @JsonProperty("is_ts_employee_submit")
    @Column(name = "is_ts_employee_submit",     columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '员工是否已提交KPI'")
    boolean TSESubmit;

    @ApiModelProperty(notes = "员工是否已全部Confirm")
    @JsonProperty("is_ts_employee_confirm")
    @Column(name = "is_ts_employee_confirm",    columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '员工是否已全部Confirm'")
    boolean TSEConfirm;

    @ApiModelProperty(notes = "小组长是否已全部Confirm")
    @JsonProperty("is_ts_delegator_confirm")
    @Column(name = "is_ts_delegator_confirm",   columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '员工是否提交Target'")
    boolean TSDConfirm;

    @ApiModelProperty(notes = "员工是否已提交KPI")
    @JsonProperty("is_apa_employee_submit")
    @Column(name = "is_apa_employee_submit",    columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '员工是否已提交KPI'")
    boolean APAESubmit;

    @ApiModelProperty(notes = "经理是否打过分（第一次）")
    @JsonProperty("is_apa_delegator_submit")
    @Column(name = "is_apa_delegator_submit",   columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '经理是否打过分（第一次）'")
    boolean APADSubmit;


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// 员工打分数据
    ///

    @ApiModelProperty(notes = "员工How自评总分数")
    @JsonProperty("employee_how_score")
    @Column(name = "employee_how_score",        columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '员工How自评总分数'")
    Integer employeeHowScore;

    @ApiModelProperty(notes = "员工What自评总分数")
    @JsonProperty("employee_what_score")
    @Column(name = "employee_what_score",       columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '员工What自评总分数'")
    Integer employeeWhatScore;

    @ApiModelProperty(notes = "员工What显示的百分比")
    @JsonProperty("employee_what_rate")
    @Column(name = "employee_what_rate",        columnDefinition = "FLOAT(4,2) UNSIGNED NULL DEFAULT NULL COMMENT '员工What显示的百分比'")
    Float employeeWhatRate;

    @ApiModelProperty(notes = "员工的总分数")
    @JsonProperty("employee_total_score")
    @Column(name = "employee_total_score",      columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '员工的总分数'")
    Integer employeeTotalScore;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "员工分数等级：A,B,C,D,E")
    @JsonProperty("employee_score_level")
    @Column(name = "employee_score_level",      columnDefinition = "CHAR(1) NULL DEFAULT NULL COMMENT '员工分数等级：A,B,C,D,E'")
    ScoreLevelEnum employeeScoreLevel;


    ///
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// 经理一次打分数据
    ///

    @ApiModelProperty(notes = "经理第一次How自评总分数")
    @JsonProperty("first_how_score")
    @Column(name = "first_how_score",           columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '经理第一次How自评总分数'")
    Integer firstHowScore;

    @ApiModelProperty(notes = "经理第一次What自评总分数")
    @JsonProperty("first_what_score")
    @Column(name = "first_what_score",          columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '经理第一次What自评总分数'")
    Integer firstWhatScore;

    @ApiModelProperty(notes = "经理第一次What显示的百分比")
    @JsonProperty("first_what_rate")
    @Column(name = "first_what_rate",           columnDefinition = "FLOAT(4,2) UNSIGNED NULL DEFAULT NULL COMMENT '经理第一次What显示的百分比'")
    Float firstWhatRate;

    @ApiModelProperty(notes = "经理第一次的总分数")
    @JsonProperty("first_total_score")
    @Column(name = "first_total_score",         columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '经理第一次的总分数'")
    Integer firstTotalScore;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "经理第一次分数等级：A,B,C,D,E")
    @JsonProperty("first_score_level")
    @Column(name = "first_score_level",         columnDefinition = "CHAR(1) NULL DEFAULT NULL COMMENT '经理第一次分数等级：A,B,C,D,E'")
    ScoreLevelEnum firstScoreLevel;

    ///
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// 经理二次打分数据
    ///

    @ApiModelProperty(notes = "经理第二次How自评总分数")
    @JsonProperty("second_how_score")
    @Column(name = "second_how_score",          columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '经理第二次How自评总分数'")
    Integer secondHowScore;

    @ApiModelProperty(notes = "经理第二次What自评总分数")
    @JsonProperty("second_what_score")
    @Column(name = "second_what_score",         columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '经理第二次What自评总分数'")
    Integer secondWhatScore;

    @ApiModelProperty(notes = "经理第二次What显示的百分比")
    @JsonProperty("second_what_rate")
    @Column(name = "second_what_rate",          columnDefinition = "FLOAT(4,2) UNSIGNED NULL DEFAULT NULL COMMENT '经理第二次What显示的百分比'")
    Float secondWhatRate;

    @ApiModelProperty(notes = "经理第二次的总分数")
    @JsonProperty("second_total_score")
    @Column(name = "second_total_score",        columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '经理第二次的总分数'")
    Integer secondTotalScore;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "经理第二次分数等级：A,B,C,D,E")
    @JsonProperty("second_score_level")
    @Column(name = "second_score_level",        columnDefinition = "CHAR(1) NULL DEFAULT NULL COMMENT '经理第二次分数等级：A,B,C,D,E'")
    ScoreLevelEnum secondScoreLevel;

    ///
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    @ApiModelProperty(notes = "经理释放第二次分数")
    @JsonProperty("is_release_score")
    @Column(name = "is_release_score",          columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '经理释放第二次分数'")
    boolean ReleaseSecond;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @ApiModelProperty(notes = "经理第二次释放时间")
    @JsonProperty("release_date")
    @Column(name = "release_date",              columnDefinition = "DATE NULL COMMENT '经理释放时间'")
    Date releaseDate;

    @ApiModelProperty(notes = "员工是否确认过当前的报表")
    @JsonProperty("is_agree_score")
    @Column(name = "is_agree_score",            columnDefinition = "TINYINT(1) NULL DEFAULT '0' COMMENT '员工是否读报表'")
    boolean AgreeScore;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
