package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "group_roles", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class GroupRole implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @ApiModelProperty(notes = "组名主键")
    @JsonProperty("group_id")
    @Column(name = "group_id",                  columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '组名主键'")
    Integer groupId;

    @ApiModelProperty(notes = "组名主键")
    @JsonProperty("group_idx")
    @Column(name = "group_idx",                 columnDefinition = "TINYINT UNSIGNED NULL DEFAULT NULL COMMENT '组名主键'")
    Integer groupIdx;

    @ApiModelProperty(notes = "模块主键")
    @JsonProperty("module_idx")
    @Column(name = "module_idx",                columnDefinition = "TINYINT UNSIGNED NULL DEFAULT NULL COMMENT '模块主键'")
    Integer moduleIdx;

    @ApiModelProperty(notes = "权限名称")
    @JsonProperty("name")
    @Column(name = "name",                      columnDefinition = "VARCHAR(255) NOT NULL COMMENT '权限名称'")
    String name;

    @ApiModelProperty(notes = "模块名称")
    @JsonProperty("module")
    @Column(name = "module",                    columnDefinition = "VARCHAR(255) NOT NULL COMMENT '模块名称'")
    String module;

    @ApiModelProperty(notes = "是否匿名查看，不需要账号")
    @JsonProperty("anonymous")
    @Column(name = "anonymous",                 columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否匿名查看，不需要账号'")
    boolean anonymous;

    @ApiModelProperty(notes = "不检查权限，但一定要有账号")
    @JsonProperty("viewer")
    @Column(name = "viewer",                    columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '不检查权限，但一定要有账号'")
    boolean viewer;

    @ApiModelProperty(notes = "是否可添加")
    @JsonProperty("add")
    @Column(name = "`add`",                     columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否可添加'")
    boolean add;

    @ApiModelProperty(notes = "是否可查看")
    @JsonProperty("check")
    @Column(name = "`check`",                   columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否可查看'")
    boolean check;

    @ApiModelProperty(notes = "是否可编辑")
    @JsonProperty("edit")
    @Column(name = "edit",                      columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否可编辑'")
    boolean edit;

    @ApiModelProperty(notes = "是否可下载")
    @JsonProperty("download")
    @Column(name = "download",                  columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否可下载'")
    boolean download;

    @ApiModelProperty(notes = "是否可授权")
    @JsonProperty("delegation")
    @Column(name = "delegation",                columnDefinition = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否可授权'")
    boolean delegation;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
