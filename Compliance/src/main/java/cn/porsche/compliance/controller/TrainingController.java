package cn.porsche.compliance.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.*;
import cn.porsche.compliance.emnu.TrainingTypeEnum;
import cn.porsche.compliance.service.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/training")
@ApiModel(value = "/training", description = "培训结果的操作类")
public class TrainingController extends BaseController {
    @Autowired
    DepartmentService departmentService;

    @Autowired
    TrainingService trainingService;

    @Autowired
    StatisticalService reportService;


    @ApiOperation(
            value = "获取当前年度的培训结果"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/by_year")
    public String byYear() {
        if (noPromise("")) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        int year = Calendar.getInstance().get(Calendar.YEAR);


        HashMap<String, Object> json = new HashMap<>();
        json.put("vp", EmployeeTypeEnum.VP.equals(user.getType()));
        json.put("year", year);

        json.put(TrainingTypeEnum.Question.getType(), reportService.findByYearAndUserAndType(year, user, TrainingTypeEnum.Question));
        json.put(TrainingTypeEnum.FileView.getType(), reportService.findByUserAndType(user, TrainingTypeEnum.FileView));

        Map<String, List> details = new HashMap<>();
        details.put("online",  trainingService.findAllByYearAndUserAndType(year, user, TrainingTypeEnum.Question));
        details.put("offline", trainingService.findAllByUserAndType(user, TrainingTypeEnum.FileView));

        json.put("details", details);

        if (true) {
            // 本部门的人可看
            List<User> list = userService.findByDepartment(departmentService.findById(user.getDepartmentId()));
            List<Map> summaries = new ArrayList<>();
            HashMap<String, Object> person;
            for (User item : list) {
                person = new HashMap<>();
                person.put(TrainingTypeEnum.Question.getType(), reportService.findByYearAndUserAndType(year, item, TrainingTypeEnum.Question));
                person.put(TrainingTypeEnum.FileView.getType(), reportService.findByUserAndType(item, TrainingTypeEnum.FileView));
                person.put("employee", item);

                summaries.add(person);
            }

            if (summaries.size() > 0) {
                json.put("list", summaries);
            }
        }

        if (user.getIsManager() != null && user.getIsManager()) {
            // 准备对应的报表数据
            // 1、部门的报表
            Department department = departmentService.findByCode("PCN");
            DepartmentTree tree   = departmentService.buildTree(department);
            if (department == null || tree == null) {
                return ApiResult.isBad("no department");
            }

            List<Department> deps = new ArrayList<>();
            for (DepartmentTree item : tree.getSubTree()) {
                if (item.getSubTree() != null || item.getSubTree().size() > 0) {
                    // 有子部门
                    for (DepartmentTree subItem : item.getSubTree()) {
                        deps.add(subItem.getDepartment());
                    }
                } else {
                    deps.add(item.getDepartment());
                }
            }

            Map<String, Object> departments = new HashMap<>();

            List<Statistical> summaries;
            TrainingTypeEnum[] types = TrainingTypeEnum.values();
            for (int i = 0; i < types.length; i++) {
                List<Map> list = new ArrayList<>();

                for (Department item : deps) {
                    int total = 0, attends = 0, person = 0;
                    summaries = reportService.findAllEmployeesByYearAndDepartment(year, item, types[i]);
                    if (summaries.size() == 0) {
                        continue;
                    }

                    for (Statistical summary : summaries) {
                        total += summary.getTotal();
                        attends += summary.getAttend();

                        person ++;
                    }

                    Map<String, Object> detail = new HashMap<>();
                    detail.put("name", item.getName());
                    detail.put("person", person);
                    detail.put("total", total);
                    detail.put("attends", attends);

                    list.add(detail);
                }

                departments.put(types[i].getType(), list);
            }

            json.put("departments", departments);
        }

        return ApiResult.builder().data(json).toJson();
    }
}
