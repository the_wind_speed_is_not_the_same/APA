package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.domain.Function;
import cn.porsche.web.domain.User;
import cn.porsche.web.service.DepartmentService;
import cn.porsche.web.service.PositionService;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Log4j2
@RestController
@RequestMapping("/user")
@ApiModel(value = "/user", description = "员工相关的操作类")
public class UserController extends BaseController {

    @Autowired
    PositionService positionService;

    @Autowired
    DepartmentService departmentService;

    @ApiOperation(
            value = "获取当前员工的个人信息",
            notes = "只显示当前员工的个人信息"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/me")
    public String me() {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.CHECK, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }
        Map<String, Object> data = new HashMap<>();

        // 用户信息
        data.put("user", user);
        // 组信息
        data.put("group", group);
//        // 消息通知
//        data.put("notice", userService.findById(user.getManagerId()));
        if (null != user.getManagerId()) {
            // 直属上级
            data.put("manager", userService.findById(user.getManagerId()));
        } else {
            data.put("manager", null);
        }

        // 职位信息
        data.put("position", positionService.findById(user.getPositionId()));
        // 部门信息
        data.put("department", departmentService.findById(user.getDepartmentId()));

        List<String> roles = new ArrayList<>();
        Function function = functionService.getLastFunction();
        if (null != function) {
            if (null != ( kpiUser = getKPIUser(function, user) )) {
                // 部门信息
                data.put("kpi", kpiUser);

                if ( kpiUser.getIsDelegator()) {
                    roles.add("delegator");
                }
            }
        }

        List<User> list = userService.findAllByManager(user);
        if (list.size() > 0) {
            roles.add("manager");
        }

        // 添加角色
        data.put("roles", roles);

        return ApiResult.builder().data(data).toJson();
    }

    @ApiOperation(
            value = "获取当前员工的个人信息",
            notes = "只显示当前员工的个人信息"
    )
    @ApiResponses({
            @ApiResponse(code = 0,   message = "成功", response = User.class)
    })
    @GetMapping(value = "/menu")
    public String menu() {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.CHECK, GroupNameEnum.Employee, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager)) {
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        boolean isTopManager = groupService.inTopManagerGroup(user);
        boolean isManager    = groupService.inManagerGroup(user);
        boolean isDelegator  = false;
        boolean isHRTeam     = groupService.inHRTeamGroup(user);

        // 查看用户在当前年份是否为代理人
        Function function = functionService.getLastFunction();
        if (function != null) {
            if (null != ( kpiUser = getKPIUser(function) )) {
                isDelegator = kpiUser.getIsDelegator();
            }
        }


        List<Map<String, Object>> menus = new ArrayList<>();
        Map<String, Object> menu;
        menu = new HashMap<>();
        menu.put("name", "Home");
        menu.put("path", "/home");

        menus.add(menu);
        // TS 和 APA 只有员工组和DM可见
        if (!isTopManager) {
            // 非CEO, VP
//            if (user.getNeedHands()) {
                // 只有手动人员
                menu = new HashMap();
                menu.put("name", "Target Setting");
                menu.put("path", "/ts");
                menus.add(menu);
//            }

            menu = new HashMap();
            menu.put("name", "Annual Performance Appraisal");
            menu.put("path", "/apa");
            menus.add(menu);
        }

        if (isTopManager || isManager || isDelegator) {
            // VP DM 或 Delegator
            menu = new HashMap();
            menu.put("name", "My Team");
            menu.put("path", "/team");
            menus.add(menu);
        }

        if (isTopManager || isHRTeam) {
            // HR
            menu = new HashMap();
            menu.put("name", "Reporting");
            menu.put("path", "/reporting");
            menus.add(menu);
        }

        List<Map<String, Object>> settings = new ArrayList<>();
        if (isHRTeam) {
            // HR
            menu = new HashMap();
            menu.put("name", "Function Release");
            menu.put("path", "/release");
            settings.add(menu);


            menu = new HashMap();
            menu.put("name", "User Information");
            menu.put("path", "/users");
            settings.add(menu);

            menu = new HashMap();
            menu.put("name", "Set HireDate");
            menu.put("path", "/hire");
            settings.add(menu);

            menu = new HashMap();
            menu.put("name", "Set Manager");
            menu.put("path", "/manager");
            settings.add(menu);


            menu = new HashMap();
            menu.put("name", "Super Admin");
            menu.put("path", "/admin");
            settings.add(menu);

//            menu = new HashMap();
//            menu.put("name", "Authorization");
//            menu.put("path", "/auth");
//            settings.add(menu);
        }

        if (isManager) {
            // Manager
            menu = new HashMap();
            menu.put("name", "Delegation");
            menu.put("path", "/delegation");
            settings.add(menu);
        }

        if (settings.size() > 0) {
            menu = new HashMap();
            menu.put("name", "Setting");
            menu.put("children", settings);

            menus.add(menu);
        }
//
//        menu = new HashMap();
//        menu.put("name", "Logout");
//        menu.put("path", "/logout");
//        menus.add(menu);

        return ApiResult.builder().list(menus).toJson();
    }

}
