package cn.porsche.web.service.impl;

import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.GroupRole;
import cn.porsche.web.repository.GroupRoleRepository;
import cn.porsche.web.service.GroupRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class GroupRoleServiceImpl implements GroupRoleService {
    @Autowired
    GroupRoleRepository repository;

    @Override
    public GroupRole update(GroupRole role) {
        return repository.save(role);
    }

    @Override
    public GroupRole findByGroupAndMoudle(Group group, ModuleNameEnum nameEnum) {
        return repository.findByGroupIdAndModule(group.getId(), nameEnum.getName());
    }

    @Override
    public List<GroupRole> findByGroup(Group group) {
        return repository.findAllByGroupId(group.getId());
    }
}
