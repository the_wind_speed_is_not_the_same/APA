package cn.porsche.web.service.impl;



import cn.porsche.web.constant.CommentTypeEnum;
import cn.porsche.web.domain.*;
import cn.porsche.web.repository.MessageRepository;
import cn.porsche.web.service.KPIUserService;
import cn.porsche.web.service.MessageService;
import cn.porsche.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MessageServiceImpl implements MessageService {
    final static String SystemTitle                    = "System Notice";
    final static String SystemEmailAddress             = "hr-noreply@porsche-cloudservice.com";

    // Target Setting开启，Manager组用户收取Target Setting通知
    final static String TargetSettingReleaseSubject    = "The Performance Management cycle has been kicked off";
    final static String TargetSettingReleaseMessage    = "Dear %s,\n" +
            "\n" +
            "The Performance Management Cycle %d has been kicked off. Please log in to the Performance Management System to set your new-year targets after the discussion with your manager.\n" +
            "\n" +
            "For more information, kindly refer to the guideline released by HR.\n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";


    // Target Setting阶段 - 经理comment 但未reject Target后，Member收到的邮件通知
    final static String DelegatorCommentOnTSSubject = "You have new unread messages, please log in to check the details.";
    final static String DelegatorCommentOnTSMessage = "Dear %s,\n" +
            "\n" +
            "Your manager has reviewed and commented on your targets. Please log in to the Performance Management System to review his/her inputs and update accordingly.\n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";


    // Target Setting阶段 - Target comment send通知 （下属给老板留comment）
    final static String EmployeeCommentOnTSSubject = "You have new unread messages, please log in to check the details.";
    final static String EmployeeCommentOnTSMessage = "Dear %s,\n" +
            "\n" +
            "Your team member, %s, has input a comment for the target setting. Please log in to the Performance Management System to check.\n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";

    // Target Setting阶段 - Member提交Target后，经理收到通知
    final static String EmployeeSubmitOnTSSubject = "Your team member has submitted his/her targets";
    final static String EmployeeSubmitOnTSMessage = "Dear %s,\n" +
            "\n" +
            "Your team member, %s, has submitted his/her targets. Please log in to the Performance Management System to review and confirm your team member’s targets.\n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";

    //Target Setting阶段 - 经理拒绝Target后，Member收到的邮件通知
    final static String DelegatorRejectOnTSSubject = "Your input of target setting has been rejected. Please log in to check the details.";
    final static String DelegatorRejectOnTSMessage = "Dear %s,\n" +
            "\n" +
            "Your manager has rejected your input of targets. Please log in to the Performance Management System to check, revise and re-submit your target after the offline discussion with your manager. \n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";


    // Target Setting阶段 – Member修改了已提交的Target后，经理收到通知
    final static String EmployeeModifyOnTSSubject = "Your team member has updated his/her targets";
    final static String EmployeeModifyOnTSMessage = "Dear %s,\n" +
            "\n" +
            "Your team member, %s, has updated his/her targets. Please log in to the Performance Management System to review and confirm your team member’s targets.\n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";


    //Target Setting阶段 - 经理确认Target后，员工收到通知
    final static String DelegatorConfirmTargetSubject = "Your manager has approved your input of target setting, please log in to confirm";
    final static String DelegatorConfirmTargetMessage = "Dear %s,\n" +
            "\n" +
            "Your manager, %s, has confirmed your targets. Please log in to the Performance Management System to confirm so as to complete your Target Setting activity for this year.\n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";


    //Target Setting阶段 - 员工double confirm Target后，经理收到通知
    final static String EmployeeConfirmOnTSSubject = "Your employee has confirmed his/her targets";
    final static String EmployeeConfirmOnTSMessage = "Dear %s,\n" +
            "\n" +
            "Your team member, %s, has confirmed his/her targets. His/her Target Setting activity for this year is now completed.\n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";


    //APA阶段 - 员工完成自评后，经理收到通知
    final static String EmployeeSubmitOnAPASubject = "Your employee has submitted the self-appraisal, please log in to review and comment.";
    final static String EmployeeSubmitOnAPAMessage = "Dear %s,\n" +
            "\n" +
            "Your team member, %s, has submitted his/her self-appraisal. As the line manager, there are two steps you need to complete. \n" +
            "\n" +
            "    1) Log in to the Performance Management System and review the employee’s self-appraisal. \n" +
            "    2) Once you review it, please provide your initial appraisal score and comments, which is the base for further calibrations. \n" +
            "\n" +
            "Important Remark: your initial appraisal score will not be displayed to your team member. Only the calibrated and final score will be individually accessible at the end of the APA season in the Performance Management System. For more information, kindly refer to the APA timeline released by HR. \n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";


    //APA阶段 - 经理完成最终分数，员工收到通知
    final static String ManagerReleaseFinalScoreSubject     = "Your final performance appraisal result is available, please log in to view";
    final static String ManagerReleaseFinalScoreMessage     = "Dear %s,\n" +
            "\n" +
            "Your final performance appraisal result is available now. You may log in to the Performance Management System to view it.\n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";


    // APA 预打分阶段 - 一个部门所有员工都被打分后，部门的VP收到提示邮件
    final static String VPNoticeOnFinalScoreSubject = "All the employees in your department have been scored by their manager";
    final static String VPNoticeOnFinalScoreMessage = "Dear %s,\n" +
            "\n" +
            "All the employees in your department have been scored by their manager. For details please log in to the Performance Management System and download the XXX report to prepare for the calibration session. \n" +
            "\n" +
            "Important Remark: initial appraisal scores will not be displayed to your team members. Only the calibrated and final scores will be individually accessible at the end of the APA season in the Performance Management System. \n" +
            "For more information, kindly refer to the APA timeline released by HR. \n" +
            "\n" +
            "Link to the system: <a target=\"_blank\" href=\"https://iporsche%s.porsche-pcn.com/as\">click to view</a>\n" +
            "\n" +
            "Kind regards,\n" +
            "\n" +
            "Your HR Team\n";




    final static String TS_URI          = "/ts";
    final static String APA_URI         = "/apa";
    final static String MY_TEAM_URI     = "/team";

    @Autowired
    MessageRepository repository;

    @Autowired
    UserService userService;

    @Autowired
    KPIUserService kpiUserService;

    @Value("${spring.profiles.active}")
    protected String active;

    private String getActiveString() {
        return active.equals("prod") ? "" : "-test";
    }

    @Override
    public Message findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Message tsReleaseNotice(Function function, User employee) {
        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(null)
                .ownerId(employee.getId())
                .subject(TargetSettingReleaseSubject)
                .message(String.format(TargetSettingReleaseMessage, employee.getEnName(), function.getYear(), getActiveString()))
                .senderId(employee.getId())
                .receiverId(employee.getId())
                .sender(SystemTitle)
                .senderEmail(SystemEmailAddress)
                .receiver(employee.getEnName())
                .receiverEmail(employee.getEmail())
                .uri(TS_URI)
                .read(false)
                .build();

        // 不需要存储起来，邮件通知而已
        return message;
    }

    protected List<Message> findByReceiverAndUri(User receiver, String uri) {
        return repository.findAllByReceiverIdAndUriAndRead(receiver.getId(), uri, false);
    }

//    @Override
//    public Message addOVCommentMessage(Function function, Target target, User sender, User receiver, CommentTypeEnum type, String other) {
//        List<Message> messages = findByReceiverAndUri(receiver, type.getName());
//        if (messages.size() > 0) {
//            // 类似未读的消息有消息存在，不需要再次提示
//            return messages.get(0);
//        }
//
//        Message message = Message.builder()
//                .functionId(function.getId())
//                .targetId(target.getId())
//                .ownerId(sender.getId())
//                .subject(AddCommentsSubmit)
//                .message(String.format(AddCommentsMessage, receiver.getEnName(), other, sender.getEnName(), type.getName().toLowerCase(), function.getId()))
//                .senderId(sender.getId())
//                .receiverId(receiver.getId())
//                .sender(sender.getEnName())
//                .senderEmail(sender.getEmail())
//                .receiver(receiver.getEnName())
//                .receiverEmail(receiver.getEmail())
//                .uri(type.getName())
//                .whatId(null)
//                .read(false)
//                .build();
//
//        return message;
//    }

    @Override
    public Message addEmployeeOVComment(Function function, Target target, User employee, User delegator) {
        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(target.getId())
                .ownerId(employee.getId())
                .subject(EmployeeCommentOnTSSubject)
                .message(String.format(EmployeeCommentOnTSMessage, delegator.getEnName(), employee.getEnName(), getActiveString()))
                .senderId(employee.getId())
                .receiverId(delegator.getId())
                .sender(employee.getEnName())
                .senderEmail(employee.getEmail())
                .receiver(delegator.getEnName())
                .receiverEmail(delegator.getEmail())
                .uri(MY_TEAM_URI)
                .whatId(null)
                .read(false)
                .build();

        return message;
    }

    @Override
    public Message addDelegatorOVComment(Function function, Target target, User delegator, User employee, CommentTypeEnum type) {
        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(target.getId())
                .ownerId(delegator.getId())
                .subject(DelegatorCommentOnTSSubject)
                .message(String.format(DelegatorCommentOnTSMessage, employee.getEnName(), getActiveString()))
                .senderId(delegator.getId())
                .receiverId(employee.getId())
                .sender(delegator.getEnName())
                .senderEmail(delegator.getEmail())
                .receiver(employee.getEnName())
                .receiverEmail(employee.getEmail())
                .uri(type.getName().toLowerCase())
                .whatId(null)
                .read(false)
                .build();

        return message;
    }

    @Override
    public Message employeeSubmitOnTS(Function function, User employee, Target target) {
        KPIUser kuser  = kpiUserService.findByFunction(function, employee);
        if (null == kuser) {
            return null;
        }

        User delegator = userService.findById(kuser.getDelegatorId());
        if (null == delegator) {
            return null;
        }

        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(target.getId())
                .ownerId(employee.getId())
                .subject(EmployeeSubmitOnTSSubject)
                .message(String.format(EmployeeSubmitOnTSMessage, delegator.getEnName(), employee.getEnName(), getActiveString()))
                .senderId(employee.getId())
                .receiverId(delegator.getId())
                .sender(employee.getEnName())
                .senderEmail(employee.getEmail())
                .receiver(delegator.getEnName())
                .receiverEmail(delegator.getEmail())
                .uri(MY_TEAM_URI)
                .whatId(null)
                .read(false)
                .build();

        return message;
    }

    @Override
    public Message employeeModifyOnTS(Function function, User employee, Target target) {
        KPIUser kuser  = kpiUserService.findByFunction(function, employee);
        if (null == kuser) {
            return null;
        }

        User delegator = userService.findById(kuser.getDelegatorId());
        if (null == delegator) {
            return null;
        }

        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(target.getId())
                .ownerId(employee.getId())
                .subject(EmployeeModifyOnTSSubject)
                .message(String.format(EmployeeModifyOnTSMessage, delegator.getEnName(), employee.getEnName(), getActiveString()))
                .senderId(employee.getId())
                .receiverId(delegator.getId())
                .sender(employee.getEnName())
                .senderEmail(employee.getEmail())
                .receiver(delegator.getEnName())
                .receiverEmail(delegator.getEmail())
                .uri(MY_TEAM_URI)
                .whatId(null)
                .read(false)
                .build();

        return message;
    }

    @Override
    public Message employeeConfirmOnTS(Function function, User delegator, User employee, Target target) {
        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(target.getId())
                .ownerId(employee.getId())
                .subject(EmployeeConfirmOnTSSubject)
                .message(String.format(EmployeeConfirmOnTSMessage, delegator.getEnName(), employee.getEnName(), getActiveString()))
                .senderId(employee.getId())
                .receiverId(delegator.getId())
                .sender(employee.getEnName())
                .senderEmail(employee.getEmail())
                .receiver(delegator.getEnName())
                .receiverEmail(delegator.getEmail())
                .uri(MY_TEAM_URI)
                .read(false)
                .build();

        // 不需要存储起来，邮件通知而已
        return message;
    }

    @Override
    public Message delegatorConfirmOnTS(Function function, User delegator, User employee, Target target) {
        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(target.getId())
                .ownerId(delegator.getId())
                .subject(DelegatorConfirmTargetSubject)
                .message(String.format(DelegatorConfirmTargetMessage, employee.getEnName(), delegator.getEnName(), getActiveString()))
                .senderId(delegator.getId())
                .receiverId(employee.getId())
                .sender(delegator.getEnName())
                .senderEmail(delegator.getEmail())
                .receiver(employee.getEnName())
                .receiverEmail(employee.getEmail())
                .uri(TS_URI)
                .read(false)
                .build();

        // 不需要存储起来，邮件通知而已
        return message;
    }

    @Override
    public Message delegatorRejectOnTS(Function function, User employee, Target target, TargetWhat what) {
        KPIUser kuser  = kpiUserService.findByFunction(function, employee);
        if (null == kuser) {
            return null;
        }

        User delegator = userService.findById(kuser.getDelegatorId());
        if (null == delegator) {
            return null;
        }

        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(target.getId())
                .whatId(what.getId())
                .ownerId(delegator.getId())
                .subject(DelegatorRejectOnTSSubject)
                .message(String.format(DelegatorRejectOnTSMessage, employee.getEnName(), getActiveString()))
                .senderId(delegator.getId())
                .receiverId(employee.getId())
                .sender(delegator.getEnName())
                .senderEmail(delegator.getEmail())
                .receiver(employee.getEnName())
                .receiverEmail(employee.getEmail())
                .uri(TS_URI)
                .read(false)
                .build();

        return message;
    }


    @Override
    public Message employeeSubmitAPA(Function function, User employee, Target target) {
        KPIUser kuser  = kpiUserService.findByFunction(function, employee);
        if (null == kuser) {
            return null;
        }

        User delegator = userService.findById(kuser.getDelegatorId());
        if (null == delegator) {
            return null;
        }

        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(target.getId())
                .whatId(null)
                .ownerId(employee.getId())
                .subject(EmployeeSubmitOnAPASubject)
                .message(String.format(EmployeeSubmitOnAPAMessage, kuser.getManager(), kuser.getEnName(), getActiveString()))
                .senderId(employee.getId())
                .receiverId(delegator.getId())
                .sender(employee.getEnName())
                .senderEmail(employee.getEmail())
                .receiver(delegator.getEnName())
                .receiverEmail(delegator.getEmail())
                .uri(MY_TEAM_URI)
                .read(false)
                .build();

        return message;
    }

    @Override
    public Message noticeByReleaseFinalScore(Function function, User employee, Target target) {
        KPIUser kuser  = kpiUserService.findByFunction(function, employee);
        if (null == kuser) {
            return null;
        }

        User delegator = userService.findById(kuser.getDelegatorId());
        if (null == delegator) {
            return null;
        }

        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(null)
                .whatId(null)
                .ownerId(employee.getId())
                .subject(ManagerReleaseFinalScoreSubject)
                .message(String.format(ManagerReleaseFinalScoreMessage, kuser.getEnName(), getActiveString()))
                .senderId(employee.getId())
                .receiverId(delegator.getId())
                .sender(employee.getEnName())
                .senderEmail(employee.getEmail())
                .receiver(delegator.getEnName())
                .receiverEmail(delegator.getEmail())
                .uri(APA_URI)
                .read(false)
                .build();

        return message;
    }

    @Override
    public Message noticeVPByFirstScore(Function function, User vp) {
        Message message = Message.builder()
                .functionId(function.getId())
                .targetId(null)
                .whatId(null)
                .ownerId(vp.getId())
                .subject(VPNoticeOnFinalScoreSubject)
                .message(String.format(VPNoticeOnFinalScoreMessage, vp.getEnName(), getActiveString()))
                .senderId(vp.getId())
                .receiverId(vp.getId())
                .sender(vp.getEnName())
                .senderEmail(vp.getEmail())
                .receiver(vp.getEnName())
                .receiverEmail(vp.getEmail())
                .uri(APA_URI)
                .read(false)
                .build();

        return message;
    }


    @Override
    public Message readMessage(Message message, User user) {
        if (!message.getReceiverId().equals(user.getId())) {
            return null;
        }

        message.setRead(true);

        return repository.save(message);
    }

    @Override
    public List<Message> findUnReadMessages(User owner) {
        return repository.findAllByReceiverIdAndRead(owner.getId(), false);
    }
}
