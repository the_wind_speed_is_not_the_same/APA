package cn.porsche.compliance.emnu;

public enum TrainingTypeEnum {
    Question("OnLine"),     // E-Learning
    FileView("OffLine"); // Customized Training

    private String type;

    TrainingTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }


    //将字符串转换为词性对象
    public static TrainingTypeEnum from(String string) {
        TrainingTypeEnum[] values = TrainingTypeEnum.values();

        for (TrainingTypeEnum item : values) {
            if (item.getType().equalsIgnoreCase(string)) {
                return item;
            }
        }

        return null;
    }
}
