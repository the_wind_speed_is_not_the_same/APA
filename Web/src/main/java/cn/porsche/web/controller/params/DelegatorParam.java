package cn.porsche.web.controller.params;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class DelegatorParam {
    @ApiModelProperty(notes = "阶段主键")
    @JsonProperty("function_id")
    Integer functionId;

    @ApiModelProperty(notes = "委托人主键")
    @JsonProperty("delegator_id")
    Integer delegatorId;

    @ApiModelProperty(notes = "员工主键列表")
    @JsonProperty("employee_ids")
    List<Integer> employeeIds;
}
