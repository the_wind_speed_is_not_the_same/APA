package cn.porsche.compliance.repository;

import cn.porsche.compliance.domain.Risk;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RiskRepository extends CrudRepository<Risk, Integer> {
    List<Risk> findAll();

    Risk findByUserId(Integer userId);

    @Modifying
    @Transactional
    void deleteByYear(Integer year);
}
