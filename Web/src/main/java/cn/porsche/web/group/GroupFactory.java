package cn.porsche.web.group;

import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.GroupRole;
import cn.porsche.web.service.GroupRoleService;
import cn.porsche.web.service.GroupService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class GroupFactory implements IGroup {
    @Autowired
    GroupService groupService;

    @Autowired
    GroupRoleService groupRoleService;


    @Override
    public boolean noPromise(@NotNull Group group, @NotNull String module, @NotNull OperatorText operator) {
        Assert.notNull(group, "The [group] argument is null");
        Assert.notNull(module, "The [moudle] argument is null");
        Assert.notNull(operator, "The [operator] argument is null");

        GroupRole role = findByGroupAndModule(group, module);
        if (role == null) {
            return true;
        }

        switch (operator) {
            case ADD: return !role.isAdd();
            case CHECK: return !role.isCheck();
            case DOWNLOAD: return !role.isDownload();
            case DELEGATION: return !role.isDelegation();
        }

        return true;
    }

    @Override
    public Group findByName(@NotNull String name) {
        Assert.notNull(name, "The [name] argument is null");

        return groupService.findByName(name);
    }

    @Override
    public GroupRole update(GroupRole role) {
        return groupRoleService.update(role);
    }

    @Override
    public GroupRole findByGroupAndModule(@NotNull Group group, @NotNull String module) {
        List<GroupRole> roles = getRoles(group);
        if (roles == null) {
            return null;
        }

        for (GroupRole role : roles) {
            if (role.getModule().equalsIgnoreCase(module)) {
                return role;
            }
        }

        return null;
    }

    @Override
    public List<GroupRole> getRoles(@NotNull Group group) {
        Assert.notNull(group, "The [group] argument is null");

        return groupRoleService.findByGroup(group);
    }
}
