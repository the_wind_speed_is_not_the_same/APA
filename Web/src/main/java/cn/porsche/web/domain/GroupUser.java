package cn.porsche.web.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "group_users", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class GroupUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @ApiModelProperty(notes = "组名主键")
    @JsonProperty("group_id")
    @Column(name = "group_id",                  columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '组名主键'")
    Integer groupId;
//
//    @ApiModelProperty(notes = "与组相关的权限主键")
//    @JsonProperty("group_role_id")
//    @Column(name = "group_role_id",             columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '与组相关的权限主键'")
//    Integer groupRoleId;

    @ApiModelProperty(notes = "成员主键")
    @JsonProperty("user_id")
    @Column(name = "user_id",                   columnDefinition = "INT UNSIGNED NOT NULL DEFAULT '0' COMMENT '成员主键'")
    Integer userId;

    @ApiModelProperty(notes = "手动导入的用户主键，权重较高")
    @JsonProperty("by_hands_user_id")
    @Column(name = "by_hands_user_id",          columnDefinition = "INT UNSIGNED NULL DEFAULT NULL COMMENT '手动导入的用户主键'")
    Integer byHandsUserId;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}