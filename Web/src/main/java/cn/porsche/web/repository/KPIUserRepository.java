package cn.porsche.web.repository;

import cn.porsche.web.domain.KPIUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KPIUserRepository extends CrudRepository<KPIUser, Integer> {
    void deleteByTargetId(Integer targetId);

    List<KPIUser> findAllByEmployeeId(Integer employeeId);

    KPIUser findByTargetId(Integer targetId);
    KPIUser findByFunctionIdAndEmployeeId(Integer functionId, Integer employeeId);

    Page<KPIUser> findAllByFunctionId(Integer functionId, Pageable pageable);

    List<KPIUser> findAllByFunctionIdAndManagerId(Integer functionId, Integer managerId);
    List<KPIUser> findAllByFunctionIdAndDelegatorId(Integer functionId, Integer delegatorId);
    List<KPIUser> findAllByFunctionIdAndDepartmentId(Integer functionId, Integer departmentId);

    Page<KPIUser> findAllByFunctionIdAndDelegatorId(Integer functionId, Integer delegatorId, Pageable pageable);
    Page<KPIUser> findAllByFunctionIdAndDepartmentId(Integer functionId, Integer departmentId, Pageable pageable);

    // 按组排名
    @Query("SELECT ku from KPIUser ku LEFT JOIN GroupUser gu ON ku.employeeId=gu.userId WHERE ku.functionId=:functionId AND gu.groupId=:groupId")
    Page<KPIUser> findAllByFunctionIdAndGroupId(Integer functionId, Integer groupId, Pageable pageable);

    // 委托人第一次打分
    @Query("SELECT ku from KPIUser ku LEFT JOIN Target t ON ku.targetId=t.id WHERE t.functionId=:functionId and ku.delegatorId=:delegatorId ORDER BY t.firstScoreLevel ASC")
    List<KPIUser> findUsersByFunctionAndDelegatorOrderByFirstLevelASC(Integer functionId, Integer delegatorId);

    // 委托人第二次打分
    @Query("SELECT ku from KPIUser ku LEFT JOIN Target t ON ku.targetId=t.id WHERE ku.functionId=:functionId and ku.delegatorId=:delegatorId ORDER BY t.secondScoreLevel ASC")
    List<KPIUser> findUsersByFunctionAndDelegatorOrderBySecondLevelASC(Integer functionId, Integer delegatorId);

//    // 委托人第二次打分
//    @Query("SELECT ku from KPIUser ku LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId and t.delegatorId=:delegatorId ORDER BY t.DTLevel ASC")
//    List<KPIUser> findUsersByFunctionAndDepartmentId(Integer functionId, Integer delegatorId);
}
