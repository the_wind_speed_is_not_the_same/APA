package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.CommentTypeEnum;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.controller.params.KPIParam;
import cn.porsche.web.controller.params.WhatParam;
import cn.porsche.web.domain.*;
import cn.porsche.web.email.IEmail;
import cn.porsche.web.exceptions.TargetException;
import cn.porsche.web.service.CommentService;
import cn.porsche.web.service.DepartmentReportService;
import cn.porsche.web.service.DepartmentService;
import cn.porsche.web.service.MessageService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.*;

@RestController
@RequestMapping("/target")
@ApiModel(value = "/target", description = "员工相关的Target操作类")
public class TargetController extends BaseController {

    @Autowired
    DepartmentReportService reportService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    MessageService messageService;

    @Autowired
    CommentService commentService;

    @Autowired
    IEmail mailService;

    @ApiOperation(
            value = "【员工】在【绩效】页中 新增或修改 What 的接口",
            notes = "JSON(WhatParams)参数<br/>" +
                    "员工新增某个What，What中kpis由一个数组构，内部有五个元素（TargetKPI.class），每个元素的index 从[0-1]。<br />" +
                    "每个新增动作，必需在APA开启前<br />" +
                    "详情：https://gitee.com/the_wind_speed_is_not_the_same/APA/issues/I1S3YE"
    )
    @PostMapping(value = "/update_what", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String updateWhat(@RequestBody WhatParam params) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(params.getFunctionId());
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }


        if (null == (target = getTarget(function))) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (function.getAPA() && target.isTSEConfirm() && target.isTSDConfirm()) {
            return ApiResult.isBad("Your manager has confirmed your performance data and cannot be modified or deleted");
        }

        if (null == targetService.updateWhat(target, params, user)) {
            // 新增失败
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "【经理】拒绝员工的绩效数据的接口",
            notes = "GET参数<br/>" +
                    "function_id：阶段主键<br />" +
                    "what_id：需要删除的What主键<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", required = true, dataTypeClass = Integer.class, paramType = "Header", value = "阶段主键"),

            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "what_id", required = true, dataTypeClass = Integer.class, value = "What主键"),

    })
    @PostMapping(value = "/reject_what")
    public String rejectWhat(@RequestBody WhatParam params) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (params.getWhatId() == null || params.getRejectComment() == null || params.getRejectComment().length() == 0) {
            return ApiResult.isBad("Unable to find the Reject data, please contact your HR to fix it");
        }

        TargetWhat what = targetService.findWhatById(params.getWhatId());
        if (null == what) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        Function function = functionService.findById(what.getFunctionId());
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        User employee;
        if (null == what.getEmployeeId() || null == (employee = userService.findById(what.getEmployeeId()))) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if ((target = targetService.findByUser(function, employee)) == null) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        // 如果 APA开始，不能拒绝，不能修改已经确认过的What
        if (function.getAPA() && target.isTSDConfirm() && target.isTSEConfirm()) {
            return ApiResult.isBad("At the beginning of APA, the performance confirmed by Employee And Manager cannot be rejected");
        }


        if ((kpiUser = kpiUserService.findByFunction(function, employee)) == null
                || !user.getId().equals(kpiUser.getDelegatorId()) && !user.getId().equals(kpiUser.getManagerId())
        ) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        Comment comment = commentService.add(function, user, employee, user, CommentTypeEnum.TS, what.getId(), params.getRejectComment());
        if (null == comment) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        Message message = messageService.delegatorRejectOnTS(function, employee, target, what);
        if (null != message) {
            try {
                mailService.sendHtml(message.getSubject(), message.getMessage(), message.getReceiverEmail());
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }

        targetService.rejectWhatByDelegator(target, what, comment);
        reportService.refreshReporting(function, departmentService.findById(employee.getDepartmentId()));

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "【员工】在【绩效】页中 删除 What 的接口",
            notes = "GET参数<br/>" +
                    "function_id：阶段主键<br />" +
                    "what_id：需要删除的What主键<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", required = true, dataTypeClass = Integer.class, paramType = "Header", value = "阶段主键"),

            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "what_id", required = true, dataTypeClass = Integer.class, value = "What主键"),

    })
    @GetMapping(value = "/delete_what")
    public String deleteWhat(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "what_id") Integer whatId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if ((target = getTarget(function)) == null) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        // 如果 APA开始，不能新增，不能修改已经确认过的What
        if (function.getAPA() && target.isTSEConfirm() && target.isTSDConfirm()) {
            return ApiResult.isBad("Your manager has confirmed your performance data and cannot be modified or deleted");
        }

        // 如果 APA开始，不能新增,修改,删除已经确认过的What
        TargetWhat what = targetService.findWhatById(whatId);
        if (null == what || !user.getId().equals(what.getEmployeeId())) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (null == targetService.deleteWhat(target, what)) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "【员工】在【绩效】页中 新增 What 的接口",
            notes = "JSON(WhatParams)参数<br/>" +
                    "员工新增某个What，What中kpis由一个数组构，内部有五个元素（TargetKPI.class），每个元素的index 从[0-1]。<br />" +
                    "每个新增动作，必需在APA开启前<br />" +
                    "详情：https://gitee.com/the_wind_speed_is_not_the_same/APA/issues/I1S3YE"
    )
    @PostMapping(value = "/update_how", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String updateHow(@RequestBody TargetHow param) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(param.getFunctionId());
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if ((target = getTarget(function)) == null) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        // 如果 APA开始，不能新增，不能修改已经确认过的What
        if (function.getAPA() && target.isTSEConfirm() && target.isTSDConfirm()) {
            return ApiResult.isBad("Your manager has confirmed your performance data and cannot be modified or deleted");
        }

        if (null == targetService.updateHow(target, param, user)) {
            // 新增失败
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "检查Target及自身所有的数据是否符合要求",
            notes = "符合要求：所有字段都返回 yes<br/>" +
                    "异常字段：异常字段返回 null"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = WhatParam.class, responseContainer = "Map<String, Map<String, String>>"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/check")
    public String check(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.CHECK, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (null == (target = getTarget(function))) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        return ApiResult.builder().data(targetService.check(target)).toJson();
    }

    @ApiOperation(
            value = "员工提交Target",
            notes = "检查返回Target的is_submit字段，如果为true，者提交成功<br />" +
                    "如果Target未通过检查，返回 check fail，需要重新调用 /target/check 接口获取对应的错误字段"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = WhatParam.class, responseContainer = "Map<String, Map<String, String>>"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/submit")
    public String tsSubmit(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.CHECK, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if ((target = getTarget(function)) == null) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (target.isTSESubmit()) {
            return ApiResult.isOk();
        }

        try {
            target = targetService.tsESubmit(function, user);
        } catch (TargetException e) {
            // 有异常发生
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        User delegator = userService.findDelegator(function, user);
        if (null == delegator) {
            // 没有管理员
            return ApiResult.isBad("The delegator not found, Please contact HR to fix it");
        }

        // 新增一条通知消息
        Message message = messageService.employeeSubmitOnTS(function, user, target);
        try {
            mailService.sendHtml(message.getSubject(), message.getMessage(), delegator.getEmail());
        } catch (MessagingException e) {
            e.printStackTrace();
            return ApiResult.isBad("Email send failed. Please try again or contact HR to fix it");
        }

        reportService.refreshReporting(function, departmentService.findById(user.getDepartmentId()));

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【委托人】进行【Confirm】操作",
            notes = "进行Confirm时，需要对Target下所有的TargetWhat检查是否符合要求，如果检查未通过，返回 check fail，需要重新调用 /target/check 接口获取对应的错误字段<br />" +
                    "只有在【员工】对【绩效】进行【Submit】后才可提交Confirm请求<br />"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "employee_id", required = true, dataTypeClass = Integer.class, value = "员工主键")
    })
    @GetMapping(value = "/delegator_confirm")
    public String delegatorConfirm(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "employee_id") Integer employeeId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function;
        if (null == (function = functionService.findById(functionId))) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        User employee = userService.findById(employeeId);
        if (null == employeeId) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (null == (target = targetService.findByUser(function, employee))) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (!target.isTSESubmit()) {
            return ApiResult.isBad("Please inform your team member to re-submit the target before you can click \"confirm\"");
        }

        if (null == (kpiUser = targetService.findKPIUserByTarget(target))) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (!user.getId().equals(kpiUser.getDelegatorId()) && !user.getId().equals(kpiUser.getManagerId())) {
            // 员工的主管不是当前用户
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }


        if (null == (target = targetService.confirmByDelegator(target))) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        Message message = messageService.delegatorConfirmOnTS(function, user, employee, target);
        if (null != message) {
            // 发送邮件
            try {
                mailService.sendHtml(message.getSubject(), message.getMessage(), employee.getEmail());
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }

        reportService.refreshReporting(function, departmentService.findById(employee.getDepartmentId()));

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【员工】进行【Confirm】操作",
            notes = "进行Confirm时，需要对Target下所有的TargetWhat检查是否符合要求，如果检查未通过，返回 check fail，需要重新调用 /target/check 接口获取对应的错误字段<br />" +
                    "只有在【员工】对【绩效】进行【Submit】后才可提交Confirm请求<br />" +
                    "经理在Confirm员工时需要指定employee_id（员工主键）"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
    })
    @GetMapping(value = "/employee_confirm")
    public String employeeConfirm(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function;
        if (null == (function = functionService.findById(functionId))) {
            return ApiResult.isBad("The APA is not opened, please contact your HR");
        }

        // 经理 Confirm 员工
        if (null == (target = targetService.findByUser(function, user))) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (!target.isTSDConfirm()) {
            return ApiResult.isBad("The Manager has not been confirmed, Please try again or contact Your Manager");
        }

        if (null == (kpiUser = targetService.findKPIUserByTarget(target))) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        User delegator = userService.findDelegator(function, user);
        if (null == delegator) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        target = targetService.confirmByEmployee(target);
        if (null == target) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        Message message = messageService.employeeConfirmOnTS(function, delegator, user, target);
        if (null != message) {
            // 发送邮件
            try {
                mailService.sendHtml(message.getSubject(), message.getMessage(), delegator.getEmail());
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }

        reportService.refreshReporting(function, departmentService.findById(user.getDepartmentId()));

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "【经理】或【委托人】获取自己的【Target】详情接口",
            notes = "GET参数<br/>" +
                    "function_id（阶段主键）<br />" +
                    "employee_id（员工主键）<br />"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = KPIParam.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "employee_id", required = false, dataTypeClass = Integer.class, value = "员工主键"),
    })
    @GetMapping(value = "/by_year")
    public String detailByYear() {
        if (noPromise(ModuleNameEnum.TargetsReview, OperatorText.CHECK, GroupNameEnum.Employee, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.getLastFunction();
        if (null == function) {
            return ApiResult.isOk();
        }

        return detail(function.getId(), null);
    }



    @ApiOperation(
            value = "【经理】或【委托人】获取自己的【Target】详情接口",
            notes = "GET参数<br/>" +
                    "function_id（阶段主键）<br />" +
                    "employee_id（员工主键）<br />"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = KPIParam.class),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true,  dataTypeClass = Integer.class, value = "阶段主键"),
            @ApiImplicitParam(name = "employee_id", required = false, dataTypeClass = Integer.class, value = "员工主键"),
    })
    @GetMapping(value = "/detail")
    public String detail(
            @RequestParam(value = "function_id") Integer functionId,
            @RequestParam(value = "employee_id", required = false) Integer employeeId
    ) {
        if (noPromise(ModuleNameEnum.TargetsReview, OperatorText.CHECK, GroupNameEnum.Employee, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function;
        if (null == functionId || null == ( function = functionService.findById(functionId) )) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        User employee = employeeId == null ? user : userService.findById(employeeId);
        if (null == employee) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (null == ( kpiUser = kpiUserService.findByFunction(function, employee) )) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (employeeId != null && !user.getId().equals(kpiUser.getDelegatorId()) && !user.getId().equals(kpiUser.getManagerId())) {
            // 员工的主管不是当前登录用户
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        if (null == ( target = targetService.findByUser(function, employee) )) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (employeeId == null) {
            // 是自己的数据，不清除敏感数据
            return ApiResult.builder().data(targetService.findTargetDetail(target, false)).toJson();
        }

        return ApiResult.builder().data(targetService.findTargetDetail(target, kpiUser.getIsDelegator())).toJson();
    }

    @ApiOperation(
            value = "当前【经理】或者【委托人】获取所有员工设定的【Target】列表的接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "正常返回", response = Target.class, responseContainer = "List"),
    })
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键")
    })
    @GetMapping(value = "/list")
    public String list(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetsReview, OperatorText.CHECK, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager, GroupNameEnum.TopManagement)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        kpiUser = kpiUserService.findByFunction(function, user);
        List<KPIParam> kpis = new ArrayList<>();

        boolean isVP = false;
        boolean isDM = false;
        boolean isDelegator = false;

        List<Target> targets = null;
        if (( isVP = groupService.inTopManagerGroup(user) )) {
            targets = targetService.findAllByManager(function, user);
        }

        if (null == targets) {
            if (( isDM = groupService.inManagerGroup(user) )) {
                targets = targetService.findAllByManager(function, user);
            }
        }

        if (null == targets) {
            if (( isDelegator = groupService.inIDelegatorGroup(user) )) {
                targets = targetService.findAllByDelegator(function, user);
            }
        }

        if (null != targets) {
            Target target;
            KPIParam params;
            for (Target item : targets) {
                params = targetService.findTargetDetail(item, isDelegator);

                // 如果当前用户只是 代理人，只显示 TS阶段数据
                if (isDelegator) {
                    target = params.getTarget();

                    target.setAPADSubmit(false);
                    target.setAPADSubmit(false);

                    target.setEmployeeHowScore(null);
                    target.setEmployeeWhatScore(null);
                    target.setEmployeeWhatRate(null);
                    target.setEmployeeTotalScore(null);
                    target.setEmployeeScoreLevel(null);

                    target.setFirstHowScore(null);
                    target.setFirstWhatScore(null);
                    target.setFirstWhatRate(null);
                    target.setFirstTotalScore(null);
                    target.setFirstScoreLevel(null);

                    target.setSecondHowScore(null);
                    target.setSecondWhatScore(null);
                    target.setSecondWhatRate(null);
                    target.setSecondTotalScore(null);
                    target.setSecondScoreLevel(null);

                    target.setReleaseSecond(false);
                    target.setReleaseDate(null);
                    target.setAgreeScore(false);
                }

                kpis.add(params);
            }
        }

        return ApiResult.builder().list(kpis).toJson();
    }
}
