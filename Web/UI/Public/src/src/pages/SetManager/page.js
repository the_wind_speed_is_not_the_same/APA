import { request } from '../../common/request.js';
import { mapState, mapActions } from 'vuex'
import Confirm from '../../components/Confirm/page.vue';

export default {
    data() {
        return {
            currentIndex: null,
            departmentId: '',
            filterManager: null,
            departmentList: [],
            keyWord: '',
            isShowLoading: false,
            tableHeight: 412,
            selectType: 1,
            selectionArr: [],
            isShowDialog: false,
            moveTo: null,
            importText: "",
            isShowImportDialog: false,

            isShowButtonLeft: false,
            isShowConfirm: false,
            titleTxt: 'Error',
            confirmTxt: 'Do you want to delete this member?',
            confirmTitle: 'Warning',
            users: [],
            totalRecord: 0,
            pageSize: 25,
            pageIndex: 1,
            groups: [
                { label: 'HRPerformanceTeam', name: 'HR Team' },
                { label: 'TopManagement', name: 'Top Management' },
                // {label:'DisciplinaryManager',name:'Disciplinary Manager'},
                { label: 'Employee', name: 'Employee' },
                { label: 'LongLeaves', name: 'Long Leaves' },
                { label: 'NewEmployeesInProbation', name: 'Under Probation' }
            ],
            isShowNotice: false,
            groupIndex: null,
            groupFilter: null,
            groupFilters: [
                { label: 'HRPerformanceTeam', name: 'HR Team' },
                { label: 'TopManagement', name: 'Top Management' },
                { label: 'InProbation', name: 'Under Probation' },
                { label: 'LongLeaves', name: 'Long Leaves' }
            ],
            years: [],
            role: null,
            role_function_id: null
        }
    },
    components: {
        Confirm
    },
    computed: {
        ...mapState({
        }),
        pageNumbers() {
            return this.totalRecord ? Math.ceil(this.totalRecord / this.pageSize) : 0
        }
    },
    methods: {
        ...mapActions(['setDelegatorInfo', 'setYearList']),
        getUsers() {
            let params = {
                page: this.pageIndex,
                size: this.pageSize
            };

            if (this.departmentId != 0) {
                params['department_id'] = this.departmentId;
            }

            if (this.filterManager === 'Yes') {
                // 只显示经理组人员
                params['only'] = 'Manager';
            }

            if (this.keyWord) {
                params['key'] = this.keyWord;
            }

            request({
                url: 'getEmployeeList',
                method: 'get',
                params: params,
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.users = [];
                    this.totalRecord = 0;

                    return false;
                }

                this.users = res.list;
                this.totalRecord = res['pagebar']['total'];
            })
        },
        setHeaderClass({ row, column, rowIndex, columnIndex }) {
            if (rowIndex === 0) {
                return columnIndex === 0 ? "cell-hide" : (columnIndex === 1 ? 'special-cell' : null)
            }
        },
        setCellClass({ row, column, rowIndex, columnIndex }) {
            if (columnIndex === 1) {
                return 'cell-hide'
            }
        },
        selectCell(selection, row) {
            this.selectionArr = selection
            if (this.users.length === selection.length) {
                this.selectType = 2
            } else {
                this.selectType = 1
            }
        },
        selectAllCell() {
            if (this.selectType === 1) {
                this.selectType = 2
                this.selectionArr = this.users
            } else {
                this.selectType = 1
                this.selectionArr = []
            }
            this.$refs.table.toggleAllSelection()
        },
        rowClick(row, column, event) {
            if (this.selectionArr.length > 0) {
                let index = null
                this.selectionArr.forEach((item, idx) => {
                    if (item.id === row.id) {
                        index = idx
                    }
                })
                if (index === null) {
                    this.$refs.table.toggleRowSelection(row, true)
                    this.selectionArr.push(row)
                } else {
                    this.$refs.table.toggleRowSelection(row, false)
                    this.selectionArr.splice(index, 1)
                }

            } else {
                this.$refs.table.toggleRowSelection(row, true)
                this.selectionArr.push(row)
            }
        },
        spanMethod({ row, column, rowIndex, columnIndex }) {

            if (columnIndex === 9) {
                return [1, 2]
            }


        },
        switchRole() {
            if (this.selectionArr.length === 0) {
                return false
            }

            let employees = [];
            this.selectionArr.forEach((item) => {
                employees.push(item.id)
            });

            request({
                url: 'switchRole',
                method: 'post',
                type: 'form',
                data: {
                    employees,
                    role: this.role
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.confirmTitle = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;
                    return;
                }

                if (res.code === 0) {
                    this.isShowNotice = true
                    this.getUsers()
                    this.selectionArr.length = 0;
                }

                this.role = null;
            })
            this.isShowDialog = false
        },
        showManagerUI() {
            this.isShowImportDialog = true
        },
        submitManagerDate() {
            if (this.importText == '') {
                this.titleTxt = 'Error';
                this.confirmTxt = 'Please input the import content';
                this.isShowConfirm = true;

                return;
            }

            this.isShowImportDialog = false;
            this.isDialogLoading    = true;
            request({
                url: 'importManager',
                method: 'post',
                data: {
                    function_id: this.function_id,
                    text: this.importText
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm      = true;
                    this.isShowImportDialog = true;

                    return false;
                }

                this.pageIndex  = 1;
                this.importText = '';
                this.isShowDateSelect   = false;

                this.titleTxt   = 'Success';
                this.confirmTxt = 'Import Successfully';
                this.isShowConfirm    = true;
                this.isShowButtonLeft = false;

                this.getUsers();
            })
        },
        cancelMove() {
            this.isShowDialog = false
        },
        moveRole() {
            this.isShowDialog = true
        },
        deleteRole() {
            this.isShowConfirm = true
        },
        confirmDelete() {
            this.isShowConfirm = false
        },
        cancelDelete() {
            this.isShowConfirm = false
        },
        changeDepartment() {
            this.pageIndex = 1;
            this.keyWord = "";

            this.getUsers()
        },
        changeManager(value) {
            this.filterManager = value;
            this.keyWord = "";

            this.getUsers();
        },
        pageChange(e) {
            this.pageIndex = e
            this.getUsers()
        },
        filterGroup() {
            this.filter = !this.filter;
            this.getUsers()
        },
        confirm() {
            this.isShowConfirm = false;
        }
    },
    created() {
        this.getUsers();

    },
    mounted() {
        this.$nextTick(() => {
            // this.tableHeight = this.$refs.tableWrapper.offsetHeight - 28;
        })

        // 获取可筛选的部门
        request({
            url: 'getDepartmentByCompany',
            method: 'get',
            params: {}
        }).then(res => {
            if (res && res.data) {
                var list = [{'id': 0, 'name': 'All'}];

                res.data.forEach((item, idx) => {
                    list.push(item['department']);

                    if (item['sub_tree'].length) {
                        item['sub_tree'].forEach((sItem, sIdx) => {
                            sItem['department']['name'] = sItem['department']['name'] + "(" + item['department']['name'] + ")";
                            list.push(sItem['department']);

                            if (sItem['sub_tree'].length) {
                                sItem['sub_tree'].forEach((ssItem, ssIdx) => {
                                    ssItem['department']['name'] = ssItem['department']['name'] + "(" + item['department']['name'] + ")";

                                    list.push(ssItem['department']);
                                });
                            }
                        });
                    }
                });

                this.departmentList = list;
            }
        })
    }
}
