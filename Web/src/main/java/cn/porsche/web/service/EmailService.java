package cn.porsche.web.service;


import cn.porsche.web.domain.Email;

public interface EmailService {
    Email update(Email email);
}
