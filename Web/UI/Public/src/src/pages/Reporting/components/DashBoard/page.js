import echarts from 'echarts';

const colorData = [{
    type: 'not_started',
    name: 'Not Started',
    itemStyle: {color: '#F1F1F1'},
    value: 0
}, {
    type: 'in_progress',
    name: 'In Progress',
    itemStyle: {color: '#c4c4c4'},
    value: 0
}, {
    type: 'finished',
    name: 'Finished',
    itemStyle: {color: '#018A16'},
    value: 0
}];

export default {
    props: {
        pieData: {
            type: Object,
        },
        chartTitle: String,
        radius: {
            type: Array,
            default() {
                return ['23%', '46%']
            }
        }
    },
    data() {
        return {
            colorData: []
        }
    },
    components: {},
    watch: {
        'pieData': function (newval, oldval) {
            if (newval) {
                this.initMyChart()
            }
        }
    },
    computed: {
        circleData() {
            if (this.pieData) {
                colorData.forEach((item) => {
                    for (let i in this.pieData) {
                        if (item.type === i) {
                            item.value = this.pieData[i];
                        }
                    }
                })
            }

            return colorData;
        }
    },
    methods: {
        showList() {
            this.isShowListSelect = true;
        },
        confirmIndex(index) {

        },
        initMyChart() {
            const myChart = echarts.init(this.$refs.myChart);
            let option = {
                title: {
                    text: this.chartTitle,
                    left: 24,
                    top: 24,
                    textStyle: {
                        fontWeight: 'bold',
                        fontSize: 18,
                        color: '#000'
                    }
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{b}: {c} ({d}%)'
                },
                legend: {
                    left: 'center',
                    bottom: '20',
                    icon: 'circle',
                    selectedMode: false
                },
                series: [{
                    name: this.chartTitle,
                    type: 'pie',
                    radius: this.radius,
                    avoidLabelOverlap: false,
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)',
                        }
                    },
                    label: {
                        formatter: '{b|{c}}',
                        rich: {
                            b: {
                                color: '#000',
                                fontSize: 14,
                                lineHeight: 26,
                            }
                        }
                    },
                    labelLine: {
                        show: false
                    },
                    // data: [{'name': 'not start', value: 1024},{'name': 'a', value: 1024}]
                    data: this.circleData
                }]
            };
            myChart.setOption(option);
        }
    },
    mounted() {
        // this.initMyChart()
    }
}
