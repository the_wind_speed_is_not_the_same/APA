-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 140.179.16.188    Database: PFC
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `department_employees`
--

DROP TABLE IF EXISTS `department_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `employee_id` int(10) unsigned NOT NULL COMMENT '员工主键',
  `department_1` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  `department_2` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  `department_3` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  `department_4` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  `department_5` int(10) unsigned DEFAULT NULL COMMENT '公司代码',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=536 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级部门主键',
  `manager_id` int(10) unsigned DEFAULT NULL COMMENT '管理员主键',
  `company_code` varchar(63) NOT NULL COMMENT '公司代码',
  `parent_code` varchar(63) DEFAULT NULL COMMENT '上级部门代码',
  `code` varchar(63) NOT NULL COMMENT '部门代码',
  `name` varchar(255) NOT NULL COMMENT '部门名称',
  `count` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '员工总数',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(10) unsigned NOT NULL COMMENT '用户登录的主键',
  `by` varchar(256) NOT NULL COMMENT '用户登录的方式',
  `access_token` varchar(1024) NOT NULL COMMENT '用户登录的密钥',
  `access_token_expire_time` datetime DEFAULT NULL COMMENT '登陆的过期时间',
  `refresh_token` varchar(1024) NOT NULL COMMENT '用户登录的密钥',
  `refresh_token_expire_time` datetime DEFAULT NULL COMMENT '登陆的过期时间',
  `ticket` text COMMENT '第三方凭证',
  `last_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `last_ua` text COMMENT '最后登录的流程器信息',
  `last_dev` text COMMENT '最后登录的UA',
  `auth_id` varchar(256) DEFAULT NULL COMMENT '登录之后的Cookie标识',
  `is_logout` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已登出',
  `logout_time` datetime DEFAULT NULL COMMENT '登出时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=188 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `id` int(10) unsigned NOT NULL COMMENT '主键',
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'HRIS系统中上级主键',
  `company_code` varchar(63) DEFAULT NULL COMMENT 'HRIS Company Code',
  `hris_code` varchar(63) DEFAULT NULL COMMENT 'HRIS系统代码',
  `function` varchar(511) DEFAULT NULL COMMENT '显示抬头',
  `occupied` tinyint(1) unsigned DEFAULT NULL COMMENT '是否已占用完毕',
  `risks` varchar(255) DEFAULT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `season` tinyint(3) unsigned NOT NULL COMMENT '一、二、三、四复季度',
  `year` smallint(5) unsigned DEFAULT NULL COMMENT '部门主键',
  `en_question` text COMMENT '英文版本问题',
  `cn_question` text COMMENT '英文版本问题',
  `cn_option_a` text COMMENT '中文版本问题',
  `cn_option_b` text COMMENT '中文版本问题',
  `cn_option_c` text COMMENT '中文版本问题',
  `cn_option_d` text COMMENT '中文版本问题',
  `en_option_a` text COMMENT '英文版本问题',
  `en_option_b` text COMMENT '英文版本问题',
  `en_option_c` text COMMENT '英文版本问题',
  `en_option_d` text COMMENT '英文版本问题',
  `help_answer` varchar(20) DEFAULT NULL COMMENT '帮助后的排队答案',
  `answer` varchar(20) DEFAULT NULL COMMENT '答案',
  `score` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '分数',
  `en_tip` text COMMENT '英文提示',
  `cn_tip` text COMMENT '中文提示',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `risks`
--

DROP TABLE IF EXISTS `risks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `risk` varchar(63) NOT NULL COMMENT '风险名称',
  `empty` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否未使用',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `training_summary`
--

DROP TABLE IF EXISTS `training_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_summary` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(10) unsigned NOT NULL COMMENT '主键',
  `department_id` int(10) unsigned NOT NULL COMMENT '部门主键',
  `type` smallint(5) unsigned NOT NULL COMMENT '培训类别',
  `year` smallint(5) unsigned DEFAULT NULL COMMENT '参与年份',
  `total` smallint(5) unsigned NOT NULL COMMENT '参与类别总数',
  `attend` smallint(5) unsigned NOT NULL COMMENT '未参与类别总数',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `department` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=456 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trainings`
--

DROP TABLE IF EXISTS `trainings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(10) unsigned NOT NULL COMMENT '主键',
  `department_id` int(10) unsigned NOT NULL COMMENT '部门主键',
  `year` smallint(5) unsigned DEFAULT NULL COMMENT '参与年份',
  `type` smallint(5) unsigned NOT NULL COMMENT '参与类别',
  `subject` varchar(511) NOT NULL COMMENT '培训主题',
  `is_attend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否与会',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5616 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_answer`
--

DROP TABLE IF EXISTS `user_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '员工主键',
  `year` smallint(5) unsigned NOT NULL COMMENT '年份',
  `season` tinyint(1) unsigned NOT NULL COMMENT '季度',
  `department_id` int(10) unsigned DEFAULT NULL COMMENT '部门主键',
  `question_id` int(10) unsigned NOT NULL COMMENT '问题主键',
  `answer` char(32) NOT NULL COMMENT '选择答案',
  `right` tinyint(1) NOT NULL DEFAULT '0' COMMENT '回答是否正确',
  `score` tinyint(2) NOT NULL DEFAULT '0' COMMENT '回答正确之后的分数',
  `create_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_answered`
--

DROP TABLE IF EXISTS `user_answered`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_answered` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '员工主键',
  `year` smallint(5) unsigned NOT NULL COMMENT '年份',
  `season` tinyint(1) unsigned NOT NULL COMMENT '季度',
  `department_id` int(10) unsigned DEFAULT NULL COMMENT '部门主键',
  `score` tinyint(2) NOT NULL DEFAULT '0' COMMENT '回答正确之后的分数',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL COMMENT '主键',
  `position_id` int(10) unsigned NOT NULL COMMENT '职位描述',
  `department_id` int(10) unsigned DEFAULT NULL COMMENT '部门主键',
  `manager_id` int(10) unsigned DEFAULT NULL COMMENT '上级主管主键',
  `is_manager` tinyint(1) unsigned DEFAULT '0' COMMENT '是否为VP',
  `code` char(32) NOT NULL COMMENT '外部系统中的员工代码',
  `avatar` varchar(1023) DEFAULT NULL COMMENT '用户头像',
  `first_name` varchar(255) NOT NULL COMMENT '姓',
  `last_name` varchar(255) NOT NULL COMMENT '名',
  `cn_name` varchar(255) NOT NULL COMMENT '中文名称',
  `en_name` varchar(255) NOT NULL COMMENT '英文名称',
  `title` varchar(255) DEFAULT NULL COMMENT '用户职称',
  `email` varchar(255) DEFAULT NULL COMMENT '用户邮箱',
  `mobile` varchar(255) NOT NULL COMMENT '手机号码',
  `tel` varchar(255) DEFAULT NULL COMMENT '电话号码',
  `risks` varchar(255) NOT NULL DEFAULT '' COMMENT '用户的风险点',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '数据状态',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


-- Dump completed on 2021-01-13 22:33:48
