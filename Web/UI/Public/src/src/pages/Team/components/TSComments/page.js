import { request } from '../../../../common/request.js';
import { formatDateTime } from '../../../../common/utils.js';

export default {
    props:['user', 'isBtnDisabled', 'comments', 'functionId','componentType','tableData','currentIndex'],
    data() {
        return {
            topicId: '',
            comment: '',
            commentsArr:this.comments
        }
    },
    computed: {
    },
    methods:{
        previous() {
            this.$emit('changeIndex',{name:this.componentType === 'manage-overall'?'apa':'ts',target:'pre',current:'comment'});
        },
        next() {
            this.$emit('changeIndex',{name:this.componentType === 'manage-overall'?'apa':'ts',target:'next',current:'comment'});
        },
        closeComponent() {
          this.$emit('changeComponent', 'close-manage-target-review');
        },
        addOAComment() {
            if (this.isBtnDisabled || !this.comment) return;
            request({
                url: 'addOAComment',
                isShowLoading: true,
                data: {
                    function_id: this.functionId,
                    employee_id: this.tableData[this.currentIndex].target.employee_id,
                    message: this.comment,
                    type: 'TS'
                }
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;

                    return;
                }


                this.$set(this.commentsArr, this.commentsArr.length, {
                    send_time:formatDateTime(new Date()),
                    name: this.user.en_name,
                    is_read: true,
                    type: 'me',
                    message:this.comment
                });

                this.comment = '';
            })
        },
        scrollToBottom() {
            this.$nextTick(() => {
                let commentList = this.$refs.commentList;
                commentList.scrollTop = commentList.scrollHeight;
            })
        }
    },
    created() {
    },
    mounted() {
        this.scrollToBottom();
    }
}
