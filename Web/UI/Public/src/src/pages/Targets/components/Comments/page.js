import { request } from '../../../../common/request.js';
import Confirm from '../../../../components/Confirm/page.vue';
export default {
    props:['func', 'user', 'target', 'delegator', 'isSaveBtnDisabled', 'comments','isAgreeScore'],
    data() {
        return {
            comment: '',
            titleTxt: '',
            confirmTxt: '',
            isShowConfirm: false,
        }
    },
    computed: {

    },
	components:{
		Confirm
	},
    methods:{
		confirm(){
			this.isShowConfirm = false
		},
        previous() {
            this.$emit('previousIndex');
        },
        closeComponent() {
            this.$emit('closeComponent');
        },
        getOaComments() {
            request({
                url: 'getOaComments',
                method: 'get',
                isShowLoading: true,
                params: {
                    function_id: this.func['id']
                }
            }).then(res => {
                if (res && res.list) {
                    res.list.forEach((item, index) => {
                        if (item['owner_id'] == this.user['id']) {
                            item.type = 'me';
                            item.name = item['employee_name'];
                        } else {
                            item.type = 'other';
                            item.name = item['delegator_name'];
                        }
                    });

                    this.$emit('setComments', res.list);

                    this.scrollToBottom();
                }
            })
        },
        addOAComment() {
          if (this.comment.length === 0) {
            return ;
          }

          if (this.isAgreeScore) return;
          request({
              url: 'addOAComment',
              isShowLoading: true,
              data: {
                  function_id: this.func['id'],
                  delegator_id: this.user['delegator_id'],
                  message: this.comment,
                  type: 'TS'
              }
          }).then(res => {
              if (res) {
                  this.comment = '';
                  this.getOaComments();
              }
          })
        },
        scrollToBottom() {
            this.$nextTick(() => {
                let commentList = this.$refs.commentList;
                commentList.scrollTop = commentList.scrollHeight;
            })
        }
    },
    created() {

    },
    mounted() {
        this.scrollToBottom();
    }
}
