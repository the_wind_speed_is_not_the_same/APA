package cn.porsche.web.service;


import cn.porsche.web.domain.*;

import java.util.List;
import java.util.Map;

public interface TargetKPIService {
    void deleteByTarget(Target target);
    TargetKPI update(TargetKPI kpi);

    List<TargetKPI> findByTarget(Target target, TargetWhat what);

    Map<String, Map<String, String>> check(Target target, TargetWhat what);
}
