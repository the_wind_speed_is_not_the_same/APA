import {request} from '../../common/request.js';
import UserInfo from './components/UserInfo/page.vue';
import {setAuthorization} from '../../common/utils.js';
import {mapState, mapActions} from 'vuex';
import Confirm from '../../components/Confirm/page.vue';

export default {
    data() {
        return {
            tableData: [],
            currentRow: null,
            tableHeight: 412,
            function_id: null,
            titleTxt: 'Confirm',
            confirmTxt: '',
            isShowConfirm: false,
            manager: null
        }
    },
    computed: {
        ...mapState({
            currentYear: state => (state.allState.currentYear || {year: new Date().getFullYear()}),
            userInfo: state => state.allState.userInfo,
            user: state => state.allState.user ? state.allState.user : '',
            notice: state => state.allState.notice,
            position: state => state.allState.position,
            department: state => state.allState.department ? state.allState.department : '',
            allList: state => state.allState.allList
        })
    },
    components: {
        UserInfo,
        Confirm
    },
    methods: {
        ...mapActions(['getUserInfo', 'setYearList', 'setCurrentYear']),
        getRowClass() {

        },
        rowClick(row, event, column) {
            // console.log(row, event, column);
        },
        handleCurrentChange1(val) {
            if (val) {
                setTimeout(() => {
                    this.$refs.table2.setCurrentRow(null);
                }, 0)
            }
        },
        handleCurrentChange2(val) {
            if (val) {
                setTimeout(() => {
                    this.$refs.table1.setCurrentRow(null);
                })
            }
        },
        confirm() {
            this.isShowConfirm = false
        },
        getTargetsList() {
            request({
                url: 'currentYearDetail',
                method: 'get',
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    // 没有任何 function 开启，不显示
                    return;
                }

                this.isShowLoading = false;
                if (res && res.data) {
                    this.tableData = res.data.whats
                } else {
                    this.tableData = [];
                }
            })
        }
    },
    async created() {
        setAuthorization(this.$route.query.type);
        if (!this.userInfo) {
            try {
                await this.getUserInfo();
            } catch (e) {
            }
        }

        this.manager = JSON.parse(localStorage.getItem('manager'));

        this.getTargetsList();

    },
    mounted() {
        this.$nextTick(() => {
            this.tableHeight = this.$refs.tableWrapper.offsetHeight;
        })
    }
}
