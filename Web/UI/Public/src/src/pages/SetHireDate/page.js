import {request} from '../../common/request.js';
import {mapState, mapActions} from 'vuex'
import Confirm from '../../components/Confirm/page.vue';

export default {
    data() {
        return {
            currentIndex: null,
            departmentId: '',
            filterHireDate: null,
            departmentList: [],
            keyWord: '',
            isShowLoading: false,
            tableHeight: 412,
            personalInfor: {},
            confirmTxt: null,
            isShowDialog: false,
            isDialogLoading: false,
            users: [],
            pageIndex: 1,
            pageSize: 25,
            totalRecord: 0,
            initHireDate: null,
            isShowNotice: false,
            isShowImportDialog: false,
            isShowDateSelect: false,
            selectIndex: 0,
            to: null,
            importText: '',

            titleTxt: '',
            isShowConfirm: false,
            isShowButtonLeft: false
        }
    },
    components: {
        Confirm
    },
    computed: {
        ...mapState({
        }),
        pageNumbers() {
            return this.totalRecord ? Math.ceil(this.totalRecord / this.pageSize) : 0
        }
    },
    filters: {
        deleteQuate(val) {
            if (val) {
                return val.replace(/(^["'])|(["']$)/g, "")
            }
            return ''
        }
    },
    methods: {
        ...mapActions(['setDelegatorInfo', 'setYearList']),
        rowClick(row) {
            this.personalInfor = row
            this.initHireDate = this.personalInfor.hire_date
            this.isShowDialog = true
        },
        beforeCloseDialog(done) {
            done()
            // this.$refs.table.setCurrentRow()
        },
        closeDialog() {
            this.isShowDialog = false
            this.isDialogLoading = false
            // this.$refs.table.setCurrentRow()
        },
        getUsers() {
            let params = {
                page: this.pageIndex,
                size: this.pageSize
            };

            if (this.departmentId != 0) {
                params['department_id'] = this.departmentId;
            }

            if (this.filterHireDate && this.filterHireDate === 'No HireDate') {
                params['only'] = 'HireDate';
            }

            if (this.keyWord) {
                params['key'] = this.keyWord;
            }

            request({
                url: 'getEmployeeList',
                method: 'get',
                params: params,
                isShowLoading: true
            }).then(res => {
                if (res && res.list) {
                    this.users = res.list;
                    this.totalRecord = res['pagebar'].total;
                }
            })
        },
        updateDate() {
            this.isDialogLoading = true
            if (this.initHireDate != this.personalInfor.hire_date) {
                request({
                    url: 'setEmployeeHireDate',
                    method: 'post',
                    data: {
                        employee_id: this.personalInfor.id,
                        hire_date: this.personalInfor.hire_date
                    },
                }).then(res => {
                    this.isShowNotice = true
                    if (res && res.code === 0) {
                        this.getUsers()
                    }

                    setTimeout(() => {
                        this.isShowNotice = false
                    }, 1500)
                    this.closeDialog()
                })
            } else {
                this.closeDialog()
            }
        },
        submitHireData() {
            if (this.importText == '') {
                this.titleTxt = 'Error';
                this.confirmTxt = 'Please input the import content';
                this.isShowConfirm = true;

                return;
            }

            this.isShowImportDialog = false;
            this.isDialogLoading    = true;
            request({
                url: 'importHireDate',
                method: 'post',
                data: {
                    function_id: this.function_id,
                    text: this.importText
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm      = true;
                    this.isShowImportDialog = true;

                    return false;
                }

                this.pageIndex  = 1;
                this.importText = '';
                this.isShowDateSelect   = false;

                this.titleTxt   = 'Success';
                this.confirmTxt = 'Import Successfully';
                this.isShowConfirm    = true;
                this.isShowButtonLeft = false;

                this.getUsers();
            })
        },
        changeDepartment(){
            this.pageIndex = 1
            this.getUsers()
        },
        changeHireDate(value) {
            this.filterHireDate = value;
            this.getUsers();
        },
        pageChange(e) {
            this.pageIndex = e
            this.getUsers()
        },
        importData() {
            this.isShowImportDialog = true
        },
        uploadSuccess(response, file, fileList) {
            // console.log(response, file)
        },
        cancel() {
            this.isShowConfirm = false;
        },
        confirm() {
            this.isShowConfirm = false;
        },
    },
    created() {
        this.getUsers();
    },
    mounted() {
        this.$nextTick(() => {
            // this.tableHeight = this.$refs.tableWrapper.offsetHeight - 28;
        })

        // 获取可筛选的部门
        request({
            url: 'getDepartmentByCompany',
            method: 'get',
            params: {}
        }).then(res => {
            if (res && res.data) {
                var list = [{'id': 0, 'name': 'All'}];

                res.data.forEach((item, idx) => {
                    list.push(item['department']);

                    if (item['sub_tree'].length) {
                        item['sub_tree'].forEach((sItem, sIdx) => {
                            sItem['department']['name'] = sItem['department']['name'] + "(" + item['department']['name'] + ")";
                            list.push(sItem['department']);

                            if (sItem['sub_tree'].length) {
                                sItem['sub_tree'].forEach((ssItem, ssIdx) => {
                                    ssItem['department']['name'] = ssItem['department']['name'] + "(" + item['department']['name'] + ")";

                                    list.push(ssItem['department']);
                                });
                            }
                        });
                    }
                });

                this.departmentList = list;
            }
        })
    }
}
