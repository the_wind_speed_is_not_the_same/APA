package cn.porsche.web.repository;

import cn.porsche.web.domain.Login;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends CrudRepository<Login, Integer> {
    Page<Login> findByUserId(Integer userId, Pageable pageable);
}
