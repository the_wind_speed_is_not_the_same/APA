package cn.porsche.compliance.email;

import cn.porsche.compliance.domain.Email;

import javax.mail.MessagingException;
import java.util.Properties;

public interface IEmail {
    void send(Email email) throws MessagingException;
    void send(String subject, String content, String to) throws MessagingException;

    void sendHtml(Email email) throws MessagingException;
    void sendHtml(String subject, String content, String to) throws MessagingException;

    Properties getProperties();
}
