package cn.porsche.web.repository;

import cn.porsche.web.domain.Target;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TargetRepository extends CrudRepository<Target, Integer> {
    @Query("SELECT t from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE ku.id=:kpiUserId")
    Target findByKPIUserId(Integer kpiUserId);


    @Query("SELECT t from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE ku.functionId=:functionId and ku.employeeId=:employeeId")
    Target findByFunctionIdAndEmployeeId(Integer functionId, Integer employeeId);

    @Query("SELECT t from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE ku.functionId=:functionId and ku.managerId=:managerId order by ku.enName asc")
    List<Target> findAllByFunctionIdAndManagerId(Integer functionId, Integer managerId);

    @Query("SELECT t from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE ku.functionId=:functionId and ku.delegatorId=:delegatorId order by ku.enName asc")
    List<Target> findAllByFunctionIdAndDelegatorId(Integer functionId, Integer delegatorId);

    @Query("SELECT t from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE ku.functionId=:functionId and ku.departmentId=:departmentId")
    List<Target> findAllByFunctionIdAndDepartmentId(Integer functionId, Integer departmentId);

    @Query("SELECT t from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE ku.functionId=:functionId and ku.departmentId=:departmentId")
    Page<Target> findAllByFunctionIdAndDepartmentId(Integer functionId, Integer departmentId, Pageable pageable);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// My Team DashBroad 处 的图表数据
    ///

    // 根据部门统计未开始的员工数
    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE t.TSESubmit=false and t.TSEConfirm=false and t.functionId=:functionId and ku.departmentId=:departmentId")
    int countTSByNotStartedByDepartmentId(Integer functionId, Integer departmentId);

    // 根据部门统计已Submitted的员工数
    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE t.TSESubmit=true and t.TSEConfirm=false and t.TSDConfirm=false and t.functionId=:functionId and ku.departmentId=:departmentId")
    int countTSBySubmittedByDepartmentId(Integer functionId, Integer departmentId);

    // 根据部门统计已Review的员工数
    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE t.TSESubmit=true and t.TSEConfirm=false and t.TSDConfirm=true and t.functionId=:functionId and ku.departmentId=:departmentId")
    int countTSByReviewedByDepartmentId(Integer functionId, Integer departmentId);

    // 根据部门统计已Confirmed的员工数
    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId WHERE t.TSESubmit=true and t.TSEConfirm=true and t.TSDConfirm=true and t.functionId=:functionId and ku.departmentId=:departmentId")
    int countTSByConfirmedByDepartmentId(Integer functionId, Integer departmentId);

    ///
    /// My Team DashBroad 处 的图表数据
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Reporting 处 的图表数据
    ///


    // 处理一级部门下的TS统计数据
    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department1=:departmentId AND t.TSESubmit=false")
    int countByNotStartedByDepartment1(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department1=:departmentId AND t.TSESubmit=true AND t.AgreeScore=false")
    int countByInProgressByDepartment1(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department1=:departmentId AND t.TSESubmit=true and t.AgreeScore=true")
    int countByFinishedByDepartment1(Integer functionId, Integer departmentId);



    // 处理二级部门下的TS统计数据
    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department2=:departmentId AND t.TSESubmit=false")
    int countTSByNotStartedByDepartment2(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department2=:departmentId AND t.TSESubmit=true AND t.TSEConfirm=false")
    int countTSByInProgressByDepartment2(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department2=:departmentId AND t.TSEConfirm=true")
    int countTSByFinishedByDepartment2(Integer functionId, Integer departmentId);


    // 处理二级部门下的APA统计数据
    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department2=:departmentId AND t.APAESubmit=false")
    int countAPAByNotStartedByDepartment2(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department2=:departmentId AND t.APAESubmit=true AND t.AgreeScore=false")
    int countAPAByInProgressByDepartment2(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department2=:departmentId AND t.AgreeScore=true")
    int countAPAByFinishedByDepartment2(Integer functionId, Integer departmentId);



    // 处理三级部门下的TS统计数据
    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department3=:departmentId AND t.TSESubmit=false")
    int countTSByNotStartedByDepartment3(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department3=:departmentId AND t.TSESubmit=true AND t.TSEConfirm=false")
    int countTSByInProgressByDepartment3(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department3=:departmentId AND t.TSEConfirm=true")
    int countTSByFinishedByDepartment3(Integer functionId, Integer departmentId);


    // 处理三级部门下的APA统计数据
    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department3=:departmentId AND t.APAESubmit=false")
    int countAPAByNotStartedByDepartment3(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department3=:departmentId AND t.APAESubmit=true AND t.AgreeScore=false")
    int countAPAByInProgressByDepartment3(Integer functionId, Integer departmentId);

    @Query("SELECT count(t) from Target t LEFT JOIN KPIUser ku ON t.id=ku.targetId LEFT JOIN DepartmentEmployee de ON ku.employeeId=de.employeeId WHERE ku.functionId=:functionId AND de.department3=:departmentId AND t.AgreeScore=true")
    int countAPAByFinishedByDepartment3(Integer functionId, Integer departmentId);


    ///
    /// Reporting 处 的图表数据
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
