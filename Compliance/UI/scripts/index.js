;(function(win){
	var docEl = win.document.documentElement;  //考虑以及兼容了 屏幕旋转的事件
	var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';  
	var recalc = function () { 
	    var clientWidth = docEl.clientWidth;          
	    if (!clientWidth) return;           
	    if (clientWidth >= 750) {
			docEl.style.fontSize = '200px';
		} else {
			docEl.style.fontSize = 100 * (clientWidth / 750) * 2 + 'px';
		}
	};   
	
	if (!win.document.addEventListener) return;
	win.addEventListener(resizeEvt, recalc, false);     // 屏幕大小以及旋转变化自适应
	win.document.addEventListener('DOMContentLoaded', recalc, false);     // 页面初次打开自适应
	recalc();
	
//	 接口地址
	var baseUrl = '/v1.0/';
	if(document.domain === "127.0.0.1") {
		baseUrl = 'http://fp.compliance-test.porsche-cloudservice.com:81/v1.0/'
	}
	
	win.apiUrl = {
		getQuestion:'question/answer',
		getSeason:'question/by_season',
		getUserInfor:'question/me',
		getTraining:'training/by_year',
		getRisk:'user/risks'
	}
	
	win.axios.defaults.baseURL = baseUrl;
	axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';
	axios.defaults.headers.post['Accept'] = 'application/json, text/plain, */*';
	
	var util = {
		/*
		* @params url 链接地址
		* @params item 参数名
		* @return 参数值
		*/
		getQuery:function(url, item){
			if(!url || !item || url.indexOf(item) === -1) return '';
			
			var regq = /\?/;
			var rega = /&/;
			var querys,res;
			
			if(regq.test(url)){
				querys = url.split('?')[1];
			}else{
				querys = url;
			}
			
			if(querys.indexOf(item) === -1) return '';
			
			if(rega.test(querys)){
				querys.split('&').map(function(v,i){
					var val = v.split('=')
					if(val[0] == item){
						res = val[1]
					}
				})
			}else{
				res = querys.split('=')[1];
			}
			
			return res;
		},
		/* 
		* @params node 设置的节点
		* @params attrs 属性集合
		*/
		setAttribute:function(node,attrs){
			for(let attr in attrs){
				node.setAttribute(attr,attrs[attr])
			}
		},
		/*
		* 创建弹窗
		*/
		createDialog:function(opt){
			var div = document.createElement('div');
			var id = 'id' + new Date()*1;
			var obj = {
				title: opt.title || '', // 弹窗标题
				content: opt.content || '', // 弹窗内容
				canClose: opt.canClose != undefined?opt.canClose:true, // 弹窗关闭按钮是否显示，默认显示
				dur: opt.dur || 0, // 弹窗显示的时间，默认一直在
				offset: opt.offset || {left:0,top:0}, // 弹窗位置偏移
				haveCancel: opt.haveCancel != undefined?opt.haveCancel:true, // 弹窗取消按钮是否存在，默认存在
				haveConfirm: opt.haveConfirm != undefined?opt.haveConfirm:true, // 弹窗确定按钮是否存在，默认存在
				id: opt.id || id, // 弹窗id
				cancelTxt: opt.cancelTxt || 'CANCEL', // 弹窗取消按钮名称
				confirmTxt: opt.confirmTxt || 'CONFIRM', // 弹窗确定按钮名称
				cover: opt.cover != undefined?opt.cover:true ,// 弹窗遮罩层
				cancelCallback: opt.cancelCallback,
				confirmCallback: opt.confirmCallback ? opt.confirmCallback : 'util.closeAllDialog'
			}
			
			var objDiv = {
				id: obj.id,
				class: 'common-dialog flex-box flex-center'
			}
			
			var html = '',className;
			
			if(obj.haveCancel && obj.haveConfirm){
				className = 'group-btn'
			}else{
				className = 'single-btn'
			}
			
			this.setAttribute(div,objDiv);
			
			function removeDialog(){
				var dialogId = document.getElementById(id)
				dialogId.parentNode.removeChild(dialogId)
			};
			
			html = `
				${obj.cover?'<div class="dialog-cover"></div>':''}
				<div class="content-box flex-box flex-column flex-center" style="margin:${obj.offset.top?obj.offset.top:0} 0 0 ${obj.offset.left?obj.offset.left:0};">
					<div class="content-top">
						<div class="title ${obj.title?'':'none'}">${obj.title}</div>
						${obj.canClose?'<div class="close" onClick="' + obj.cancelCallback + '()">&times;</div>':''}
					</div>
					<div class="content-mid">
						${obj.content}
					</div>
					<div class="content-bott ${className}">
						${obj.haveCancel?'<div class="cancel-btn btn" onClick="' + obj.cancelCallback + '()">'+obj.cancelTxt+'</div>':''}
						${obj.haveConfirm?'<div class="confirm-btn btn" onClick="' + obj.confirmCallback + '()" >' + obj.confirmTxt + '</div>':''}
					</div>
				</div>
			`;
			
			div.innerHTML = html;
			document.body.appendChild(div);
			
			if (obj.dur) {
				setTimeout(removeDialog,obj.dur)
			}

			return objDiv;
		},
		setStorage(item,val){
			win.localStorage.setItem(item,val)
		},
		getStorage(item){
			return win.localStorage.getItem(item)
		},
		removeStorage(item){
			win.localStorage.removeItem(item)
		},
		getCookie(name){
			var prefix = name + "="
			var start = document.cookie.indexOf(prefix)
			if (start == -1) {
				return null;
			}
			var end = document.cookie.indexOf(";", start + prefix.length)
			if (end == -1) {
				end = document.cookie.length;
			}
			return document.cookie.substring(start + prefix.length, end)
		},
        clearCookie() {
            var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
            if (keys) {
                for (var i = keys.length; i--;)
                    document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString()
            }
        },
		showLoading(){
			var div = document.createElement('div')
			div.className = 'loading'
			div.innerHTML = '<span>loading...</span>'
			document.body.appendChild(div);
		},
		closeLoading(){
			var loading = document.querySelector('.loading')
			if(loading){
				loading.parentNode.removeChild(loading)
			}
		},
		closeAllDialog(){
			util.closeLoading();
			var loading = document.querySelector('.common-dialog')
			if (loading) {
				loading.parentNode.removeChild(loading)
			}
		}
	}
	
	var st = util.getQuery(location.href,'st');
	var pathname = win.location.pathname;

	 if (!!st) {
		 util.clearCookie();

        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }

        // 组装 sso 登录需要的 service（根据不同域名环境组装不同的 service）
		 var test    = window.location.hostname.indexOf('-test') !== 0 ? '&test=true' : '';
		 var service = encodeURIComponent(window.location.origin + window.location.pathname);

        return window.location.href = `/login/sso_h5_validate?ticket=${st}&uri=${pathname}&service=${service}${test}`;
	 } else {
	 	if (!util.getCookie('auth_id')) {
	 		util.createDialog({
	 			title:'Warning',
	 			content:'Please login at first',
	 			canClose:false,
	 			haveCancel:false,
	 			haveConfirm:false
	 		})
	 	}
	 }

	win.util = util;
}(window || {}))