package cn.porsche.compliance.task.parsing.exception;

public class ParsingException extends Exception {
    public static ParsingException TheDirectoryIsNotRead(String directory) {
        return new ParsingException(String.format("指定的目录[%s]无法访问", directory));
    }

    public static ParsingException ItemsLengthException(Integer rightLength, Integer errorLength, String filed ) {
        return new ParsingException(String.format("解析期望的长度不一致：期望长度：%d，而实际长度：%d，对应内容:%s", rightLength, errorLength, filed));
    }

    public static ParsingException PositionNotExistInDatabase(String filed ) {
        return new ParsingException(String.format("解析数据出错：对应的Job信息【%s】未存储到数据库中.", filed));
    }


    public static ParsingException DepartmentNotExistInDatabase(String filed ) {
        return new ParsingException(String.format("解析数据出错：对应的Department信息【%s】未存储到数据库中.", filed));
    }

    public static ParsingException ParsingFileException(String filename, String message) {
        return new ParsingException(String.format("解析文件[%s]出错，提示信息：%s", filename, message));
    }

    public static ParsingException ParsingApiException(String response) {
        return new ParsingException(String.format("请求 Api 接口成功，但与预期不符。返回内容信息：%s", response));
    }

    public static ParsingException ParsingApiItemException(String api, String key) {
        return new ParsingException(String.format("请求 Api[%s] 接口成功，但返回的结构中无法获取[%s]。", api, key));
    }

    public ParsingException(String message) {
        new Exception(message);
    }
}
