import Confirm from '../../components/Confirm/page.vue';
import {request} from '../../common/request.js';
import ListSelect from '../../components/ListSelect/page.vue';

export default {
    data() {
        return {
            start: '2019',
            finish: new Date().getFullYear(),
            releaseYear: '2020',
            is_ts: null,
            is_apa: null,
            isTsDisabled: true,
            isApaDisabled: true,
            isShowDialog: false,
            email: '',
            titleTxt: 'Confirm',
            confirmTxt: '',
            isShowButtonLeft: false,
            isShowConfirm: false,
            tsId: null,
            isShowListSelect: false,
            dateList: [],
            selectIndex: 1,
            currentDate: new Date(),
            user: {}
        }
    },
    components: {
        Confirm,
        ListSelect
    },
    methods: {
        initDateList() {
            let lastYear = new Date().getFullYear() + 1;
            let dateList = [];
            for (let i = 0; i < 5; i++) {
                dateList.push(lastYear - i);
            }

            this.dateList = dateList;

            if (localStorage.getItem('user')) {
                this.user = JSON.parse(localStorage.getItem('user'))
            }
        },
        showDateList() {
            this.isShowListSelect = true;
        },
        confirmIndex(index) {
            this.selectIndex = index;
            this.isShowListSelect = false;
            this.finish = this.dateList[this.selectIndex];
            this.getFRStatus();
        },
        setStorage(value) {
            localStorage.setItem('function_id', value);
        },
        getFRStatus() {
            if (!this.finish) {
                return;
            }

            request({
                url: 'getFRStatus',
                method: 'get',
                params: {
                    year: this.finish
                }
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.is_apa = !status;
                    this.confirmTxt = res.message;
                    this.titleTxt = 'Fail';

                    return;
                }

                if (res && res.data) {
                    let data = res.data;
                    this.is_ts = data.is_ts;
                    this.is_apa = data.is_apa;
                    this.tsId = data.id;
                    this.isTsDisabled = this.is_ts;
                    this.isApaDisabled = this.is_apa;

                    if (res.data.id) {
                        this.setStorage(res.data.id);
                    }
                }
            })
        },
        sendEmail() {
            this.isShowDialog = false;
            this.isShowConfirm = true;
        },
        openTs(status) {
            request({
                url: 'tsRelease',
                method: 'get',
                params: {
                    year: this.finish
                },
                isShowLoading: true,
                timeout: 0
            }).then(res => {
                this.isShowConfirm = true;
                this.isShowButtonLeft = false;

                if (!res || res.code !== 0 || !res.data.is_ts) {
                    this.confirmTxt = res.message ? res.message : 'Error in release action. Please try again?';
                    this.titleTxt = 'Error';

                    return false;
                }

                this.is_ts  = res.data.is_ts;
                this.is_apa = res.data.is_apa;
                this.tsId = res.data.id;
                this.setStorage(res.data.id);

                this.confirmTxt = 'The Target Setting Release Successfully';
                this.titleTxt = 'Success';
            })
        },
        openApa(status) {
            request({
                url: 'apaRelease',
                method: 'get',
                params: {
                    year: this.finish
                },
                isShowLoading: true,
                timeout: 0
            }).then(res => {
                this.isShowButtonLeft = false;
                this.isShowConfirm = true;
                if (!res || res.code !== 0 || !res.data['is_apa']) {
                    this.is_apa = false;

                    this.confirmTxt = res && res.message ? res.message : 'Error in release action. Please try again?';
                    this.titleTxt = 'Error';

                    return false;
                }


                this.is_apa = res.data.is_apa;
                this.setStorage(res.data.id);

                this.titleTxt = 'Success';
                this.confirmTxt = 'The APA Release Successfully';
            })
        },
        confirm() {
            this.isShowConfirm = false;
            // let url = this.apaId?'apaEmail':'tsEmail'
            // let function_id = this.apaId?this.apaId:this.tsId
            //
            // request({
            //   url: url,
            //   data: {
            //     content: this.email,
            //     function_id: function_id,
            //     subject: ''
            //   }
            // }).then(res => {
            //   if (res && res.data) {
            //     this.isShowConfirm = false;
            //   }
            // })
        },
        cancel() {
            this.isShowConfirm = false;
            // this.isShowDialog = true;
        },
        closeDialog() {
            this.isShowDialog = false;
        },
        tsChange(status) {
            this.openTs(status);
        },
        apaChange(status) {
            this.openApa(status)
            // this.isShowDialog = status;
        }
    },
    created() {
        this.initDateList();
        this.getFRStatus();
    }
}
