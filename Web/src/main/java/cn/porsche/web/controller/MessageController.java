package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.CommentTypeEnum;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.domain.*;
import cn.porsche.web.email.IEmail;
import cn.porsche.web.service.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;

@RestController
@RequestMapping("/message")
@ApiModel(value = "/message", description = "用户的消息列表")
public class MessageController extends BaseController {
    @Autowired
    UserService userService;

    @Autowired
    IEmail mailService;

    @Autowired
    TargetService targetService;

    @Autowired
    MessageService messageService;

    @Autowired
    KPIUserService kpiUserService;

    @Autowired
    TargetWhatService whatService;

    @Autowired
    FunctionService functionService;

    @Autowired
    CommentService commentService;


    @ApiOperation(
            value = "一次性返回用户的消息列表"
    )
    @GetMapping(value = "/unread")
    public String list() {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.CHECK, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        return ApiResult.builder().list(messageService.findUnReadMessages(user)).toJson();
    }


    @ApiOperation(
            value = "将消息设置为已读"
    )
    @GetMapping(value = "/read")
    public String read(
            @RequestParam(value = "message_id") Integer messageId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Message message = messageService.findById(messageId);
        if (null == message) {
            return ApiResult.isBad("Unable to find the Message data, please contact your HR to fix it");
        }

        if (!message.getReceiverId().equals(user.getId())) {
            return ApiResult.isBad("Unable to find the Message data, please contact your HR to fix it");
        }

        if (message.isRead()) {
            return ApiResult.isOk();
        }

        if (null == messageService.readMessage(message, user)) {
            return ApiResult.isBad("Operation failed. Please try again or contact HR to fix it");
        }

        return ApiResult.isOk();
    }

    // TODO: 发送邮件
    @ApiOperation(
            value = "【员工】或【委托人】在 Overall 界面新增 Comment 接口",
            notes = "JSON(Comment对象)参数：employee_id（发送给员工），delegator_id（发送给管理员），message（消息）两个参数"
    )
    @PostMapping(value = "/add_oa_comment")
    public String addOAComment(@RequestBody Comment comment) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (StringUtils.isEmpty(comment.getMessage())) {
            return ApiResult.isBad("no message");
        }

        if (null == comment.getType()) {
            // 此处的Type 只是用来生成邮件通知时的路径使用
            return ApiResult.isBad("no type");
        }

        Function function;
        if (null == comment.getFunctionId() || null == ( function = functionService.findById(comment.getFunctionId()) )) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        Target target;
        Message message;
        User delegator, employee;
        if (comment.getEmployeeId() != null) {
            // 说明是委托人发送给员工的消息
            delegator = user;
            employee  = userService.findById(comment.getEmployeeId());
            kpiUser   = kpiUserService.findByFunction(function, employee);
            if (!delegator.getId().equals(kpiUser.getManagerId()) && !delegator.getId().equals(kpiUser.getDelegatorId())) {
                return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
            }

            if (null == ( target = targetService.findByUser(function, employee) )) {
                return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
            }

            employee  = userService.findById(comment.getEmployeeId());
            message   = messageService.addDelegatorOVComment(function, target, delegator, employee, comment.getType());

        } else {
            // 说明是员工发给委托人的消息
            employee  = user;
            kpiUser   = kpiUserService.findByFunction(function, user);
            delegator = userService.findById(kpiUser.getDelegatorId());

            if (null == ( target = targetService.findByUser(function, employee) )) {
                return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
            }

            message = messageService.addEmployeeOVComment(function, target, employee, delegator);
        }

        if (delegator == null || employee == null) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }

        commentService.add(function, delegator, employee, user, CommentTypeEnum.OV, target.getId(), comment.getMessage());

        try {
            mailService.sendHtml(message.getSubject(), message.getMessage(), message.getReceiverEmail());
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "【委托人】或【管理员】对【What】部分 新增 Comment 接口（只有Delegator和Manager可以添加TS的Comment，员工只有能看）",
            notes = "JSON(Comment对象)参数：function_id（阶段主键），message（消息）, topic_id（每个What的主键）"
    )
    @PostMapping(value = "/add_ts_comment")
    public String addTSComment(@RequestBody Comment comment) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Delegator, GroupNameEnum.DisciplinaryManager)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (null == comment.getMessage() || comment.getMessage().length() == 0) {
            return ApiResult.isBad("Unable to find the Message data, please contact your HR to fix it");
        }

        Function function;
        if (null == comment.getFunctionId() || null == ( function = functionService.findById(comment.getFunctionId()) )) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        // 获取到当前用户的Detegator数据
        TargetWhat what;
        if (null == comment.getTopicId() || null == ( what = whatService.get(comment.getTopicId()))) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        User employee;
        if (null == what.getEmployeeId() || null == ( employee = userService.findById(what.getEmployeeId()) )) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }

        if (null == ( target = targetService.findByUser(function, employee) )) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }

        if (( kpiUser = kpiUserService.findByFunction(function, employee) ) == null
                || !user.getId().equals(kpiUser.getDelegatorId()) && !user.getId().equals(kpiUser.getManagerId())
        ) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }

        Comment data = commentService.add(function, user, employee, user, CommentTypeEnum.TS, what.getId(), comment.getMessage());
        if (null ==  data) {
            return ApiResult.isBad("Email send failed. Please try again or contact HR to fix it");
        }

        if (!what.isEmployeeRed()) {
            // 经理发送给员工，需要红点提示
            what.setEmployeeRed(true);

            whatService.update(what);
        }

        // 发送消息
        Message message = messageService.addDelegatorOVComment(function, target, user, employee, CommentTypeEnum.TS);
        try {
            mailService.sendHtml(message.getSubject(), message.getMessage(), message.getReceiverEmail());
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return ApiResult.builder().data(data).toJson();
    }


    @ApiOperation(
            value = "【员工】或【委托人】在APA阶段 新增 Comment 接口",
            notes = "JSON(Comment对象)参数：function_id（阶段主键），message（消息）, topic_id（每个What的主键）"
    )
    @PostMapping(value = "/add_apa_comment")
    public String addAPAComment(@RequestBody Comment comment) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        if (null == comment.getMessage() || comment.getMessage().length() == 0) {
            return ApiResult.isBad("Unable to find the Message data, please contact your HR to fix it");
        }

        Function function;
        if (null == comment.getFunctionId() || null == ( function = functionService.findById(comment.getFunctionId()) )) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }


        TargetWhat what;
        if (null == comment.getTopicId() || null == ( what = whatService.get(comment.getTopicId())) ) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        // 发送消息
        Message message;
        User employee, delegator;
        if (!what.getEmployeeId().equals(user.getId())) {
            // 当前为管理员
            if (!what.isEmployeeRed()) {
                what.setEmployeeRed(true);
                whatService.update(what);
            }

            delegator = user;
            employee  = userService.findById(what.getEmployeeId());
            message   = messageService.addDelegatorOVComment(function, target, delegator, employee, CommentTypeEnum.APA);
        } else {
            if (!what.isDelegatorRed()) {
                what.setDelegatorRed(true);
                whatService.update(what);
            }

            KPIUser me = kpiUserService.findByFunction(function, user);

            employee  = user;
            delegator = userService.findById(me.getDelegatorId());
            message   = messageService.addEmployeeOVComment(function, target, user, employee);
        }

        // 获取到当前用户的Detegator数据
        if (null == delegator || null == employee) {
            return ApiResult.isBad("Unable to find the Employee or Delegator, please contact your HR to fix it");
        }

        if (null == commentService.add(function, delegator, employee, user, CommentTypeEnum.APA, what.getId(), comment.getMessage()) ) {
            return ApiResult.isBad("Email send failed. Please try again or contact HR to fix it");
        }

        try {
            mailService.sendHtml(message.getSubject(), message.getMessage(), message.getReceiverEmail());
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "【员工】在【TargetSetting】页 【Overall】部分 获取所有的 Comment 接口",
            notes = "GET参数：function_id（阶段主键）"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "function_id", required = true, dataTypeClass = Integer.class, value = "阶段主键")
    })
    @GetMapping(value = "/get_oa_comments")
    public String getOAComments(
            @RequestParam(value = "function_id") Integer functionId
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findById(functionId);
        if (null == function) {
            return ApiResult.isBad("Unable to find the Function data, please contact your HR to fix it");
        }

        if (null == ( target = getTarget(function) )) {
            return ApiResult.isBad("Unable to find the Target data, please contact your HR to fix it");
        }

        if (null == ( kpiUser = targetService.findKPIUserByTarget(target) )) {
            return ApiResult.isBad("Unable to find the Employee data, please contact your HR to fix it");
        }

        return ApiResult.builder().list(commentService.findByTopicByIdAsc(CommentTypeEnum.OV, target.getId())).toJson();
    }

    @ApiOperation(
            value = "消息已读",
            notes = "GET参数：topic_id（消息主键），type:TS,APA,OV"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topic_id", required = true, dataTypeClass = Integer.class, value = "消息主键")
    })
    @GetMapping(value = "/read_comment")
    public String ovRead(
            @RequestParam(value = "topic_id") Integer topicId,
            @RequestParam(value = "type") CommentTypeEnum type
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.ADD, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        switch (type) {
            case OV:
                commentService.read(user, type, topicId);
                break;
            default:
                commentService.read(user, type, topicId);

                whatService.clearRedDot(user, whatService.get(topicId));
        }

        return ApiResult.isOk();
    }
}
