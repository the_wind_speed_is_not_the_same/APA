package cn.porsche.web.controller;


import cn.porsche.common.response.ApiResult;
import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.constant.ScoreLevelEnum;
import cn.porsche.web.domain.*;
import cn.porsche.web.email.IEmail;
import cn.porsche.web.exceptions.FunctionException;
import cn.porsche.web.service.*;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;


@Log4j2
@RestController
@RequestMapping("/function")
@Api(value = "/function", description = "绩效团队开启考评类")
public class FunctionController extends BaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    DepartmentService departmentService;

    @Autowired
    DepartmentReportService reportService;

    @Autowired
    MessageService messageService;

    @Autowired
    IEmail mailService;


    @ApiOperation(
            value = "【员工】查看所有的年份的 Function 列表接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功", response = Function.class, responseContainer = "List")
    })
    @GetMapping(value = "/available")
    public String available() {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.CHECK, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        return ApiResult.builder().list(functionService.available()).toJson();
    }

    @ApiOperation(
            value = "【HR】给定年份查看对应【Function】的接口",
            notes = "GET参数：finish_year（结束年份）"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "finish_year", required = true,  value = "KPI考核结束年份"),
    })
    @GetMapping(value = "/status")
    public String status(
            @RequestParam(value = "year") Integer year
    ) {
        if (noPromise(ModuleNameEnum.TargetSettingAndUpdate, OperatorText.CHECK, GroupNameEnum.Employee)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findByYear(year);
        if (function == null) {
            function = Function.builder()
                    .id(null)
                    .year(year)
                    .TS(false)
                    .TSTime(null)
                    .TSUserId(null)
                    .lastTSEmailId(null)
                    .TSMailSuccess(false)
                    .APA(false)
                    .APATime(null)
                    .APAUserId(null)
                    .lastAPAEmailId(null)
                    .APAMailSuccess(false)
                    .complete(false)
                    .completeTime(null)
                    .build();
        }

        return ApiResult.builder().data(function).toJson();
    }


    @ApiOperation(
            value = "【HR】在【Function】开启对应的【绩效】的接口",
            notes = "GET参数：start_year（开始年份），finish_year（结束年份）<br />" +
                    "开启有两步：一步点击【release】按钮，最后准备发送邮件。停留在任何一步，下次进来会进到对应的界面。详情：https://gitee.com/the_wind_speed_is_not_the_same/APA/issues/I1S3YE"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "year",  required = true, dataTypeClass = Integer.class, value = "KPI考核开始年份"),
    })
    @GetMapping(value = "/ts_release")
    @Transactional(rollbackFor = FunctionException.class)
    public String tsRelease(@RequestParam(value = "year") Integer year) throws FunctionException {
        if (noPromise(ModuleNameEnum.FunctionSetting, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findByYear(year);
        if (function == null) {
            try {
                function = functionService.releaseTS(year, getUser());
            } catch (FunctionException e) {
                e.printStackTrace();

                throw e;
            }
        }

        try {
            // 自动填充不用手动填写的TargetSetting
            autoTS(function);
            tsReleaseNotice(function);
        } catch (Exception e) {
            e.printStackTrace();

            throw FunctionException.AutoTargetSettingError();
        }

        function.setTS(true);
        functionService.update(function);

        // 采用异步操作
        class InitReporting implements Runnable {
            private Function function;
            InitReporting(Function function) {
                this.function = function;
            }

            @Override
            public void run() {
                for (Department item : departmentService.findAllDepartments()) {
                    // 初始化部门报表
                    reportService.initReporting(function, item);

                    // 获取所有部门的报表数据
                    reportService.refreshReporting(function, item);
                }
            }
        }

        // 开始创建部门报表
        new Thread(new InitReporting(function)).start();

        return ApiResult.builder().data(function).toJson();
    }

    @ApiOperation(
            value = "【HR】在【Function】开启对应的【APA】的接口",
            notes = "GET参数：year（查询年份）<br />" +
                    "开启有两步：一步点击【release】按钮，最后准备发送邮件。停留在任何一步，下次进来会进到对应的界面。详情：https://gitee.com/the_wind_speed_is_not_the_same/APA/issues/I1S3YE"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "year", required = true, dataTypeClass = Integer.class, value = "KPI考核结束年份"),
    })
    @GetMapping(value = "/apa_release")
    @Transactional(rollbackFor = FunctionException.class)
    public String apaRelease(@RequestParam(value = "year") Integer year) throws FunctionException {
        if (noPromise(ModuleNameEnum.FunctionSetting, OperatorText.CHECK, GroupNameEnum.HRPerformanceTeam)) {
            // 未有权限
            return ApiResult.isBad("You do not have permission to access, please contact your HR to request access");
        }

        Function function = functionService.findByYear(year);
        if (function == null) {
            return ApiResult.isBad("The Target Setting is not opened, please contact your HR");
        }

        function = functionService.releaseAPA(year, user);

        // 设置长假员工
        List<User> users = groupService.findByGroup(groupService.findByName(GroupNameEnum.LongLeaves.getName()));
        for (User item : users) {
            targetService.autoAppraisal(function, item, ScoreLevelEnum.C);
        }

        return ApiResult.builder().data(function).toJson();
    }


    // 大量手动填写的人员，自动填写对应的Target Setting
    private void autoTS(Function function) throws Exception {
        // 利用多线程添加数据

        // 利用多核 CPU 加快速度
        float count = userService.count();
        int page  = Runtime.getRuntime().availableProcessors() + 1;
        int size  = (int) Math.ceil(count / page);

        if (count < page) {
            page = 1;
            size = 1;
        }

        log.info("一共{}页，准备启用 {} 个线程，每个线程处理 {} 个内容", page, page, count);
        CountDownLatch latch = new CountDownLatch(page);

        for (int i = 1; i <= page; i++) {
            final int finalPage = i;
            final int finalSize = size;
            log.info("一共{}页，准备启用 {} 个线程，当前第{}页", page, page, finalPage);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    List<User> users = userService.findAllByPage(finalPage, finalSize).getContent();
                    log.info("当前第{}页, 处理 {} 个数", finalPage, users.size());

                    for (User item : users) {
                        if (!item.getNeedKPI()) {
                            // 不需要参与绩效考核的人员
                            continue;
                        }

                        if (item.getNeedHands() || groupService.inTopManagerGroup(item)) {
                            log.info("用户需要手动填写TS：{}, {}", item.getId(), item.getEnName());
                            continue;
                        }

                        try {
                            targetService.autoTargetSetting(function, item);
                            log.info("用户TS自动填写完毕：{}, {}", item.getId(), item.getEnName());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    latch.countDown();
                }
            }).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();

            throw e;
        }

        log.info("所有自动打分线程完成工作");
    }

    // 邮件提醒
    private void tsReleaseNotice(Function function) {
        class SendMail implements Runnable {
            private IEmail mailService;
            private UserService userService;
            private MessageService messageService;

            public SendMail(UserService userService, MessageService messageService, IEmail mailService) {
                this.mailService = mailService;

                this.userService = userService;
                this.messageService = messageService;
            }

            @Override
            public void run() {
                float count = userService.countByNeedHands();
                log.info("准备给 {} 个用户发送开启绩效评估邮件", count);
                int page = 0, size = 50, max = (int) Math.ceil(count / size);

                List<User> users;
                while (( ++ page) <= max) {
                    users = userService.findAllByHands(page, size).getContent();
                    for (User item : users) {
                        log.info("准备给 {}, {} 发送邮件", item.getId(), item.getEnName());
                        Message message = messageService.tsReleaseNotice(function, item);
                        try {
                            mailService.sendHtml(message.getSubject(), message.getMessage(), message.getReceiverEmail());
                            log.info("{}, {} 发送邮件已都完成", item.getId(), item.getEnName());
                        } catch (MessagingException e) {
                            log.info("{}, {} 发送邮件出现异常", item.getId(), item.getEnName());

                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        new Thread(new SendMail(userService, messageService, mailService)).start();
    }
}
