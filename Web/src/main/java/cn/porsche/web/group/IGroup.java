package cn.porsche.web.group;

import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.GroupRole;

import java.util.List;

public interface IGroup {
    boolean noPromise(Group group, String module, OperatorText operator);


    Group findByName(String name);
    GroupRole update(GroupRole role);
    GroupRole findByGroupAndModule(Group group, String module);

    List<GroupRole> getRoles(Group group);
}
