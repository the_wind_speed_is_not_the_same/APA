package cn.porsche.web.email.impl;

import cn.porsche.web.email.AbstractEmail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class SpringBootEmail extends AbstractEmail {
    private static Properties properties;

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.properties.mail.smtp.port}")
    private int port;

    @Value("${spring.mail.username}")
    private String username;

    @Value("${spring.mail.password}")
    private String password;

    @Value("${spring.mail.properties.mail.transport.protocol}")
    private String protocol;

    @Value("${spring.mail.properties.debug}")
    private String debug;

    @Value("${spring.mail.properties.test}")
    private String test;


    @Override
    public Properties getProperties() {
        if (properties == null) {
            Properties props = new Properties();

            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", port);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", true);
            props.put("mail.smtp.starttls.required", true);

            props.put("mail.transport.protocol", protocol);
            props.put("mail.username", username);
            props.put("mail.password", password);
            props.put("mail.debug", debug);
            props.put("mail.test", test);

            properties = props;
        }

        return properties;
    }
}
