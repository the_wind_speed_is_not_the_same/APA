package cn.porsche.web.constant;

public enum LoginTypeEnum {
    PASSWORD("password"),
    SSO("sso"),
    ;

    private String type;
    LoginTypeEnum(String type) {
        this.type = type;
    }
}
