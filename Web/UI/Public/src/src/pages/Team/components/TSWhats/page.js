import {request} from '../../../../common/request.js';
import ListSelect from '../../../../components/ListSelect/page.vue';
import {mapState, mapActions} from 'vuex';
import Confirm from '../../../../components/Confirm/page.vue';

export default {
    props: ['user', 'whats', 'whatIndex', 'currentIndex', 'componentType', 'tableData'],
    data() {
        return {
            tableHeight: 500,
            howData: [
                {
                    name: 'Dimensions'
                },
                {
                    name: 'Overall Comments'
                }
            ],
            selectIndex: 0,
            isShowListSelect: false,
            titleTxt: '',
            isShowConfirm: false,
            confirmTxt: '',
            showTime: 0
        }
    },
    watch: {
        whatIndex: {
            handler(n, o) {
                if (n > -1) {
                    this.$refs.table1.setCurrentRow(this.whats[n], true)
                    this.$refs.table2.setCurrentRow()
                } else {
                    this.$refs.table1.setCurrentRow()
                    if (n == -1) {
                        this.$refs.table2.setCurrentRow(this.howData[0], true)
                    } else {
                        this.$refs.table2.setCurrentRow(this.howData[1], true)
                    }
                }
            }
        },
    },
    computed: {
        ...mapState({
            allList: state => state.allState.allList,
            yearList: state => state.allState.yearList
        }),
        tartgetDetail() {
            return this.tableData[this.currentIndex].target
        },
        hasCommentUnread() {
            let comments = this.tableData[this.currentIndex].overall;

            if (!comments || !comments.length) {
                return false;
            }

            return comments.some((item) => (!item['is_read'] && item['owner_id'] != this.user['id']));
        }
    },
    components: {
        ListSelect,
        Confirm
    },
    methods: {
        ...mapActions(['setYearList']),
        close() {
            this.$emit('changeComponent', 'close-ts');
        },
        showList() {
            this.isShowListSelect = true;
        },
        confirmIndex(index) {
            this.selectIndex = index;
            this.isShowListSelect = false;
            this.$emit('getTargetList', this.allList[index]);
        },
        handleCurrentChange1(val) {
            this.$emit('changeComponent', 'manage-ts-detail', val);
            this.$refs.table2.setCurrentRow()
        },
        handleCurrentChange2(val) {
            let index = this.howData.indexOf(val);
            this.$refs.table1.setCurrentRow()
            if (index === 0) {
                this.$emit('changeComponent', 'dimension');
            } else {
                this.$emit('changeComponent', 'overall');
                if (this.hasCommentUnread) {
                    request({
                        url: 'readComment',
                        method: 'get',
                        params: {
                            type: 'OV',
                            topic_id: this.tableData[this.currentIndex]['overall'][0]['topic_id']
                        }
                    }).then(res => {
                        if (!res || res.code !== 0) {
                            this.titleTxt = 'Error';
                            this.confirmTxt = res.message;
                            this.isShowButtonLeft = false;
                            this.isShowConfirm = true;

                            return false;
                        }

                        let comments = this.tableData[this.currentIndex]['overall'];
                        comments.forEach(item => {
                            if (item['owner_id'] != this.user['id']) {
                                item['is_read'] = true;
                            }
                        });
                    });
                }
            }
        },
        confirm() {
            this.isShowConfirm = false;
        },
        confirmTs() {
            if (!this.tartgetDetail.is_ts_delegator_confirm && this.tartgetDetail.is_ts_employee_submit) {
                request({
                    url: 'setTsDelegatorConfirm',
                    method: 'get',
                    params: {
                        function_id: this.tartgetDetail.function_id,
                        employee_id: this.tableData[this.currentIndex].user.employee_id
                    },
                    isShowLoading: true
                }).then(res => {
                    if (!res || res.code !== 0) {
                        this.isShowConfirm = true;
                        this.titleTxt = 'Error';
                        this.confirmTxt = res.message ? res.message : 'Confirm Failed, Please try again';

                        return false;
                    }

                    this.isShowConfirm = true;
                    this.titleTxt = 'Success';
                    this.confirmTxt = 'Confirm successfully';
                    this.$emit('confirmDelegator', true)
                    this.tableData[this.currentIndex].target.is_ts_delegator_confirm = true
                })
            }
        },
    },
    created() {
    },
    mounted() {
        this.$nextTick(() => {
            this.tableHeight = this.$refs.tableTop.offsetHeight - 24;
        })

        this.$refs.table1.setCurrentRow(this.whats[0], true)
    }
}
