package cn.porsche.compliance.controller;

import cn.porsche.common.response.ApiResult;
import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.*;
import cn.porsche.compliance.emnu.SeasonEnum;
import cn.porsche.compliance.emnu.TrainingTypeEnum;
import cn.porsche.compliance.service.*;
import io.swagger.annotations.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CountDownLatch;


@Log4j2
@RestController
@RequestMapping("/upload")
public class UploadController extends BaseController {
    final static String NoAttend = "#N/A";

    @Value("${app.upload.path}")
    String uploadPath;

    @Autowired
    QuestionService questionService;

    @Autowired
    TrainingService trainingService;

    @Autowired
    RiskService riskService;

    @Autowired
    PositionService positionService;

    @Autowired
    StatisticalService statisticalService;

    @Autowired
    FileService fileService;

    @ApiOperation(
            value = "上传员工某个季度的题库接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功", response = ApiResult.class)
    })
    @PostMapping("/staff_question")
    public String staffUploadQuestion(
            @RequestParam("year") int year,
            @RequestParam("season") SeasonEnum season,
            @RequestParam("file") MultipartFile file
    ) {
        if (noPromise("Human Resources")) {
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (file.isEmpty()) {
            return ApiResult.isBad("no file");
        }

        Path path = null;
        try {
            byte[] bytes = file.getBytes();

            path = Paths.get(System.getProperty("java.io.tmpdir") + file.getOriginalFilename());
            Files.write(path, bytes);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (path == null) {
            return ApiResult.isBad("无法解析Excel文件");
        }

        EmployeeTypeEnum type = EmployeeTypeEnum.STAFF;
        try {
            saveQuestionExcel(path.toString(), type, year, season);
        } catch (Exception e) {
            return ApiResult.isBad(e.getMessage());
        }

        // 将员工的所有答题数据全清除
        resetAttendByType(year, season, type);

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "上传VP某个季度的题库接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功", response = ApiResult.class)
    })
    @PostMapping("/vp_question")
    public String vpUploadQuestion(
            @RequestParam("file") MultipartFile file,
            @RequestParam("year") int year,
            @RequestParam("season") SeasonEnum season
    ) {
        if (noPromise("Human Resources")) {
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (file.isEmpty()) {
            return ApiResult.isBad("no file");
        }

        Path path = null;
        try {
            byte[] bytes = file.getBytes();

            path = Paths.get(System.getProperty("java.io.tmpdir") + file.getOriginalFilename());
            Files.write(path, bytes);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (path == null) {
            return ApiResult.isBad("无法解析Excel文件");
        }

        EmployeeTypeEnum type = EmployeeTypeEnum.VP;

        // VP 只有上半年和下半年之分
        if (SeasonEnum.Q2.equals(season)) {
            season = SeasonEnum.Q1;
        } else if (SeasonEnum.Q4.equals(season)) {
            season = SeasonEnum.Q3;
        }
        try {
            saveQuestionExcel(path.toString(), type, year, season);
        } catch (Exception e) {
            return ApiResult.isBad(e.getMessage());
        }

        // 将VP的所有答题数据全清除
        resetAttendByType(year, season, type);

        return ApiResult.isOk();
    }


    @ApiOperation(
            value = "上传VP某个季度的题库接口"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功", response = ApiResult.class)
    })
    @PostMapping("/outer_question")
    public String outerUploadQuestion(
            @RequestParam("file") MultipartFile file,
            @RequestParam("year") int year,
            @RequestParam("season") SeasonEnum season
    ) {
        if (noPromise("Human Resources")) {
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (file.isEmpty()) {
            return ApiResult.isBad("no file");
        }

        Path path = null;
        try {
            byte[] bytes = file.getBytes();

            path = Paths.get(System.getProperty("java.io.tmpdir") + file.getOriginalFilename());
            Files.write(path, bytes);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (path == null) {
            return ApiResult.isBad("无法解析Excel文件");
        }

        EmployeeTypeEnum type = EmployeeTypeEnum.OUTER;
        try {
            saveQuestionExcel(path.toString(), type, year, season);
        } catch (Exception e) {
            return ApiResult.isBad(e.getMessage());
        }

        // 将VP的所有答题数据全清除
        resetAttendByType(year, season, type);

        return ApiResult.isOk();
    }

    @ApiOperation(
            value = "上传培训文件供员工查阅"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功", response = ApiResult.class)
    })
    @PostMapping("/train_file")
    public String uploadViewer(@RequestParam("file") MultipartFile file) {
        if (noPromise("Human Resources")) {
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (file.isEmpty()) {
            return ApiResult.isBad("no file");
        }

        String md5 = DigestUtils.md5DigestAsHex(file.getOriginalFilename().getBytes());
        if (null != fileService.findByMd5(md5)) {
            return ApiResult.isBad("此文件名已经存在，请改名之后再上传");
        }

        Calendar calendar = Calendar.getInstance();
        String dateToken = java.io.File.separator + calendar.get(Calendar.YEAR)
                + java.io.File.separator + ((calendar.get(Calendar.MONTH) + 1) < 10 ? "0" + (calendar.get(Calendar.MONTH) + 1) : calendar.get(Calendar.MONTH) + 1)
                + java.io.File.separator + (calendar.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + calendar.get(Calendar.DAY_OF_MONTH) : calendar.get(Calendar.DAY_OF_MONTH));

        // 检查需要存储文件的目录是否存在
        java.io.File path = new java.io.File(uploadPath + dateToken);
        if (!path.exists() && !path.isDirectory()) {
            //创建文件夹
            if (!path.mkdirs() || !path.setReadable(true)) {
                return ApiResult.isBad("服务器创建文件失败，请联系管理员，检查磁盘是否已满");
            }
        }

        // 检查上传文件当天是否已经存在
        java.io.File exist = new java.io.File(path.getAbsolutePath() + java.io.File.separator + file.getOriginalFilename());
        if (exist.exists()) {
            return ApiResult.isBad("服务器已经存在相同的文件，请更名之后再上传");
        }

        try {
            Files.write(exist.toPath(), file.getBytes());
            exist.setReadable(true); //让文件可读
        } catch (Exception e) {
            e.printStackTrace();

            return ApiResult.isBad("服务器存储失败，请重新上传或联系管理员");
        }

        File viewer = File.builder()
                .userId(user.getId())
                .md5(md5)
                .name(file.getOriginalFilename().trim())
                .path(dateToken + java.io.File.separator + file.getOriginalFilename())
                .build();

        fileService.update(viewer);

        addAttendByViewer(viewer, TrainingTypeEnum.FileView, EmployeeTypeEnum.STAFF);

        return ApiResult.builder().data(viewer).toJson();
    }


    @ApiOperation(
            value = "上传具有风险的人员名单"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功", response = ApiResult.class)
    })
    @PostMapping("/risk")
    public String uploadRisk(@RequestParam("file") MultipartFile file) {
        if (noPromise("Human Resources")) {
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        if (file.isEmpty()) {
            return ApiResult.isBad("no file");
        }

        Path path = null;
        try {
            byte[] bytes = file.getBytes();

            path = Paths.get(System.getProperty("java.io.tmpdir") + file.getOriginalFilename());
            Files.write(path, bytes);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (path == null) {
            return ApiResult.isBad("无法解析Excel文件");
        }

        Calendar calendar = Calendar.getInstance();
        try {
            readRiskExcel(path.toString(), calendar.get(Calendar.YEAR));
        } catch (Exception e) {
            return ApiResult.isBad(e.getMessage());
        }

        return ApiResult.isOk();
    }



    @ApiOperation(
            value = "上传特殊的人员名单"
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功", response = ApiResult.class)
    })
    @PostMapping("/vps")
    public String uploadRisk(
            @RequestParam(value = "vps") List<String> vps
    ) {
        if (noPromise("Human Resources")) {
            return ApiResult.isBad("You do not have permission to access, please contact your Legal Officer to request");
        }

        User vp;
        userService.removeManagerTag();
        for (String code : vps) {
            if (StringUtils.isEmpty( code = StringUtils.trim(code) )) {
                continue;
            }

            if (null == ( vp = userService.findByCode(code) )) {
                continue;
            }

            vp.setIsManager(true);

            userService.update(vp);
        }

        return ApiResult.isOk();
    }


    private void saveQuestionExcel(String file, final EmployeeTypeEnum type, final int year, final SeasonEnum season) {
//        // 删除当年的题目
        questionService.removeByYearAndSeasonAndEmployeeType(year, season, type);
        Row row;
        Cell cell;
        Sheet sheet;
        Workbook workbook = null;

        try {
            workbook = getReadWorkBookType(file);
            List<String> cells;
            List<List<String>> rows;

            String value;
            sheet = workbook.getSheetAt(0);
            rows = new ArrayList<>();

            //第0行是表名，忽略，从第二行开始读取
            for (int rowIdx = 1; rowIdx < sheet.getPhysicalNumberOfRows(); rowIdx++) {
                row = sheet.getRow(rowIdx);
                cells = new ArrayList<>();

                for (int cellIdx = 1; cellIdx < 15; cellIdx++) {
                    cell = row.getCell(cellIdx);
                    value = getCellStringVal(cell).trim();

                    if (!StringUtils.isEmpty(value)) {
                        cells.add(getCellStringVal(cell).trim());
                    }
                }

                if (cells.size() != 14) {
                    continue;
                }

                rows.add(cells);
            }

            List<String> item;
            for (int rowIdx = 0, len = rows.size(), score = 6; rowIdx < len; ++ rowIdx) {
                item = rows.get(rowIdx);

//                if (type.equals(EmployeeTypeEnum.VP)) {
//                    score = 10;
//                } else {
//                    score = 6;
//                }
//
//                if ((type.equals(EmployeeTypeEnum.STAFF) || type.equals(EmployeeTypeEnum.OUTER)) && rowIdx + 1 == len) {
//                    // 最后一题16分
//                    score = 16;
//                }


                if (rowIdx + 1 == len) {
                    // 最后一题16分
                    score = 16;
                }

                Question question = Question.builder()
                        .year(year)
                        .season(season)
                        .type(type)
                        .enQuestion(item.get(0))
                        .cnQuestion(item.get(1))
                        .enOptionA(item.get(2))
                        .cnOptionA(item.get(3))
                        .enOptionB(item.get(4))
                        .cnOptionB(item.get(5))
                        .enOptionC(item.get(6))
                        .cnOptionC(item.get(7))
                        .enOptionD(item.get(8))
                        .cnOptionD(item.get(9))
                        .helpAnswer(item.get(10))
                        .answer(item.get(11))
                        .enTip(item.get(12))
                        .cnTip(item.get(13))
                        .score(score)
                        .build();

                questionService.insert(question);
            }
        } catch (Exception e) {
            e.printStackTrace();

            throw e;
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }


    private void resetAttendByType(int year, SeasonEnum season, EmployeeTypeEnum type) {
        SeasonEnum seasons[] = SeasonEnum.values();

        // 利用多线程添加数据
        int count = userService.countByType(type),
                page = Runtime.getRuntime().availableProcessors() + 1,
                size = (int) Math.ceil(Float.parseFloat(String.valueOf(count)) / page);

        CountDownLatch latch = new CountDownLatch(page);
        // 利用多核 CPU 加快速度
        do {
            log.info("一共{}页，每页共{}个，准备启用 {} 个线程", page, size, page);

            final int finalPage = page;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Training training;
                    List<Training> changes = new ArrayList<>();
                    List<User> users = userService.findAllByTypeAndPage(type, finalPage, size).getContent();
                    for (User item : users) {
                        for (int j = 0; j < seasons.length; j++) {
                            // 已经存在的，不需要再次创建
                            training = trainingService.findByYearAndUserAndTypeAndSubject(year, item, TrainingTypeEnum.Question, seasons[j].getName());
                            if (null == training) {
                                training = Training.builder()
                                        .year(year)
                                        .type(TrainingTypeEnum.Question)
                                        .attend(false)
                                        .subject(seasons[j].getName())
                                        .userId(item.getId())
                                        .departmentId(item.getDepartmentId())
                                        .build();

                                if (type.equals(EmployeeTypeEnum.VP)) {
                                    // VP只有两套题
                                    if (SeasonEnum.Q2.equals(seasons[j]) || SeasonEnum.Q4.equals(seasons[j]) ) {
                                        // VP 只生成上半年和下半年的题
                                        log.info("VP题库略过: {}", seasons[j].toString());
                                        continue;
                                    }
                                }

                                changes.add(training);

                                log.info("insert page: {} userId: {}", finalPage, item.getId());

                            } else if (training.getSubject().equalsIgnoreCase(season.toString()) && training.getAttend()) {
                                // 用户参与过答题，需要重置
                                training.setAttend(false);

                                changes.add(training);

                                log.info("update page: {} userId: {}", finalPage, item.getId());
                            }
                        }
                    }

                    if (changes.size() > 0) {
                        trainingService.update(changes);
                    }

                    for (User item : users) {
                        statisticalService.refreshQuestion(item);
                    }

                    latch.countDown();
                }}
            ).start();

        } while (( -- page ) > 0);

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void addAttendByViewer(File viewer, TrainingTypeEnum type, EmployeeTypeEnum employeeType) {
        // 利用多线程添加数据
        int count = userService.countByType(employeeType),
                page = Runtime.getRuntime().availableProcessors() + 1,
                size = (int) Math.ceil(Float.parseFloat(String.valueOf(count)) / page);

        CountDownLatch latch = new CountDownLatch(page);
        // 利用多核 CPU 加快速度
        do {
            log.info("一共{}页，每页共{}个，准备启用 {} 个线程，新增Training数据", page, size, page);

            final int finalPage = page;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    List<Training> changes = new ArrayList<>();
                    List<User> users = userService.findAllByTypeAndPage(employeeType, finalPage, size).getContent();

                    for (User item : users) {
                        if (null != ( trainingService.findByUserAndTypeAndSubject(item, type, viewer.getName()) )) {
                            continue;
                        }

                        changes.add(Training.builder()
                                .userId(item.getId())
                                .departmentId(item.getDepartmentId())
                                .type(type)

                                .subject(viewer.getName())
                                .attend(false)

                                .build());

                    }

                    if (changes.size() > 0) {
                        trainingService.update(changes);

                        log.debug("当前第{}页，新增{}个用户，参加的文件为：{}，未参加的培训记录", finalPage, changes.size(), viewer.getName());
                    }

                    for (User item : users) {
                        statisticalService.refreshViewer(item);
                    }

                    latch.countDown();
                }}
            ).start();

        } while (( -- page ) > 0);

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void readRiskExcel(String file, Integer year) throws Exception {
        // 删除所有的风险点，重新来过
        riskService.deleteByYear(year);

        Row row;
        Cell cell;
        Sheet sheet;
        Workbook workbook = null;

        try {
            workbook = getReadWorkBookType(file);
            List<String> cells;
            List<List<String>> rows;

            String value;
            sheet = workbook.getSheetAt(0);
            rows = new ArrayList<>();

            //第0行是表名，忽略，从第二行开始读取
            for (int rowIdx = 1; rowIdx < sheet.getPhysicalNumberOfRows(); rowIdx++) {
                row = sheet.getRow(rowIdx);
                cells = new ArrayList<>();

                for (int cellIdx = 0; cellIdx < 7; cellIdx++) {
                    if (null == row) {
                        continue;
                    }

                    cell = row.getCell(cellIdx);
                    value = getCellStringVal(cell).trim();

                    if (!StringUtils.isEmpty(value)) {
                        cells.add(getCellStringVal(cell).trim());
                    }
                }

                if (cells.size() != 7) {
                    continue;
                }

                rows.add(cells);
            }

            List<String> item;
            User user;
            Position position;
            Department department;
            for (int rowIdx = 0, len = rows.size(); rowIdx < len; ++ rowIdx) {
                item = rows.get(rowIdx);
                if (StringUtils.isEmpty(item.get(0))) {
                    continue;
                }

                if (null == ( user = userService.findByCode(item.get(1)) )) {
                    continue;
                }

                position = positionService.findById(user.getPositionId());
                department = departmentService.findById(user.getDepartmentId());

                Risk risk = Risk.builder()
                        .year(year)
                        .userId(user.getId())
                        .nickname(user.getEnName())

                        .departmentId(user.getDepartmentId())
                        .departmentName(department != null ? department.getName() : "")

                        .positionId(position != null ? position.getId() : 0)
                        .position(position != null ? position.getFunction() : "")
                        .number1((int) Math.round(Double.valueOf(item.get(2))))
                        .number2((int) Math.round(Double.valueOf(item.get(3))))
                        .number3((int) Math.round(Double.valueOf(item.get(4))))
                        .number4((int) Math.round(Double.valueOf(item.get(5))))
                        .number5((int) Math.round(Double.valueOf(item.get(6))))
                        .build();

                riskService.update(risk);
            }
        } catch (Exception e) {
            e.printStackTrace();

            throw e;
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }


    private Workbook getReadWorkBookType(String filePath) {
        //xls-2003, xlsx-2007
        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);
            if (filePath.toLowerCase().endsWith("xlsx")) {
                return new XSSFWorkbook(is);
            } else if (filePath.toLowerCase().endsWith("xls")) {
                return new HSSFWorkbook(is);
            } else {
                //  抛出自定义的业务异常
                return null;
            }
        } catch (IOException e) {
            //  抛出自定义的业务异常
            e.printStackTrace();
            return null;
        } finally {
            IOUtils.closeQuietly(is);
        }
    }


    private String getCellStringVal(Cell cell) {
        CellType cellType = cell.getCellTypeEnum();
        switch (cellType) {
            case NUMERIC:
                return String.valueOf(cell.getNumericCellValue());
            case STRING:
                return cell.getStringCellValue();
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case FORMULA:
                return cell.getCellFormula();
            case BLANK:
                return "";
            case ERROR:
                return String.valueOf(cell.getErrorCellValue());
            default:
                return StringUtils.EMPTY;
        }
    }
}