package cn.porsche.web.repository;

import cn.porsche.web.constant.CommentTypeEnum;
import cn.porsche.web.domain.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {
    List<Comment> findAllByTopicIdAndTypeOrderByIdDesc(Integer topicId, CommentTypeEnum type);
    List<Comment> findAllByTopicIdAndTypeOrderByIdAsc(Integer topicId, CommentTypeEnum type);
}
