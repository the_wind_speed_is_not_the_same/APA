package cn.porsche.compliance.controller;

import cn.porsche.compliance.domain.*;
import cn.porsche.compliance.service.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

@Log4j2
public abstract class BaseController {
    protected User user;

    @Autowired
    protected UserService userService;

    @Autowired
    protected LoginService loginService;

    @Autowired
    protected DepartmentService departmentService;

    protected String encode(String uri) {
        try {
            return URLEncoder.encode(uri, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "";
    }

    protected int checkPage(int page) {
        if (page > 1000) {
            return 1000;
        }

        if (page < 0) {
            return 1;
        }


        return page;
    }

    protected int checkSize(int size) {
        if (size > 100) {
            return 100;
        }

        if (size < 0) {
            return 1;
        }

        return size;
    }

    protected boolean noPromise(String department) {
        if (null == ( user = getUser() )) {
            return true;
        }

        if (StringUtils.isEmpty(department)) {
            return false;
        }

        Department depart = departmentService.findById(user.getDepartmentId());
        if (null == depart) {
            return true;
        }

        return !depart.getName().equalsIgnoreCase(department);
    }

    protected User getUser() {
//        return user = userService.findByCode("PCN744");       // HR
//        return user = userService.findByCode("PCN918");       // CEO
//        return user = userService.findByCode("PCN922");       // VP
        return user = userService.findByCode("PCN781");       // Frank Yu
//        return user = loginService.getUserFromCookie();
    }

    @InitBinder
    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
}
