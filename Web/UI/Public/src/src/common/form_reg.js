// 验证金额的格式
export const checkPrice = (rule, value, callback) => {
  if (value) {
    const reg = /^(([1-9]\d{1,9})|\d)(\.\d{1,2})?$/
    if (!reg.test(value)) {
      return callback(new Error('请输入正确的金额'));
    }
  }
  if (value == 0) {
    return callback(new Error('请输入大于0的金额'));
  }
  callback()
}

// 验证是否只包含数字
export const checkNumber = (rule, value, callback) => {
  if (value) {
    const reg = /^[0-9]*$/
    if (!reg.test(value)) {
      return callback(new Error('请输入正确的格式'));
    }
  }
  callback()
}

export const email = (rule, value, callback) => {
  if (value) {
    const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
    if (!reg.test(value)) {
      return callback(new Error('请输入正确的电子邮箱地址'));
    }
  }
  callback()
}

// 验证整数
export const isPositiveNum = (rule, value, callback) => {
  if(!value) {
    return callback(new Error('请输入'))
  }
  if(value) {
    const reg =  /^[1-9]+[0-9]*$/;
    if(!reg.test(value)) {
      return callback(new Error('请输入整数'))
    }
  }
  callback()
}

// 验证两位小数
export const checkTwoDecimal = (rule, value, callback) => {
  if(!value) {
    return callback(new Error('请输入'))
  }
  if(value) {
    const reg = /(^[1-9]\d*(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;
    if(!reg.test(value)) {
      return callback(new Error('只支持两位小数'))
    }
  }
  callback()
}
