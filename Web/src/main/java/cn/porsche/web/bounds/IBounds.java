package cn.porsche.web.bounds;

import cn.porsche.web.constant.ScoreLevelEnum;
import org.springframework.stereotype.Service;

@Service
public interface IBounds {
    float getWhatRate(int whatScore);

    ScoreLevelEnum getLevelByScore(int score);
    int getScoreByLevel(ScoreLevelEnum levelEnum);

    int getFinalScore(int whatScore, int howScore);
}
