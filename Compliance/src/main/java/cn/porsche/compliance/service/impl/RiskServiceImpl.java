package cn.porsche.compliance.service.impl;

import cn.porsche.compliance.domain.Risk;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.repository.RiskRepository;
import cn.porsche.compliance.service.RiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RiskServiceImpl implements RiskService {
    @Autowired
    RiskRepository repository;

    @Override
    public Risk update(Risk risk) {
        return repository.save(risk);
    }

    @Override
    public Risk findByUser(User user) {
        return repository.findByUserId(user.getId());
    }

    @Override
    public void deleteByYear(Integer year) {
        repository.deleteByYear(year);
    }

    @Override
    public List<Risk> findAll() {
        return repository.findAll();
    }
}
