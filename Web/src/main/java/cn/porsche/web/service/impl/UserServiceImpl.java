package cn.porsche.web.service.impl;

import cn.porsche.web.constant.DataStatus;
import cn.porsche.web.domain.Function;
import cn.porsche.web.domain.KPIUser;
import cn.porsche.web.domain.Position;
import cn.porsche.web.domain.User;
import cn.porsche.web.repository.UserRepository;
import cn.porsche.web.service.KPIUserService;
import cn.porsche.web.service.PositionService;
import cn.porsche.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository repository;

    @Autowired
    PositionService positionService;

    @Autowired
    KPIUserService kpiUserService;

    @Override
    public User update(User user) {
        return repository.save(user);
    }

    @Override
    public User findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public User findByCode(String code) {
        return repository.findByCode(code);
    }

    @Override
    public User findByPositionId(Integer id) {
        return repository.findByPositionId(id);
    }

    @Override
    public Integer count() {
        return repository.countByStatus(DataStatus.OK);
    }

    @Override
    public Page<User> findAllByPage(Integer page, Integer size) {
        return repository.findAllByStatus(DataStatus.OK, PageRequest.of(page-1, size));
    }

    @Override
    public List<User> findAllByManager(User manager) {
        return repository.findAllByManagerId(manager.getId());
    }

    @Override
    public Page<User> findAllByEnabled(Integer page, Integer size) {
        return repository.findAllByNeedKPI(true, PageRequest.of(page-1, size));
    }

    @Override
    public Integer countByNeedHands() {
        return repository.countByNeedHands(true);
    }

    @Override
    public Page<User> findAllByHands(Integer page, Integer size) {
        return repository.findAllByNeedHands(true, PageRequest.of(page-1, size));
    }

    @Override
    public List<User> findAllInKPIUsers(Function function) {
        return repository.findAllByFunctionId(function.getId(), PageRequest.of(0, count())).getContent();
    }

    @Override
    public User getManager(User user) {
        // 先查看user.getManagerId 是否有
        if (user.getManagerId() != null) {
            return findById(user.getManagerId());
        }

        if (null == user.getPositionId()) {
            return null;
        }

        Position position = positionService.findById(user.getPositionId());
        if (null == position || null == position.getParentId()) {
            return null;
        }

        return repository.findByPositionId(position.getParentId());
    }

    @Override
    public User findDelegator(Function function, User user) {
        KPIUser kUser = kpiUserService.findByFunction(function, user);
        if (null == kUser || null == kUser.getDelegatorId()) {
            return null;
        }

        return findById(kUser.getDelegatorId());
    }

    @Override
    public User updateHireDate(User user, Date hireDate) {
        if (hireDate == null) {
            return null;
        }

        if (hireDate.equals(user.getHireDate())) {
            return user;
        }

        user.setHireDate(hireDate);
        List<KPIUser> kUsers = kpiUserService.findAllByUser(user);
        for (KPIUser kUser : kUsers) {
            if (!hireDate.equals(kUser.getHireDate())) {
                kUser.setHireDate(user.getHireDate());

                kpiUserService.update(kUser);
            }
        }

        return repository.save(user);
    }
}
