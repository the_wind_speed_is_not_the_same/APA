package cn.porsche.web.controller;

import cn.porsche.web.constant.GroupNameEnum;
import cn.porsche.web.constant.ModuleNameEnum;
import cn.porsche.web.constant.OperatorText;
import cn.porsche.web.domain.*;
import cn.porsche.web.group.GroupFactory;
import cn.porsche.web.service.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

@Log4j2
public abstract class BaseController {
    protected User user;
    protected Group group;

    protected Target target;
    protected KPIUser kpiUser;

    private static final int MinPageSize = 1;
    private static final int MaxPageSize = 50;

    private static final int MinPageIndex = 1;
    private static final int MaxPageIndex = 100;


    @Autowired
    protected GroupFactory groupFactory;

    @Autowired
    protected UserService userService;

    @Autowired
    protected GroupService groupService;

    @Autowired
    protected LoginService loginService;

    @Autowired
    protected KPIUserService kpiUserService;

    @Autowired
    protected TargetService targetService;

    @Autowired
    protected FunctionService functionService;

    protected String encode(String uri) {
        try {
            return URLEncoder.encode(uri, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "";
    }

    protected int checkPage(int page) {
        if (page > MaxPageIndex) {
            return MaxPageIndex;
        }

        if (page < 0) {
            return MinPageIndex;
        }


        return page;
    }
    protected int checkSize(int size) {
        if (size > MaxPageSize) {
            return MaxPageSize;
        }

        if (size < 0) {
            return MinPageSize;
        }

        return size;
    }

    protected int defaultPageIndex() {
        return MinPageIndex;
    }

    protected int defaultPageSize() {
        return MaxPageSize;
    }


    protected boolean noPromise(ModuleNameEnum module, OperatorText operator, GroupNameEnum... groups) {
        for (GroupNameEnum group : groups) {
            if (!noPromise(getGroup(group), module.getName(), operator)) {
                return false;
            }
        }

        // 关闭功能
        return false;
//        return true;
    }

    protected boolean noPromise(Group group, String module, OperatorText operator) {
        if (null == group) {
            return true;
        }

        if (null == ( user = getUser() )) {
            return true;
        }

//        if (user == null ) {
//            // 未登录
//            log.debug("用户未登录，开始获取用户登录信息");
//            user = getUser();
//
//            return true;
//        }

        log.debug("用户登录，登录信息{}, {}", user.getId(), user.getEnName());

        return groupFactory.noPromise(group, module, operator);
    }


    protected User getUser() {
        User user = loginService.getUserFromCookie();
        if (null != user) {
            return user;
        }

        // FOR TEST
//        return userService.findById(3314);      // HR
//        return userService.findById(3436);

        return loginService.getUserFromHeader();
    }

    protected KPIUser getKPIUser(Function function) {
        return getKPIUser(function, getUser());
    }

    protected KPIUser getKPIUser(Function function, User user) {
        return kpiUserService.findByFunction(function, user);
    }

    protected Target getTarget(Function function) {
        return targetService.findByUser(function, getUser());
    }

    // 根据用户的权限获取对应的组名
    protected Group getGroup(GroupNameEnum name) {
        return group = getGroupByName(name.getName());
    }

    public Group getGroupByName(String name) {
        return groupService.findByName(name);
    }

    @InitBinder
    protected void init(HttpServletRequest request, ServletRequestDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
}
