package cn.porsche.common.auth;


import cn.porsche.common.auth.impl.TestAuth.DemoAuth;

public class AuthFactory {
    private enum  Provider {
        Wechat("wechat"),
        Weibo("weibo"),
        Porsche("porsche"),
        UNKNOWN("UNKNOWN"),
        ;

        private String name;

        Provider(String name) {
            this.name = name;
        }

        //将字符串转换为词性对象
        public static Provider from(String string) {
            try {
                return Provider.valueOf(string);
            } catch (IllegalArgumentException exception) {
                return Provider.UNKNOWN;
            }
        }
    }

    private AuthFactory() {
    }

    public static IAuth getAPP(String appId, String appSecret, String pound) {
        AbstractAuth provider;

        switch (Provider.from(pound)) {
            case Wechat:  provider = new DemoAuth(); break;
            case Weibo:   provider = new DemoAuth(); break;
            case Porsche: provider = new DemoAuth(); break;
            default:      provider = null;
        }

        if (null == provider) {
            return null;
        }

        provider.setApp(new App(appId, appSecret, pound));

        return provider;
    }


    public static IAuth getAPP(String pound) {
        AbstractAuth provider;

        switch (Provider.from(pound)) {
            case Wechat:  provider = new DemoAuth(); break;
            case Weibo:   provider = new DemoAuth(); break;
            case Porsche: provider = new DemoAuth(); break;
            default:      provider = null;
        }

        if (null == provider) {
            return null;
        }

        provider.setApp(new App(null, null, pound));

        return provider;
    }
}
