import {request} from '../../../../common/request.js';
import {mapState} from 'vuex'
import {downloadFile} from '../../../../common/utils.js';
// import Confirm from '../../../../components/Confirm/page.vue';

export default {
    props: {
        query: {
            type: Object
        },
        function_id: {
            type: Number | String,
            required: true
        }
    },
    data() {
        return {
            tableData: [],
            tableHeight: 412,
            pageIndex: 1,
            pageSize: 25,
            // totalRecord:0
            confirmTxt: '',
            titleTxt: 'Error',
            isShowConfirm: false
        }
    },
    // components: {
    //     Confirm,
    // },
    watch: {
        'function_id': function (val) {
            localStorage.removeItem('reportingStatus')
            localStorage.removeItem('totalCount')
            localStorage.removeItem('currentData')
            this.getData()
        }
    },
    filters: {
        formatNumber(data) {
            var reg = /^\d+(\.\d+)?$/
            if (!data || !reg.test(data)) return '0.00'
            return Number(data).toFixed(1)
        }
    },
    computed: {
        ...mapState({
            department: (state) => state.allState.reportingDepartment || 'department',
            department_id: (state) => state.allState.reportingDepartmentId
        }),
        // pageNumbers(){
        //   return this.totalRecord?Math.ceil(this.totalRecord/this.pageSize):0
        // }
    },
    methods: {
        goBack() {
            this.$emit('changeComponent', {page: 'parent', query: this.query})
        },
        confirm() {
            this.isShowConfirm = false
        },
        getData() {
            request({
                url: 'getTargetByDepartment',
                method: 'get',
                params: {
                    department_id: this.department_id,
                    function_id: this.function_id,
                    // page:this.pageIndex,
                    // size:this.pageSize
                },
                isShowLoading: true
            }).then((res) => {
                // console.log(res)
                if (!res || res.code !== 0) {
                    this.confirmTxt = res.message;
                    this.titleTxt = 'Error';
                    this.isShowConfirm = true;

                    return;
                }

                if (res && res.list) {
                    this.tableData = res.list
                }
            })
        },
        pageChange(e) {
            this.pageIndex = e
        },
        setHeaderClass({row, column, rowIndex, columnIndex}) {
            if (rowIndex === 1 && (columnIndex == 2 || columnIndex == 5)) {
                return 'border-right'
            }
        },
        setCellClass({row, column, rowIndex, columnIndex}) {
            if (columnIndex == 2 || columnIndex == 5) {
                return 'border-right'
            }
        },
        exportAllFile() {
            request({
                url: 'downloadAPAByDReporting',
                method: 'download',
                params: {
                    function_id: this.function_id,
                    department_id: this.department_id
                }
            }).then((res) => {
                if (!res || res.code !== 0) {
                    this.confirmTxt = res.message;
                    this.titleTxt = 'Error';
                    this.isShowConfirm = true;

                    return;
                }

                if (res && res.data) {
                    downloadFile(res.data)
                }
            })
        }
    },
    created() {
        this.getData()
    },
    mounted() {
        // this.$nextTick(()=>{
        //   this.tableHeight = this.$refs.tableWrapper.offsetHeight
        // })
    }
}
