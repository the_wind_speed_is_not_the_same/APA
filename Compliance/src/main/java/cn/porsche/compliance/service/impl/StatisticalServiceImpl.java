package cn.porsche.compliance.service.impl;

import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.Statistical;
import cn.porsche.compliance.domain.Training;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.emnu.TrainingTypeEnum;
import cn.porsche.compliance.repository.TrainingSummaryRepository;
import cn.porsche.compliance.service.StatisticalService;
import cn.porsche.compliance.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class StatisticalServiceImpl implements StatisticalService {
    @Autowired
    TrainingSummaryRepository repository;

    @Autowired
    TrainingService trainingService;

    @Override
    public Statistical update(Statistical summary) {
        return repository.save(summary);
    }

    @Override
    public void refresh(User user) {
        refreshViewer(user);
        refreshQuestion(user);
    }

    @Override
    public void refreshQuestion(User user) {
        int total;
        int year;
        Statistical report;
        List<Training> list;

        year   = Calendar.getInstance().get(Calendar.YEAR);
        total  = 0;
        list   = trainingService.findAllByYearAndUserAndType(year, user, TrainingTypeEnum.Question);
        report = findByUserAndType(user, TrainingTypeEnum.Question);
        for (Training item : list) {
            if (item.getAttend()) {
                total ++;
            }
        }

        if (null == report) {
            report = Statistical.builder()
                    .userId(user.getId())
                    .departmentId(user.getDepartmentId())
                    .type(TrainingTypeEnum.Question)

                    .year(year)
                    .total(0)
                    .attend(0)
                    .build();
        }

        if (!report.getAttend().equals(total) || !report.getTotal().equals(list.size())) {
            report.setAttend(total);
            report.setTotal(list.size());

            update(report);
        }
    }

    @Override
    public void refreshViewer(User user) {
        int total;
        List<Training> list;
        Statistical report;

        total  = 0;
        list   = trainingService.findAllByUserAndType(user, TrainingTypeEnum.FileView);
        for (Training item : list) {
            if (item.getAttend()) {
                total ++;
            }
        }

        report = findByUserAndType(user, TrainingTypeEnum.FileView);
        if (null == report) {
            report = Statistical.builder()
                    .userId(user.getId())
                    .departmentId(user.getDepartmentId())
                    .type(TrainingTypeEnum.FileView)

                    .total(0)
                    .attend(0)
                    .build();
        }

        if (!report.getAttend().equals(total) || !report.getTotal().equals(list.size())) {
            report.setAttend(total);
            report.setTotal(list.size());

            update(report);
        }
    }

    @Override
    public Statistical findByYearAndUserAndType(Integer year, User user, TrainingTypeEnum type) {
        return repository.findByYearAndUserIdAndType(year, user.getId(), type);
    }

    @Override
    public Statistical findByUserAndType(User user, TrainingTypeEnum type) {
        return repository.findByUserIdAndType(user.getId(), type);
    }

    @Override
    public List<Statistical> findAllEmployeesByYearAndDepartment(Integer year, Department department, TrainingTypeEnum type) {
        return repository.findAllByYearAndDepartmentIdAndType(year, department.getId(), type);
    }
}
