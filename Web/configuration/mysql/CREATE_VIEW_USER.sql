CREATE OR REPLACE VIEW users_view AS
    SELECT
    `u`.`id`,
    `u`.`code`,
    `u`.`department_id`,
	`d`.`name` as `department`,
	`m`.`en_name` as `manager`,
    `u`.`en_name`,
	`u`.`cn_name`,
	`u`.`title`,
	`u`.`mobile`,
	`u`.`tel`,
	`u`.`email`,
    `u`.`hire_date`,
    `u`.`need_hands`
    FROM `users` AS u LEFT JOIN `users` AS m ON `u`.`manager_id`=`m`.`id`
     LEFT JOIN `departments` AS d ON `u`.`department_id` = `d`.`id`;