package cn.porsche.compliance.domain;

import cn.porsche.compliance.emnu.TrainingTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "training", indexes = {
//        @Index(name = "category_top_and_parent",            columnList = "top_id, parent_id")
})
public class Training implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",                        columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer id;

    @JsonIgnore
    @ApiModelProperty(notes = "员工主键")
    @Column(name = "user_id",                   columnDefinition = "INT UNSIGNED NOT NULL COMMENT '主键'")
    private Integer userId;

    @ApiModelProperty(notes = "部门主键")
    @Column(name = "department_id",             columnDefinition = "INT UNSIGNED NOT NULL COMMENT '部门主键'")
    private Integer departmentId;

    @ApiModelProperty(notes = "参与年份")
    @JsonProperty("year")
    @Column(name = "year",                      columnDefinition = "SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT '参与年份'")
    Integer year;

    @ApiModelProperty(notes = "类别：答题还是文件")
    @Enumerated(EnumType.STRING)
    @JsonProperty("type")
    @Column(name = "type",                      columnDefinition = "CHAR(12) NOT NULL COMMENT '答题还是文件'")
    TrainingTypeEnum type;

    @ApiModelProperty(notes = "培训主题")
    @JsonProperty("subject")
    @Column(name = "subject",                   columnDefinition = "VARCHAR(511) NOT NULL COMMENT '培训主题'")
    String subject;

    @ApiModelProperty(notes = "是否与会")
    @JsonProperty("is_attend")
    @Column(name = "is_attend",                 columnDefinition = "TINYINT(1) NOT NULL DEFAULT '0' COMMENT '是否与会'")
    Boolean attend = false;

    @JsonIgnore
    @Column(name = "create_at",                 columnDefinition = "DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '创建时间'", insertable = false, updatable = false)
    Date createAt;
}
