package cn.porsche.compliance.service.impl;

import cn.porsche.compliance.constant.EmployeeTypeEnum;
import cn.porsche.compliance.domain.Department;
import cn.porsche.compliance.domain.Position;
import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.repository.UserRepository;
import cn.porsche.compliance.service.PositionService;
import cn.porsche.compliance.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository repository;

    @Autowired
    PositionService positionService;

    @Override
    @Transactional
    public void removeManagerTag() {
        repository.removeManagerTag();
    }

    @Override
    public User update(User user) {
        return repository.save(user);
    }

    @Override
    public User findById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public User findByCode(String code) {
        return repository.findByCode(code);
    }

    @Override
    public User findByPositionId(Integer positionId) {
        return repository.findByPositionId(positionId);
    }

    @Override
    public Integer count() {
        return Integer.valueOf((int) repository.count());
    }

    @Override
    public Integer countByType(EmployeeTypeEnum type) {
        return repository.countByType(type);
    }

    @Override
    public List<User> findByManager(User manager) {
        return repository.findAllByManagerId(manager.getId());
    }

    @Override
    public List<User> findByDepartment(Department department) {
        return repository.findAllByDepartmentId(department.getId());
    }


    @Override
    public Page<User> findAllByPage(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page-1, size));
    }

    @Override
    public Page<User> findAllByTypeAndPage(EmployeeTypeEnum type, Integer page, Integer size) {
        return repository.findAllByType(type, PageRequest.of(page-1, size));
    }

    @Override
    public User getManager(User user) {
        Position position = positionService.findById(user.getPositionId());
        if (null == position) {
            return null;
        }

        return repository.findByPositionId(position.getParentId());
    }
}
