package cn.porsche.web.exceptions;

import cn.porsche.common.exception.BaseException;
import cn.porsche.common.response.ApiResult;
import cn.porsche.web.domain.Target;
import cn.porsche.web.domain.TargetKPI;
import cn.porsche.web.domain.TargetWhat;
import lombok.Getter;

@Getter
public class TargetException extends BaseException {
    private String filed;
    private Integer kpiId;
    private Integer whatId;
    private Integer targetId;

    public static TargetException TheWeightError(Target target) {
        return new TargetException(target, null, null, "weight", "The weight is not equal to 100");
    }


    public static TargetException TargetSettingIsEmpty(Target target) {
        return new TargetException(target, null, null, null, "数据异常，无法操作");
    }

    public static TargetException TargetCheckFail(Target target) {
        return new TargetException(target, null, null, null, "请填写完整的绩效信息");
    }

    public static TargetException TheWeightIsZore(Target target, TargetWhat what) {
        return new TargetException(target, what, null, null, "Target Setting is Empty");
    }

    public TargetException(Target target, TargetWhat what, TargetKPI kpi, String field, String message) {
        super(ApiResult.RootErrorCode, message);

        if (null != target) {
            this.targetId = target.getId();
        }

        if (null != what) {
            this.whatId = what.getId();
        }

        if (null != kpi) {
            this.kpiId = kpi.getId();
        }

        this.filed = field;
    }
}
