package cn.porsche.web.util;


import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class HttpUtils {
    private final static String PATH = "/";
    public static String getHeader(String name) {
        Assert.notNull(name, "email must not be null");

        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader(name);
    }

    public static String getCookie(String name) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        Cookie[] cookies = request.getCookies();
        if (null == cookies) {
            return null;
        }

        for (Cookie cookie : cookies) {
            if (name.toLowerCase().equals(name.toLowerCase())) {
                return cookie.getValue();
            }
        }

        return null;
    }

    public static Cookie setCookie(HttpServletResponse response, String name, String value, int expireIn, String path) {
        // 创建一个 cookie对象
        Cookie cookie = new Cookie(name, value);
        cookie.setPath(path == null ? PATH : path);
        cookie.setMaxAge(expireIn);

        // 将cookie对象加入response响应
        response.addCookie(cookie);

        return cookie;
    }

    public static Cookie setCookie(HttpServletResponse response, String name, String value) {
        // 创建一个 cookie对象
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");

        // 将cookie对象加入response响应
        response.addCookie(cookie);

        return cookie;
    }


    public static void delCookie(HttpServletResponse response, String name) {
        setCookie(response, name, "", 0, null);
    }
}
