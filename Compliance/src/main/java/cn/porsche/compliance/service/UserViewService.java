package cn.porsche.compliance.service;


import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.domain.view.UserView;

import java.util.List;

public interface UserViewService {
    UserView findById(Integer id);
    UserView findByCode(String code);

    Integer count();
    List<UserView> findEmployeesByManager(User manager);

    UserView getManager(User user);
}
