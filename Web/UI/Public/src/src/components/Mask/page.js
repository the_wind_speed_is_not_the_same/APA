export default {
    props: {
        isShowMask: {
            type: Boolean,
            default: true
        },
        isShowMaskColor: {
            type: Boolean,
            default: false
        }
    },
    data() {
        return {
            
        }
    },
    computed: {
        
    },
    methods: {
        closeMask() {
            this.$emit('maskClick')
        }
    },
    mounted() {
        
    }
}