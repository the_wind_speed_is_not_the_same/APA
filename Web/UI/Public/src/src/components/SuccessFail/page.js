export default {
    props: {
        status: {
            type: String,
            default: 'draft'
        }
    },
    data() {
        return {
            
        }
    },
    computed: {
        
    },
    methods: {
        clickEvent(val) {
            this.$emit('clickEvent', val)
        }
    },
    mounted() {
        
    }
}