package cn.porsche.web.service.impl;

import cn.porsche.web.domain.Department;
import cn.porsche.web.domain.Group;
import cn.porsche.web.domain.view.UserView;
import cn.porsche.web.repository.view.UserViewRepository;
import cn.porsche.web.service.UserViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class UserViewServiceImpl implements UserViewService {
    @Autowired
    UserViewRepository repository;

    @Override
    public Page<UserView> findByPage(Integer page, Integer size) {
        return repository.findAll(PageRequest.of(page -1, size, Sort.by(Sort.Direction.ASC, "departmentId")));
    }

    @Override
    public Page<UserView> searchByNeedHands(Integer page, Integer size) {
        return repository.findAllByNeedHands(true, PageRequest.of(page -1, size));
    }

    @Override
    public Page<UserView> searchByGroup(Group group, Integer page, Integer size) {
        return repository.findAllByGroupId(group.getId(), PageRequest.of(page -1, size));
    }

    @Override
    public Page<UserView> searchByDepartment(Department department, Integer page, Integer size) {
        return repository.findAllByDepartmentId(department.getId(), PageRequest.of(page -1, size));
    }

    @Override
    public Page<UserView> searchByName(String name, Integer page, Integer size) {
        return repository.findAllByEnNameLike("%" + name + "%", PageRequest.of(page -1, size));
    }

    public Page<UserView> searchByDepartmentAndName(Department department, String name, Integer page, Integer size) {
        return repository.findAllByDepartmentIdAndEnNameLike(department.getId(), "%" + name + "%",  PageRequest.of(page -1, size));
    }

    @Override
    public Page<UserView> searchByDepartmentAndNeedHands(Department department, boolean needHands, Integer page, Integer size) {
        return repository.findAllByDepartmentIdAndNeedHands(department.getId(), needHands, PageRequest.of(page -1, size));
    }

    @Override
    public Page<UserView> searchByHireDateIsNull(Integer page, Integer size) {
        return repository.findAllByHireDateIsNull(PageRequest.of(page -1, size));
    }

    @Override
    public Page<UserView> searchByDepartmentAndHireDateIsNull(Department department, Integer page, Integer size) {
        return repository.findAllByDepartmentIdAndHireDateIsNull(department.getId(), PageRequest.of(page -1, size));
    }
}
