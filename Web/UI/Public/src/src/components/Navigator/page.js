import { mapActions } from 'vuex';
import { request } from '@/src/common/request.js';

export default {
    props: ['navigator'],
    data() {
        return {
            userName: '仝腾',
            userId: 'tt802388',
            infoVisible: false,
            menusKey: '',
            isShowMessage:false,
            activeN: 1,
            messageList:[],
            totalMessages:0,
			user:{}
        }
    },
    computed: {
        menus() {
            return this.navigator;
        },
    },
    filters:{
      filterNumber(val){
        if(!val) return 0
        return val > 10?'10+':val
      }
    },
    components:{
		
    },
    methods: {
    	...mapActions(['getUserInfo']),
        openMenu() {
            this.$emit('openMenu');
        },
        toHome() {
            this.$router.push({
                path: '/home'
            })
        },
        showMessage(e){
          this.getMessages()
          this.isShowMessage = true
        },
        listener(e){
          if(e.target.className != 'header-box'){
            this.isShowMessage = false
          }
        },
        tabClick(n){
          this.activeN = n
        },
        getMessages(){
          // setMessageReaded
          request({
            url:'getMessageList',
            method:'get'
          }).then((res)=>{
            if (res && res.list) {
              this.messageList = [];
              for (let i=0, len=res.list.length; i<len; ++i) {
                // TODO:
              }

              this.totalMessages = this.messageList.length;
            }
          })
        },
        handleMessage(id){
          request({
            url:'setMessageReaded',
            method:'get',
            params:{
              message_id:id
            }
          }).then((res)=>{
            if(res && res.code === 0){
              this.messageList.forEach((item,idx)=>{
                if(item.id === id){
                  this.messageList.splice(idx,1)
                }
              })
            }
          })
        },
        readMessage(row){
          if(row){
            this.handleMessage(row.id)
          }else{
            this.messageList.forEach((item)=>{
              this.handleMessage(item.id)
            })
          }
        }
    },
    created(){
      // this.getMessages()
      let user = localStorage.getItem("user")
      if(user){
        this.user = JSON.parse(user)
      }
    },
    mounted(){
      document.addEventListener('click',this.listener)
    }
}
