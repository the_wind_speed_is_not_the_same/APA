package cn.porsche.common.response;

import cn.porsche.common.exception.BaseException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import cn.porsche.common.response.page.Pagebar;
import cn.porsche.common.util.JsonUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "Rest返回基础类", description = "所有接口返回的基类\r\n"
        + "每一次调用返回的结构中一定包含： code 和 message。 code = 0，表示逻辑按预期完成，调用成功。\r\n"
        + "每一次调用返回的结构中选择性返回：list = List<T> 以数组返回，data = T 以单个对象返回, pagebar 分页数据")
public class ApiResult<T> extends ApiResponse {
    private transient final Logger logger = LoggerFactory.getLogger(this.getClass());

    @JsonProperty("data")
    @ApiModelProperty(value = "返回单个对象数据")
    T data;                 //返回单个类数据

    @JsonProperty("list")
    @ApiModelProperty(value = "返回的列表对象")
    List<T> list;           //返回列表

    @JsonProperty("pagebar")
    @ApiModelProperty(value = "根据列表而返回的分页数据")
    Pagebar pagebar;        //列表的分布数据

    public ApiResult() {
        super(RootSuccessCode, RootSuccessMessage);
    }

    private ApiResult(int code, String message) {
        super(code, message);
    }

    public static ApiResult builder() {
        return builder(RootSuccessCode, RootSuccessMessage);
    }

    public static ApiResult builder(Integer code, String message) {
        return new ApiResult(code, message);
    }

    public static String isOk() {
        return ApiResult.builder(RootSuccessCode, RootSuccessMessage).toJson();
    }

    public static String isBad() {
        return ApiResult.builder(RootErrorCode, RootErrorMessage).toJson();
    }

    public static String isBad(String message) {
        return ApiResult.builder(RootErrorCode, message).toJson();
    }

    public static String noLogin() {
        return ApiResult.builder(RootErrorCode, "This User Not Login").toJson();
    }

    public static String exception(BaseException e) {
        return ApiResult.builder(e.getCode(), e.getMessage()).toJson();
    }

    public ApiResult<T> data(T data) {
        this.data       = data;

        this.pagebar    = null;

        return this;
    }


    public ApiResult<T> list(List<T> list) {
        this.list       = list;
        this.pagebar    = null;

        return this;
    }

    public ApiResult<T> list(Page<T> pageable) {
        this.list       = pageable.getContent();
        this.pagebar    = Pagebar.fromPageable(pageable);

        return this;
    }

    public String toJson() {
        return JsonUtils.toString(this);
    }
}
