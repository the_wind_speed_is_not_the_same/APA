import { request } from '../../../../common/request.js';
import { deepClone, formatDate, formatDateTime } from '../../../../common/utils.js';
import Confirm from '../../../../components/Confirm/page.vue';

const kpis = [{
	desc: '',
	index: 4,
}, {
	desc: '',
	index: 3,
}, {
	desc: '',
	index: 2,
}, {
	desc: '',
	index: 1,
}, {
	desc: '',
	index: 0,
}]

export default {
    props:[ 'tableData', 'yearVal', 'target', 'comments', 'dimension'],
    data() {
        return {
            isShowConfirm: false,
            confirmTxt: '',
            titleTxt: '',
            showTime: 0,
            confirmType: '',
            reportData:[],
            dimensionList:[
                {
                    name: 'Innovation',
                    subTxt: 'Looking Ahead Strategically (Mandatory for management)<br />' + 'Developing and Implementing New Ideas',
                    weight: '25%',
                    description: "",
                    score:''
                }, {
                    name: "Team",
                    subTxt: 'Promoting a Learning Culture\n' + 'Working Together\n' + 'Empowering and Leading People (Mandatory for Leadership)',
                    weight: '25%',
                    description: '',
                    score:''
                }, {
                    name: 'Results',
                    subTxt: 'Managing Complexity\n' + 'Being Effective',
                    weight: '25%',
                    description: '',
                    score:''
                }, {
                    name: 'Reflection',
                    subTxt: 'Living Integrity\n' + 'Acting Responsibly\n' + 'Realizing Self-Correction and Self Development',
                    weight: '25%',
                    description: '',
                    score:''
                }
            ],
            targetInfor: this.target || {}
        }
    },
    filters: {
        filterDate(val) {
            return formatDateTime(val).split(' ')[0];
        }
    },
    components: {
        Confirm,
    },
    computed: {

    },
    methods:{
        closeComponent() {
            this.$emit('closeComponent');
        },
        read() {
            request({
                url: 'agree',
                method: 'get',
                params: {
                    function_id: this.yearVal.id
                },
                isShowLoading: true
            }).then(res => {
                if (!res || res.code !== 0) {
                    this.titleTxt = 'Error';
                    this.confirmTxt = res.message;
                    this.isShowConfirm = true;
                    return ;
                }

                this.titleTxt = 'Success';
                this.confirmTxt = 'Read Successfully';
                this.isShowConfirm = true;

                this.target.is_agree_score = true;
            })
        },
        saveAdd() {

        },
        callBack() {
        },
        confirm() {
            this.isShowConfirm = false;
        },
        filterScore(score) {
            if (!score) return '';

            score = String(score);
            if (score.length === 1) {
                return score + '.0';
            } else {
                return score;
            }
        }
    },
    created(){
      let tableData = deepClone(this.tableData)

      if(tableData && tableData.length){
        tableData.forEach((item)=>{
          if(item.kpis && item.kpis.length){
            item.kpis.sort((a,b)=>(b.index-a.index))
          }else{
            item.kpis = deepClone(kpis)
          }
        })

        this.reportData = tableData
      }

      this.dimensionList.forEach((item,idx)=>{
          item.description = this.dimension && this.dimension[`how${idx+1}`] || ''
          item.score = this.dimension && this.filterScore(this.dimension[`how${idx+1}_e_score`]) || '0.0'
      })
    },
    mounted() {

    }
}
