package cn.porsche.compliance.service;


import cn.porsche.compliance.domain.User;
import cn.porsche.compliance.domain.QuestionAnswer;
import cn.porsche.compliance.emnu.SeasonEnum;
import cn.porsche.compliance.emnu.TrainingTypeEnum;

import java.util.List;

public interface QuestionAnswerService {
    QuestionAnswer insert(QuestionAnswer answer);

    void deleteByUser(User user, Integer year, SeasonEnum season);

    int getTopScoreByYearAndSeason(Integer year, Integer season);
    int summaryScoreByUserAndYearAndSeason(User user, Integer year, SeasonEnum season);

    List<QuestionAnswer> findUserAndYearAndSeason(User user, Integer year, SeasonEnum season);

    List<QuestionAnswer> findAllByYearAndSeason(Integer year, Integer season);
}
